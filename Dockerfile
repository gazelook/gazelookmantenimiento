FROM node:lts-alpine3.13 as builder

ENV NODE_ENV build

USER node
WORKDIR /usr/src/app

COPY . /usr/src/app



RUN npm ci \
    && npm run build

# ---

FROM node:lts-alpine3.13

ENV NODE_ENV qa
ENV TRANSLATOR_TEXT_SUBSCRIPTION_KEY 4ef8d7e4672245e4bbf8a37d1fc909a8
ENV TRANSLATOR_TEXT_ENDPOINT https://api.cognitive.microsofttranslator.com

USER node
WORKDIR /usr/src/app

COPY --from=builder /usr/src/app/package*.json /usr/src/app/
COPY --from=builder /usr/src/app/dist/ /usr/src/app/dist/
COPY --from=builder /usr/src/app/configsEnv/ /usr/src/app/configsEnv/


RUN mkdir -p /usr/src/app/logs

RUN npm ci

EXPOSE 5000
ENV NODEJSAPIMAIN_URLS http://+:5000

CMD ["node", "dist/main.js"]
