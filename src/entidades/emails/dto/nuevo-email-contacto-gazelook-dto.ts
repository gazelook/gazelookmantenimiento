import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsOptional } from "class-validator";

export class CrearEmailContactoGazelookDto {
    @ApiProperty()
    @IsNotEmpty()
    nombre: string;

    @ApiProperty()
    @IsNotEmpty()
    email: string;

    @ApiProperty()
    @IsOptional()
    @IsNotEmpty()
    pais: string;

    @ApiProperty()
    @IsOptional()
    @IsNotEmpty()
    direccion: string;

    @ApiProperty()
    @IsNotEmpty()
    tema: string;
    
    @ApiProperty()
    @IsNotEmpty()
    mensaje: string;

} 