import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Controller, Post, Body, Res, HttpStatus, Get, Param, Render, HttpService } from '@nestjs/common';
import { ConfirmarEmailService } from '../casos_de_uso/confirmar-email.service';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from '../../../shared/funcion';
import * as mongoose from 'mongoose';
import { Observable } from 'rxjs';
import { ConfigService } from '../../../config/config.service';
import { servidor } from '../../../shared/enum-sistema';
import { ActualizarEstadoEmailService } from '../casos_de_uso/actualizar-estado-email.service';
import { CatalogoIdiomasService } from '../../catalogos/casos_de_uso/catalogo-idiomas.service';
import { isUUID } from 'class-validator';
import { ObtenerEmailService } from '../casos_de_uso/obtener-email.service';
import { NuevoEmailCuentaService } from '../casos_de_uso/nuevo-email-cuenta.service';

@ApiTags('email')
@Controller('api/email')
export class NuevoEmailController {

    constructor(
        private readonly i18n: I18nService,
        private readonly funcion: Funcion,
        private readonly catalogoIdiomasService: CatalogoIdiomasService,
        private readonly nuevoEmailCuentaService: NuevoEmailCuentaService
    ) { }

    @Get('/nuevo-email-cuenta/:token/:idioma')
    @ApiOperation({ summary: 'Este metodo envia un nuevo correo para validar la cuenta, en el caso de que el enlace fallara' })
    public async nuevoEmail(
        @Param('token') token,
        @Param('idioma') idioma,
        @Res() res
    ) {

        try {
            const codIdioma = await this.catalogoIdiomasService.verificarIdioma(idioma);

            if (isUUID(token)) {
                const nuevoEmail = await this.nuevoEmailCuentaService.nuevoEmailCuenta(token, idioma)
                if (nuevoEmail) {
                    const EMAIL_ENVIADO = await this.i18n.translate(codIdioma.concat('.EMAIL_ENVIADO'), {
                        lang: codIdioma
                    });
                    res.render('index', { message: EMAIL_ENVIADO });
                } else {
                    const LINK_NO_VALIDO = await this.i18n.translate(codIdioma.concat('.LINK_NO_VALIDO'), {
                        lang: codIdioma
                    });
                    res.render('index', { message: LINK_NO_VALIDO });
                }
            } else {
                const PARAMETROS_NO_VALIDOS = await this.i18n.translate(codIdioma.concat('.PARAMETROS_NO_VALIDOS'), {
                    lang: codIdioma
                });
                res.render('index', { message: PARAMETROS_NO_VALIDOS });
            }

        } catch (e) {
            return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR, mensaje: e.message });
        }
    }

}



