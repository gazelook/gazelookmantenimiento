import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Controller, Post, Body, Res, HttpStatus, Get, Param, Render, } from '@nestjs/common';
import { ConfirmarEmailService } from '../casos_de_uso/confirmar-email.service';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from '../../../shared/funcion';
import * as mongoose from 'mongoose';
import { CatalogoIdiomasService } from '../../catalogos/casos_de_uso/catalogo-idiomas.service';
import { isUUID } from 'class-validator';
import { ObtenerEmailService } from '../casos_de_uso/obtener-email.service';
import { idiomas } from 'src/shared/enum-sistema';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';



@ApiTags('email')
@Controller('api/email')
export class ConfirmarActualizarEmailResponsableControlador {

    constructor(private readonly confirmarEmailService: ConfirmarEmailService,
        private readonly i18n: I18nService,
        private readonly funcion: Funcion,
        private readonly catalogoIdiomasService: CatalogoIdiomasService,
        private obtenerEmailService: ObtenerEmailService,
    ) { }

    @Get('/confirmar-cambio-email-responsable/:token/:idioma')
    @ApiOperation({ summary: 'Confirma el cambio de email del responsable y lo redirige a un formulario para actualizar el email' })
    //@Render('actualizarEmailResponsable')
    public async confirmarEmailRecuperarContrasenia(
        @Param('token') token,
        @Param('idioma') idioma,
        @Res() res
    ) {
        // vuelve idioma por defecto
        const codIdioma = await this.catalogoIdiomasService.verificarIdioma(idioma);
        let parte1 = 'Cambiar Email Responsable';
        let parte2 = 'Actualizar Email Responsable';
        let parte3 = 'Nuevo Email';
        let parte4: 'Actualizar';
        if (codIdioma !== idiomas.espanol) {

            let textoUnido = parte1 +
                ' [-]' + parte2 + ' [-]' + parte3 + ' [-]' + parte4;

            let textoTraducido = await traducirTexto(codIdioma, textoUnido);
            const textS = textoTraducido.textoTraducido.split('[-]');

            parte1 = textS[0].trim();
            parte2 = textS[1].trim();
            parte3 = textS[2].trim();
            parte4 = textS[3].trim();

            console.log('-----------DATA TRADUCIDA-------------')
            console.log('parte1: ', parte1)
            console.log('parte2: ', parte2)
        }
        try {
            if (isUUID(token)) {
                const getDataEmail = await this.obtenerEmailService.obtenerEmailByToken(token);
                const verificaEmail = await this.confirmarEmailService.confirmarEmail(token);


                if (verificaEmail && getDataEmail) {
                    res.render('actualizarEmailResponsable', { usuario: getDataEmail.usuario,
                                                                parte1: parte1,
                                                                parte2: parte2,
                                                                parte3: parte3,
                                                                parte4: parte4,
                                                                codIdioma: codIdioma
                                                                });
                    //return { usuario: idUsuario };

                } else {
                    const LINK_NO_VALIDO = await this.i18n.translate(codIdioma.concat('.LINK_NO_VALIDO'), {
                        lang: codIdioma
                    });
                    res.render('index', { message: LINK_NO_VALIDO });
                    //return { message: LINK_NO_VALIDO };
                }
            } else {
                const PARAMETROS_NO_VALIDOS = await this.i18n.translate(codIdioma.concat('.PARAMETROS_NO_VALIDOS'), {
                    lang: codIdioma
                });
                res.render('index', { message: PARAMETROS_NO_VALIDOS });
            }

        } catch (e) {
            return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR, mensaje: e.message });
        }
    }
}



