import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { Controller, Res, HttpStatus, Get, Param } from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from '../../../shared/funcion';
import { CatalogoIdiomasService } from '../../catalogos/casos_de_uso/catalogo-idiomas.service';
import { isUUID } from 'class-validator';
import { ReenvioConfirmacionActualizacionEmailService } from '../casos_de_uso/reenvio-confirmacion-actualizacion-email.service';

@ApiTags('email')
@Controller('api/email')
export class ReenvioConfirmacionActualizacionEmailController {

    constructor(
        private readonly i18n: I18nService,
        private readonly funcion: Funcion,
        private readonly catalogoIdiomasService: CatalogoIdiomasService,
        private readonly reenvioConfirmacionActualizacionEmailService: ReenvioConfirmacionActualizacionEmailService
    ) { }

    @Get('/reenvio-confirmacion-actualizacion-email/:token/:idioma')
    @ApiOperation({ summary: 'Este metodo envia un nuevo correo para validar el nuevo email que actualizo el usuario' })
    public async nuevoEmail(
        @Param('token') token,
        @Param('idioma') idioma,
        @Res() res
    ) {

        try {
            const codIdioma = await this.catalogoIdiomasService.verificarIdioma(idioma);

            if (isUUID(token)) {
                const nuevoEmail = await this.reenvioConfirmacionActualizacionEmailService.reenvioConfirmacionCambioEmail(token, idioma)
                if (nuevoEmail) {
                    const EMAIL_ENVIADO = await this.i18n.translate(codIdioma.concat('.EMAIL_ENVIADO'), {
                        lang: codIdioma
                    });
                    res.render('index', { message: EMAIL_ENVIADO });
                } else {
                    const LINK_NO_VALIDO = await this.i18n.translate(codIdioma.concat('.LINK_NO_VALIDO'), {
                        lang: codIdioma
                    });
                    res.render('index', { message: LINK_NO_VALIDO });
                }
            } else {
                const PARAMETROS_NO_VALIDOS = await this.i18n.translate(codIdioma.concat('.PARAMETROS_NO_VALIDOS'), {
                    lang: codIdioma
                });
                res.render('index', { message: PARAMETROS_NO_VALIDOS });
            }

        } catch (e) {
            return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR, mensaje: e.message });
        }
    }

}



