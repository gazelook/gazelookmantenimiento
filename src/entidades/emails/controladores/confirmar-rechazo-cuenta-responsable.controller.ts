import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Controller, Post, Body, Res, HttpStatus, Get, Param, Render, } from '@nestjs/common';
import { ConfirmarEmailService } from '../casos_de_uso/confirmar-email.service';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from '../../../shared/funcion';
import { RechazoCuentaResponsableService } from '../../usuario/casos_de_uso/rechazo-cuenta-responsable.service';
import { CatalogoIdiomasService } from '../../catalogos/casos_de_uso/catalogo-idiomas.service';
import { ObtenerEmailService } from '../casos_de_uso/obtener-email.service';
import { isUUID } from 'class-validator';


@ApiTags('email')
@Controller('api/email')
export class ConfirmarRechazoCuentaResponsableController {

    constructor(private readonly confirmarEmailService: ConfirmarEmailService,
        private readonly i18n: I18nService,
        private readonly funcion: Funcion,
        private rechazoCuentaResponsableService: RechazoCuentaResponsableService,
        private readonly catalogoIdiomasService: CatalogoIdiomasService,
        private obtenerEmailService: ObtenerEmailService
    ) { }

    @Get('/confirmar-rechazo-cuenta-responsable/:token/:idioma')
    @ApiOperation({ summary: 'El usuario rechaza la creación de la cuenta, y se bloquea' })
    @Render('cuentaRechazada')
    public async confirmarEmailRecuperarContrasenia(
        @Param('token') token,
        @Param('idioma') idioma
    ) {
        const codIdioma = await this.catalogoIdiomasService.verificarIdioma(idioma);

        try {

            if (isUUID(token)) {
                const getEmail = await this.obtenerEmailService.obtenerEmailByToken(token)
                const verificaEmail = await this.confirmarEmailService.confirmarEmail(token);
                if (verificaEmail) {

                    const result = await this.rechazoCuentaResponsableService.rechazoCuenta(getEmail.usuario);
                    if (result) {
                        const ACTUALIZACION_CORRECTA = await this.i18n.translate(codIdioma.concat('.ACTUALIZACION_CORRECTA'), {
                            lang: codIdioma
                        });
                        return { resultRechazo: ACTUALIZACION_CORRECTA };
                    } else {
                        const ERROR_ACTUALIZAR = await this.i18n.translate(codIdioma.concat('.ERROR_ACTUALIZAR'), {
                            lang: codIdioma
                        });
                        return { resultRechazo: ERROR_ACTUALIZAR };
                    }

                } else {
                    const LINK_NO_VALIDO = await this.i18n.translate(codIdioma.concat('.LINK_NO_VALIDO'), {
                        lang: codIdioma
                    });
                    return { resultRechazo: LINK_NO_VALIDO };
                }
            } else {
                const PARAMETROS_NO_VALIDOS = await this.i18n.translate(codIdioma.concat('.PARAMETROS_NO_VALIDOS'), {
                    lang: codIdioma
                });
                return { resultRechazo: PARAMETROS_NO_VALIDOS };
            }

        } catch (e) {
            return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR, mensaje: e.message });
        }
    }
}



