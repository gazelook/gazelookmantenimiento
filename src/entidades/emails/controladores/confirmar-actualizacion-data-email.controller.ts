import { Controller, Post, Body, Res, HttpStatus, Get, Param, Render, Headers, UseGuards } from '@nestjs/common';
import { ConfirmarEmailService } from '../casos_de_uso/confirmar-email.service';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from '../../../shared/funcion';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { CatalogoIdiomasService } from '../../catalogos/casos_de_uso/catalogo-idiomas.service';
import { ObtenerEmailService } from '../casos_de_uso/obtener-email.service';
import { isUUID } from 'class-validator';
import { LocalAuthGuard } from '../../login/drivers/local-auth.guard';
import { idiomas } from 'src/shared/enum-sistema';
import { contactEmail, recursosSistemaEN } from 'src/shared/enum-recursos-sistema';
import { ConfigService } from 'src/config/config.service';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';

@ApiTags('email')
@Controller('api/email')
export class ConfirmarActualizacionDataEmailController {

    constructor(private readonly confirmarEmailService: ConfirmarEmailService,
        private readonly traduccionEstaticaController: TraduccionEstaticaController,
        private readonly i18n: I18nService,
        private readonly funcion: Funcion,
        private readonly catalogoIdiomasService: CatalogoIdiomasService,
        private obtenerEmailService: ObtenerEmailService,
        private readonly config: ConfigService
    ) { }

    @Get('/confirmar-actualizacion-data-email/:token/:idioma')
    @ApiOperation({ summary: 'Confirma la actualización del email de la cuenta gazelook' })
    @Render('index')
    public async confirmarEmail(
        @Param('token') token,
        @Param('idioma') idioma,
    ) {
        const codIdioma = await this.catalogoIdiomasService.verificarIdioma(idioma);

        const urlRecursoSistema = this.config.get<string>("URL_RECURSOS_SISTEMA");
        let urlHeaderEmail;
        let urlFooterEmail;
        let contact;
        if (codIdioma === idiomas.espanol) {
            urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_ES}`;
            urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_ES}`;
            contact = contactEmail.contactES;
        }
        if (codIdioma === idiomas.ingles) {
            urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_EN}`;
            urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_EN}`;
            contact = contactEmail.contactEN;
        }
        if (codIdioma === idiomas.frances) {
            urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_FR}`;
            urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_FR}`;
            contact = contactEmail.contactFR;
            // nombreUser = nombreUser.concat(' ');
        }
        if (codIdioma === idiomas.italiano) {
            urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_IT}`;
            urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_IT}`;
            contact = contactEmail.contactIT;
        }
        if (codIdioma === idiomas.aleman) {
            urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_DE}`;
            urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_DE}`;
            contact = contactEmail.contactDE;
        }
        if (codIdioma === idiomas.portugues) {
            urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_PT}`;
            urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_PT}`;
            contact = contactEmail.contactPT;
        }


        let texto = 'E-mail verificado'
        let textoTraducido = await traducirTexto(codIdioma, texto);
        const emailVerificado = textoTraducido.textoTraducido;


        try {
            if (isUUID(token)) {
                //const getDataEmail = await this.obtenerEmailService.obtenerEmailByToken(token);
                const verificaEmail = await this.confirmarEmailService.confirmarEmail(token);

                if (verificaEmail) {
                    // const EMAIL_VERIFICADO = await this.traduccionEstaticaController.traduccionEstatica(codIdioma, 'EMAIL_VERIFICADO');
                    return {
                        message: emailVerificado,
                        urlHeaderEmail: urlHeaderEmail, contact: contact
                    };

                } else {
                    const LINK_NO_VALIDO = await this.traduccionEstaticaController.traduccionEstatica(codIdioma, 'LINK_NO_VALIDO');
                    return { message: LINK_NO_VALIDO };
                }
            } else {
                const PARAMETROS_NO_VALIDOS = await this.i18n.translate(codIdioma.concat('.PARAMETROS_NO_VALIDOS'), {
                    lang: codIdioma
                });
                return { message: PARAMETROS_NO_VALIDOS };
            }

        } catch (e) {
            return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR, mensaje: e.message });
        }
    }
}



