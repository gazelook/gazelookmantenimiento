import { Body, Controller, Headers, HttpStatus, Post } from '@nestjs/common';
import { ApiHeader, ApiOperation, ApiSecurity, ApiTags } from '@nestjs/swagger';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from '../../../shared/funcion';
import { CatalogoIdiomasService } from '../../catalogos/casos_de_uso/catalogo-idiomas.service';
import { CrearEmailContactoGazelookService } from '../casos_de_uso/crear-email-contacto-gazelook.service';
import { CrearEmailContactoGazelookDto } from '../dto/nuevo-email-contacto-gazelook-dto';

@ApiTags('email')
@Controller('api/email')
export class CrearEmailContactoGazelookController {

    constructor(
        private readonly i18n: I18nService,
        private readonly funcion: Funcion,
        private readonly catalogoIdiomasService: CatalogoIdiomasService,
        private crearEmailContactoGazelookService: CrearEmailContactoGazelookService
    ) { }

    @Post('/crear-email-contacto-gazelook')
    @ApiSecurity('Authorization')
    @ApiHeader({ name: 'idioma', description: 'codigo del idioma. Ejm: es, en, fr' })
    @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
    @ApiOperation({ summary: 'Ruta para enviar un correo a gazelook cuando alguna persona o usuario desea mas información de la empresa' })
    public async nuevoEmail(
        @Headers() headers,
        @Body() contactoGazelookDto: CrearEmailContactoGazelookDto
    ) {

        try {
            const codIdioma = await this.catalogoIdiomasService.verificarIdioma(headers.idioma);

            const nuevoEmail = await this.crearEmailContactoGazelookService.crearEmailContactoGazelook(contactoGazelookDto, codIdioma)
            if (nuevoEmail) {
                const EMAIL_ENVIADO = await this.i18n.translate(codIdioma.concat('.EMAIL_ENVIADO'), {
                    lang: codIdioma
                });
                return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.OK, mensaje: EMAIL_ENVIADO });
            } else {
                const EMAIL_NO_PUEDE_CREARSE = await this.i18n.translate(codIdioma.concat('.EMAIL_NO_PUEDE_CREARSE'), {
                    lang: codIdioma
                });
                return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.NOT_FOUND, mensaje: EMAIL_NO_PUEDE_CREARSE });
            }


        } catch (e) {
            return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR, mensaje: e.message });
        }
    }

}



