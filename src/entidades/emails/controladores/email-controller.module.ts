import { EmailServicesModule } from '../casos_de_uso/email-services.module';
import { Module, forwardRef, HttpModule } from '@nestjs/common';
import { UsuarioServicesModule } from '../../usuario/casos_de_uso/usuario-services.module';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { Funcion } from '../../../shared/funcion';
import { HttpErrorFilter } from '../../../shared/filters/http-error.filter';
import { ConfirmarEmailPeticionDatosControlador } from './confirmar-email-peticion-datos.controller';
import { ConfirmarEmailRecuperarContraseniaControlador } from './confirmar-email-recuperar-contrasenia.controller';
import { CrearEmailRecuperarContraseniaControlador } from './crear-email-recuperar-contrasenia.controller';
import { ConfirmarEmailPeticionEliminacionDatosControlador } from './confirmar-email-eliminacion-datos.controller';
import { ConfirmarEmailTranferirProyectoControlador } from './confirmar-email-trasferir-proyecto.controller';
import { ConfirmarActualizarEmailResponsableControlador } from './confirmar-actualizar-email-responsable.controller';
import { ConfirmarEmailCuentaControlador } from './confirmar-email-cuenta.controller';
import { ConfirmarRechazoCuentaResponsableController } from './confirmar-rechazo-cuenta-responsable.controller';
import { CatalogosServiceModule } from '../../catalogos/casos_de_uso/catalogos-services.module';
import { NuevoEmailController } from './nuevo-email-cuenta.controller';
import { CrearEmailContactoGazelookController } from './crear-email-contacto-gazelook.controller';
import { ConfirmarActualizacionDataEmailController } from './confirmar-actualizacion-data-email.controller';
import { ReenvioConfirmacionActualizacionEmailController } from './reenvio-confirmacion-actualizacion-email.controller';




@Module({
  imports: [
    EmailServicesModule,
    HttpModule,
    forwardRef(() => UsuarioServicesModule),
    CatalogosServiceModule
  ],
  providers: [TraduccionEstaticaController, Funcion],
  exports: [],
  controllers: [
    ConfirmarEmailPeticionDatosControlador,
    ConfirmarEmailRecuperarContraseniaControlador,
    CrearEmailRecuperarContraseniaControlador,
    ConfirmarEmailPeticionEliminacionDatosControlador,
    ConfirmarEmailTranferirProyectoControlador,
    ConfirmarActualizarEmailResponsableControlador,
    ConfirmarEmailCuentaControlador,
    ConfirmarRechazoCuentaResponsableController,
    NuevoEmailController,
    CrearEmailContactoGazelookController,
    ConfirmarActualizacionDataEmailController,
    ReenvioConfirmacionActualizacionEmailController,
  ],
})
export class EmailControllerModule { }
