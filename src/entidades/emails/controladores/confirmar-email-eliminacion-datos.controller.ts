import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Controller, Post, Body, Res, HttpStatus, Get, Param, Render, HttpService } from '@nestjs/common';
import { ConfirmarEmailService } from '../casos_de_uso/confirmar-email.service';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import { I18nService } from 'nestjs-i18n';
import { idiomas, servidor } from 'src/shared/enum-sistema';
import { Funcion } from '../../../shared/funcion';
import { ConfigService } from '../../../config/config.service';
import { Observable } from 'rxjs';
import * as mongoose from 'mongoose';
import { ObtenerEmailService } from '../casos_de_uso/obtener-email.service';
import { isUUID } from 'class-validator';
import { CatalogoIdiomasService } from '../../catalogos/casos_de_uso/catalogo-idiomas.service';
import { ActualizarEstadoEmailService } from '../casos_de_uso/actualizar-estado-email.service';
import { contactEmail, recursosSistemaEN } from 'src/shared/enum-recursos-sistema';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';


@ApiTags('email')
@Controller('api/email')
export class ConfirmarEmailPeticionEliminacionDatosControlador {

    constructor(private readonly confirmarEmailService: ConfirmarEmailService,
        private readonly i18n: I18nService,
        private readonly funcion: Funcion,
        private config: ConfigService,
        private httpService: HttpService,
        private obtenerEmailService: ObtenerEmailService,
        private readonly catalogoIdiomasService: CatalogoIdiomasService,
        private readonly actualizarEstadoEmailService: ActualizarEstadoEmailService
    ) { }

    @Get('/confirmar-peticion-eliminacion-datos/:token/:validar/:idioma')
    @ApiOperation({ summary: 'Confirma que el usuario elimine sus datos completamente de la aplicación' })
    @Render('eliminacionDatos')
    public async confirmarEmailPeticionEliminacionDatos(
        @Param('token') token,
        @Param('validar') validar,
        @Param('idioma') idioma,
    ) {

        try {

            const codIdioma = await this.catalogoIdiomasService.verificarIdioma(idioma);

            const urlRecursoSistema = this.config.get<string>("URL_RECURSOS_SISTEMA");
            let urlHeaderEmail;
            let urlFooterEmail;
            let contact;
            if (codIdioma === idiomas.espanol) {
                urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_ES}`;
                urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_ES}`;
                contact = contactEmail.contactES;
            }
            if (codIdioma === idiomas.ingles) {
                urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_EN}`;
                urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_EN}`;
                contact = contactEmail.contactEN;
            }
            if (codIdioma === idiomas.frances) {
                urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_FR}`;
                urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_FR}`;
                contact = contactEmail.contactFR;
                // nombreUser = nombreUser.concat(' ');
            }
            if (codIdioma === idiomas.italiano) {
                urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_IT}`;
                urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_IT}`;
                contact = contactEmail.contactIT;
            }
            if (codIdioma === idiomas.aleman) {
                urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_DE}`;
                urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_DE}`;
                contact = contactEmail.contactDE;
            }
            if (codIdioma === idiomas.portugues) {
                urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_PT}`;
                urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_PT}`;
                contact = contactEmail.contactPT;
            }

            let texto = "Su información ha sido eliminada de la aplicación"
            let textoTraducido = await traducirTexto(codIdioma, texto);
            const eliminacionCorrecta = textoTraducido.textoTraducido;

            if (isUUID(token) && validar) {
                const getDataEmail = await this.obtenerEmailService.obtenerEmailByToken(token);
                const verificaEmail = await this.confirmarEmailService.confirmarEmail(token);
                console.log("getDataEmail", getDataEmail);

                if (getDataEmail && verificaEmail && validar === "true") {
                    const peticionEliminacionDatos = await this.eliminarDatos(getDataEmail.usuario, token).toPromise() //await this.eliminarInformacionCompleto.eliminarInformacionCompletoUsuario(idUsuario);
                    console.log("peticionEliminacionDatos: ", peticionEliminacionDatos.data);

                    if (peticionEliminacionDatos.data.codigoEstado === HttpStatus.OK) {
                        const DATOS_ELIMINADOS = await this.i18n.translate(codIdioma.concat('.DATOS_ELIMINADOS'), {
                            lang: codIdioma
                        });
                        return { eliminacionOk: eliminacionCorrecta, 
                            urlHeaderEmail: urlHeaderEmail, contact: contact
                        };
                    } else if (
                        peticionEliminacionDatos.data.codigoEstado === HttpStatus.NOT_FOUND ||
                        peticionEliminacionDatos.data.codigoEstado === HttpStatus.UNAUTHORIZED
                    ) {
                        const NO_PERMISO_ACCION = await this.i18n.translate(codIdioma.concat('.NO_PERMISO_ACCION'), {
                            lang: codIdioma
                        });
                        return { eliminacionOk: NO_PERMISO_ACCION,
                            urlHeaderEmail: urlHeaderEmail, contact: contact
                        }

                    } else {
                        const ERROR_ELIMINAR = await this.i18n.translate(codIdioma.concat('.ERROR_ELIMINAR'), {
                            lang: codIdioma
                        });
                        return { eliminacionOk: ERROR_ELIMINAR,
                            urlHeaderEmail: urlHeaderEmail, contact: contact
                        }
                    }

                } else if (getDataEmail && verificaEmail && validar === "false") {
                    // actualizar estado rechazado
                    await this.actualizarEstadoEmailService.actualizarEstadoEmailRechazado(token);

                    const SOLICITUD_CANCELADA = await this.i18n.translate(codIdioma.concat('.SOLICITUD_CANCELADA'), {
                        lang: codIdioma
                    });
                    return { eliminacionOk: SOLICITUD_CANCELADA,
                        urlHeaderEmail: urlHeaderEmail, contact: contact
                    };

                } else {
                    const LINK_NO_VALIDO = await this.i18n.translate(codIdioma.concat('.LINK_NO_VALIDO'), {
                        lang: codIdioma
                    });
                    return { eliminacionOk: LINK_NO_VALIDO,
                        urlHeaderEmail: urlHeaderEmail, contact: contact
                    };
                }
            } else {
                const PARAMETROS_NO_VALIDOS = await this.i18n.translate(codIdioma.concat('.PARAMETROS_NO_VALIDOS'), {
                    lang: codIdioma
                });
                return { eliminacionOk: PARAMETROS_NO_VALIDOS,
                    urlHeaderEmail: urlHeaderEmail, contact: contact };
            }

        } catch (e) {
            return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR, mensaje: e.message });
        }
    }

    eliminarDatos(idUsuario, tokenEmail): Observable<any> {
        try {
            const apiKey = this.config.get('API_KEY');
            const HOST_LOCAL_PRINCIPAL = this.config.get('HOST_LOCAL_PRINCIPAL');
            console.log("tokenEmail", tokenEmail);


            const headersRequest: any = {
                'Content-Type': 'application/json',
                'apiKey': apiKey,
                'token': `${tokenEmail}`
            };
            //return this.httpService.delete(`${HOST_LOCAL_PRINCIPAL}${servidor.path_eliminar_datos_usuario}/${idUsuario}`, { headers: headersRequest });
            return this.httpService.delete(`${HOST_LOCAL_PRINCIPAL}${servidor.path_eliminar_cuenta_usuario}/${idUsuario}`, { headers: headersRequest });
        } catch (error) {
            throw error;
        }
    }
}