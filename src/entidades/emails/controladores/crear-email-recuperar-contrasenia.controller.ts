
import { Controller, Post, Body, Res, HttpStatus, Get, Param, Headers, UseGuards, Query } from '@nestjs/common';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import { idiomas } from 'src/shared/enum-sistema';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from '../../../shared/funcion';
import { ApiOperation, ApiSecurity, ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { CrearEmailRecuperarContraseniaService } from '../casos_de_uso/crear-email-recuperar-contrasenia.service';
import { isEmail } from 'class-validator';
import { CatalogoIdiomasService } from '../../catalogos/casos_de_uso/catalogo-idiomas.service';

@ApiTags('email')
@Controller('api/email')
export class CrearEmailRecuperarContraseniaControlador {
    constructor(private readonly crearEmailRecuperarContraseniaService: CrearEmailRecuperarContraseniaService,
        private readonly traduccionEstaticaController: TraduccionEstaticaController,
        private readonly i18n: I18nService,
        private readonly funcion: Funcion,
        private readonly catalogoIdiomasService: CatalogoIdiomasService
    ) { }
    @Post('/crear-email-recuperar-contrasenia')
    @ApiOperation({ summary: 'Este método envia un email al usuario, para que confirme la recuperación de la contraseña' })
    public async crearEmailRecuperarContrasenia(@Query('email') email: string, @Headers() headers) {

        const codIdioma = await this.catalogoIdiomasService.verificarIdioma(headers.idioma);

        try {
            if (isEmail(email)) {
                const emailCreado = await this.crearEmailRecuperarContraseniaService.crearEmailRecuperarContrasenia(email, codIdioma);
                if (!emailCreado) {
                    //llama al metodo de traduccion estatica
                    const ERROR_CREACION = await this.traduccionEstaticaController.traduccionEstatica(codIdioma, 'ERROR_CREACION');
                    return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.NOT_FOUND, mensaje: ERROR_CREACION });
                } else {
                    const EMAIL_REGISTRADO = await this.traduccionEstaticaController.traduccionEstatica(codIdioma, 'EMAIL_REGISTRADO');
                    return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.CREATED, mensaje: EMAIL_REGISTRADO });
                }
            } else {
                const PARAMETROS_NO_VALIDOS = await this.i18n.translate(codIdioma.concat('.PARAMETROS_NO_VALIDOS'), {
                    lang: codIdioma
                });
                return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.NOT_ACCEPTABLE, mensaje: PARAMETROS_NO_VALIDOS });
            }

        } catch (e) {
            if (e?.codigoNombre) {
                const MENSAJE = await this.i18n.translate(codIdioma.concat(`.${e.codigoNombre}`), {
                    lang: codIdioma
                });
                return this.funcion.enviarRespuestaOptimizada({ codigoEstado: e.codigo, mensaje: MENSAJE });
            } else {
                return this.funcion.enviarRespuestaOptimizada({
                    codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
                    mensaje: e.message
                });
            }
        }
    }

}

