import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Controller, Post, Body, Res, HttpStatus, Get, Param, Render, HttpService } from '@nestjs/common';
import { ConfirmarEmailService } from '../casos_de_uso/confirmar-email.service';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from '../../../shared/funcion';
import * as mongoose from 'mongoose';
import { Observable } from 'rxjs';
import { ConfigService } from '../../../config/config.service';
import { servidor } from '../../../shared/enum-sistema';
import { ActualizarEstadoEmailService } from '../casos_de_uso/actualizar-estado-email.service';
import { CatalogoIdiomasService } from '../../catalogos/casos_de_uso/catalogo-idiomas.service';
import { isUUID } from 'class-validator';
import { ObtenerEmailService } from '../casos_de_uso/obtener-email.service';

@ApiTags('email')
@Controller('api/email')
export class ConfirmarEmailTranferirProyectoControlador {

    constructor(private readonly confirmarEmailService: ConfirmarEmailService,
        private readonly i18n: I18nService,
        private readonly funcion: Funcion,
        private config: ConfigService,
        private httpService: HttpService,
        private readonly catalogoIdiomasService: CatalogoIdiomasService,
        private obtenerEmailService: ObtenerEmailService,
        private readonly actualizarEstadoEmailService: ActualizarEstadoEmailService
    ) { }

    @Get('/confirmar-email-trasferir-proyecto/:token/:validar/:idioma')
    @ApiOperation({ summary: 'Este metodo confirma y lleva a cabo la transferencia del proyecto' })
    @Render('transferirProyecto')
    public async confirmarEmailTrasferirProyecto(
        @Param('token') token,
        @Param('validar') validar,
        @Param('idioma') idioma
    ) {

        const codIdioma = await this.catalogoIdiomasService.verificarIdioma(idioma);
        try {

            if (isUUID(token) && validar) {
                const getEmail = await this.obtenerEmailService.obtenerEmailByToken(token)
                const verificaEmail = await this.confirmarEmailService.confirmarEmail(token);

                if (getEmail && verificaEmail && validar === 'true') {
                    // peticion a servidor principal
                    const data = {
                        token: token,
                        idProyecto: getEmail.datosProceso.idProyecto,
                        idPerfilNuevo: getEmail.datosProceso.perfilNuevo,
                        idPerfilPropietario: getEmail.datosProceso.perfilPropietario,
                        idioma: idioma
                    }

                    const transProyecto = await this.transferirProyecto(data).toPromise();
                    console.log("transProyecto: ", transProyecto);
                    if (transProyecto.data.codigoEstado === 200) {
                        const TRANSFERIR_PROYECTO = await this.i18n.translate(codIdioma.concat('.TRANSFERIR_PROYECTO'), {
                            lang: codIdioma
                        });
                        return { transferenciaProyecto: TRANSFERIR_PROYECTO };
                    } else {
                        return { transferenciaProyecto: transProyecto.data.respuesta?.mensaje };
                    }
                } else if (getEmail && verificaEmail && validar === 'false') {
                    // actualizar estado rechazado
                    await this.actualizarEstadoEmailService.actualizarEstadoEmailRechazado(token);

                    const SOLICITUD_CANCELADA = await this.i18n.translate(codIdioma.concat('.SOLICITUD_CANCELADA'), {
                        lang: codIdioma
                    });
                    return { transferenciaProyecto: SOLICITUD_CANCELADA };
                } else {
                    const LINK_NO_VALIDO = await this.i18n.translate(codIdioma.concat('.LINK_NO_VALIDO'), {
                        lang: codIdioma
                    });
                    return { transferenciaProyecto: LINK_NO_VALIDO };
                }
            } else {
                const PARAMETROS_NO_VALIDOS = await this.i18n.translate(codIdioma.concat('.PARAMETROS_NO_VALIDOS'), {
                    lang: codIdioma
                });
                return { transferenciaProyecto: PARAMETROS_NO_VALIDOS };
            }

        } catch (e) {
            return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR, mensaje: e.message });
        }
    }

    transferirProyecto(data): Observable<any> {
        try {
            const apiKey = this.config.get('API_KEY');
            const HOST_LOCAL_PRINCIPAL = this.config.get('HOST_LOCAL_PRINCIPAL');

            const headersRequest: any = {
                'Content-Type': 'application/json',
                'apiKey': apiKey,
            };
            return this.httpService.post(`${HOST_LOCAL_PRINCIPAL}${servidor.path_transferir_proyecto}`, data, { headers: headersRequest });
        } catch (error) {
            throw error;
        }
    }
}



