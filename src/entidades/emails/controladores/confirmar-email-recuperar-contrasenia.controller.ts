import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Controller, Post, Body, Res, HttpStatus, Get, Param, Render, } from '@nestjs/common';
import { ConfirmarEmailService } from '../casos_de_uso/confirmar-email.service';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import { I18nService } from 'nestjs-i18n';
import { idiomas } from 'src/shared/enum-sistema';
import { Funcion } from '../../../shared/funcion';
import { ActualizarEstadoEmailService } from '../casos_de_uso/actualizar-estado-email.service';
import { CatalogoIdiomasService } from '../../catalogos/casos_de_uso/catalogo-idiomas.service';
import { isUUID } from 'class-validator';
import { ObtenerEmailService } from '../casos_de_uso/obtener-email.service';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import { contactEmail, recursosSistemaEN } from 'src/shared/enum-recursos-sistema';
import { ConfigService } from 'src/config/config.service';



@ApiTags('email')
@Controller('api/email')
export class ConfirmarEmailRecuperarContraseniaControlador {

    constructor(private readonly confirmarEmailService: ConfirmarEmailService,
        private readonly i18n: I18nService,
        private readonly funcion: Funcion,
        private readonly catalogoIdiomasService: CatalogoIdiomasService,
        private obtenerEmailService: ObtenerEmailService,
        private readonly config: ConfigService
    ) { }

    @Get('/confirmar-email-recuperar-contrasenia/:token/:idioma')
    @ApiOperation({ summary: 'El usuario confirma el cambio de contraseña y se le presenta un formulario para la actualización' })
    //@Render('indexNewPass')
    public async confirmarEmailRecuperarContrasenia(
        @Param('token') token,
        @Param('idioma') idioma,
        @Res() res
    ) {
        console.log("idioma", idioma);
        
        const codIdioma = await this.catalogoIdiomasService.verificarIdioma(idioma);
        
        
        const urlRecursoSistema = this.config.get<string>("URL_RECURSOS_SISTEMA");
        let urlHeaderEmail;
        let urlFooterEmail;
        let contact;
        if (codIdioma === idiomas.espanol) {
            urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_ES}`;
            urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_ES}`;
            contact = contactEmail.contactES;
        }
        if (codIdioma === idiomas.ingles) {
            urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_EN}`;
            urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_EN}`;
            contact = contactEmail.contactEN;
        }
        if (codIdioma === idiomas.frances) {
            urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_FR}`;
            urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_FR}`;
            contact = contactEmail.contactFR;
            // nombreUser = nombreUser.concat(' ');
        }
        if (codIdioma === idiomas.italiano) {
            urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_IT}`;
            urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_IT}`;
            contact = contactEmail.contactIT;
        }
        if (codIdioma === idiomas.aleman) {
            urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_DE}`;
            urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_DE}`;
            contact = contactEmail.contactDE;
        }
        if (codIdioma === idiomas.portugues) {
            urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_PT}`;
            urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_PT}`;
            contact = contactEmail.contactPT;
        }
        
        let nuevaContrasenia = 'Nueva Contraseña';
        let confirmarContrasenia = 'Confirmar Contraseña';
        let confirmar = 'Confirmar';
        let cambiarContrasenia: 'Cambiar Contraseña';
        if (codIdioma !== idiomas.espanol) {

            let textoUnido = nuevaContrasenia +
                ' [-]' + confirmarContrasenia + ' [-]' + confirmar + ' [-]'+ cambiarContrasenia;

            let textoTraducido = await traducirTexto(codIdioma, textoUnido);
            const textS = textoTraducido.textoTraducido.split('[-]');

            nuevaContrasenia = textS[0].trim();
            confirmarContrasenia = textS[1].trim();
            confirmar = textS[2].trim();
            cambiarContrasenia = textS[3].trim();

            console.log('-----------DATA TRADUCIDA-------------')
            console.log('nuevaContrasenia: ', nuevaContrasenia)
            console.log('confirmarContrasenia: ', confirmarContrasenia)

        }
        try {
            if (isUUID(token)) {
                const getDataEmail = await this.obtenerEmailService.obtenerEmailByToken(token);
                const verificaEmail = await this.confirmarEmailService.confirmarEmail(token);
                console.log('verificaEmail: ', verificaEmail)
                if (verificaEmail) {
                    res.render('indexNewPass', {
                        email: getDataEmail.emailDestinatario,
                        nuevaContrasenia: nuevaContrasenia,
                        confirmarContrasenia: confirmarContrasenia,
                        confirmar: confirmar,
                        cambiarContrasenia: cambiarContrasenia,
                        codIdioma: codIdioma,
                        urlHeaderEmail: urlHeaderEmail,
                        contact: contact
                    });
                    //return { email: email };

                } else {
                    // const LINK_NO_VALIDO = await this.i18n.translate(codIdioma.concat('.LINK_NO_VALIDO'), {
                    //     lang: codIdioma
                    // });
                    let texto = "Enlace no válido"
                    let textoTraducido1 = await traducirTexto(codIdioma, texto);
                    let enlaceInvalido = textoTraducido1.textoTraducido;

                    console.log('enlaceInvalido: ', enlaceInvalido)
                    res.render('index', { message: enlaceInvalido,
                        urlHeaderEmail : urlHeaderEmail, contact: contact
                    });
                    //return { message: LINK_NO_VALIDO };
                }
            } else {
                const PARAMETROS_NO_VALIDOS = await this.i18n.translate(codIdioma.concat('.PARAMETROS_NO_VALIDOS'), {
                    lang: codIdioma
                });
                res.render('index', { message: PARAMETROS_NO_VALIDOS,
                    urlHeaderEmail : urlHeaderEmail, contact: contact
                });
            }

        } catch (e) {
            return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR, mensaje: e.message });
        }
    }
}



