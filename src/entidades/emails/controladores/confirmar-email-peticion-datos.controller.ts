import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Controller, Post, Body, Res, HttpStatus, Get, Param, Render, } from '@nestjs/common';
import { ConfirmarEmailService } from '../casos_de_uso/confirmar-email.service';
import { I18nService } from 'nestjs-i18n';
import { InformacioAplicacionService } from '../../usuario/casos_de_uso/informacion-aplicacion.service';
import { Funcion } from '../../../shared/funcion';
import * as mongoose from 'mongoose';
import { ObtenerEmailService } from '../casos_de_uso/obtener-email.service';
import { isUUID } from 'class-validator';
import { CatalogoIdiomasService } from '../../catalogos/casos_de_uso/catalogo-idiomas.service';
import { ActualizarEstadoEmailService } from '../casos_de_uso/actualizar-estado-email.service';



@ApiTags('email')
@Controller('api/email')
export class ConfirmarEmailPeticionDatosControlador {

    constructor(private readonly confirmarEmailService: ConfirmarEmailService,
        private readonly informacioAplicacionService: InformacioAplicacionService,
        private readonly i18n: I18nService,
        private obtenerEmailService: ObtenerEmailService,
        private readonly catalogoIdiomasService: CatalogoIdiomasService,
        private readonly actualizarEstadoEmailService: ActualizarEstadoEmailService
    ) { }

    @Get('/confirmar-peticion-datos/:usuario/:validar/:idioma')
    @ApiOperation({ summary: 'Confirmación de resumen de datos de la aplicación por parte del usuario. Se envia un correo al usuario para que pueda descargar su información' })
    @Render('solicitudInformacion')
    public async confirmarEmailPeticionDatos(
        @Param('usuario') usuario,
        @Param('validar') validar,
        @Param('idioma') idioma,
    ) {

        try {

            const codIdioma = await this.catalogoIdiomasService.verificarIdioma(idioma);

            if (usuario) {
                // const getDataEmail = await this.obtenerEmailService.obtenerEmailByToken(token);
                // console.log("getDataEmail", getDataEmail);

                // const verificaEmail = await this.confirmarEmailService.confirmarEmail(token);

                if (validar === 'true') {

                    this.informacioAplicacionService.solicitarInformacionUsuario(usuario).then(data => {
                        console.log("dataSolicitudOk");
                    });

                    
                    const SOLICITUD_ACEPTADA = await this.i18n.translate(codIdioma.concat('.SOLICITUD_ACEPTADA'), {
                        lang: codIdioma
                    });
                    return { peticionOk: SOLICITUD_ACEPTADA };
                } else if (validar === 'false') {
                    // actualizar estado rechazado
                    // await this.actualizarEstadoEmailService.actualizarEstadoEmailRechazado(token);

                    const SOLICITUD_CANCELADA = await this.i18n.translate(codIdioma.concat('.SOLICITUD_CANCELADA'), {
                        lang: codIdioma
                    });
                    return { peticionOk: SOLICITUD_CANCELADA };
                } else {
                    const LINK_NO_VALIDO = await this.i18n.translate(codIdioma.concat('.LINK_NO_VALIDO'), {
                        lang: codIdioma
                    });
                    return { peticionOk: LINK_NO_VALIDO };
                }
            } else {
                const PARAMETROS_NO_VALIDOS = await this.i18n.translate(codIdioma.concat('.PARAMETROS_NO_VALIDOS'), {
                    lang: codIdioma
                });
                return { peticionOk: PARAMETROS_NO_VALIDOS };
            }

        } catch (e) {
            //return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR, mensaje: e.message });
            return { peticionOk: e.message };
        }
    }
}