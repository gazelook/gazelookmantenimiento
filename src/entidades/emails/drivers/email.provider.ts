import { emailSchema } from './../../../drivers/mongoose/modelos/emails/email.modelo';


import { Connection } from 'mongoose';
import { CatalogoTipoEmailSchema } from 'src/drivers/mongoose/modelos/catalogo_tipo_email/catalogoTipoEmail';
import { perfilModelo } from 'src/drivers/mongoose/modelos/perfil/perfil.schema';

export const emailProviders = [
    {
      provide: 'EMAIL_MODEL',
      useFactory: (connection: Connection) => connection.model('emails', emailSchema, 'emails'),
      inject: ['DB_CONNECTION'],
    },
    {
      provide: 'PERIL_MODEL',
      useFactory: (connection: Connection) => connection.model('perfil', perfilModelo, 'perfil'),
      inject: ['DB_CONNECTION'],
    },
 
 

  ];
