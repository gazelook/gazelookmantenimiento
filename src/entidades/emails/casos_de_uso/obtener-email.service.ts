import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Email } from 'src/drivers/mongoose/interfaces/email/email.interface';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { codigoCatalogosTipoEmail, nombreAcciones, nombrecatalogoEstados, nombreEntidades } from 'src/shared/enum-sistema';

@Injectable()
export class ObtenerEmailService {
    ;
    constructor(@Inject('EMAIL_MODEL') private readonly emailModel: Model<Email>,
        private nombreEntidad: CatalogoEntidadService,
        private nombreAccion: CatalogoAccionService,
        private nombreEstado: CatalogoEstadoService,
        private crearHistoricoService: CrearHistoricoService,

    ) { }

    // obtiene el email a traves del token y que este en estado activa
    async obtenerEmailByToken(token): Promise<Email> {
        try {
            const entidad = await this.nombreEntidad.obtenerNombreEntidad(nombreEntidades.email);
            let codEntidad = entidad.codigo;

            const estado = await this.nombreEstado.obtenerNombreEstado(nombrecatalogoEstados.activa, codEntidad);

            const getEmail = await this.emailModel.findOne({
                $and: [
                    { estado: estado.codigo },
                    { tokenEmail: token }
                ]
            });
            return getEmail;
        } catch (error) {
            throw error;
        }


    }

    async obtenerEmailByTokenSinEstado(token): Promise<Email> {
        try {
            const getEmail = await this.emailModel.findOne({
                $and: [
                    { tokenEmail: token }
                ]
            });
            return getEmail;
        } catch (error) {
            throw error;
        }


    }

}