import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Email } from 'src/drivers/mongoose/interfaces/email/email.interface';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { codigoCatalogosTipoEmail, nombreAcciones, nombrecatalogoEstados, nombreEntidades } from 'src/shared/enum-sistema';
import * as moment from 'moment';
const mongoose = require('mongoose');

@Injectable()
export class ActualizarEstadoEmailService {
    ;
    constructor(@Inject('EMAIL_MODEL') private readonly emailModel: Model<Email>,
        private nombreEntidad: CatalogoEntidadService,
        private nombreAccion: CatalogoAccionService,
        private nombreEstado: CatalogoEstadoService,
        private crearHistoricoService: CrearHistoricoService,

    ) { }

    // obtiene el email a traves del token y que este en estado activa
    async actualizarEstadoEmailFechaExpirada(token, opts?: any): Promise<any> {
        let session;
        let optsLocal;
        if (!opts) {
            session = await mongoose.startSession();
            session.startTransaction();
        }
        try {
            if (!opts) {
                optsLocal = true;
                opts = { session };
            }

            const accion = await this.nombreAccion.obtenerNombreAccion(nombreAcciones.modificar);
            const entidad = await this.nombreEntidad.obtenerNombreEntidad(nombreEntidades.email);
            let codEntidad = entidad.codigo;

            const estado = await this.nombreEstado.obtenerNombreEstado(nombrecatalogoEstados.activa, codEntidad);

            const getEmail = await this.emailModel.findOne({
                $and: [
                    { tokenEmail: token }
                ]
            });
            //Obtiene fecha actual
            const fechaActual = moment().format('YYYY-MM-DD');
            let formatFechaActual = new Date(fechaActual);

            if (getEmail.estado === estado.codigo && moment(formatFechaActual) > moment(getEmail.fechaValidacion)) {
                const estadoBlo = await this.nombreEstado.obtenerNombreEstado(nombrecatalogoEstados.bloqueado, codEntidad);
                const datoActua = {
                    estado: estadoBlo.codigo
                }
                const actualizarEmail = await this.emailModel.findByIdAndUpdate(getEmail._id, datoActua);

                const newHistorico: any = {
                    datos: actualizarEmail,
                    usuario: getEmail.usuario,
                    accion: accion.codigo,
                    entidad: codEntidad
                }
                
                this.crearHistoricoService.crearHistoricoServer(newHistorico);
                
                if (optsLocal) {
                    await session.commitTransaction();
                    await session.endSession();
                }
                return actualizarEmail;
            } else {
                if (optsLocal) {
                    //Aborta la transaccion
                    await session.abortTransaction();
                    //Finaliza la transaccion
                    await session.endSession();
                }
                return null;
            }
        } catch (error) {
            if (optsLocal) {
                //Aborta la transaccion
                await session.abortTransaction();
                //Finaliza la transaccion
                await session.endSession();
            }
            throw error
        }


    }

    async actualizarEstadoEmailRechazado(token, opts?: any): Promise<any> {
        let session;
        let optsLocal;
        if (!opts) {
            session = await mongoose.startSession();
            session.startTransaction();
        }
        try {
            if (!opts) {
                optsLocal = true;
                opts = { session };
            }

            const accion = await this.nombreAccion.obtenerNombreAccion(nombreAcciones.modificar);
            const entidad = await this.nombreEntidad.obtenerNombreEntidad(nombreEntidades.email);
            let codEntidad = entidad.codigo;

            const estado = await this.nombreEstado.obtenerNombreEstado(nombrecatalogoEstados.activa, codEntidad);

            const getEmail = await this.emailModel.findOne({
                $and: [
                    { tokenEmail: token }
                ]
            });
            //Obtiene fecha actual
            const fechaActual = moment().format('YYYY-MM-DD');
            let formatFechaActual = new Date(fechaActual);

            if (getEmail && moment(formatFechaActual) < moment(getEmail?.fechaValidacion)) {
                const estadoRechazado = await this.nombreEstado.obtenerNombreEstado(nombrecatalogoEstados.rechazoEmail, codEntidad);
                const datoActua = {
                    estado: estadoRechazado.codigo
                }
                const actualizarEmail = await this.emailModel.findByIdAndUpdate(getEmail._id, datoActua);

                const newHistorico: any = {
                    datos: actualizarEmail,
                    usuario: getEmail.usuario,
                    accion: accion.codigo,
                    entidad: codEntidad
                }
                await this.crearHistoricoService.crearHistoricoTransaccion(newHistorico, opts);
                if (optsLocal) {
                    await session.commitTransaction();
                    await session.endSession();
                }
                return actualizarEmail;
            } else {
                if (optsLocal) {
                    //Aborta la transaccion
                    await session.abortTransaction();
                    //Finaliza la transaccion
                    await session.endSession();
                }
                return null;
            }
        } catch (error) {
            if (optsLocal) {
                //Aborta la transaccion
                await session.abortTransaction();
                //Finaliza la transaccion
                await session.endSession();
            }
            throw error
        }
    }

}