import { Observable } from 'rxjs';
import { Response } from 'express';
import { Inject, Injectable, HttpService, HttpStatus } from '@nestjs/common';
import { Model } from 'mongoose';
import { Email } from 'src/drivers/mongoose/interfaces/email/email.interface';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { codigoCatalogosTipoEmail, estadosUsuario, nombreAcciones, nombrecatalogoEstados, nombreEntidades } from 'src/shared/enum-sistema';
import { ConfigService } from '../../../config/config.service';
import { ObtenerIdUsuarioService } from '../../usuario/casos_de_uso/obtener-id-usuario.service';
import { RecuperarContrasenaService } from '../../servicio_email/casos_de_uso/recuperar-contrasena.service';
import { erroresGeneral, erroresPerfil } from '../../../shared/enum-errores';
const mongoose = require('mongoose');
import { v4 as uuidv4 } from 'uuid';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';

@Injectable()
export class CrearEmailRecuperarContraseniaService {

    constructor(@Inject('EMAIL_MODEL') private readonly emailModel: Model<Email>,
        private nombreEntidad: CatalogoEntidadService,
        private nombreAccion: CatalogoAccionService,
        private nombreEstado: CatalogoEstadoService,
        private crearHistoricoService: CrearHistoricoService,
        private httpService: HttpService,
        private config: ConfigService,
        private obtenerIdUsuarioService: ObtenerIdUsuarioService,
        private recuperarContrasenaService: RecuperarContrasenaService,
        private catalogoIdiomasService: CatalogoIdiomasService
    ) { }

    async crearEmailRecuperarContrasenia(email: string, idioma): Promise<any> {

        //Obtiene el codigo de la accion crear
        const accion = await this.nombreAccion.obtenerNombreAccion(nombreAcciones.crear);
        let getAccion = accion.codigo;

        //Obtiene el codigo de la entidad Email
        const entidad = await this.nombreEntidad.obtenerNombreEntidad(nombreEntidades.email);
        let codEntidad = entidad.codigo;

        //Obtiene el estado activa de la entidad email
        const estado = await this.nombreEstado.obtenerNombreEstado(nombrecatalogoEstados.activa, codEntidad);
        let codEstado = estado.codigo;

        const fechaCreacion = new Date();
        const fechaValid = this.sumarDias(new Date());
        //const token = this.random() + this.random() + this.random();
        const token = uuidv4().toString();

        const usuario = await this.obtenerIdUsuarioService.obtenerUsuarioByEmail(email);

        console.log('usuario: ', usuario)
        if (!usuario) {
            throw { codigo: HttpStatus.OK, codigoNombre: erroresPerfil.USUARIO_NO_REGISTRADO };
        }

        /* if (usuario.estado != estadosUsuario.activa) {
            throw { codigo: HttpStatus.OK, codigoNombre: erroresGeneral.NO_AUTORIZADO };
        } */
        let getIdioma  = await this.catalogoIdiomasService.obtenerIdiomaByCodigo(usuario.idioma)
        const session = await mongoose.startSession();
        session.startTransaction();
        try {
            const opts = { session };

            const nuevoEmail = {
                usuario: usuario._id,
                emailDestinatario: usuario.email,
                estado: codEstado,
                fechaCreacion: fechaCreacion,
                fechaValidacion: fechaValid,
                tokenEmail: token,
                codigo: codigoCatalogosTipoEmail.recuperar_contrasenia,
                idioma: getIdioma.codNombre,
                enviado: false,
                descripcionEnvio: ''
            };
            const dataEmail = {
                tokenEmail: token,
                idioma: getIdioma.codNombre,
                emailDestinatario: usuario.email,
                usuario: usuario.perfiles[0].nombre
            }
            const sendEmail = await this.recuperarContrasenaService.recuperarContrasena(dataEmail);
            if (sendEmail.response) {

                nuevoEmail['enviado'] = true;
                nuevoEmail['descripcionEnvio'] = sendEmail.response;


            } else {

                nuevoEmail['enviado'] = false;
                nuevoEmail['descripcionEnvio'] = sendEmail.code;
            }
            const email = await new this.emailModel(nuevoEmail).save(opts);

            //datos para guardar el historico
            const newHistorico: any = {
                datos: email,
                usuario: nuevoEmail.usuario,
                accion: getAccion,
                entidad: codEntidad
            };
            
            this.crearHistoricoService.crearHistoricoServer(newHistorico);

            await session.commitTransaction();
            await session.endSession();
            return true;

        } catch (error) {
            await session.abortTransaction();
            await session.endSession();
            throw error;
        }

    }
    sumarDias(fecha) {
        fecha.setDate(fecha.getDate() + 15);
        return fecha;
    }
    random() {
        return Math.random().toString(36).substr(2);
    };




}