import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Email } from 'src/drivers/mongoose/interfaces/email/email.interface';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { codigoCatalogosTipoEmail, nombreAcciones, nombrecatalogoEstados, nombreEntidades } from 'src/shared/enum-sistema';
const mongoose = require('mongoose');
import { ObtenerIdUsuarioService } from '../../usuario/casos_de_uso/obtener-id-usuario.service';
import { EstadoSuscripcionService } from '../../servicio_email/casos_de_uso/estado-suscripcion.service';

@Injectable()
export class CrearEmailEstadoSuscripcionService {

    constructor(@Inject('EMAIL_MODEL') private readonly emailModel: Model<Email>,
        private nombreEntidad: CatalogoEntidadService,
        private nombreAccion: CatalogoAccionService,
        private nombreEstado: CatalogoEstadoService,
        private crearHistoricoService: CrearHistoricoService,
        private estadoSuscripcionService: EstadoSuscripcionService,
        private obtenerIdUsuarioService: ObtenerIdUsuarioService
    ) { }

    async emailNotificarEstadoSuscripcion(crearEmailDto: any, opts?: any): Promise<any> {

        //Obtiene el codigo de la accion crear
        const accion = await this.nombreAccion.obtenerNombreAccion(nombreAcciones.crear);
        let getAccion = accion.codigo;

        //Obtiene el codigo de la entidad Email
        const entidad = await this.nombreEntidad.obtenerNombreEntidad(nombreEntidades.email);
        let codEntidad = entidad.codigo;

        //Obtiene el estado activa de la entidad email
        const estado = await this.nombreEstado.obtenerNombreEstado(nombrecatalogoEstados.activa, codEntidad);
        let codEstado = estado.codigo;

        const fechaCreacion = new Date();
        const fechaValid = this.sumarDias(new Date());

        //const token = uuidv4().toString();

        let session;
        let optsLocal;
        if (!opts) {
            session = await mongoose.startSession();
            session.startTransaction();
        }

        try {
            if (!opts) {
                optsLocal = true;
                opts = { session };
            }

            const nuevoEmail = {
                usuario: crearEmailDto.idUsuario,
                emailDestinatario: crearEmailDto.emailDestinatario,
                estado: codEstado,
                fechaCreacion: fechaCreacion,
                //fechaValidacion: fechaValid,
                codigo: codigoCatalogosTipoEmail.notificacionEstadoSuscripcion,
            };
            // enviar email
            const dataEmail = {
                emailDestinatario: crearEmailDto.emailDestinatario,
                nombre: crearEmailDto.nombre,
                idioma: crearEmailDto.idioma
            }
            const sendEmail = await this.estadoSuscripcionService.notificarEstadoSuscripcion(dataEmail);
            console.log("sendEmail:", sendEmail);

            if (sendEmail.response) {

                nuevoEmail['enviado'] = true;
                nuevoEmail['descripcionEnvio'] = sendEmail.response;


            } else {

                nuevoEmail['enviado'] = false;
                nuevoEmail['descripcionEnvio'] = sendEmail.code;
            }
            const email = await new this.emailModel(nuevoEmail).save(opts);

            //datos para guardar el historico
            const newHistorico: any = {
                datos: email,
                usuario: nuevoEmail.usuario,
                accion: getAccion,
                entidad: codEntidad
            };
            
            this.crearHistoricoService.crearHistoricoServer(newHistorico);

            // termina transaccion
            if (optsLocal) {
                await session.commitTransaction();
                await session.endSession();
            }
            return true;

        } catch (error) {
            if (optsLocal) {
                //Aborta la transaccion
                await session.abortTransaction();
                //Finaliza la transaccion
                await session.endSession();
            }
            throw error
        }

    }
    sumarDias(fecha) {
        fecha.setDate(fecha.getDate() + 3);
        return fecha;
    }
    random() {
        return Math.random().toString(36).substr(2);
    };




}