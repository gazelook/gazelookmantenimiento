import { HttpService, Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Email } from 'src/drivers/mongoose/interfaces/email/email.interface';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoTipoEmailService } from 'src/entidades/catalogos/casos_de_uso/catalogo-tipo-email.service';
import { ActualizarUsuarioService } from '../../usuario/casos_de_uso/actualizar-usuario.service';
import { nombreAcciones, nombrecatalogoEstados, nombreCatalogoTipoEmail, nombreEntidades, estadosUsuario, servidor } from '../../../shared/enum-sistema';
import * as mongoose from 'mongoose';
import * as moment from 'moment';
import { ActualizarEstadoEmailService } from './actualizar-estado-email.service';
import { ConfigService } from '../../../config/config.service';
import { Observable } from 'rxjs';
import { ConfirmarCambioEmailService } from 'src/entidades/servicio_email/casos_de_uso/confirmar-cambio-email.service';
import { ObtenerIdUsuarioService } from 'src/entidades/usuario/casos_de_uso/obtener-id-usuario.service';

@Injectable()
export class ConfirmarEmailService {


    constructor(@Inject('EMAIL_MODEL') private readonly emailModel: Model<Email>,
        private nombreEntidad: CatalogoEntidadService,
        private nombreAccion: CatalogoAccionService,
        private nombreEstado: CatalogoEstadoService,
        private crearHistoricoService: CrearHistoricoService,
        private nombreTipoEmail: CatalogoTipoEmailService,
        private actualizarUsuario: ActualizarUsuarioService,
        private readonly actualizarEstadoEmailService: ActualizarEstadoEmailService,
        private config: ConfigService,
        private httpService: HttpService,
        private confirmarCambioEmailService: ConfirmarCambioEmailService,
        private obtenerIdUsuarioService: ObtenerIdUsuarioService
    ) { }

    async confirmarEmail(tokenVa: string): Promise<any> {

        const accion = await this.nombreAccion.obtenerNombreAccion(nombreAcciones.modificar);
        let getAccion = accion.codigo;

        const entidad = await this.nombreEntidad.obtenerNombreEntidad(nombreEntidades.email);
        let codEntidad = entidad.codigo;

        const estadoAc = await this.nombreEstado.obtenerNombreEstado(nombrecatalogoEstados.activa, codEntidad);
        let codEstadoAc = estadoAc.codigo;

        const estadoConfirmado = await this.nombreEstado.obtenerNombreEstado(nombrecatalogoEstados.confirmadoEmail, codEntidad);
        let codEstadoConfirmado = estadoConfirmado.codigo;


        const codTipEma = await this.nombreTipoEmail.obtenerCodigoTipoEmail(nombreCatalogoTipoEmail.validacion);
        let codTipoEmailValid = codTipEma.codigo;

        const codTipEmaR = await this.nombreTipoEmail.obtenerCodigoTipoEmail(nombreCatalogoTipoEmail.validacionResponsable);
        let codEstadoValiRes = codTipEmaR.codigo;

        const codTipTransProy = await this.nombreTipoEmail.obtenerCodigoTipoEmail(nombreCatalogoTipoEmail.trasferirProyecto);
        let codEstadoTransProyecto = codTipTransProy.codigo;

        const codTipRecContras = await this.nombreTipoEmail.obtenerCodigoTipoEmail(nombreCatalogoTipoEmail.recuperarContrasenia);
        let codEstadoRecContrasenia = codTipRecContras.codigo;

        const codTipPeticionDatos = await this.nombreTipoEmail.obtenerCodigoTipoEmail(nombreCatalogoTipoEmail.peticionDatos);
        let codPeticionDatos = codTipPeticionDatos.codigo;

        const codTipEliminacionDatos = await this.nombreTipoEmail.obtenerCodigoTipoEmail(nombreCatalogoTipoEmail.eliminacionDatos);
        let codPeticionEliminacionDatos = codTipEliminacionDatos.codigo;

        const confirmarActualizacionEmail = await this.nombreTipoEmail.obtenerCodigoTipoEmail(nombreCatalogoTipoEmail.confirmarActualizacionEmail);
        let codConfirmarActualizacionEmail = confirmarActualizacionEmail.codigo;

        console.log('tokenVa: ', tokenVa)
        const fechaActual = new Date();

        const session = await mongoose.startSession()
        session.startTransaction();

        try {
            const opts = { session };
            let verificacion = false;
            //Obtiene fecha actual
            const fechaActualValidar = moment().format('YYYY-MM-DD');
            let formatFechaActual = new Date(fechaActualValidar);

            const verificarLink = await this.emailModel.findOne({ tokenEmail: tokenVa, estado: codEstadoAc }).session(opts.session);
            
            if (verificarLink && moment(formatFechaActual) <= moment(verificarLink?.fechaValidacion)) {
                // __________________validar email CUENTA normal____________________________
                const getUser = await this.obtenerIdUsuarioService.obtenerPerfilesUsuarioSessionById(verificarLink.usuario)
                if (verificarLink.codigo === codTipoEmailValid) {
                    const datoActua = {
                        estado: codEstadoConfirmado
                    }
                    const emailVerifica = {
                        emailVerificado: true,
                        estado: estadosUsuario.activa
                    }
                    const actualizarEmail = await this.emailModel.findByIdAndUpdate(verificarLink._id, datoActua, opts);

                    const actualizarUsuario = await this.actualizarUsuario.actualizarDatosUsuario(verificarLink.usuario, emailVerifica, opts);
                    //return true;
                    verificacion = true;
                    // ________________________validar email CUENTA menor edad (email responsable)_____________________
                } else if (verificarLink.codigo === codEstadoValiRes) {
                    const fechaAct = this.getDateTime(fechaActual);
                    const fechaLink = this.getDateTime(verificarLink.fechaValidacion)

                    if (fechaAct <= fechaLink) {

                        const datoActua = {
                            estado: codEstadoConfirmado
                        }
                        const actualizarEmail = await this.emailModel.findByIdAndUpdate(verificarLink._id, datoActua, opts);
                        const emailVerifica = {
                            emailVerificado: true,
                            estado: estadosUsuario.activa
                        }
                        const actualizarUsuario = await this.actualizarUsuario.actualizarDatosUsuario(verificarLink.usuario, emailVerifica, opts);
                        //return true;
                        verificacion = true;
                    }
                    else {
                        //return false;
                        verificacion = false;
                    }

                } else if (verificarLink.codigo === codEstadoTransProyecto) {
                    const fechaAct = this.getDateTime(fechaActual);
                    const fechaLink = this.getDateTime(verificarLink.fechaValidacion)

                    if (fechaAct <= fechaLink) {

                        const datoActua = {
                            estado: codEstadoConfirmado
                        }
                        const actualizarEmail = await this.emailModel.findByIdAndUpdate(verificarLink._id, datoActua, opts);
                        //return true;
                        verificacion = true;
                    } else {
                        //return false;
                        verificacion = false;
                    }
                } else if (verificarLink.codigo === codEstadoRecContrasenia) {
                    const fechaAct = this.getDateTime(fechaActual);
                    const fechaLink = this.getDateTime(verificarLink.fechaValidacion)

                    if (fechaAct <= fechaLink) {

                        const datoActua = {
                            estado: codEstadoConfirmado
                        }
                        const actualizarEmail = await this.emailModel.findByIdAndUpdate(verificarLink._id, datoActua, opts);
                        //return true;
                        verificacion = true;
                    }
                    else {
                        //return false;
                        verificacion = false;
                    }
                } else if (verificarLink.codigo === codPeticionDatos) {
                    const fechaAct = this.getDateTime(fechaActual);
                    const fechaLink = this.getDateTime(verificarLink.fechaValidacion)

                    if (fechaAct <= fechaLink) {

                        const datoActua = {
                            estado: codEstadoConfirmado
                        }
                        const actualizarEmail = await this.emailModel.findByIdAndUpdate(verificarLink._id, datoActua, opts);
                        //return true;
                        verificacion = true;
                    }
                    else {
                        //return false;
                        verificacion = false;
                    }
                } else if (verificarLink.codigo === codPeticionEliminacionDatos) {
                    const fechaAct = this.getDateTime(fechaActual);
                    const fechaLink = this.getDateTime(verificarLink.fechaValidacion)

                    if (fechaAct <= fechaLink) {

                        const datoActua = {
                            estado: codEstadoConfirmado
                        }
                        const actualizarEmail = await this.emailModel.findByIdAndUpdate(verificarLink._id, datoActua, opts);
                        //return true;
                        verificacion = true;
                    }
                    else {
                        //return false;
                        verificacion = false;
                    }
                    // ___________________Confirmar la actualizacion del email_____________________
                } else if (verificarLink.codigo === codConfirmarActualizacionEmail) {
                    const fechaAct = this.getDateTime(fechaActual);
                    const fechaLink = this.getDateTime(verificarLink.fechaValidacion)

                    if (fechaAct <= fechaLink) {

                        const datoActua = {
                            estado: codEstadoConfirmado
                        }
                        await this.emailModel.findByIdAndUpdate(verificarLink._id, datoActua, opts);

                        const dataProcesar = {
                            emailVerificado: true,
                            email: verificarLink.datosProceso.nuevoEmail,
                            idDispositivo: verificarLink.datosProceso.idDispositivo
                        }

                        let envioEmail ={
                            idioma: getUser.idioma,
                            emailDestinatario: verificarLink.datosProceso.nuevoEmail,
                            usuario: getUser.perfiles[0].nombre
                        }
                        this.actualizarCambioEmail(verificarLink.usuario.toString(), dataProcesar, verificarLink.tokenEmail).subscribe(() => {
                            console.log("Se cambio el email correctamenteee")
                            // this.confirmarCambioEmailService.confirmarCambioEmail(envioEmail)
                        },
                            (error) => {
                                console.log("Error al cambiar el email de la cuenta: ", error)
                            })

                        //return true;
                        verificacion = true;
                    } else {
                        //return false;
                        verificacion = false;
                    }
                }

                if (verificacion) {
                    const getEmail = await this.emailModel.findOne({ tokenEmail: tokenVa }).session(opts.session);
                    let newConfEmailHistorico = {
                        _id: getEmail._id,
                        emailDestinatario: getEmail.emailDestinatario,
                        enviado: getEmail.enviado,
                        fechaValidacion: getEmail.fechaValidacion

                    }

                    const newHistorico: any = {
                        datos: newConfEmailHistorico,
                        usuario: getEmail.usuario,
                        accion: getAccion,
                        entidad: codEntidad
                    }
                    //Llama a clase generica para almacenar historico
                    this.crearHistoricoService.crearHistoricoServer(newHistorico);

                    await session.commitTransaction();
                    session.endSession();

                    return verificacion;
                } else {
                    await session.abortTransaction();
                    session.endSession();
                    return verificacion;
                }




            } else {
                console.log('actualizar email bloqueado fecha expirada')
                // actualizar email bloqueado fecha expirada
                await this.actualizarEstadoEmailService.actualizarEstadoEmailFechaExpirada(tokenVa);
                await session.commitTransaction();
                session.endSession();
                return false;
            }
        } catch (error) {
            await session.abortTransaction();
            session.endSession();
            throw error;
        }


    }


    getDateTime(date) {
        let year;
        let month;
        let day;

        year = date.getFullYear();
        month = date.getMonth() + 1;
        month = (month < 10 ? "0" : "") + month;
        day = date.getDate();
        day = (day < 10 ? "0" : "") + day;

        return year + "-" + month + "-" + day

    }

    actualizarCambioEmail(idUsuario: string, data: any, token: string): Observable<any> {
        try {
            const apiKey = this.config.get('API_KEY');
            const HOST_LOCAL_PRINCIPAL = this.config.get('HOST_LOCAL_PRINCIPAL');
            const headersRequest: any = {
                'Content-Type': 'application/json',
                'apiKey': apiKey,
                'token': `${token}`
            };
            return this.httpService.put(`${HOST_LOCAL_PRINCIPAL}${servidor.path_actualizar_cambio_email}/${idUsuario}`, data, { headers: headersRequest });
        } catch (error) {
            throw error;
        }
    }

}