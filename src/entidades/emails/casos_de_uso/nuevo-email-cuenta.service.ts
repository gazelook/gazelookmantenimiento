import { Observable } from 'rxjs';
import { Response } from 'express';
import { Inject, Injectable, HttpService, HttpStatus } from '@nestjs/common';
import { Model } from 'mongoose';
import { Email } from 'src/drivers/mongoose/interfaces/email/email.interface';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { codigoCatalogosTipoEmail, estadosUsuario, nombreAcciones, nombrecatalogoEstados, nombreEntidades } from 'src/shared/enum-sistema';
import { ConfigService } from '../../../config/config.service';
import { ObtenerIdUsuarioService } from '../../usuario/casos_de_uso/obtener-id-usuario.service';
import { RecuperarContrasenaService } from '../../servicio_email/casos_de_uso/recuperar-contrasena.service';
import { erroresGeneral, erroresPerfil } from '../../../shared/enum-errores';
const mongoose = require('mongoose');
import { v4 as uuidv4 } from 'uuid';
import { ConfirmacionEmailCuentaService } from '../../servicio_email/casos_de_uso/confirmacion-email-cuenta.service';

@Injectable()
export class NuevoEmailCuentaService {

    constructor(@Inject('EMAIL_MODEL') private readonly emailModel: Model<Email>,
        private nombreEntidad: CatalogoEntidadService,
        private nombreAccion: CatalogoAccionService,
        private nombreEstado: CatalogoEstadoService,
        private crearHistoricoService: CrearHistoricoService,
        private confirmacionEmailCuentaService: ConfirmacionEmailCuentaService
    ) { }

    async nuevoEmailCuenta(token: string, idioma): Promise<any> {

        //Obtiene el codigo de la accion crear
        const accion = await this.nombreAccion.obtenerNombreAccion(nombreAcciones.crear);
        let getAccion = accion.codigo;

        //Obtiene el codigo de la entidad Email
        const entidad = await this.nombreEntidad.obtenerNombreEntidad(nombreEntidades.email);
        let codEntidad = entidad.codigo;

        //Obtiene el estado activa de la entidad email
        const estado = await this.nombreEstado.obtenerNombreEstado(nombrecatalogoEstados.activa, codEntidad);
        let codEstado = estado.codigo;

        // estado confirmado
        const estadoConfirmado = await this.nombreEstado.obtenerNombreEstado(nombrecatalogoEstados.confirmadoEmail, codEntidad);
        let codEstadoConfirmado = estadoConfirmado.codigo;

        // estado bloqueado
        const estadobloqueado = await this.nombreEstado.obtenerNombreEstado(nombrecatalogoEstados.bloqueado, codEntidad);
        let codEstadoBloqueado = estadobloqueado.codigo;

        const fechaCreacion = new Date();
        const fechaValid = this.sumarDias(new Date());
        //const token = this.random() + this.random() + this.random();
        const getToken = uuidv4().toString();


        const session = await mongoose.startSession();
        session.startTransaction();
        try {
            const opts = { session };

            const getEmail = await this.emailModel.findOne({ tokenEmail: token });
            if (getEmail.estado === codEstadoConfirmado || getEmail.estado === codEstadoBloqueado) {
                return false;
            }
            // cambiar estado token anterior
            await this.emailModel.updateOne({ tokenEmail: token }, { estado: codEstadoBloqueado }, opts);

            // nuevo email
            const nuevoEmail = {
                usuario: getEmail.usuario,
                emailDestinatario: getEmail.emailDestinatario,
                estado: codEstado,
                fechaCreacion: fechaCreacion,
                fechaValidacion: fechaValid,
                tokenEmail: getToken,
                codigo: codigoCatalogosTipoEmail.validacion_correo_normal,
                enviado: false,
                descripcionEnvio: ''
            };
            const dataEmail = {
                tokenEmail: getToken,
                idioma: idioma,
                emailDestinatario: getEmail.emailDestinatario,
                usuario: getEmail.usuario,
                codigo: codigoCatalogosTipoEmail.validacion_correo_normal,
            }
            const sendEmail = await this.confirmacionEmailCuentaService.confirmacionEmailCuenta(dataEmail);
            if (sendEmail.response) {

                nuevoEmail['enviado'] = true;
                nuevoEmail['descripcionEnvio'] = sendEmail.response;


            } else {

                nuevoEmail['enviado'] = false;
                nuevoEmail['descripcionEnvio'] = sendEmail.code;
            }
            const email = await new this.emailModel(nuevoEmail).save(opts);

            //datos para guardar el historico
            const newHistorico: any = {
                datos: email,
                usuario: nuevoEmail.usuario,
                accion: getAccion,
                entidad: codEntidad
            };
            
            this.crearHistoricoService.crearHistoricoServer(newHistorico);

            await session.commitTransaction();
            await session.endSession();
            return true;

        } catch (error) {
            await session.abortTransaction();
            await session.endSession();
            throw error;
        }

    }
    sumarDias(fecha) {
        fecha.setDate(fecha.getDate() + 15);
        return fecha;
    }
    random() {
        return Math.random().toString(36).substr(2);
    };




}