import { Observable } from 'rxjs';
import { Response } from 'express';
import { Inject, Injectable, HttpService, HttpStatus } from '@nestjs/common';
import { Model } from 'mongoose';
import { Email } from 'src/drivers/mongoose/interfaces/email/email.interface';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { codigoCatalogosTipoEmail, contactEmails, nombreAcciones, nombrecatalogoEstados, nombreEntidades, servidor } from 'src/shared/enum-sistema';

const mongoose = require('mongoose');
import { v4 as uuidv4 } from 'uuid';
import { ConfirmacionEmailCuentaService } from '../../servicio_email/casos_de_uso/confirmacion-email-cuenta.service';
import { CrearEmailContactoGazelookDto } from '../dto/nuevo-email-contacto-gazelook-dto';
import { Perfil } from 'src/drivers/mongoose/interfaces/perfil/perfil.interface';
import { ConfirmacionCrearEmailContactoService } from 'src/entidades/servicio_email/casos_de_uso/confirmacion-crear-email-contacto-gazelook.service';
import { ConfigService } from '../../../config/config.service';

@Injectable()
export class CrearEmailContactoGazelookService {

    constructor(@Inject('EMAIL_MODEL') private readonly emailModel: Model<Email>,
        @Inject('PERIL_MODEL') private readonly perfilModel: Model<Perfil>,
        private nombreEntidad: CatalogoEntidadService,
        private nombreAccion: CatalogoAccionService,
        private nombreEstado: CatalogoEstadoService,
        private crearHistoricoService: CrearHistoricoService,
        private confirmacionCrearEmailContactoService: ConfirmacionCrearEmailContactoService,
        private config: ConfigService
    ) { }

    async crearEmailContactoGazelook(contactoGazelookDto: CrearEmailContactoGazelookDto, idioma): Promise<any> {

        console.log('contactoGazelookDto: ', contactoGazelookDto)
        //Obtiene el codigo de la accion crear
        const accion = await this.nombreAccion.obtenerNombreAccion(nombreAcciones.crear);
        let getAccion = accion.codigo;

        //Obtiene el codigo de la entidad Email
        const entidad = await this.nombreEntidad.obtenerNombreEntidad(nombreEntidades.email);
        let codEntidad = entidad.codigo;

        //Obtiene el estado activa de la entidad email
        const estado = await this.nombreEstado.obtenerNombreEstado(nombrecatalogoEstados.activa, codEntidad);
        let codEstado = estado.codigo;


        const fechaCreacion = new Date();
        const fechaValid = this.sumarDias(new Date());
        //const token = this.random() + this.random() + this.random();
        const getToken = uuidv4().toString();

        //const EMAIL_CONTACT = this.config.get('EMAIL_CONTACT');
        const EMAIL_CONTACT = this.getEmailContact(idioma);

        const session = await mongoose.startSession();
        session.startTransaction();
        try {
            const opts = { session };
            //Si llega nombre de contacto
            let idPerfil = null

            // nuevo email
            const nuevoEmail = {
                usuario: idPerfil,
                emailDestinatario: EMAIL_CONTACT,
                estado: codEstado,
                fechaCreacion: fechaCreacion,
                fechaValidacion: fechaCreacion,
                tokenEmail: getToken,
                codigo: codigoCatalogosTipoEmail.contact_gazelook_email,
                enviado: false,
                descripcionEnvio: '',
                datosProceso: {
                    email: contactoGazelookDto.email,
                    pais: contactoGazelookDto.pais,
                    direccion: contactoGazelookDto.direccion,
                    tema: contactoGazelookDto.tema,
                    mensaje: contactoGazelookDto.mensaje,
                    nombre: contactoGazelookDto.nombre
                }
            };
            //datos para enviar al usuario por mail
            let dataEmail: any = {
                idioma: idioma,
                emailDestinatario: EMAIL_CONTACT,
                email: contactoGazelookDto.email,
                pais: contactoGazelookDto.pais,
                direccion: contactoGazelookDto.direccion,
                tema: contactoGazelookDto.tema,
                mensaje: contactoGazelookDto.mensaje,
                nombre: contactoGazelookDto.nombre
            }

            const sendEmail = await this.confirmacionCrearEmailContactoService.confirmacionCrearEmailContactoGazelook(dataEmail);
            if (sendEmail.response) {

                nuevoEmail['enviado'] = true;
                nuevoEmail['descripcionEnvio'] = sendEmail.response;


            } else {

                nuevoEmail['enviado'] = false;
                nuevoEmail['descripcionEnvio'] = sendEmail.code;
            }
            const email = await new this.emailModel(nuevoEmail).save(opts);

            //datos para guardar el historico
            const newHistorico: any = {
                datos: email,
                usuario: nuevoEmail.usuario,
                accion: getAccion,
                entidad: codEntidad
            };

            this.crearHistoricoService.crearHistoricoServer(newHistorico);

            await session.commitTransaction();
            await session.endSession();
            return true;

        } catch (error) {
            await session.abortTransaction();
            await session.endSession();
            throw error;
        }

    }

    getEmailContact(idioma: string): string {
        const email = contactEmails[`email_${idioma}`];
        if (!email) {
            return contactEmails[`email_en`];
        }
        return email;
    }

    sumarDias(fecha) {
        fecha.setDate(fecha.getDate() + 15);
        return fecha;
    }
    random() {
        return Math.random().toString(36).substr(2);
    };




}