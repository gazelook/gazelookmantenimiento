import { Observable } from 'rxjs';
import { Response } from 'express';
import { Inject, Injectable, HttpService } from '@nestjs/common';
import { Model } from 'mongoose';
import { Email } from 'src/drivers/mongoose/interfaces/email/email.interface';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { nombreAcciones, nombrecatalogoEstados, nombreEntidades } from 'src/shared/enum-sistema';
import { ConfigService } from '../../../config/config.service';
import { ConfirmacionEmailCuentaService } from '../../servicio_email/casos_de_uso/confirmacion-email-cuenta.service';
const mongoose = require('mongoose');
import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class CrearEmailConfirmacionCuentaService {

    constructor(@Inject('EMAIL_MODEL') private readonly emailModel: Model<Email>,
        private nombreEntidad: CatalogoEntidadService,
        private nombreAccion: CatalogoAccionService,
        private nombreEstado: CatalogoEstadoService,
        private crearHistoricoService: CrearHistoricoService,
        private httpService: HttpService,
        private config: ConfigService,
        private confirmacionEmailCuentaService: ConfirmacionEmailCuentaService
    ) { }

    async crearEmailConfirmacionCuenta(crearEmailDto: any, opts?: any): Promise<any> {

        const accion = await this.nombreAccion.obtenerNombreAccion(nombreAcciones.crear);
        let getAccion = accion.codigo;

        const entidad = await this.nombreEntidad.obtenerNombreEntidad(nombreEntidades.email);
        let codEntidad = entidad.codigo;

        const estado = await this.nombreEstado.obtenerNombreEstado(nombrecatalogoEstados.activa, codEntidad);
        let codEstado = estado.codigo;

        const fechaCreacion = new Date();
        const fechaValid = this.sumarDias(new Date());
        //const token = this.random() + this.random() + this.random();
        const token = uuidv4().toString();

        let session;
        let optsLocal;
        if (!opts) {
            session = await mongoose.startSession();
            session.startTransaction();
        }

        try {

            if (!opts) {
                optsLocal = true;
                opts = { session };
            }

            const nuevoEmail = {
                usuario: crearEmailDto.usuario,
                emailDestinatario: crearEmailDto.emailDestinatario,
                estado: codEstado,
                fechaCreacion: fechaCreacion,
                fechaValidacion: fechaValid,
                tokenEmail: token,
                codigo: crearEmailDto.codigo,
                idioma: crearEmailDto.idioma
            };
            const getEmail = await this.confirmacionEmailCuentaService.confirmacionEmailCuenta(nuevoEmail);
            if (getEmail?.response) {

                nuevoEmail['enviado'] = true;
                nuevoEmail['descripcionEnvio'] = getEmail.response;


            } else {

                nuevoEmail['enviado'] = false;
                nuevoEmail['descripcionEnvio'] = getEmail.code;
            }
            const email = await new this.emailModel(nuevoEmail).save(opts);

            //datos para guardar el historico
            const newHistorico: any = {
                datos: email,
                usuario: nuevoEmail.usuario,
                accion: getAccion,
                entidad: codEntidad
            };
            
            this.crearHistoricoService.crearHistoricoServer(newHistorico);

            if (optsLocal) {
                await session.commitTransaction();
                await session.endSession();
            }
            return true;

        } catch (error) {
            if (optsLocal) {
                //Aborta la transaccion
                await session.abortTransaction();
                //Finaliza la transaccion
                await session.endSession();
            }
            throw error
        }

    }

    sumarDias(fecha) {
        fecha.setDate(fecha.getDate() + 15);
        return fecha;
    }
    random() {
        return Math.random().toString(36).substr(2);
    };




}