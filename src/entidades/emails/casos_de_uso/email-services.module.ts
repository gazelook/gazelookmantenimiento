import { HistoricoServiceModule } from './../../historico/casos_de_uso/historico-services.module';
import { CatalogosServiceModule } from './../../catalogos/casos_de_uso/catalogos-services.module';
import { emailProviders } from '../drivers/email.provider';
import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { ServicioEmailServicesModule } from '../../servicio_email/casos_de_uso/email-services.module';
import { UsuarioServicesModule } from '../../usuario/casos_de_uso/usuario-services.module';
import { ConfirmarEmailService } from './confirmar-email.service';
import { ConfirmacionRecuperarContraseniaService } from './crear-email-confirmacion-recuperar-contrasenia.service';
import { CrearEmailRecuperarContraseniaService } from './crear-email-recuperar-contrasenia.service';
import { BloquearEmailResponsableService } from './bloquear-email-responsable.service';
import { CrearEmailConfirmacionCuentaService } from './crear-email-confirmacion-cuenta.service';
import { ObtenerEmailService } from './obtener-email.service';
import { ActualizarEstadoEmailService } from './actualizar-estado-email.service';
import { NuevoEmailCuentaService } from './nuevo-email-cuenta.service';
import { CrearEmailEnvioDatosService } from './crear-email-envio-datos.service';
import { CrearEmailContactoGazelookService } from './crear-email-contacto-gazelook.service';
import { CrearEmailNotificacionRechazoCuentaService } from './crear-email-notificacion-rechazo-cuenta.service';
import { CrearEmailEstadoSuscripcionService } from './crear-email-estado-suscripcion.service';
import { ReenvioConfirmacionActualizacionEmailService } from './reenvio-confirmacion-actualizacion-email.service';
import { CrearEmailReciboTransaccionService } from './crear-email-recibo-transaccion.service';
import { CrearEmailReciboPagoService } from './crear-email-recibo-pago.service';



@Module({
  imports: [
    DBModule,
    HttpModule,
    CatalogosServiceModule,
    HistoricoServiceModule,
    ServicioEmailServicesModule,
    forwardRef(() => UsuarioServicesModule),
  ],
  providers: [
    ...emailProviders,
    ConfirmarEmailService,
    ConfirmacionRecuperarContraseniaService,
    CrearEmailRecuperarContraseniaService,
    BloquearEmailResponsableService,
    CrearEmailConfirmacionCuentaService,
    ObtenerEmailService,
    ActualizarEstadoEmailService,
    NuevoEmailCuentaService,
    CrearEmailEnvioDatosService,
    CrearEmailContactoGazelookService,
    CrearEmailNotificacionRechazoCuentaService,
    CrearEmailEstadoSuscripcionService,
    ReenvioConfirmacionActualizacionEmailService,
    CrearEmailReciboTransaccionService,
    CrearEmailReciboPagoService
  ],
  exports: [
    ConfirmarEmailService,
    ConfirmacionRecuperarContraseniaService,
    CrearEmailRecuperarContraseniaService,
    BloquearEmailResponsableService,
    CrearEmailConfirmacionCuentaService,
    ObtenerEmailService,
    ActualizarEstadoEmailService,
    NuevoEmailCuentaService,
    CrearEmailEnvioDatosService,
    CrearEmailContactoGazelookService,
    CrearEmailNotificacionRechazoCuentaService,
    CrearEmailEstadoSuscripcionService,
    ReenvioConfirmacionActualizacionEmailService,
    CrearEmailReciboTransaccionService,
    CrearEmailReciboPagoService
  ],
  controllers: [],
})
export class EmailServicesModule { }
