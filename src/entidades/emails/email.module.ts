import { HttpModule, Module } from '@nestjs/common';
import { EmailControllerModule } from './controladores/email-controller.module';

@Module({
    imports: [EmailControllerModule, HttpModule],
    exports: [],
    providers: []
})
export class EmailModule { }
