import { HadersInterfaceNombres } from '../../../shared/header-response-interface';
import { Response } from 'express';
import { ApiResponse, ApiTags, ApiHeader, ApiOperation, ApiSecurity } from '@nestjs/swagger';
import { Controller, Res, HttpStatus, Get, Headers, Query, UseGuards } from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import { AuthGuard } from '@nestjs/passport';
import { Funcion } from 'src/shared/funcion';
import { EstadisticaTipoProyectosService } from '../casos_de_uso/estadistica-tipo-proyectos.service';

@ApiTags('Proyectos')
@Controller('api/proyectos/estadistica-tipo-proyectos')
export class EstadisticaTipoProyectosController {

    constructor(
        private readonly estadisticaTipoProyectosService: EstadisticaTipoProyectosService,
        private readonly i18n: I18nService,
        private readonly funcion: Funcion
    ) {

    }

    @Get('/')
    @ApiSecurity('Authorization')
    @ApiHeader({ name: 'idioma', description: 'codigo del idioma. Ejm: es, en, fr' })
    @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas', required: true })
    @ApiOperation({ summary: 'Obtiene el total de proyectos creados, por tipo y por su estado' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 401, description: 'No autorizado' })
    @ApiResponse({ status: 404, description: 'No se ha podido obtener los datos' })
    @ApiResponse({ status: 406, description: 'Parámetros no validos' })
    @UseGuards(AuthGuard('jwt'))


    public async obtenerProyectosRecomendados(@Headers() headers,
        @Res() response: Response
    ) {
        const headerNombre = new HadersInterfaceNombres;
        const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma)
        try {

            const proyectos = await this.estadisticaTipoProyectosService.estadisticaTiposProyectos();

            const respuesta = this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.OK, datos: proyectos })
            response.send(respuesta)
            return respuesta

        } catch (e) {
            const respuesta = this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR, mensaje: e.message })
            response.send(respuesta)
            return respuesta
        }
    }
}
