import { forwardRef, Module } from '@nestjs/common';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { ProyectosServicesModule } from '../casos_de_uso/proyectos-services.module';

import { Funcion } from 'src/shared/funcion';
import { EstadisticaTipoProyectosController } from './estadistica-tipo-proyectos.controller';



@Module({
  imports: [
    ProyectosServicesModule
  ],
  providers: [TraduccionEstaticaController, Funcion],
  exports: [],
  controllers: [
    EstadisticaTipoProyectosController
  ],
})
export class ProyectosControllerModule {}
