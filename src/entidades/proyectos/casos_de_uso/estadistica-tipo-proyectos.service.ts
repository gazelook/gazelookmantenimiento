import { Inject, Injectable } from "@nestjs/common";
import { Model } from "mongoose";
import { Proyecto } from "../../../drivers/mongoose/interfaces/proyectos/proyecto.interface";
import { TraduccionProyecto } from "../../../drivers/mongoose/interfaces/traduccion_proyecto/traduccion_proyecto.interface";
import { codigosTipoProyectos, estadosProyecto } from "../../../shared/enum-sistema";
import * as moment from 'moment';

@Injectable()
export class EstadisticaTipoProyectosService {

    constructor(
        @Inject('PROYECTO_MODEL') private readonly proyectoModel: Model<Proyecto>,
        @Inject('TRADUCCION_PROYECTO_MODEL') private readonly traduccionProyectoModel: Model<TraduccionProyecto>,
    ) {}

    async estadisticaTiposProyectos() {
        const totalProyectos = await this.proyectoModel.count({estado: {$ne: estadosProyecto.proyectoEliminado}});
        const proyectosEjecucion = await this.proyectoModel.count({estado: estadosProyecto.proyectoEnEjecucion});
        const proyectosEsperaFondos = await this.proyectoModel.count({estado: estadosProyecto.proyectoEnEsperaFondos});
        const proyectosFinanciados = await this.proyectoModel.count({estado: estadosProyecto.proyectoPreEstrategia});

        const proyectosTipoGazelook = await this.proyectoModel.count({tipo: codigosTipoProyectos.gazelook, estado: {$ne: estadosProyecto.proyectoEliminado}});
        const proyectosTipoLocal = await this.proyectoModel.count({tipo: codigosTipoProyectos.local, estado: {$ne: estadosProyecto.proyectoEliminado}});
        const proyectosTipoPais = await this.proyectoModel.count({tipo: codigosTipoProyectos.pais, estado: {$ne: estadosProyecto.proyectoEliminado}});
        const proyectosTipoMundial = await this.proyectoModel.count({tipo: codigosTipoProyectos.mundial, estado: {$ne: estadosProyecto.proyectoEliminado}});

        const result = {
            totalProyectos: totalProyectos,
            estadosProyecto: {
                ejecucion: {
                    total: proyectosEjecucion,
                    porcentaje: ((proyectosEjecucion * 100) / totalProyectos) / 100
                },
                esperaFondos: {
                    total: proyectosEsperaFondos,
                    porcentaje: ((proyectosEsperaFondos * 100) / totalProyectos) / 100
                },
                preEstrategia: {
                    total: proyectosFinanciados,
                    porcentaje: ((proyectosFinanciados * 100) / totalProyectos) / 100
                }
            },
            tiposProyecto: [
                { name: 'Gazelook', value: proyectosTipoGazelook },
                { name: 'Local', value: proyectosTipoLocal },
                { name: 'País', value: proyectosTipoPais },
                { name: 'Mundial', value: proyectosTipoMundial },
            ]
        }

        // informes por mes anual
        const formatFechaActual = moment().format('YYYY-MM-DD');
        const fechaActual = new Date(formatFechaActual);
        //moment().subtract(7, 'months')
        console.log("moment: ", moment().subtract(7, 'months'));

        return result;
    }
}