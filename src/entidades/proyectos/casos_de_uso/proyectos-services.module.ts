import { forwardRef, Module } from '@nestjs/common';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { CatalogoProviders } from 'src/entidades/catalogos/drivers/catalogo.provider';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { proyectoProviders } from '../drivers/proyecto.provider';
import { UsuarioServicesModule } from 'src/entidades/usuario/casos_de_uso/usuario-services.module';
import { EmailServicesModule } from 'src/entidades/emails/casos_de_uso/email-services.module';
import { Funcion } from 'src/shared/funcion';
import { EstadisticaTipoProyectosService } from './estadistica-tipo-proyectos.service';


@Module({
  imports: [DBModule,
    forwardRef(() => UsuarioServicesModule),
    forwardRef(() => EmailServicesModule)
  ],
  providers: [
    ...proyectoProviders,
    ...CatalogoProviders,
    CatalogoIdiomasService,
    CatalogoEstadoService,
    CatalogoEntidadService,
    CatalogoAccionService,
    TraduccionEstaticaController,
    Funcion,
    EstadisticaTipoProyectosService
  ],
  exports: [
    EstadisticaTipoProyectosService
  ],
  controllers: [],
})
export class ProyectosServicesModule { }
