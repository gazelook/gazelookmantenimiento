import { CrearHistoricoService } from './crear-historico.service';
import { forwardRef, Global, HttpModule, Module } from '@nestjs/common';
import { historicoProviders } from '../drivers/historico.provider';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { ObtenerHistoricoService } from './obtener-historico.service';

@Global()
@Module({
  imports: [
    DBModule,
    forwardRef(() => HttpModule),
  ],
  providers: [
    CrearHistoricoService,
    ObtenerHistoricoService,
    ...historicoProviders
  ],
  exports: [
    CrearHistoricoService,
    ObtenerHistoricoService
  ],
  controllers: [],
})
export class HistoricoServiceModule { }
