import { Connection } from 'mongoose';
import { HistoricoModelo } from '../../../drivers/mongoose/modelos/historico/historico.schema';

export const historicoProviders = [
    {
      provide: 'HISTORICO_MODEL',
      useFactory: (connection: Connection) => connection.model('historico', HistoricoModelo, 'historico'),
      inject: ['DB_CONNECTION'],
    },

  ];
