import { Module } from '@nestjs/common';
import { HistoricoControllerModule } from './controlador/historico-controller.module';

@Module({
  imports: [HistoricoControllerModule],
  providers: [],
  controllers: [],
})
export class HistoricoModule {}

