import { Module, Delete } from '@nestjs/common';
import { HistoricoServiceModule } from '../casos_de_uso/historico-services.module';
import { HistoricoControlador } from './historico.controller';



@Module({
  imports: [HistoricoServiceModule],
  providers: [],
  exports: [],
  controllers: [
    HistoricoControlador,
  ],
})
export class HistoricoControllerModule {}
