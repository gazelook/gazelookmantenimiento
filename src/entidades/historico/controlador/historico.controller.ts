import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { Controller, Post, Body, Res, HttpStatus } from '@nestjs/common';
import { HistoricoDto } from '../dto/historicoEntidad';
import { CrearHistoricoService } from '../casos_de_uso/crear-historico.service';
// import {CrearUsuarioAdaptador}from '../adaptador/v1/adapterCrearUsuario'

//@ApiTags('Historico')
@Controller('api/historico')
export class HistoricoControlador {

    constructor(private readonly crearHistoricoService: CrearHistoricoService) { }

    // @Post('/')
    // @ApiResponse({ status: 201, description: 'Historico creado.' })
    // @ApiResponse({ status: 403, description: 'Forbidden.' })

    public async crearHistorico(@Res() res, @Body() HistoricoDto) {
        const historico = await this.crearHistoricoService.crearHistorico(HistoricoDto);

        try {
            return res.status(HttpStatus.CREATED).json({
                statusCode: HttpStatus.CREATED,
                // header: {
                //     'Content-Type': 'application/json',
                //     'Last-Modified': new Date(historico.fechaCreacion).toUTCString()
                // },
                // timestamp: new Date().toISOString(), 
                body: {
                    historico
                }
            });

        } catch (e) {
            return res.status(HttpStatus.BAD_REQUEST).json({
                statusCode: HttpStatus.BAD_REQUEST,
                timestamp: new Date().toISOString(),
                body: {
                    mensaje: {
                        message: e.message
                    }
                }
            });
        }
    }
}

