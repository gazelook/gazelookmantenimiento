
import { AuthGuard } from '@nestjs/passport';
import { ApiResponse, ApiTags, ApiOperation, ApiHeader, ApiBody, ApiSecurity } from '@nestjs/swagger';
import { Controller, Post, Body, Res, HttpStatus, Request, HttpException, Inject, Get, Headers, Param, BadRequestException, UsePipes, ValidationPipe, UseGuards, Query } from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import * as mongoose from 'mongoose';
import { InformacioAplicacionService } from '../casos_de_uso/informacion-aplicacion.service';
import { Funcion } from '../../../shared/funcion';



@ApiTags('Usuario')
@Controller('api/usuario/solicitar-informacion')
export class InformacionAplicacionControlador {

    constructor(
        private readonly informacioAplicacionService: InformacioAplicacionService,
        private readonly i18n: I18nService,
        private readonly funcion: Funcion
    ) { }

    @Get('/:idUsuario')
    @ApiSecurity('Authorization')
    @ApiHeader({ name: 'idioma', description: 'codigo del idioma. Ejm: es, en, fr' })
    @ApiHeader({ name: 'apiKey', required: true, description: 'key para usar las rutas' })
    @ApiOperation({ summary: 'Retorna la informacion del usuario que tiene en la aplicacion' })
    @ApiResponse({ status: 201, description: 'Datos del usuario en la aplicacion' })
    @ApiResponse({ status: 401, description: 'No autorizado' })
    @ApiResponse({ status: 406, description: 'Parámetros no validos' })

    public async informacionAplicacion(@Headers() headers,
        @Param('idUsuario') idUsuario: string
    ) {

        const respuesta = new RespuestaInterface;
        const codIdioma = await this.funcion.obtenerIdiomaDefecto(headers.idioma);
        try {
            const informacion = await this.informacioAplicacionService.solicitarInformacionUsuario(idUsuario)
            if (informacion) {
                respuesta.codigoEstado = HttpStatus.OK;
                respuesta.respuesta = {
                    datos: informacion,
                    mensaje: "Seleccione el enlace para descargar la informacion"
                }
            } else {
                respuesta.codigoEstado = HttpStatus.NOT_FOUND;
                respuesta.respuesta = {
                    mensaje: 'Error al obtener los datos',
                }
            }
            return respuesta;
        } catch (e) {
            respuesta.codigoEstado = HttpStatus.INTERNAL_SERVER_ERROR;
            respuesta.respuesta = {
                mensaje: e.message
            }
            return respuesta;
        }
    }
}

