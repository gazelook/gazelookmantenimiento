import { ActualizarEmailResponsableService } from './../casos_de_uso/actualizar-email-responsable.service';
import { Controller, Post, Body, Res, HttpStatus, Get, Put, Headers } from '@nestjs/common';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import { idiomas } from 'src/shared/enum-sistema';
import { Funcion } from '../../../shared/funcion';

import * as mongoose from 'mongoose';
import { ActualizarEmailResponsableDto } from '../dto/actualizar-email-responsable.dto';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { I18nService } from 'nestjs-i18n';


@ApiTags('Usuario')
@Controller('api/usuario')
export class ActualizarEmailResponsableControlador {

    constructor(private readonly actualizarEmailService: ActualizarEmailResponsableService,
        private readonly traduccionEstaticaController: TraduccionEstaticaController,
        private readonly funcion: Funcion,
        private readonly i18n: I18nService,
    ) { }

    @Post('/actualizar-email-responsable')
    @ApiOperation({ summary: 'Actualiza el email del responsable y envia un nuevo email a dicho email, para su confirmación' })
    @ApiResponse({ status: 201, description: 'Actualizacion correcta' })
    @ApiResponse({ status: 404, description: 'Error al actualizar' })

    public async actualizarEmailResponsable(@Res() res, @Body() dataActualizar: ActualizarEmailResponsableDto, @Headers() headers) {
        const codIdioma = await this.funcion.obtenerIdiomaDefecto(dataActualizar.codIdioma);
        const session = await mongoose.startSession()
        session.startTransaction();
        try {
            console.log("dataActualizar", dataActualizar);
            if (mongoose.isValidObjectId(dataActualizar.usuario)) {

                const opts = { session };
                const updateEmail = await this.actualizarEmailService.actualizarEmailResponsable(dataActualizar, opts);

                if (!updateEmail) {
                    await session.abortTransaction();
                    session.endSession();
                    const ERROR_ACTUALIZAR = await this.traduccionEstaticaController.traduccionEstatica(codIdioma, 'ERROR_ACTUALIZAR');
                    //return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.NOT_FOUND, mensaje: ERROR_ACTUALIZAR });
                    res.render('index', { message: ERROR_ACTUALIZAR });
                } else {
                    await session.commitTransaction();
                    session.endSession();
                    const ACTUALIZACION_CORRECTA = await this.traduccionEstaticaController.traduccionEstatica(codIdioma, 'ACTUALIZACION_CORRECTA');
                    res.render('index', { message: ACTUALIZACION_CORRECTA });
                    //return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.OK, mensaje: ACTUALIZACION_CORRECTA });
                }
            } else {
                await session.abortTransaction();
                session.endSession();
                const PARAMETROS_NO_VALIDOS = await this.i18n.translate(codIdioma.concat('.PARAMETROS_NO_VALIDOS'), {
                    lang: codIdioma
                });
                res.render('index', { message: PARAMETROS_NO_VALIDOS });
                //return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.NOT_ACCEPTABLE, mensaje: PARAMETROS_NO_VALIDOS });
            }

        } catch (e) {
            await session.abortTransaction();
            session.endSession();
            return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR, mensaje: e.message });
        }
    }

}

