import { Body, Controller, Get, Headers, HttpStatus, Put, Query, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBody, ApiHeader, ApiOperation, ApiResponse, ApiSecurity, ApiTags } from '@nestjs/swagger';
import { isValidObjectId } from 'mongoose';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { ActualizarUsuarioService } from '../casos_de_uso/actualizar-usuario.service';
import { ObtenerTodosUsuariosService } from '../casos_de_uso/obtener-usuarios.service';
import { ActualizarEstadoUsuarioDto } from '../dto/usuario-dto';

@ApiTags('Usuario')
@Controller('api/usuario/actualizar-estado-usuario')
export class ActualizarEstadoUsuarioControlador {

    constructor(
        private readonly actualizarUsuarioService: ActualizarUsuarioService,
        private readonly i18n: I18nService,
        private readonly funcion: Funcion,
    ) {
    }

    @Put('/')
    @ApiSecurity('Authorization')
    @ApiBody({ type: ActualizarEstadoUsuarioDto })
    @ApiHeader({ name: 'idioma', description: 'codigo del idioma. Ejm: es, en, fr' })
    @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas', required: true })
    @ApiOperation({ summary: 'Actualiza el estado de un usuario del sistema' })
    @ApiResponse({ status: 200, description: 'OK' })
    @ApiResponse({ status: 404, description: 'Error al actualizar' })
    @ApiResponse({ status: 401, description: 'No autorizado' })
    @ApiResponse({ status: 406, description: 'Parámetros no validos' })
    @UseGuards(AuthGuard('jwt'))

    public async obtenerUsuariosAdminByEntidad(
        @Headers() headers,
        @Body() dataActualizar: ActualizarEstadoUsuarioDto
        // @Res() response: Response
    ) {

        const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
        try {
            if (isValidObjectId(dataActualizar._id) && dataActualizar.estado.codigo) {
                const usuario = await this.actualizarUsuarioService.actualizarEstadoUsuario(dataActualizar._id, dataActualizar.estado.codigo)
                if (usuario) {
                    const ACTUALIZACION_CORRECTA = await this.i18n.translate(codIdioma.concat('.ACTUALIZACION_CORRECTA'), {
                        lang: codIdioma
                    });
                    return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.OK, mensaje: ACTUALIZACION_CORRECTA })
                } else {
                    const ERROR_ACTUALIZAR = await this.i18n.translate(codIdioma.concat('.ERROR_ACTUALIZAR'), {
                        lang: codIdioma
                    });
                    return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.NOT_FOUND, mensaje: ERROR_ACTUALIZAR })
                }

            } else {
                const PARAMETROS_NO_VALIDOS = await this.i18n.translate(codIdioma.concat('.PARAMETROS_NO_VALIDOS'), {
                    lang: codIdioma
                });
                return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.NOT_ACCEPTABLE, mensaje: PARAMETROS_NO_VALIDOS })
            }
        } catch (e) {
            const respuesta = this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR, mensaje: e.message })
            // response.send(respuesta)
            return respuesta
        }

    }


}
