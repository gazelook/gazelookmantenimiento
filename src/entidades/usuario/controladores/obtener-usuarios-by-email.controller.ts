import { Controller, Get, Headers, HttpStatus, Query, Res, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiHeader, ApiOperation, ApiResponse, ApiSecurity, ApiTags } from '@nestjs/swagger';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { ObtenerTodosUsuariosService } from '../casos_de_uso/obtener-usuarios.service';
import { Response } from 'express';
import { RespObtenerTodosUsuariosDto } from '../dto/usuario-dto';
import { ObtenerUsuarioLoginService } from '../casos_de_uso/obtener-usuario-login.service';

@ApiTags('Usuario')
@Controller('api/usuario/obtener-usuarios-email')
export class ObtenerUsuarioByEmailControlador {

    constructor(
        private readonly obtenerUsuarioLoginService: ObtenerUsuarioLoginService,
        private readonly i18n: I18nService,
        private readonly funcion: Funcion,
    ) {
    }

    @Get('/')
    @ApiSecurity('Authorization')
    @ApiHeader({ name: 'idioma', description: 'codigo del idioma. Ejm: es, en, fr' })
    @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas', required: true })
    @ApiOperation({ summary: 'Devuelve un usuarios buscado por el email' })
    @ApiResponse({ status: 200, type: RespObtenerTodosUsuariosDto, description: 'OK' })
    @ApiResponse({ status: 406, description: 'Error al obtener usuario' })
    @UseGuards(AuthGuard('jwt'))

    public async obtenerUsuarioByEmail(
        @Headers() headers,
        @Query('email') email: string,
    ) {

        const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma)

        try {

            if (email) {
                const usuarios = await this.obtenerUsuarioLoginService.encontrarUsuarioByEmail(email)

                const respuesta = this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.OK, datos: usuarios })
                return respuesta

            } else {
                const PARAMETROS_NO_VALIDOS = await this.i18n.translate(codIdioma.concat('.PARAMETROS_NO_VALIDOS'), {
                    lang: codIdioma
                });
                const respuesta = this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.NOT_ACCEPTABLE, mensaje: PARAMETROS_NO_VALIDOS })
                return respuesta
            }
        } catch (e) {
            const respuesta = this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR, mensaje: e.message })
            return respuesta
        }

    }


}
