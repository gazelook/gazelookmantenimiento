import { Controller, Get, Headers, HttpStatus, Query, Res, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiHeader, ApiOperation, ApiResponse, ApiSecurity, ApiTags } from '@nestjs/swagger';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { ObtenerTodosUsuariosService } from '../casos_de_uso/obtener-usuarios.service';
import { Response } from 'express';
import { RespObtenerTodosUsuariosDto } from '../dto/usuario-dto';

@ApiTags('Usuario')
@Controller('api/usuario/obtener-todos-usuarios')
export class ObtenerTodosUsuariosControlador {

    constructor(
        private readonly obtenerTodosUsuariosService: ObtenerTodosUsuariosService,
        private readonly i18n: I18nService,
        private readonly funcion: Funcion,
    ) {
    }

    @Get('/')
    @ApiSecurity('Authorization')
    @ApiHeader({ name: 'idioma', description: 'codigo del idioma. Ejm: es, en, fr' })
    @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas', required: true })
    @ApiOperation({ summary: 'Devuelve todos los usuarios activos, activos no verificados y bloqueados del sistema' })
    @ApiResponse({ status: 200, type: RespObtenerTodosUsuariosDto, description: 'OK' })
    @ApiResponse({ status: 406, description: 'Error al obtener usuarios' })
    @UseGuards(AuthGuard('jwt'))

    public async obtenerTodosUsuarios(
        @Headers() headers,
        @Query('limite') limite: number,
        @Query('pagina') pagina: number,
        @Res() response: Response
    ) {

        console.log('__________________{{{{')
        const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma)

        try {

            if (!isNaN(limite) && !isNaN(pagina) && limite > 0 && pagina > 0) {
                const usuarios = await this.obtenerTodosUsuariosService.obtenerUsuarios(limite, pagina, response)

                const respuesta = this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.OK, datos: usuarios })
                response.send(respuesta)
                return respuesta

            } else {
                const PARAMETROS_NO_VALIDOS = await this.i18n.translate(codIdioma.concat('.PARAMETROS_NO_VALIDOS'), {
                    lang: codIdioma
                });
                const respuesta = this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.NOT_ACCEPTABLE, mensaje: PARAMETROS_NO_VALIDOS })
                response.send(respuesta)
                return respuesta
            }
        } catch (e) {
            const respuesta = this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR, mensaje: e.message })
            response.send(respuesta)
            return respuesta
        }

    }


}
