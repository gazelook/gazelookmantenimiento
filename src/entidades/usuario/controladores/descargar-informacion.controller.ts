
import { AuthGuard } from '@nestjs/passport';
import { ApiResponse, ApiTags, ApiOperation, ApiHeader, ApiBody } from '@nestjs/swagger';
import { Controller, Post, Body, Res, HttpStatus, Request, HttpException, Inject, Get, Headers, Param, BadRequestException, UsePipes, ValidationPipe, UseGuards, Query } from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import * as mongoose from 'mongoose';
import { InformacioAplicacionService } from '../casos_de_uso/informacion-aplicacion.service';
import { DescargarInformacioService } from '../casos_de_uso/descargar-informacion.service';
import { Funcion } from '../../../shared/funcion';
import { TraduccionEstaticaController } from '../../../multiIdioma/controladores/traduccion-estatica-controller';
import { CatalogoIdiomasService } from '../../catalogos/casos_de_uso/catalogo-idiomas.service';
import { isUUID } from 'class-validator';
import { ResponseObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { unlink } from 'fs';
import { Response } from 'express';



@ApiTags('descargas')
@Controller('api/informacion-usuario')
export class DescargarInformacionControlador {

    constructor(
        private readonly descargarInformacioService: DescargarInformacioService,
        private readonly i18n: I18nService,
        private readonly funcion: Funcion,
        private readonly traduccionEstaticaController: TraduccionEstaticaController,
        private readonly catalogoIdiomasService: CatalogoIdiomasService,
    ) { }

    @Get('/:token/:idioma')
    @ApiHeader({ name: 'idioma', description: 'codigo del idioma. Ejm: es, en, fr' })
    //@ApiHeader({ name: 'apiKey', required: true, description: 'key para usar las rutas' })
    @ApiOperation({ summary: 'Descarga la informacion del usuario' })
    @ApiResponse({ status: 201, description: 'Datos del usuario en la aplicación' })
    @ApiResponse({ status: 401, description: 'No autorizado' })
    @ApiResponse({ status: 406, description: 'link no valido' })
    //@UseGuards(AuthGuard('jwt'))

    public async descargarInformacion(@Headers() headers,
        @Param('token') token: string,
        @Param('idioma') idioma: string,
        @Res() res: Response
    ) {

        const respuesta = new RespuestaInterface;

        try {
            const codIdioma = await this.catalogoIdiomasService.verificarIdioma(idioma);
            if (isUUID(token)) {
                const informacion = await this.descargarInformacioService.descargarInformacion(token);
                if (informacion) {
                    res.attachment(informacion.nombreArchivo);
                    res.type('zip')

                    res.download(informacion.archivo, informacion.nombreArchivo, (error) => {
                        if (error) {
                            console.log('Some error')
                        } else {
                            // delete file temp from ->(dist/temp_files)
                            unlink(informacion.archivo, function (err) {
                                if (err) {
                                    console.error(err);
                                }
                                console.log('Temp File Delete');
                            });
                        }
                    });

                } else {
                    //respuesta.codigoEstado = HttpStatus.NOT_FOUND;
                    const LINK_NO_VALIDO = await this.i18n.translate(codIdioma.concat('.LINK_NO_VALIDO'), {
                        lang: codIdioma
                    });
                    res.render('index', { message: LINK_NO_VALIDO });
                }

            } else {
                const NO_AUTORIZADO = await this.i18n.translate(codIdioma.concat('.NO_AUTORIZADO'), {
                    lang: codIdioma
                });
                res.render('index', { message: NO_AUTORIZADO });
            }
        } catch (e) {
            respuesta.codigoEstado = HttpStatus.INTERNAL_SERVER_ERROR;
            respuesta.respuesta = {
                mensaje: e.message
            }
            return respuesta;
        }
    }
}

