import { Module, Delete } from '@nestjs/common';
import { UsuarioServicesModule } from '../casos_de_uso/usuario-services.module';
import { HttpErrorFilter } from 'src/shared/filters/http-error.filter';
import { InformacionAplicacionControlador } from './informacion-aplicacion.controller';
import { DescargarInformacionControlador } from './descargar-informacion.controller';
import { Funcion } from '../../../shared/funcion';
import { ActualizarContraseniaControlador } from './actualizar-contrasenia.controller';
import { ActualizarEmailResponsableControlador } from './actualizar-email-responsable.controller';
import { TraduccionEstaticaController } from '../../../multiIdioma/controladores/traduccion-estatica-controller';
import { CatalogosServiceModule } from '../../catalogos/casos_de_uso/catalogos-services.module';
import { ObtenerTodosUsuariosControlador } from './obtener-todos-usuarios.controller';
import { ActualizarEstadoUsuarioControlador } from './actualizar-estado-usuario.controller';
import { ObtenerUsuarioByEmailControlador } from './obtener-usuarios-by-email.controller';
import { ObtenerUsuariosCriptoControlador } from './obtener-usuarios-cripto.controller';



@Module({
  imports: [UsuarioServicesModule, CatalogosServiceModule],
  providers: [HttpErrorFilter, Funcion, TraduccionEstaticaController],
  exports: [UsuarioServicesModule],
  controllers: [
    InformacionAplicacionControlador,
    DescargarInformacionControlador,
    ActualizarContraseniaControlador,
    ActualizarEmailResponsableControlador,
    ObtenerTodosUsuariosControlador,
    ActualizarEstadoUsuarioControlador,
    ObtenerUsuarioByEmailControlador,
    ObtenerUsuariosCriptoControlador
  ],
})
export class UsuarioControllerModule {}
