
import { AuthGuard } from '@nestjs/passport';
import { ApiResponse, ApiTags, ApiOperation, ApiHeader } from '@nestjs/swagger';
import { Controller, Post, Body, Res, HttpStatus, Headers } from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import { RecuperarContraseniaService } from '../casos_de_uso/recuperar-contrasena.service';
import { ActualizarContraseniaDto } from '../dto/actualizar_contrasenia.dto';
import { Funcion } from 'src/shared/funcion';
import { idiomas } from 'src/shared/enum-sistema';
import { ConfigService } from 'src/config/config.service';
import { contactEmail, recursosSistemaEN } from 'src/shared/enum-recursos-sistema';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';

@ApiTags('Usuario')
@Controller('api/actualizar-contrasenia')
export class ActualizarContraseniaControlador {

    constructor(
        private readonly recuperarContraseniaService: RecuperarContraseniaService,
        private readonly i18n: I18nService,
        private readonly funcion: Funcion,
        private readonly config: ConfigService
    ) { }

    //funcion = new Funcion(this.i18n);

    @Post('/')
    @ApiHeader({ name: 'idioma', description: 'codigo del idioma. Ejm: es, en, fr' })
    @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
    @ApiOperation({ summary: 'Actualiza la contraseña de un usuario Body: (email, newPass, confirmPass)' })
    @ApiResponse({ status: 201, description: 'Actualizacion correcta' })
    @ApiResponse({ status: 404, description: 'Error al actualizar' })
    @ApiResponse({ status: 409, description: 'Contraseñas no son iguales' })
    @ApiResponse({ status: 401, description: 'No autorizado' })
    @ApiResponse({ status: 406, description: 'Parámetros no validos' })
    //@UseGuards(AuthGuard('jwt'))

    public async actualizarContrasenia(
        @Headers() headers,
        @Body() userData: ActualizarContraseniaDto,
        @Res() res
    ) {

        const codIdioma = await this.funcion.obtenerIdiomaDefecto(userData.codIdioma)

        const urlRecursoSistema = this.config.get<string>("URL_RECURSOS_SISTEMA");
        let urlHeaderEmail;
        let urlFooterEmail;
        let contact;
        if (codIdioma === idiomas.espanol) {
            urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_ES}`;
            urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_ES}`;
            contact = contactEmail.contactES;
        }
        if (codIdioma === idiomas.ingles) {
            urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_EN}`;
            urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_EN}`;
            contact = contactEmail.contactEN;
        }
        if (codIdioma === idiomas.frances) {
            urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_FR}`;
            urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_FR}`;
            contact = contactEmail.contactFR;
            // nombreUser = nombreUser.concat(' ');
        }
        if (codIdioma === idiomas.italiano) {
            urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_IT}`;
            urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_IT}`;
            contact = contactEmail.contactIT;
        }
        if (codIdioma === idiomas.aleman) {
            urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_DE}`;
            urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_DE}`;
            contact = contactEmail.contactDE;
        }
        if (codIdioma === idiomas.portugues) {
            urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_PT}`;
            urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_PT}`;
            contact = contactEmail.contactPT;
        }

        let texto = "Se ha actualizado con éxito"
        let textoTraducido = await traducirTexto(codIdioma, texto)
        const actualizacionCorrecta = textoTraducido.textoTraducido

        let texto1 = "Contraseñas no son idénticas"
        let textoTraducido1 = await traducirTexto(codIdioma, texto1)
        const contraseniasFallidas = textoTraducido1.textoTraducido

        try {
            if (userData.email && userData.newPass && userData.confirmPass) {
                if (userData.newPass === userData.confirmPass) {
                    const recContrasenia = await this.recuperarContraseniaService.actualizarContrasenia(userData.email, userData.newPass);

                    if (recContrasenia) {
                        const ACTUALIZACION_CORRECTA = await this.i18n.translate(codIdioma.concat('.ACTUALIZACION_CORRECTA'), {
                            lang: codIdioma
                        });
                        res.render('index', { message: actualizacionCorrecta,
                            urlHeaderEmail : urlHeaderEmail, contact: contact
                        });
                        //return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.OK, mensaje: ACTUALIZACION_CORRECTA })

                    } else {
                        const ERROR_ACTUALIZAR = await this.i18n.translate(codIdioma.concat('.ERROR_ACTUALIZAR'), {
                            lang: codIdioma
                        });
                        res.render('index', { message: ERROR_ACTUALIZAR,
                            urlHeaderEmail : urlHeaderEmail, contact: contact
                        });
                        //return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.NOT_FOUND, mensaje: ERROR_ACTUALIZAR })

                    }
                } else {
                    const ERROR_CONTRASENIAS = await this.i18n.translate(codIdioma.concat('.ERROR_CONTRASENIAS'), {
                        lang: codIdioma
                    });
                    res.render('index', { message: contraseniasFallidas,
                        urlHeaderEmail : urlHeaderEmail, contact: contact
                    });
                    //return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.CONFLICT, mensaje: ERROR_CONTRASENIAS })
                }
            } else {
                const PARAMETROS_NO_VALIDOS = await this.i18n.translate(codIdioma.concat('.PARAMETROS_NO_VALIDOS'), {
                    lang: codIdioma
                });
                res.render('index', { message: PARAMETROS_NO_VALIDOS,
                    urlHeaderEmail : urlHeaderEmail, contact: contact
                });
                //return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.NOT_ACCEPTABLE, mensaje: PARAMETROS_NO_VALIDOS })
            }
        } catch (e) {
            return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR, mensaje: e.message })
        }
    }
}

