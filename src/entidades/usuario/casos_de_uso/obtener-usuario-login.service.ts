import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Usuario } from 'src/drivers/mongoose/interfaces/usuarios/usuario.interface';
import { estadosPerfil, estadosUsuario } from '../../../shared/enum-sistema';

@Injectable()
export class ObtenerUsuarioLoginService {

  constructor(@Inject('USUARIO_MODEL') private readonly userModel: Model<Usuario>) { }

  async encontrarUsuario(emailUsuario: string): Promise<any> {
    const user = await this.userModel.findOne({
      $and:
        [
          {
            $or: [
              { estado: estadosUsuario.activaNoVerificado },
              { estado: estadosUsuario.activa },
              { estado: estadosUsuario.inactivaPago },
              { estado: estadosUsuario.rechazadoResponsable },
              { estado: estadosUsuario.noPermitirAcceso },
              { estado: estadosUsuario.inactivaPagoPaymentez }
                        ],
          },
          {email:{'$regex' : '^'.concat(emailUsuario).concat('$'), '$options' : 'i'}}
        ],
    });

    return user;
  }

  async encontrarUsuarioByEmail(emailUsuario: string): Promise<any> {

    try {
   
      const usuario = await this.userModel.findOne({
        $and:
        [
          {
            $or: [
              { estado: estadosUsuario.activaNoVerificado },
              { estado: estadosUsuario.activa },
              { estado: estadosUsuario.inactivaPago },
              { estado: estadosUsuario.rechazadoResponsable },
              { estado: estadosUsuario.noPermitirAcceso }
            ],
          },
          { email: emailUsuario }
        ],
    }).populate({
        path: 'perfiles',
        match: {
          estado: estadosPerfil.activa
        },
        populate: {
          path: 'telefonos'
        }
      })
        .populate({
          path: 'rolSistema',
          populate: {
            path: 'rolesEspecificos',
            populate: {
              path: 'acciones'
            }
          }
        });
  
      if (usuario) {
        let arrayRolSistema = [];
        
        if (usuario.rolSistema.length > 0) {
  
          for (const roles of usuario.rolSistema) {
            let obRolSistema = {
              _id: roles['_id'],
              nombre: roles['nombre'],
              rol: {
                codigo: roles['rol']
              },
              // rolesEspecificos: arrayRolesEspecificos
            }
            arrayRolSistema.push(obRolSistema);
  
          }
        }
        let arrayPerfiles = [];
        
        if (usuario.perfiles.length > 0) {
          for (const perfil of usuario.perfiles) {

            let arrayTelefono = [];
            if (perfil.telefonos.length > 0) {
              for (const telefono of perfil.telefonos) {
                let objTelefono = {
                  numero: telefono.numero,
                  pais: {
                    codigo: telefono.pais
                  }
                }
                arrayTelefono.push(objTelefono);
              }
            }
            let obPerfil = {
              _id: perfil._id,
              nombre: perfil.nombre,
              nombreContacto: perfil.nombreContacto,
              nombreContactoTraducido: perfil.nombreContactoTraducido,
              telefonos: arrayTelefono
            }
            arrayPerfiles.push(obPerfil);
          }
        }
  
        
        let objUsuario = {
          _id: usuario._id,
          rolSistema: arrayRolSistema,
          email: usuario.email,
          perfiles: arrayPerfiles,
          estado: {
            codigo: usuario.estado
          }
        }
        return objUsuario;
      }else{
        return {}
      }
  
    } catch (error) {
      throw error
    }
   
    
  }

}
