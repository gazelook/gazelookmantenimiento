import { CatalogoIdiomasService } from './../../catalogos/casos_de_uso/catalogo-idiomas.service';
import { ObtenerIdUsuarioService } from './../../usuario/casos_de_uso/obtener-id-usuario.service';
import { Injectable, Inject, HttpStatus, forwardRef } from "@nestjs/common";
import { Model } from "mongoose";
import { TipoPerfil } from 'src/drivers/mongoose/interfaces/catalogoTipoPerfil/catalogo-tipo-perfil.interface'
import { TraducionTipoPerfil } from 'src/drivers/mongoose/interfaces/traduccion_catalogo_tipo_perfil/traduccion-catalogo-tipo-perfil.interface';
import { Perfil } from 'src/drivers/mongoose/interfaces/perfil/perfil.interface';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { nombreEntidades } from '../../../shared/enum-sistema';
import { CatalogoLocalidad } from '../../../drivers/mongoose/interfaces/catalogo_localidad/catalogo-localidad.interface';
import { CatalogoPais } from '../../../drivers/mongoose/interfaces/catalogo_pais/catalogo-pais.interface';

@Injectable()
export class ObtenerPerfilUsuarioService {

    constructor(
        @Inject('TIPO_PERFIL_MODEL') private readonly catalogoTipoPerfilModelo: Model<TipoPerfil>,
        @Inject('PERFIL_MODEL') private readonly perfilModel: Model<Perfil>,
        @Inject('TRADUCCION_TIPO_PERFIL_MODEL') private readonly traduccionTipoPerfilModelo: Model<TraducionTipoPerfil>,
        @Inject('CATALOGO_PAIS_MODEL') private readonly paisModel: Model<CatalogoPais>,
        @Inject('CATALOGO_LOCALIDAD_MODEL') private readonly localidadModel: Model<CatalogoLocalidad>,
        private readonly obtenerIdUsuarioService: ObtenerIdUsuarioService,
        private readonly catalogoIdiomasModelat: CatalogoIdiomasService,
        private catalogoEstadoService: CatalogoEstadoService,
        private catalogoEntidadService: CatalogoEntidadService,
    ) { }

    // obtener perfiles de usuario con su estado (cualquier tipo de estado)
    async obtenerPerfilUsuario(idUsuario: string, idioma): Promise<any> {

        try {

            const entidadPerfil = await this.catalogoEntidadService.obtenerNombreEntidad(nombreEntidades.perfiles)

            const usuarioQuery: any = await this.obtenerIdUsuarioService.obtenerUsuarioById(idUsuario);

            const catalogoIdioma = await this.catalogoIdiomasModelat.obtenerIdiomaByCodigoNombre(idioma);

            let listaResult = [];
            let perfil: any = {
            }
            let result: any = {
                perfil: {}
            }
            /* const listaPerfiles: any = await this.catalogoTipoPerfilModelo.find()
                .select('codigo')
                .populate(
                    {
                        path: 'traducciones',
                        select: 'nombre descripcion',
                        match: { idioma: catalogoIdioma.codigo }
                    }
                ); */
            const listaPerfiles = await this.traduccionTipoPerfilModelo.find({ idioma: catalogoIdioma.codigo });

            if (!listaPerfiles || !usuarioQuery || usuarioQuery?.perfiles?.length === 0) {
                return null;
            }

            const listaPosicion = [];
            // sin populate
            for (let i = 0; i < listaPerfiles.length; i++) {
                const catTipPerfil = await this.catalogoTipoPerfilModelo.findOne({ codigo: listaPerfiles[i].codigoTipoPerfil })
                result = {
                    _id: catTipPerfil._id,
                    codigo: catTipPerfil.codigo,
                    traducciones: {
                        _id: listaPerfiles[i]._id,
                        nombre: listaPerfiles[i].nombre,
                        descripcion: listaPerfiles[i].descripcion,
                    }
                }

                listaResult.push(result)
                listaPosicion.push(result.codigo)
            }
            // con populate
            /* for (let i = 0; i < listaPerfiles.length; i++) {
                result = {
                    _id: listaPerfiles[i]._id,
                    codigo: listaPerfiles[i].codigo,
                    traducciones: listaPerfiles[i].traducciones
                }

                listaResult.push(result)
                listaPosicion.push(result.codigo)
            } */

            for (let j = 0; j < usuarioQuery.perfiles.length; j++) {
                if (listaPosicion.indexOf(usuarioQuery.perfiles[j].tipoPerfil) !== -1) {
                    const posicion = listaPosicion.indexOf(usuarioQuery.perfiles[j].tipoPerfil)
                    const getEstado = await this.catalogoEstadoService.obtenerEstadoByCodigoWithCodEntidad(usuarioQuery.perfiles[j].estado, entidadPerfil.codigo)
                    perfil = {
                        _id: usuarioQuery.perfiles[j]._id,
                        nombre: usuarioQuery.perfiles[j].nombre,
                        nombreContacto: usuarioQuery.perfiles[j].nombreContacto,
                        estado: {
                            codigo: getEstado.codigo,//usuarioQuery.perfiles[j].estado,
                            nombre: getEstado.nombre
                        }
                    }
                    listaResult[posicion].perfil = perfil
                }
            }

            // Con populate....guarda referencia de traduccion al catalogo tipo perfil
            // let lista1: any = []
            // let listaTraducciones: any = []
            // const listaPerfiles: any = await this.catalogoTipoPerfilModelo.find()
            // listaTraducciones = await this.traduccionTipoPerfilModelo.find()
            // for (let i = 0; i < listaPerfiles.length; i++) {
            //     lista1 = []
            //     for (let j = 0; j < listaTraducciones.length; j++) {
            //         if (listaTraducciones[j].codigoTipoPerfil === listaPerfiles[i].codigo) {
            //             lista1.push(listaTraducciones[j]._id)
            //         }
            //     }
            //     await this.catalogoTipoPerfilModelo.findOneAndUpdate({ _id: listaPerfiles[i]._id },{ $set: { "traducciones": lista1 } });
            // }

            /* if (usuarioQuery.idCatalogoEstado === "EST_1" || usuarioQuery.idCatalogoEstado === "EST_5") {
                const usuarioPerfil = await this.obtenerDatosUsuarioService.obtenerPerfilesUsuario(idUsuario, catalogoIdioma.codigo);
                return usuarioPerfil;
            } */
            // return true;
            return listaResult;
        } catch (error) {

            throw error;
        }

    }

    async obtenerPerfilesUsuario(idUsuario: string): Promise<any> {
        const perfil = await this.perfilModel.find({ usuario: idUsuario })
            .select('-fechaCreacion -fechaActualizacion')
        return perfil;
    }

    async obtenerDatosUsuarioWithPerfiles(idUsuario: string): Promise<any> {
        const usuario = await this.obtenerIdUsuarioService.obtenerPerfilesUsuarioSessionById(idUsuario);
        let dataUser: any = {
            _id: usuario._id,
            email: usuario.email,
            perfilGrupo: usuario.perfilGrupo,
            estado: {
                codigo: usuario.estado
            },
            idioma: {
                codigo: usuario.idioma
            }
        }
        const listaPerfiles = [];
        for (const perfil of usuario.perfiles) {
            // console.log("perfil", perfil);
            
            /* const getLocalidad = await this.localidadModel.findOne({ codigo: perfil.direcciones[0].localidad });
            const getPais: any = await this.paisModel.findOne({ codigo: getLocalidad.catalogoPais }).populate({
                path: 'traducciones',
                match: {
                    idioma: usuario.idioma
                }
            }).select('-localidades'); */

            const getPaisDireccion : any = await this.paisModel.findOne({ codigo: perfil.direcciones[0].pais }).populate({
                path: 'traducciones',
                match: {
                    idioma: usuario.idioma
                }
            }).select('-localidades');


            let data: any = {
                _id: perfil._id,
                nombre: perfil.nombre,
                nombreContacto: perfil.nombreContacto,
                tipoPerfil: {
                    codigo: perfil.tipoPerfil
                },
                estado: {
                    codigo: perfil.estado
                },
                direcciones: [
                    {
                        traducciones: perfil.direcciones[0]?.traducciones,
                        pais: {
                            traducciones: [
                                { nombre: getPaisDireccion?.traducciones[0]?.nombre, }
                            ],
                            codigo: getPaisDireccion?.codigo
                        }
                    }
                ]
            };
            listaPerfiles.push(data);
        }
        dataUser.perfiles = listaPerfiles;
        return dataUser;
    }

    async obtenerPerfilById(idPerfil: string): Promise<any> {
        const perfil = await this.perfilModel.findOne({ _id: idPerfil }).select('-fechaCreacion -fechaActualizacion')
            .populate({
                path: 'usuario',
                select: '-fechaCreacion -fechaActualizacion',
                populate: {
                    path: 'rolSistema',
                    populate: {
                        path: 'rolesEspecificos'
                    }

                }
            });
        return perfil;
    }

}