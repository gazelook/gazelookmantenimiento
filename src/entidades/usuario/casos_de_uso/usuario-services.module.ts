
import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { StorageModule } from '../../../drivers/amazon_s3/storage.module';
import { FirebaseModule } from '../../../drivers/firebase/firebase.module';
import { CatalogosServiceModule } from '../../catalogos/casos_de_uso/catalogos-services.module';
import { EmailServicesModule } from '../../emails/casos_de_uso/email-services.module';
import { HistoricoServiceModule } from '../../historico/casos_de_uso/historico-services.module';
import { ServicioEmailModule } from '../../servicio_email/servicio-email.module';
import { usuarioProviders } from '../drivers/usuario.provider';
import { ActualizarEmailResponsableService } from './actualizar-email-responsable.service';
import { ActualizarUsuarioService } from './actualizar-usuario.service';
import { DescargarInformacioService } from './descargar-informacion.service';
import { InformacioAplicacionService } from './informacion-aplicacion.service';
import { ObtenerIdUsuarioService } from './obtener-id-usuario.service';
import { ObtenerPerfilUsuarioService } from './obtener-perfil-usuario.service';
import { ObtenerUsuarioLoginService } from './obtener-usuario-login.service';
import { ObtenerUsuariosCriptoService } from './obtener-usuarios-cripto.service';
import { ObtenerTodosUsuariosService } from './obtener-usuarios.service';
import { RechazoCuentaResponsableService } from './rechazo-cuenta-responsable.service';
import { RecuperarContraseniaService } from './recuperar-contrasena.service';
import { ValidaRolUsuarioService } from './validar-rol-usuario.service';



@Module({
  imports: [
    DBModule,
    HttpModule,
    StorageModule,
    ServicioEmailModule,
    CatalogosServiceModule,
    HistoricoServiceModule,
    EmailServicesModule,
    FirebaseModule
  ],
  providers: [
    ...usuarioProviders,
    InformacioAplicacionService,
    DescargarInformacioService,
    ObtenerIdUsuarioService,
    ActualizarUsuarioService,
    RecuperarContraseniaService,
    ActualizarEmailResponsableService,
    RechazoCuentaResponsableService,
    ObtenerPerfilUsuarioService,
    ObtenerUsuarioLoginService,
    ValidaRolUsuarioService,
    ObtenerTodosUsuariosService,
    ObtenerUsuariosCriptoService
  ],
  exports: [
    InformacioAplicacionService,
    DescargarInformacioService,
    ObtenerIdUsuarioService,
    ActualizarUsuarioService,
    RecuperarContraseniaService,
    ActualizarEmailResponsableService,
    RechazoCuentaResponsableService,
    ObtenerPerfilUsuarioService,
    ObtenerUsuarioLoginService,
    ValidaRolUsuarioService,
    ObtenerTodosUsuariosService,
    ObtenerUsuariosCriptoService
  ],
  controllers: [

  ],
})
export class UsuarioServicesModule { }
