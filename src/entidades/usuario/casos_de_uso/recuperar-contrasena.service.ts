
import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { idiomas, codIdiomas, codigoCatalogosTipoEmail, nombreAcciones, nombreEntidades, estadosDispositivo } from 'src/shared/enum-sistema';
import { Usuario } from 'src/drivers/mongoose/interfaces/usuarios/usuario.interface';
import * as bcrypt from 'bcrypt';
import { ConfirmacionRecuperarContraseniaService } from '../../emails/casos_de_uso/crear-email-confirmacion-recuperar-contrasenia.service';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { Dispositivo } from '../../../drivers/mongoose/interfaces/dispositivo/dispositivo.interface';

const sw = require('stopword');
const mongoose = require('mongoose');

@Injectable()
export class RecuperarContraseniaService {

  constructor(
    @Inject('USUARIO_MODEL') private readonly usuarioModel: Model<Usuario>,
    @Inject('DISPOSTIVO_MODEL') private readonly dispositivoModel: Model<Dispositivo>,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private crearHistoricoService: CrearHistoricoService,
    private confirmacionRecuperarContraseniaService: ConfirmacionRecuperarContraseniaService,
    private catalogoIdiomasService: CatalogoIdiomasService
  ) {

  }
  getCodIdiomas = codIdiomas;
  getIdiomas = idiomas;

  //Metodo para actualizar contraseña
  async actualizarContrasenia(email, nuevaContrasenia) {

    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    await session.startTransaction();
    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      var BCRYPT_SALT_ROUNDS = 12;

      //Obtiene el codigo de la accion a realizarse
      const accion = await this.catalogoAccionService.obtenerNombreAccion(nombreAcciones.modificar)
      let getAccion = accion.codigo;

      //Obtiene la entidad usuarios
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(nombreEntidades.usuarios)
      let getCodEntUsuarios = entidad.codigo;

      //Encriptar contraseña con BCRYPT
      const hashedContrasena = await bcrypt.hash(
        nuevaContrasenia,
        BCRYPT_SALT_ROUNDS,
      );

      //Actualizo la nueva contraseña
      const updateUsuario = await this.usuarioModel.findOneAndUpdate({ email: email }, { $set: { contrasena: hashedContrasena, fechaActualizacion: new Date() } }, opts)

      const getUsuario = await this.usuarioModel.findOne({ email: email }).session(opts.session)

      const getIdioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigo(getUsuario.idioma)

      let datosUsuario = {
        _id: getUsuario._id,
        email: getUsuario.email,
        contrasena: getUsuario.contrasena,
        fechaActualizacion: getUsuario.fechaActualizacion
      }

      let newHistoricoActualizarPass: any = {
        datos: datosUsuario,//Datos de la actualizacion de contraseña de usuario
        usuario: getUsuario._id,
        accion: getAccion,
        entidad: getCodEntUsuarios
      }

      this.crearHistoricoService.crearHistoricoServer(newHistoricoActualizarPass);

      const newActualizacionPass = {
        emailDestinatario: email,
        codigo: codigoCatalogosTipoEmail.actualizacion_contrasenia,
        idioma: getIdioma.codNombre
      }
      // cerrar la sesion de todos los dispositivos
      await this.dispositivoModel.updateMany(
        {
          usuario: getUsuario._id,
          estado: estadosDispositivo.activo
        },
        { estado: estadosDispositivo.eliminado },
        opts
      );

      //Llama al metodo de enviar correo de aviso de cambio de contraseña
      // const getConfirRecContrasenia = await this.confirmacionRecuperarContraseniaService.confirmacionRecuperarContrasenia(newActualizacionPass, opts)

      //Confirma los cambios de la transaccion
      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();
      return true

    } catch (error) {
      await session.abortTransaction();
      await session.endSession();
      throw error
    }
  }

}