import { Injectable, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import { Usuario } from 'src/drivers/mongoose/interfaces/usuarios/usuario.interface';
import { CrearHistoricoService } from '../../historico/casos_de_uso/crear-historico.service';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import { nombreAcciones, nombrecatalogoEstados, nombreEntidades } from '../../../shared/enum-sistema';
import * as mongoose from 'mongoose';

@Injectable()
export class ActualizarUsuarioService {
  constructor(
    @Inject('USUARIO_MODEL') private readonly userModel: Model<Usuario>,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
  ) { }

  async actualizarDatosUsuario(usuario, dataActualizar, opts: any): Promise<any> {
    try {
      const userUpdate = await this.userModel.findByIdAndUpdate(
        usuario,
        dataActualizar,
        opts
      );
      return userUpdate;
    } catch (error) {
      console.error(error);
      throw error;
    }
  }

  async actualizarEstadoUsuarioValidacionCuenta(usuario, estado, opts: any): Promise<any> {
    try {
      const userUpdate = await this.userModel.updateOne(
        { _id: usuario },
        { estado: estado },
        opts
      );
      return userUpdate;
    } catch (error) {
      throw error;
    }
  }

  async agregarUsuarioTransaccion(idUsuario, idTransaccion, opts: any): Promise<any> {
    try {
      const userUpdate = await this.userModel.findOneAndUpdate(
        { _id: idUsuario },
        { $push: { transacciones: idTransaccion } },
        opts
      );
      return userUpdate;
    } catch (error) {
      console.error(error);
      throw error;
    }
  }
  async agregarUsuarioSuscripcion(idUsuario, idSuscripcion, opts: any): Promise<any> {
    try {
      const userUpdate = await this.userModel.findOneAndUpdate(
        { _id: idUsuario },
        { $push: { suscripciones: idSuscripcion } },
        opts
      );
      return userUpdate;
    } catch (error) {
      console.error(error);
      throw error;
    }
  }

  async actualizarTerminosYCondiciones(opts: any): Promise<any> {
    try {
      //Obtiene el codigo de la entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.usuarios,
      );
      const codEntidad = entidad.codigo;

      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        codEntidad,
      );

      const userUpdate = await this.userModel.updateMany(
        { estado: estado.codigo },
        { $set: { aceptoTerminosCondiciones: false } },
        opts
      );
      return userUpdate;
    } catch (error) {
      throw error;
    }
  }

  async actualizarEstadoUsuario(idUsuario, estado): Promise<any> {
    // Inicia proceso de transaccion
    const session = await mongoose.startSession()
    await session.startTransaction();

    try {
      const opts = { session };

      const usuario = await this.userModel.findOne(
        { _id: idUsuario }
      );

      if (usuario) {
        const userUpdate = await this.userModel.updateOne(
          { _id: idUsuario },
          { estado: estado, fechaActualizacion:new Date()},
          opts
        );
        // actualizar el nuevo pago
        await session.commitTransaction();
        session.endSession();
        return true
      } else {
        return false
      }

    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw error;
    }
  }
}
