import { Inject, Injectable } from '@nestjs/common';
import { appendFile, createWriteStream, mkdirSync, readFileSync, statSync, unlink } from 'fs';
import { Model } from 'mongoose';
import { resolve } from 'path';
import { CatalogoLocalidad } from 'src/drivers/mongoose/interfaces/catalogo_localidad/catalogo-localidad.interface';
import { CatalogoTipoVoto } from 'src/drivers/mongoose/interfaces/catalogo_tipo_voto/catalogo-tipo-voto.interface';
import { TraduccionCatalogoTipoProyecto } from 'src/drivers/mongoose/interfaces/traduccion_catalogo_tipo_proyecto/traduccion-catalogo-tipo-proyecto.interface';
import { TraduccionPensamiento } from 'src/drivers/mongoose/interfaces/traduccion_pensamiento/traduccion-pensamiento.interface';
import { TraduccionProyecto } from 'src/drivers/mongoose/interfaces/traduccion_proyecto/traduccion_proyecto.interface';
import { Usuario } from 'src/drivers/mongoose/interfaces/usuarios/usuario.interface';
import { VotoNoticia } from 'src/drivers/mongoose/interfaces/voto_noticia/voto-noticia.interface';
import { VotoProyecto } from 'src/drivers/mongoose/interfaces/voto_proyecto/voto-proyecto.interface';
import { v4 as uuidv4 } from 'uuid';
import { ConfigService } from '../../../config/config.service';
import { StorageInformacionCuentaService } from '../../../drivers/amazon_s3/services/storage-informacion-cuenta.service';
import { StorageService } from '../../../drivers/amazon_s3/services/storage.service';
import { FirebaseMessageService } from '../../../drivers/firebase/services/firebase-mensajes.service';
import { FirebaseProyectosService } from '../../../drivers/firebase/services/firebase-proyectos.service';
import { Album } from '../../../drivers/mongoose/interfaces/album/album.interface';
import { Archivo } from '../../../drivers/mongoose/interfaces/archivo/archivo.interface';
import { Asociacion } from '../../../drivers/mongoose/interfaces/asociacion/asociacion.interface';
import { TipoPerfil } from '../../../drivers/mongoose/interfaces/catalogoTipoPerfil/catalogo-tipo-perfil.interface';
import { CatalogoAlbum } from '../../../drivers/mongoose/interfaces/catalogo_album/catalogo-album.interface';
import { CatalogoIdiomas } from '../../../drivers/mongoose/interfaces/catalogo_idiomas/catalogo-idiomas.interface';
import { Media } from '../../../drivers/mongoose/interfaces/media/media.interface';
import { Noticia } from '../../../drivers/mongoose/interfaces/noticia/noticia.interface';
import { Participante } from '../../../drivers/mongoose/interfaces/participante_asociacion/participante.interface';
import { ParticipanteProyecto } from '../../../drivers/mongoose/interfaces/participante_proyecto/participante_proyecto.interface';
import { Pensamiento } from '../../../drivers/mongoose/interfaces/pensamiento/pensamiento.interface';
import { Perfil } from '../../../drivers/mongoose/interfaces/perfil/perfil.interface';
import { Proyecto } from '../../../drivers/mongoose/interfaces/proyectos/proyecto.interface';
import { TraducionTipoPerfil } from '../../../drivers/mongoose/interfaces/traduccion_catalogo_tipo_perfil/traduccion-catalogo-tipo-perfil.interface';
import { TraduccionesNoticias } from '../../../drivers/mongoose/interfaces/traduccion_noticia/traduccion-noticia.interface';
import { codigoEstadosComentario, codigosEstadoAlbum, codigosEstadosPartAsociacion, estadoActivoEntidades, estadosPerfil, servidor } from '../../../shared/enum-sistema';
import { CrearEmailEnvioDatosService } from '../../emails/casos_de_uso/crear-email-envio-datos.service';
const archiver = require('archiver');


@Injectable()
export class InformacioAplicacionService {

  constructor(@Inject('USUARIO_MODEL') private readonly userModel: Model<Usuario>,
    @Inject('PROYECTO_MODEL') private readonly proyectosModel: Model<Proyecto>,
    @Inject('TRADUCCION_PROYECTO_MODEL') private readonly traduccionProyectosModel: Model<TraduccionProyecto>,
    @Inject('TRADUCCION_CATALOGO_TIPO_PROYECTO_MODEL') private readonly traduccionTipoProyectosModel: Model<TraduccionCatalogoTipoProyecto>,
    @Inject('PARTICIPANTE_PROYECTO_MODEL') private readonly participanteProyectoModel: Model<ParticipanteProyecto>,
    @Inject('PENSAMIENTO_MODEL') private readonly pensamientoModel: Model<Pensamiento>,
    @Inject('NOTICIA_MODEL') private readonly noticiaModel: Model<Noticia>,
    @Inject('TRADUCCION_NOTICIA_MODEL') private readonly traduccionNoticiaModel: Model<TraduccionesNoticias>,
    @Inject('ALBUM_MODEL') private readonly albumModel: Model<Album>,
    @Inject('ARCHIVO_MODEL') private readonly archivoModel: Model<Archivo>,
    @Inject('ASOCIACION_MODEL') private readonly asociacionModel: Model<Asociacion>,
    @Inject('MEDIA_MODEL') private readonly mediaModel: Model<Media>,
    @Inject('PERFIL_MODEL') private readonly perfilModel: Model<Perfil>,
    @Inject('TIPO_PERFIL_MODEL') private readonly tipoPerfilModel: Model<TipoPerfil>,
    @Inject('TRADUCCION_TIPO_PERFIL_MODEL') private readonly traduccionTipoPerfilModel: Model<TraducionTipoPerfil>,
    @Inject('CATALOGO_IDIOMAS') private readonly catalogoIdiomasModel: Model<CatalogoIdiomas>,
    @Inject('CATALOGO_ALBUM_MODEL') private readonly catalogoAlbumModel: Model<CatalogoAlbum>,
    @Inject('CATALOGO_LOCALIDAD_MODEL') private readonly catalogoLocalidadModel: Model<CatalogoLocalidad>,
    @Inject('VOTO_PROYECTO_MODEL') private readonly votoProyectoModel: Model<VotoProyecto>,
    @Inject('CATALOGO_TIPO_VOTO_MODEL') private readonly catalogoTipoVotoModel: Model<CatalogoTipoVoto>,
    @Inject('TRADUCCION_PENSAMIENTO_MODEL') private readonly traduccionPensamientoModel: Model<TraduccionPensamiento>,
    @Inject('VOTO_NOTICIA_MODEL') private readonly votoNoticiaModel: Model<VotoNoticia>,
    @Inject('PARTI_ASOC_MODEL') private readonly participanteAsoModel: Model<Participante>,
    private firebaseMessageService: FirebaseMessageService,
    private storageService: StorageService,
    private config: ConfigService,
    private crearEmailEnvioDatosService: CrearEmailEnvioDatosService,
    private storageInformacionCuentaService: StorageInformacionCuentaService,
    private firebaseProyectosService: FirebaseProyectosService
  ) { }

  async solicitarInformacionUsuario(idUsuario: string): Promise<any> {

    

    let perfiles: any = [];


    let archivosDescargarAlbumPerfil: any = [];
    let archivosDescargarAlbumProyecto: any = [];
    let archivosDescargarMediasProyecto: any = [];
    let archivosDescargarAlbumNoticia: any = [];
    let archivosDescargarMediasNoticia: any = [];
    let archivosDescargarMediasMensaje: any = [];
    let medias: any = [];
    let mediasAlbumProyecto: any = [];
    let mediasProyecto: any = [];
    let mediasAlbumNoticia: any = [];
    let mediasNoticia: any = [];
    let direcciones: any = [];
    let contactosPerfil: any = [];
    let telefonos: any = [];
    let mensajes: any = [];
    let proyectos: any = [];
    let pensamientos: any = [];
    let noticias: any = [];
    let asociaciones: any = [];


    const existeUsuario: any = await this.userModel.findOne({ _id: idUsuario });


    try {
      if (!existeUsuario) {
        throw { message: "Usuario no registrado" };
      }
      const catalogoIdioma = await this.catalogoIdiomasModel.findOne({ codigo: existeUsuario.idioma })

      const user: any = await this.getUsuario(idUsuario, catalogoIdioma.codigo)

      //_____________direcciones___________
      if (user.perfiles.direcciones) {
        user.perfiles.direcciones.forEach(element => {
          direcciones.push(element);
        });
      }
    
      console.log('USUARIOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO: ',idUsuario )
      //__________________________iterar perfiles__________________________________
      for (const perfil of user.perfiles) {
        //_____________perfil___________
        const dataPerfil: any = {}
        let albunes: any = [];
        const traduccionTipoPerfil = await this.traduccionTipoPerfilModel.findOne({ $and: [{ codigoTipoPerfil: perfil.tipoPerfil }, { idioma: catalogoIdioma.codigo }] });
        dataPerfil.nombre = perfil.nombre;
        dataPerfil.nombreContacto = perfil.nombreContacto;
        dataPerfil.tipoPerfil = traduccionTipoPerfil.nombre;

        //________________Contactos_____________

        const getContactosPerfil: any = await this.getContactos(perfil._id);
        contactosPerfil = [];
        let listMensajesPerfil = [];
        let getParticipantes;
        for (const contacto of getContactosPerfil) {

          getParticipantes = await this.getParticipantesAsociacion(contacto.asociacion._id);



          const dataContactos = {
            perfil: {
              nombre: contacto.perfil.nombre,
              nombreContacto: contacto.perfil.nombreContacto,
              tipoPerfil: contacto.perfil.tipoPerfil
            }
          };
          contactosPerfil.push(dataContactos);

          //________________Mensajes Asociacion_____________
          const getMensajesPerfil = await this.firebaseMessageService.obtenerMensajesAsociacion(contacto.asociacion._id.toString());

          const listMensajesConv = [];
          for (const element of getMensajesPerfil) {

            let mensajesAsociacion: any = {
              contenido: element.contenido,
              propietario: element.propietario.perfil.nombreContacto,
            };
            let listAdjuntos = [];
            // --------- iterar medias mensajes----------
            if (element.adjuntos) {
              for (const adjunto of element.adjuntos) {
                const getArchivo = await this.archivoModel.findById(adjunto.principal.id);
                const dataAdjuntos = {
                  path: adjunto.principal.path
                };
                const dataAdjutosMensajedescargar = {
                  url: getArchivo.url,
                  filename: getArchivo.filename,
                  filenameStorage: getArchivo.filenameStorage,
                }
                listAdjuntos.push(dataAdjuntos);
                archivosDescargarMediasMensaje.push(dataAdjutosMensajedescargar);
              }
            }

            mensajesAsociacion.archivos = listAdjuntos;

            listMensajesConv.push(mensajesAsociacion);

          }
          const dataConversacion = {
            participantes: getParticipantes,
            mensajes: listMensajesConv
          }

          listMensajesPerfil.push(dataConversacion);

        }
        dataPerfil.contactos = contactosPerfil;

        //---------------Data Mensajes Perfil-----------------
        const dataMensajesPerfil = {
          perfil: dataPerfil.tipoPerfil,
          conversaciones: listMensajesPerfil
        }
        mensajes.push(dataMensajesPerfil);


        //________________album Perfil________________
        if (perfil.album) {
          //__________________________ iterar albunes__________________________________
          perfil.album.forEach(async element => {
            const album: any = await this.getAlbum(element._id, catalogoIdioma.codigo);
            const dataAlbum: any = {}
            let dataDescargarArchivo: any = {}
            const catalogoAlbum = await this.catalogoAlbumModel.findOne({ codigo: album.tipo });
            dataAlbum.tipo = catalogoAlbum.nombre;
            //dataAlbum.perfil = dataPerfil.tipoPerfil;
            //perfiles.push(dataPerfil);
            // si hay portada
            if (album.portada) {
              // data para el album json
              dataAlbum.portada = {
                path: album.portada.principal.path
              };
              // data para descargar el archivo
              dataDescargarArchivo = {}
              dataDescargarArchivo.url = album.portada.principal.url;
              dataDescargarArchivo.filename = album.portada.principal.filename;
              dataDescargarArchivo.filenameStorage = album.portada.principal.filenameStorage;
              archivosDescargarAlbumPerfil.push(dataDescargarArchivo);
            }
            // si hay media
            if (album.media) {
              album.media.forEach(element => {
                if (element.principal) {
                  // data para descargar el archivo
                  dataDescargarArchivo = {}
                  dataDescargarArchivo.url = element.principal.url;
                  dataDescargarArchivo.filename = element.principal.filename;
                  dataDescargarArchivo.filenameStorage = element.principal.filenameStorage;
                  archivosDescargarAlbumPerfil.push(dataDescargarArchivo);
                  // data para el album json
                  const mediaAlbum = {
                    path: element.principal.path
                  }
                  medias.push(mediaAlbum);
                }
              });
            }
            if (medias) {
              dataAlbum.archivos = medias;
              medias = [];
            }

            albunes.push(dataAlbum);

          });
          dataPerfil.album = albunes
        }
        perfiles.push(dataPerfil);
        //_____________proyectos___________
        const proyecto = await this.proyectosModel.find({ perfil: perfil._id });
        //__________________________iterar proyectos__________________________________
        if (proyecto.length > 0) {

          for (const getProyecto of proyecto) {
            const dataProyecto: any = {}
            let albunesProyecto: any = [];
            let arrayMediasProyecto: any = [];
            let arrayParticipantes: any = [];
            let arrayVotoProyecto: any = [];
            let arrayComentarios = [];
            const getTraduccionProyecto = await this.traduccionProyectosModel.findOne({ $and: [{ proyecto: getProyecto._id }, { original: true }] });
            dataProyecto.titulo = getTraduccionProyecto.titulo;
            dataProyecto.tituloCorto = getTraduccionProyecto.tituloCorto;
            dataProyecto.descripcion = getTraduccionProyecto.descripcion;
            dataProyecto.perfil = perfil.nombre;
            dataProyecto.estrategia = getProyecto.estrategia
            //Total votos del proyecto
            dataProyecto.totalVotos = getProyecto.totalVotos
            //Obtener localidad del proyecto
            const getLocalidad = await this.catalogoLocalidadModel.findOne({ codigo: getProyecto.localidad })
            dataProyecto.localidad = getLocalidad?.nombre || "n/a"
            //busca la traduccion del tipo de proyecto
            const traduccionTipoProyecto = await this.traduccionTipoProyectosModel.findOne({ $and: [{ referencia: getProyecto.tipo }, { idioma: catalogoIdioma.codigo }] });

            dataProyecto.tipoProyecto = traduccionTipoProyecto.nombre;

            //Participantes del proyecto
            let coautor;
            const getParticipantes = await this.getParticipanteProyecto(getProyecto._id)
            if (getParticipantes.length > 0) {
              for (const participante of getParticipantes) {
                console.log("participante proyecto: ", participante);

                let arrayRolesProyecto: any = [];
                if (participante.coautor) {
                  coautor = {
                    nombre: participante.coautor.nombre,
                    nombreContacto: participante.coautor.nombreContacto
                  }
                  for (const getRoles of participante.roles) {
                    let roles = {
                      nombre: getRoles.nombre
                    }
                    arrayRolesProyecto.push(roles)
                  }
                  let objectParticipantes = {
                    coautor: coautor,
                    roles: arrayRolesProyecto
                  }
                  arrayParticipantes.push(objectParticipantes)
                }
              }
            }
            dataProyecto.participantes = arrayParticipantes

            // Comentarios del proyecto
            const listaComentarios = await this.firebaseProyectosService.obtenerComentariosProyecto(getProyecto._id.toString());
            if (listaComentarios.length > 0) {
              for (const comentario of listaComentarios) {

                if (comentario.estado.codigo === codigoEstadosComentario.activa) {
                  const traduccionOriginal = comentario.traducciones.find(element => element.original === true);
                  const dataComentarios = {
                    coautor: comentario.coautor.coautor.nombre,
                    texto: traduccionOriginal.texto
                  }
                  arrayComentarios.push(dataComentarios)
                }
              }
            }
            dataProyecto.comentarios = arrayComentarios;

            //Votos del proyecto
            const getVotosProyecto = await this.getVotosProyecto(getProyecto._id, catalogoIdioma.codigo)
            if (getVotosProyecto.length > 0) {
              for (const votos of getVotosProyecto) {
                let arrayTraduccionesVotoProyecto: any = [];
                let perfil = {
                  nombre: votos.perfil.nombre,
                  nombreContacto: votos.perfil.nombreContacto
                }
                for (const getTraducciones of votos.traducciones) {

                  arrayTraduccionesVotoProyecto.push(getTraducciones.descripcion)
                }
                //const getTipoVoto = await this.catalogoTipoVotoModel.findOne({codigo:votos.tipo})
                let objectVotosProyecto = {
                  perfil: perfil,
                  //tipo:getTipoVoto.nombre,
                  numeroVoto: votos.numeroVoto,
                  descripcion: arrayTraduccionesVotoProyecto
                }
                arrayVotoProyecto.push(objectVotosProyecto)
              }
            }
            dataProyecto.votos = arrayVotoProyecto
            if (getProyecto.adjuntos) {
              //__________________________ iterar albunes (adjuntos)__________________________________
              getProyecto.adjuntos.forEach(async element => {
                const albumProyecto: any = await this.getAlbum(element['_id'], catalogoIdioma.codigo);
                const dataAlbumProyecto: any = {}
                let dataDescargarArchivoProyecto: any = {}
                const catalogoAlbum = await this.catalogoAlbumModel.findOne({ codigo: albumProyecto.tipo });
                dataAlbumProyecto.tipoAlbum = catalogoAlbum.nombre;
                // si hay portada
                if (albumProyecto.portada) {
                  // data para el album json
                  dataAlbumProyecto.portada = {
                    path: albumProyecto.portada.principal.path
                  };
                  // data para descargar el archivo
                  dataDescargarArchivoProyecto = {}
                  dataDescargarArchivoProyecto.url = albumProyecto.portada.principal.url;
                  dataDescargarArchivoProyecto.filename = albumProyecto.portada.principal.filename;
                  dataDescargarArchivoProyecto.filenameStorage = albumProyecto.portada.principal.filenameStorage;
                  archivosDescargarAlbumProyecto.push(dataDescargarArchivoProyecto);
                }
                // si hay media
                if (albumProyecto.media) {
                  albumProyecto.media.forEach(element => {
                    if (element.principal) {
                      // data para descargar el archivo
                      dataDescargarArchivoProyecto = {}
                      dataDescargarArchivoProyecto.url = element.principal.url;
                      dataDescargarArchivoProyecto.filename = element.principal.filename;
                      dataDescargarArchivoProyecto.filenameStorage = element.principal.filenameStorage;
                      archivosDescargarAlbumProyecto.push(dataDescargarArchivoProyecto);
                      // data para el album json
                      const mediaAlbum = {
                        path: element.principal.path
                      }
                      mediasAlbumProyecto.push(mediaAlbum);
                    }
                  });
                }
                if (mediasAlbumProyecto) {
                  dataAlbumProyecto.archivos = mediasAlbumProyecto;
                  mediasAlbumProyecto = [];
                }
                albunesProyecto.push(dataAlbumProyecto);
              });
              dataProyecto.adjuntos = albunesProyecto
              //proyectos.push(dataProyecto);
            }
            //________________medias________________
            if (getProyecto.medias) {
              //__________________________ iterar medias__________________________________
              getProyecto.medias.forEach(async element => {
                const getMediaProyecto: any = await this.getMedia(element['_id'], catalogoIdioma.codigo);
                const dataMediasProyecto: any = {}
                let dataDescargarArchivoMediaProyecto: any = {}

                // si hay media
                if (getMediaProyecto) {
                  if (getMediaProyecto.principal) {
                    // data para descargar el archivo
                    dataDescargarArchivoMediaProyecto = {}
                    dataDescargarArchivoMediaProyecto.url = getMediaProyecto.principal.url;
                    dataDescargarArchivoMediaProyecto.filename = getMediaProyecto.principal.filename;
                    dataDescargarArchivoMediaProyecto.filenameStorage = getMediaProyecto.principal.filenameStorage;
                    archivosDescargarMediasProyecto.push(dataDescargarArchivoMediaProyecto);
                    // data para el album json
                    const mediaAlbumProyecto = {
                      path: getMediaProyecto.principal.path
                    }
                    mediasProyecto.push(mediaAlbumProyecto);
                  }
                }
                if (mediasProyecto) {
                  dataMediasProyecto.archivos = mediasProyecto;
                  mediasProyecto = [];
                }
                arrayMediasProyecto.push(dataMediasProyecto);
              });
              dataProyecto.medias = arrayMediasProyecto

            }
            proyectos.push(dataProyecto);
          }
        }
        //_____________pensamientos___________
        const pensamiento = await this.pensamientoModel.find({ perfil: perfil._id, estado: estadoActivoEntidades.pensamientos });
        //__________________________iterar proyectos__________________________________
        if (pensamiento.length > 0) {

          for (const getPensamientos of pensamiento) {
            const dataPensamiento: any = {}
            const getTraduccionProyecto = await this.traduccionPensamientoModel.findOne({ $and: [{ pensamiento: getPensamientos._id }, { estado: estadoActivoEntidades.traduccionPensamientos }, { original: true }] });
            dataPensamiento.perfil = perfil.nombre;
            dataPensamiento.pensamiento = getTraduccionProyecto.texto;
            dataPensamiento.fechaCreacion = getPensamientos
            pensamientos.push(dataPensamiento);
          }
        }

        //_____________noticias___________
        const noticia = await this.noticiaModel.find({ perfil: perfil._id });
        //__________________________iterar proyectos__________________________________
        if (noticia.length > 0) {

          for (const getNoticia of noticia) {
            const dataNoticia: any = {}
            let albunesNoticias: any = [];
            let arrayMediasNoticias: any = [];
            let arrayParticipantes: any = [];
            let arrayVotoProyecto: any = [];
            const getTraduccionNoticia = await this.traduccionNoticiaModel.findOne({ $and: [{ referencia: getNoticia._id }, { estado: estadoActivoEntidades.traduccionNoticias }, { original: true }] });
            if (getTraduccionNoticia) {
              dataNoticia.titulo = getTraduccionNoticia.titulo;
              dataNoticia.tituloCorto = getTraduccionNoticia.tituloCorto;
              dataNoticia.descripcion = getTraduccionNoticia.descripcion;
            }
            dataNoticia.perfil = perfil.nombre;
            //Total votos de la noticia
            dataNoticia.totalVotos = getNoticia.totalVotos
            //Obtener localidad del noticia
            const getLocalidad = await this.catalogoLocalidadModel.findOne({ codigo: getNoticia.localidad })
            dataNoticia.localidad = getLocalidad?.nombre || "n/a"

            //Votos del proyecto
            const getVotosNoticia = await this.getVotosNoticia(getNoticia._id, catalogoIdioma.codigo)
            if (getVotosNoticia.length > 0) {
              for (const votos of getVotosNoticia) {
                let arrayTraduccionesVotoProyecto: any = [];
                let perfil = {
                  nombre: votos.perfil.nombre,
                  nombreContacto: votos.perfil.nombreContacto
                }
                for (const getTraducciones of votos.traducciones) {

                  arrayTraduccionesVotoProyecto.push(getTraducciones.descripcion)
                }

                let objectVotosProyecto = {
                  perfil: perfil,
                  //tipo:getTipoVoto.nombre,
                  numeroVoto: votos.numeroVoto,
                  descripcion: arrayTraduccionesVotoProyecto
                }
                arrayVotoProyecto.push(objectVotosProyecto)
              }
            }
            dataNoticia.votos = arrayVotoProyecto
            if (getNoticia.adjuntos) {
              //__________________________ iterar albunes (adjuntos)__________________________________
              getNoticia.adjuntos.forEach(async element => {
                const albumNoticia: any = await this.getAlbum(element['_id'], catalogoIdioma.codigo);
                const dataAlbumNoticia: any = {}
                let dataDescargarArchivoNoticia: any = {}
                const catalogoAlbum = await this.catalogoAlbumModel.findOne({ codigo: albumNoticia.tipo });
                dataAlbumNoticia.tipoAlbum = catalogoAlbum.nombre;
                // si hay portada
                if (albumNoticia.portada) {
                  // data para el album json
                  dataAlbumNoticia.portada = {
                    path: albumNoticia.portada.principal.path
                  };
                  // data para descargar el archivo
                  dataDescargarArchivoNoticia = {}
                  dataDescargarArchivoNoticia.url = albumNoticia.portada.principal.url;
                  dataDescargarArchivoNoticia.filename = albumNoticia.portada.principal.filename;
                  dataDescargarArchivoNoticia.filenameStorage = albumNoticia.portada.principal.filenameStorage;
                  archivosDescargarAlbumNoticia.push(dataDescargarArchivoNoticia);
                }
                // si hay media
                if (albumNoticia.media) {
                  albumNoticia.media.forEach(element => {
                    if (element.principal) {
                      // data para descargar el archivo
                      dataDescargarArchivoNoticia = {}
                      dataDescargarArchivoNoticia.url = element.principal.url;
                      dataDescargarArchivoNoticia.filename = element.principal.filename;
                      dataDescargarArchivoNoticia.filenameStorage = element.principal.filenameStorage;
                      archivosDescargarAlbumNoticia.push(dataDescargarArchivoNoticia);
                      // data para el album json
                      const mediaAlbum = {
                        path: element.principal.path
                      }
                      mediasAlbumNoticia.push(mediaAlbum);
                    }
                  });
                }
                if (mediasAlbumNoticia) {
                  dataAlbumNoticia.archivos = mediasAlbumNoticia;
                  mediasAlbumNoticia = [];
                }
                albunesNoticias.push(dataAlbumNoticia);
              });
              dataNoticia.adjuntos = albunesNoticias

            }
            //________________medias________________
            if (getNoticia.medias) {
              //__________________________ iterar medias__________________________________
              getNoticia.medias.forEach(async element => {
                const getMediaNoticia: any = await this.getMedia(element['_id'], catalogoIdioma.codigo);
                const dataMediasNoticia: any = {}
                let dataDescargarArchivoMediaNoticia: any = {}

                // si hay media
                if (getMediaNoticia) {
                  if (getMediaNoticia.principal) {
                    // data para descargar el archivo
                    dataDescargarArchivoMediaNoticia = {}
                    dataDescargarArchivoMediaNoticia.url = getMediaNoticia.principal.url;
                    dataDescargarArchivoMediaNoticia.filename = getMediaNoticia.principal.filename;
                    dataDescargarArchivoMediaNoticia.filenameStorage = getMediaNoticia.principal.filenameStorage;
                    archivosDescargarMediasNoticia.push(dataDescargarArchivoMediaNoticia);
                    // data para el album json
                    const mediaAlbumNoticia = {
                      path: getMediaNoticia.principal.path
                    }
                    mediasNoticia.push(mediaAlbumNoticia);
                  }
                }
                if (mediasNoticia) {
                  dataMediasNoticia.archivos = mediasNoticia;
                  mediasNoticia = [];
                }
                arrayMediasNoticias.push(dataMediasNoticia);
              });
              dataNoticia.medias = arrayMediasNoticias

            }
            noticias.push(dataNoticia);
          }
        }
        //_____________asociaciones___________
        const asociacion = await this.asociacionModel.find({ perfil: perfil._id });
        asociaciones.push(asociacion)
      }



      //_______________________________generar archivos________________________________
      const folderInformacion = resolve(__dirname, `../../temp_files/${user.email}`);
      const folderAlbumArchivosPerfil = resolve(__dirname, `../../temp_files/${user.email}/albums-perfil`);
      const folderAlbumArchivosProyecto = resolve(__dirname, `../../temp_files/${user.email}/adjuntos-proyecto`);
      const folderMediasArchivosProyecto = resolve(__dirname, `../../temp_files/${user.email}/medias-proyecto`);

      const folderAlbumArchivosNoticia = resolve(__dirname, `../../temp_files/${user.email}/adjuntos-noticia`);
      const folderMediasArchivosNoticia = resolve(__dirname, `../../temp_files/${user.email}/medias-noticia`);

      const folderMediasArchivosMensajes = resolve(__dirname, `../../temp_files/${user.email}/medias-mensajes`);

      const extensionJson = 'json';
      // crear carpetas
      mkdirSync(`${folderInformacion}`, { recursive: true }); // carpeta principal
      mkdirSync(`${folderAlbumArchivosPerfil}`, { recursive: true }); // carpeta de archivos del album de perfil
      if (proyectos.length > 0) {
        mkdirSync(`${folderAlbumArchivosProyecto}`, { recursive: true }); // carpeta de archivos del album de proyectos
        mkdirSync(`${folderMediasArchivosProyecto}`, { recursive: true }); // carpeta de archivos de medias de proyectos
      }
      if (noticias.length > 0) {
        mkdirSync(`${folderAlbumArchivosNoticia}`, { recursive: true }); // carpeta de archivos del album de noticias
        mkdirSync(`${folderMediasArchivosNoticia}`, { recursive: true }); // carpeta de archivos de medias de noticias
      }
      if (archivosDescargarMediasMensaje.length > 0) {
        mkdirSync(`${folderMediasArchivosMensajes}`, { recursive: true }); // carpeta de archivos de medias de mensajes
      }
      // archivo perfiles
      const nameArchivoPerfiles = `perfiles-${uuidv4().toString()}.${extensionJson}`;
      const archivoPerfiles = resolve(__dirname, `../../temp_files/${user.email}/${nameArchivoPerfiles}`);
      await this.crearArchivo(archivoPerfiles, JSON.stringify(perfiles));

      // archivo Albunes perfil
      // const nameArchivoAlbunes = `albunes-${uuidv4().toString()}.${extensionJson}`;
      // const archivoAlbunes = resolve(__dirname, `../../../temp_files/${user.email}/${nameArchivoAlbunes}`);
      // await this.crearArchivo(archivoAlbunes, JSON.stringify(albunes));

      //  proyectos
      if (proyectos.length > 0) {
        const nameArchivoProyectos = `proyectos-${uuidv4().toString()}.${extensionJson}`;
        const pathProyectos = resolve(__dirname, `../../temp_files/${user.email}/${nameArchivoProyectos}`);
        await this.crearArchivo(pathProyectos, JSON.stringify(proyectos));
      }
      
      //  mensajes
      if (mensajes.length > 0) {
        const nameArchivoMensajes = `mensajes-${uuidv4().toString()}.${extensionJson}`;
        const pathMensajes = resolve(__dirname, `../../temp_files/${user.email}/${nameArchivoMensajes}`);
        await this.crearArchivo(pathMensajes, JSON.stringify(mensajes));
      }

      //  noticias
      if (noticias.length > 0) {
        const nameArchivoNoticias = `noticias-${uuidv4().toString()}.${extensionJson}`;
        const pathNoticias = resolve(__dirname, `../../temp_files/${user.email}/${nameArchivoNoticias}`);
        await this.crearArchivo(pathNoticias, JSON.stringify(noticias));
      }

      //  pensamientos
      if (pensamientos.length > 0) {
        const nameArchivoPensamientos = `pensamientos-${uuidv4().toString()}.${extensionJson}`;
        const pathPensamientos = resolve(__dirname, `../../temp_files/${user.email}/${nameArchivoPensamientos}`);
        await this.crearArchivo(pathPensamientos, JSON.stringify(pensamientos));
      }
      // descargar Archivos del Album de Perfil
      if (archivosDescargarAlbumPerfil.length > 0) {
        for (const archivo of archivosDescargarAlbumPerfil) {
          if (archivo.filenameStorage) {
            const archivoMediaAlbumPerfil = resolve(__dirname, `../../temp_files/${user.email}/albums-perfil/${archivo.filenameStorage}`);
            const fileStorage = await this.storageService.getArchivo(archivo.filenameStorage);
            await this.crearArchivo(archivoMediaAlbumPerfil, fileStorage.Body);
          }
        }
      }

      // descargar Archivos del Album de Proyecto
      if (archivosDescargarAlbumProyecto.length > 0) {
        for (const archivo of archivosDescargarAlbumProyecto) {
          if (archivo.filenameStorage) {
            const archivoMediaAlbumProyecto = resolve(__dirname, `../../temp_files/${user.email}/adjuntos-proyecto/${archivo.filenameStorage}`);
            const fileStorage = await this.storageService.getArchivo(archivo.filenameStorage);
            await this.crearArchivo(archivoMediaAlbumProyecto, fileStorage.Body);
          }
        }
      }

      // descargar Archivos de las medias del Proyecto
      if (archivosDescargarMediasProyecto.length > 0) {
        for (const media of archivosDescargarMediasProyecto) {
          if (media.filenameStorage) {
            const archivoMediaAlbumProyecto = resolve(__dirname, `../../temp_files/${user.email}/medias-proyecto/${media.filenameStorage}`);
            const fileStorage = await this.storageService.getArchivo(media.filenameStorage);
            await this.crearArchivo(archivoMediaAlbumProyecto, fileStorage.Body);
          }
        }
      }

      // descargar Archivos del Album de Noticia
      if (archivosDescargarAlbumNoticia.length > 0) {
        for (const archivo of archivosDescargarAlbumNoticia) {
          if (archivo.filenameStorage) {
            const archivoMediaAlbumNoticia = resolve(__dirname, `../../temp_files/${user.email}/adjuntos-noticia/${archivo.filenameStorage}`);
            const fileStorage = await this.storageService.getArchivo(archivo.filenameStorage);
            await this.crearArchivo(archivoMediaAlbumNoticia, fileStorage.Body);
          }
        }
      }
      // descargar Archivos de las medias de Noticia
      if (archivosDescargarMediasNoticia.length > 0) {
        for (const media of archivosDescargarMediasNoticia) {
          if (media.filenameStorage) {
            const archivoMediaAlbumNoticia = resolve(__dirname, `../../temp_files/${user.email}/medias-noticia/${media.filenameStorage}`);
            const fileStorage = await this.storageService.getArchivo(media.filenameStorage);
            await this.crearArchivo(archivoMediaAlbumNoticia, fileStorage.Body);
          }
        }
      }
      if (archivosDescargarMediasMensaje.length > 0) {
        for (const media of archivosDescargarMediasMensaje) {
          if (media.filenameStorage) {
            const archivoMediaAlbumNoticia = resolve(__dirname, `../../temp_files/${user.email}/medias-mensajes/${media.filenameStorage}`);
            const fileStorage = await this.storageService.getArchivo(media.filenameStorage);
            await this.crearArchivo(archivoMediaAlbumNoticia, fileStorage.Body);
          }
        }
      }

      // comprimir archivos
      let date = new Date();
      const filenameZip = `${user.email}-${date.getTime()}.zip`;
      const zip = resolve(__dirname, `../../temp_files/${filenameZip}`);
      await this.comprimirArchivo(folderInformacion, zip);

      const stats = statSync(zip)
      const readFileZip = readFileSync(zip);

      const dataFileUpload = {
        buffer: readFileZip,
        size: stats.size,
        originalname: filenameZip,
        userEmail: user.email
      }

      // subir archivo al storage
      const archivoStorage = await this.storageInformacionCuentaService.cargarArchivo(dataFileUpload);

      console.log('eliminarDirectorio')
      // eliminar la carpeta generada y el zip
      this.eliminarDirectorio(folderInformacion);
      this.eliminarArchivo(zip);

      // ________________________________respuesta_____________________________________
      //url de descarga
      const HOST_REMOTO = this.config.get('HOST_REMOTO');
      const result = {
        url: `${HOST_REMOTO}${servidor.path_descargar_informacion}/${filenameZip}`
      }
      const dataEmail = {
        emailDestinatario: user.email,
        nombreArchivo: archivoStorage.filename,
        nombre: user.perfiles[0].nombre,
        idioma: catalogoIdioma.codNombre
      }
      if (user.menorEdad) {
        console.log("Menor de edad");
        dataEmail.emailDestinatario = user.emailResponsable;
      }
      // __________________ENVIAR CORREO con los datos de la cuenta de usuario________________________
      const envioEmail = await this.crearEmailEnvioDatosService.envioDatos(dataEmail);

      return result;
    } catch (error) {
      throw error;
    }

  }

  //______________________________________________________________FIN_______________________________________
  async getContactos(idPerfil: string) {
    const contacts = await this.participanteAsoModel.find({
      $and: [
        { contactoDe: idPerfil },
        { estado: codigosEstadosPartAsociacion.contacto },
      ]
    }).populate([
      {
        path: 'asociacion',
        select: 'nombre tipo estado'
      },
      {
        path: 'perfil',
        select: 'nombre nombreContacto tipoPerfil album estado',
      },
      {
        path: 'invitadoPor',
        select: '_id estado perfil',
      }
    ]);

    return contacts;
  }

  async getParticipantesAsociacion(idAsociacion: string) {
    let participantes;
    const getAsociacion: any = await this.asociacionModel.findById(idAsociacion);
    for (const participante of getAsociacion.participantes) {
      const getParticipante: any = await this.participanteAsoModel.findById(participante).populate([
        {
          path: 'perfil',
          select: 'nombre nombreContacto tipoPerfil album estado',
        },
      ]);

      participantes = participantes ? getParticipante.perfil.nombreContacto + "___" + participantes : getParticipante.perfil.nombreContacto;
    }

    return participantes;
  }

  async getAlbum(idAlbum: string, codigoIdioma: string) {
    const album: any = await this.albumModel.findOne({
      $and: [
        { _id: idAlbum },
        { estado: { $ne: codigosEstadoAlbum.eliminado } }
      ]
    })
      .populate([
        {
          path: 'media',
          select: '-_id -fechaCreacion -fechaActualizacion -__v',
          match: { estado: estadoActivoEntidades.media },
          populate: [
            {
              path: 'principal',
              select: 'url path filename filenameStorage',
            },
            {
              path: 'miniatura',
              select: 'url path filename filenameStorage',
            },
            {
              path: 'traducciones',
              match: {
                idioma: codigoIdioma,
                estado: estadoActivoEntidades.traduccionMedia
              }
            },
          ],
        },
        {
          path: 'portada',
          select: '-_id -fechaCreacion -fechaActualizacion -__v',
          match: { estado: estadoActivoEntidades.media },
          populate: [
            {
              path: 'principal',
              select: 'url path filename filenameStorage',
            },
            {
              path: 'miniatura',
              select: 'url path filename filenameStorage',
            },
            {
              path: 'traducciones',
              match: {
                idioma: codigoIdioma,
                estado: estadoActivoEntidades.traduccionMedia
              }
            },
          ],
        }
      ]);
    return album;
  }
  async getMedia(idMedia, codigoIdioma) {
    const media: any = await this.mediaModel.findOne({ _id: idMedia, estado: estadoActivoEntidades.media })
      .select('-_id -fechaCreacion -fechaActualizacion -__v')
      .populate([
        {
          path: 'principal',
          select: 'url path filename filenameStorage',
        },
        {
          path: 'miniatura',
          select: 'url path filename filenameStorage',
        },
        {
          path: 'traducciones',
          match: {
            idioma: codigoIdioma,
            estado: estadoActivoEntidades.traduccionMedia
          }
        }
      ]);
    return media;
  }
  async getUsuario(idUsuario, codigoIdioma): Promise<any> {
    const user: any = await this.userModel.findOne({ _id: idUsuario })
      .select('-fechaCreacion -fechaActualizacion -__v')
      .populate({
        path: 'perfiles',
        select: '-fechaCreacion -fechaActualizacion -__v',
        match: {
          estado: { $ne: estadosPerfil.eliminado }
        },
        populate: [
          {
            path: 'album', select: '-fechaCreacion -fechaActualizacion -__v',
            populate: [
              {
                path: 'idMedia',
                select: '-fechaCreacion -fechaActualizacion -__v -catalogoMedia',
                populate: [
                  {
                    path: 'traduccionMedia',
                    select: 'idioma descripcion',
                    match: { idioma: codigoIdioma }
                  },
                  {
                    path: 'principal',
                    select: 'url, filenameStorage',
                  },
                  {
                    path: 'miniatura',
                    select: 'url, filenameStorage',
                  },
                ]
              },
              {
                path: 'portada',
                select: '-fechaCreacion -fechaActualizacion -__v -catalogoMedia',
                populate: [
                  {
                    path: 'traduccionMedia',
                    select: 'idioma descripcion',
                    match: { idioma: codigoIdioma }
                  },
                  {
                    path: 'principal',
                    select: 'url, filenameStorage',
                  },
                  {
                    path: 'miniatura',
                    select: 'url, filenameStorage',
                  },
                ]
              }
            ]
          },
          {
            path: 'direcciones',
            select: '-fechaCreacion -fechaActualizacion -__v',
          },
          {
            path: 'telefonos',
            select: '-fechaCreacion -fechaActualizacion -__v',
          }
        ]
      });
    return user;
  }
  async getParticipanteProyecto(idProyecto): Promise<any> {
    const getParticipantes: any = await this.participanteProyectoModel.find({ proyecto: idProyecto, estado: estadoActivoEntidades.participante_proyecto })
      .select('-fechaCreacion -fechaActualizacion -__v')
      .populate({
        path: 'roles',
        select: '-fechaCreacion -fechaActualizacion -__v'
      })
      .populate({
        path: 'coautor',
        select: '-fechaCreacion -fechaActualizacion -__v'
      })
    return getParticipantes;
  }
  async getVotosProyecto(idProyecto: string, codigoIdioma: string): Promise<any> {
    const getVotos: any = await this.votoProyectoModel.find({ proyecto: idProyecto, estado: estadoActivoEntidades.voto_proyecto })
      .select('-fechaCreacion -fechaActualizacion -__v')
      .populate({
        path: 'traducciones',
        match: {
          idioma: codigoIdioma
        },
        select: '-fechaCreacion -fechaActualizacion -__v'
      })
      .populate({
        path: 'perfil',
        select: '-fechaCreacion -fechaActualizacion -__v'
      })
    return getVotos;
  }
  async getVotosNoticia(idNoticia, codigoIdioma): Promise<any> {
    const getVotos: any = await this.votoNoticiaModel.find({ noticia: idNoticia, estado: estadoActivoEntidades.voto_noticia })
      .select('-fechaCreacion -fechaActualizacion -__v')
      .populate({
        path: 'traducciones',
        match: {
          idioma: codigoIdioma
        },
        select: '-fechaCreacion -fechaActualizacion -__v'
      })
      .populate({
        path: 'perfil',
        select: '-fechaCreacion -fechaActualizacion -__v'
      })
    return getVotos;
  }
  async getPensamiento(idPensamiento, idPerfil, codigoIdioma): Promise<any> {
    const proyecto = await this.pensamientoModel.findOne({ _id: idPensamiento, perfil: idPerfil }).populate([
      {

      }
    ]);

    return proyecto;
  }
  async getProyecto(idProyecto, idPerfil, codigoIdioma): Promise<any> {
    const proyecto = await this.proyectosModel.findOne({ _id: idProyecto, perfil: idPerfil }).populate([
      {

      }
    ]);

    return proyecto;
  }
  async crearArchivo(path, data) {
    return new Promise(async function (resolve, reject) {
      setTimeout(function () {
        appendFile(path, data, (error) => {
          if (error) {
            throw error;
          } else {
            resolve(true);
          }
        })
      }, 100);
    });
  }
  async comprimirArchivo(source, out): Promise<any> {
    const archive = archiver('zip', { zlib: { level: 9 } });
    const stream = createWriteStream(out);

    return new Promise((resolve, reject) => {
      archive
        .directory(source, false)
        .on('error', err => reject(err))
        .pipe(stream)
        ;

      stream.on('close', () => resolve(true));
      archive.finalize();
    });
  }

  async eliminarArchivo(path) {
    return new Promise(async function (resolve, reject) {
      setTimeout(function () {
        unlink(path, async (err) => {
          if (err) throw err;
          resolve(true);
        });
      }, 0);
    });
  }

  /* async eliminarDirectorio(path) {
    return new Promise(async function (resolve, reject) {
      setTimeout(function () {
        rmdir(path, { recursive: true }, (err) => {
          if (err) {
            reject(err);
          }
          resolve(true);
        })
      }, 0);
    });
  } */
  eliminarDirectorio(path) {
    const exec = require('child_process').exec;
    const child = exec(`rm -rf ${path}`, function (err, out) {
      if (err) {
        console.log(err);
        throw err;
      } else {
        console.log(out);
        return true;
      }
    });
  }

  getFilenameUrl(url) {
    var posicionUltimaBarra = url.lastIndexOf("/");
    var rutaRelativa = url.substring(posicionUltimaBarra + "/".length, url.length);
    return rutaRelativa;
  }

}
