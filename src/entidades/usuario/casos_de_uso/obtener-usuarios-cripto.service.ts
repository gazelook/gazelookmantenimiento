import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Usuario } from 'src/drivers/mongoose/interfaces/usuarios/usuario.interface';
import { estadosUsuario } from '../../../shared/enum-sistema';

@Injectable()
export class ObtenerUsuariosCriptoService {
  constructor(
    @Inject('USUARIO_MODEL') private readonly userModel: Model<Usuario>,
  ) { }

  async obtenerUsuariosCripto(): Promise<any> {
    const user = await this.userModel
      .find({
        estado: estadosUsuario.cripto
      })
      .populate({
        path: 'perfiles',
      })
      .populate({
        path: 'transacciones',
        select: '_id informacionPago',
        populate: { path: 'informacionPago', select: '_id idPago' }
      }).
      sort({fechaCreacion:-1})


    let arrayUsuarios = []
    if (user.length > 0) {
      for (const usuarios of user) {
        const arrayPerfiles = []
        if (usuarios.perfiles.length > 0) {
          for (const perfil of usuarios.perfiles) {
            let objPerfil = {
              _id: perfil._id,
              nombre: perfil.nombre,
              nombreContacto: perfil.nombreContacto
            }
            arrayPerfiles.push(objPerfil)
          }
        }
        let objTransaccion
        if (usuarios.transacciones.length > 0) {
          for (const transaccion of usuarios.transacciones) {
            
            objTransaccion = {
              _id: transaccion['_id'],
              informacionPago: transaccion['informacionPago']
            }
            // arrayPerfiles.push(objPerfil)
          }
        }
        let objUser = {
          _id: usuarios._id,
          email: usuarios.email,
          perfiles: arrayPerfiles,
          transacciones: [objTransaccion],
          estado:
            { codigo: usuarios.estado }
        }
        arrayUsuarios.push(objUser)
      }
    }

    return arrayUsuarios;
  }
}
