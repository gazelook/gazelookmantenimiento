import { Inject, Injectable } from '@nestjs/common';
import { Model, PaginateModel } from 'mongoose';
import { Usuario } from 'src/drivers/mongoose/interfaces/usuarios/usuario.interface';
import { HadersInterfaceNombres } from 'src/shared/header-response-interface';
import { estadosPerfil, estadosUsuario } from '../../../shared/enum-sistema';

@Injectable()
export class ObtenerTodosUsuariosService {

  constructor(
    @Inject('USUARIO_MODEL') private readonly userModel: PaginateModel<Usuario>
  ) { }

  async obtenerUsuarios(limite: number, pagina: number, response: any): Promise<any> {

    const headerNombre = new HadersInterfaceNombres;

    try {
      const perfiles = ({
        path: 'perfiles',
        match: {
          estado: estadosPerfil.activa
        }
      })
      const rolSistema = ({
        path: 'rolSistema',
        populate: {
          path: 'rolesEspecificos',
          populate: {
            path: 'acciones'
          }
        }
      })

      const options = {
        lean: true,
        sort: { fechaCreacion: -1 },
        populate: [
          perfiles,
          rolSistema
        ],
        page: Number(pagina),
        limit: Number(limite)
      }

      const user = await this.userModel
        .paginate(
          {
            $and:
              [
                {
                  $or: [
                    { estado: estadosUsuario.activaNoVerificado },
                    { estado: estadosUsuario.activa },
                    { estado: estadosUsuario.bloqueadoSistema }
                  ],
                }
              ],
          }, options
        )

      let arrayUsuario = [];
      console.log('user.docs.length: ', user.docs.length)
      if (user.docs.length > 0) {

        for (const usuario of user.docs) {

          let arrayRolSistema = [];
          if (usuario.rolSistema.length > 0) {
            if (usuario.rolSistema[0] != null) {
              for (const rolSistema of usuario.rolSistema) {
                let obRolSistema = {
                  _id: rolSistema['_id'],
                  nombre: rolSistema['nombre'],
                  rol: {
                    codigo: rolSistema['rol']
                  },
                  // rolesEspecificos: arrayRolesEspecificos
                }
                arrayRolSistema.push(obRolSistema);

              }
            }

          }
          let arrayPerfiles = [];
          if (usuario.perfiles.length > 0) {
            for (const perfil of usuario.perfiles) {

              let obPerfil = {
                _id: perfil._id,
                nombre: perfil.nombre,
                nombreContacto: perfil.nombreContacto,
                nombreContactoTraducido: perfil.nombreContactoTraducido
              }
              arrayPerfiles.push(obPerfil);
            }
          }

          let objUsuario = {
            _id: usuario._id,
            rolSistema: arrayRolSistema,
            email: usuario.email,
            perfiles: arrayPerfiles,
            estado: {
              codigo: usuario.estado
            }
          }
          arrayUsuario.push(objUsuario);
        }

        console.log('arrayUsuario.length: ', arrayUsuario.length)
        response.set(headerNombre.totalDatos, user.totalDocs)
        response.set(headerNombre.totalPaginas, user.totalPages)
        response.set(headerNombre.proximaPagina, user.hasNextPage)
        response.set(headerNombre.anteriorPagina, user.hasPrevPage)


        return arrayUsuario;


      } else {

        response.set(headerNombre.totalDatos, user.totalDocs)
        response.set(headerNombre.totalPaginas, user.totalPages)
        response.set(headerNombre.proximaPagina, user.hasNextPage)
        response.set(headerNombre.anteriorPagina, user.hasPrevPage)

        return user.docs;
      }


    } catch (error) {
      throw error
    }
  }
}
