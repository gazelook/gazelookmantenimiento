import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Usuario } from 'src/drivers/mongoose/interfaces/usuarios/usuario.interface';
import { estadosPerfil, estadosTransaccion, estadosUsuario } from '../../../shared/enum-sistema';

@Injectable()
export class ObtenerIdUsuarioService {

  constructor(@Inject('USUARIO_MODEL') private readonly userModel: Model<Usuario>) { }

  async obtenerUsuarioByEmail(email: string): Promise<any> {
    const userLogin = await this.userModel
      .findOne({
        $and: [
          { email: email },
          { estado: { $ne: estadosUsuario.eliminado } },
          { estado: { $ne: estadosUsuario.bloqueadoSistema } },
          { estado: { $ne: estadosUsuario.bloqueadoRefund } }
      ]
        
      })
      .select('-fechaCreacion -fechaActualizacion')
      .populate({
        path: 'perfiles',
        select: '-fechaCreacion -fechaActualizacion'
      })
      .populate({
        path: 'rolSistema',
        select: 'nombre rol rolesEspecificos',
        populate: {
          path: 'rolesEspecificos',
          select: 'nombre rol entidad acciones',
          populate: {
            path: 'acciones',
            select: 'codigo nombre'
          }
        }
      });
    return userLogin;
  }

  async obtenerUsuarioById(id: string): Promise<any> {
    const userLogin = await this.userModel.findOne({ _id: id }).select('-fechaCreacion -fechaActualizacion')
      .populate({
        path: 'perfiles',
        select: '-fechaCreacion -fechaActualizacion'
      });
    return userLogin;
  }
  async obtenerUsuarioWithTransaccionByEmail(email: string): Promise<any> {
    const userLogin = await this.userModel.findOne({ email: email }).select('-fechaCreacion -fechaActualizacion')
      .populate({
        path: 'transacciones',
        select: '-fechaCreacion -fechaActualizacion',
        match: {
          estado: estadosTransaccion.pendiente,// estado pendiente
        },
        populate: {
          path: 'informacionPago'
        }
      });
    return userLogin;
  }

  async obtenerUsuario(id: string, opts?: any): Promise<any> {
    let userLogin;

    if (opts) {
      userLogin = await this.userModel.findOne({ _id: id }).select('-fechaCreacion -fechaActualizacion')
        .populate([
          {
            path: 'transacciones',
            select: '-fechaCreacion -fechaActualizacion',
            match: {
              estado: estadosTransaccion.pendiente,// estado pendiente
            },
            populate: {
              path: 'informacionPago'
            }
          },
          {
            path: 'perfiles',
          },
          {
            path: 'rolSistema',
            populate: {
              path: 'rolesEspecificos',
              populate: {
                path: 'acciones'
              }
            }
          }
        ])
        .session(opts.session);
    } else {
      // consulta sin session
      userLogin = await this.userModel.findOne({ _id: id }).select('-fechaCreacion -fechaActualizacion')
        .populate([
          {
            path: 'transacciones',
            select: '-fechaCreacion -fechaActualizacion',
            match: {
              estado: estadosTransaccion.pendiente,// estado pendiente
            },
            populate: {
              path: 'informacionPago'
            }
          },
          {
            path: 'perfiles',
          },
          {
            path: 'rolSistema',
            populate: {
              path: 'rolesEspecificos',
              populate: {
                path: 'acciones'
              }
            }
          }
        ]);
    }
    return userLogin;
  }

  async obtenerPerfilesUsuarioSessionById(id: string): Promise<any> {
    const userLogin = await this.userModel.findOne({ _id: id }).select('email estado perfilGrupo idioma')
      .populate({
        path: 'perfiles',
        //select: 'nombre nombreContacto tipoPefil estado'
        match: {
          estado: { $ne: estadosPerfil.eliminado }
        },
        populate: [
          {
            path: 'direcciones'
          }
        ]
      });
    return userLogin;
  }

  async obtenerPerfilesActivosFromUsuarioById(id: string): Promise<any> {
    const user = await this.userModel.findOne({ _id: id }).select('-fechaCreacion -fechaActualizacion')
      .populate({
        path: 'perfiles',
        select: '-fechaCreacion -fechaActualizacion',
        match: {
          estado: { $ne: estadosPerfil.eliminado }
        }
      });
    return user;
  }

  async obtenerUsuarioWithTransaccion(id: string, opts?: any): Promise<any> {
    let userLogin;

    if (opts) {
      userLogin = await this.userModel.findOne({ _id: id }).select('-fechaCreacion -fechaActualizacion')
        .populate([
          {
            path: 'transacciones',
            select: '-fechaCreacion -fechaActualizacion',
            match: {
              estado: estadosTransaccion.pendiente,// estado pendiente
            },
            populate: [
              {
                path: 'informacionPago'
              }
            ]
          },
          {
            path: 'perfiles',
          }
        ]).session(opts.session);
    } else {
      // consulta sin session
      userLogin = await this.userModel.findOne({ _id: id }).select('-fechaCreacion -fechaActualizacion')
        .populate([
          {
            path: 'transacciones',
            select: '-fechaCreacion -fechaActualizacion',
            match: {
              estado: estadosTransaccion.pendiente,// estado pendiente
            },
            populate: {
              path: 'informacionPago'
            }
          },
          {
            path: 'perfiles',
          }
        ]);
    }
    return userLogin;
  }

  //Obtiene usuarios segun el pais enviado
  async obtenerUsuarioByPais(codPais: string): Promise<any> {
    const usuarios = await this.userModel
      .find({
        $or: [
          { estado: estadosUsuario.activa },
          { estado: estadosUsuario.activaNoVerificado },
        ],
      })
      .select('-fechaCreacion -fechaActualizacion')
      .populate({
        path: 'direccion'
      });

    let arrayUsuarios = [];
    
    if (usuarios.length > 0) {
      for (const getUsuario of usuarios) {
        
        if (getUsuario.direccion && getUsuario.direccion['pais']) {
          // let getLocalidad = await this.localidadModel.findOne({ codigo: getUsuario.direccion['localidad'] })
          let objUsuario: any;

          if (getUsuario.direccion['pais'] === codPais) {
            objUsuario = {
              _id: getUsuario._id,
            };
            arrayUsuarios.push(objUsuario);
          }
        }
      }
    }
    return arrayUsuarios;
  }

  async obtenerUsuarioWithID(id: string, opts?: any): Promise<any> {
    let userLogin;

    if (opts) {
      userLogin = await this.userModel
        .findOne({ _id: id })
        .select('-fechaCreacion -fechaActualizacion')
        .populate([
          {
            path: 'transacciones',
            select: '-fechaCreacion -fechaActualizacion',
            match: {
              estado: estadosTransaccion.pendiente, // estado pendiente
              // origen: catalogoOrigen.suscripciones,
            },
            populate: [
              {
                path: 'informacionPago',
              },
            ],
          },
          {
            path: 'perfiles',
          },
        ])
        .session(opts.session);
    } else {
      // consulta sin session
      userLogin = await this.userModel
        .findOne({ _id: id })
        .select('-fechaCreacion -fechaActualizacion')
        .populate([
          {
            path: 'transacciones',
            select: '-fechaCreacion -fechaActualizacion',
            match: {
              estado: estadosTransaccion.pendiente, // estado pendiente
              // origen: catalogoOrigen.suscripciones,
            },
            populate: {
              path: 'informacionPago',
            },
          },
          {
            path: 'perfiles',
          },
        ]);
    }
    return userLogin;
  }

}
