import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Email } from 'src/drivers/mongoose/interfaces/email/email.interface';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { nombreAcciones, nombrecatalogoEstados, nombreEntidades } from 'src/shared/enum-sistema';
import { Usuario } from '../../../drivers/mongoose/interfaces/usuarios/usuario.interface';
import { BloquearEmailResponsableService } from '../../emails/casos_de_uso/bloquear-email-responsable.service';
import { CrearEmailConfirmacionCuentaService } from '../../emails/casos_de_uso/crear-email-confirmacion-cuenta.service';
import { CatalogoTipoEmailService } from '../../catalogos/casos_de_uso/catalogo-tipo-email.service';
import { ObtenerIdUsuarioService } from './obtener-id-usuario.service';
import { CatalogoIdiomasService } from '../../catalogos/casos_de_uso/catalogo-idiomas.service';

@Injectable()
export class ActualizarEmailResponsableService {

    constructor(
        @Inject('USUARIO_MODEL') private readonly userModel: Model<Usuario>,
        private nombreEntidad: CatalogoEntidadService,
        private nombreAccion: CatalogoAccionService,
        private nombreEstado: CatalogoEstadoService,
        private crearHistoricoService: CrearHistoricoService,
        private bloquearEmailResponsableService: BloquearEmailResponsableService,
        private crearEmailConfirmacionCuentaService: CrearEmailConfirmacionCuentaService,
        private catalogoTipoEmailService: CatalogoTipoEmailService,
        private obtenerIdUsuarioService: ObtenerIdUsuarioService,
        private catalogoIdiomasService: CatalogoIdiomasService
    ) { }

    async actualizarEmailResponsable(data: any, opts: any): Promise<any> {


        try {
            const accion = await this.nombreAccion.obtenerNombreAccion(nombreAcciones.modificar);
            let getAccion = accion.codigo;

            const entidad = await this.nombreEntidad.obtenerNombreEntidad(nombreEntidades.usuarios);
            let codEntidad = entidad.codigo;

            const estado = await this.nombreEstado.obtenerNombreEstado(nombrecatalogoEstados.activa, codEntidad);
            let codEstado = estado.codigo;

            // verificar que el email no sean iguales
            const getUsuario = await this.obtenerIdUsuarioService.obtenerUsuarioById(data.usuario);
            if (getUsuario.email === data.email) {
                throw { message: 'El email del responsable debe ser diferente que el email del titular de la cuenta'}
            }

            const dataupdate = {
                emailResponsable: data.email,
            }
            const updateEmail = await this.userModel.findByIdAndUpdate(
                data.usuario,
                dataupdate,
                opts
            );

            // bloquear el link del email del responsable anterior
            await this.bloquearEmailResponsableService.bloquearEmailResponsable(data.usuario, opts);

            //enviar email nuevo responsable
            const idiomaUsuario = await this.catalogoIdiomasService.obtenerIdiomaByCodigo(getUsuario.idioma);
            const codTipEmaR = await this.catalogoTipoEmailService.obtenerCodigoTipoEmail('validacionResponsable');
            const dataEmail = {
                usuario: data.usuario,
                emailDestinatario: data.email,
                codigo: codTipEmaR.codigo,
                idioma: idiomaUsuario.codNombre
            }
            const confirmacion = await this.crearEmailConfirmacionCuentaService.crearEmailConfirmacionCuenta(dataEmail, opts);


            // historico usuario
            const dataHistUser = JSON.parse(JSON.stringify(updateEmail));
            delete dataHistUser.fechaActualizacion;
            delete dataHistUser.fechaCreacion;
            const newHistoricoUsuario: any = {
                datos: dataHistUser,
                usuario: data.usuario,
                accion: getAccion,
                entidad: codEntidad
            }

            this.crearHistoricoService.crearHistoricoServer(newHistoricoUsuario);
            
            return confirmacion;
        } catch (error) {
            throw error;
        }


    }

}