import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Usuario } from 'src/drivers/mongoose/interfaces/usuarios/usuario.interface';
import { codigosCatalogoRol } from 'src/shared/enum-sistema';

@Injectable()
export class ValidaRolUsuarioService {

  constructor(@Inject('USUARIO_MODEL') private readonly userModel: Model<Usuario>) { }

  //VALIDA ROL ADMINISTRADOR
  async validarRolAdmin(user: any): Promise<any> {

    console.log('user.rolSistema.length: ', user.rolSistema.length)
    if (user.rolSistema.length > 0) {
      for (const getRolSistema of user.rolSistema) {
        let getRol = getRolSistema.rol;

        //Verifica si el rol es administrador
        if (getRol == codigosCatalogoRol.administrador 
          || getRol == codigosCatalogoRol.administradorAnuncios
          || getRol == codigosCatalogoRol.administradorGeneralAnuncios 
          || getRol == codigosCatalogoRol.administradorTraductorAnuncios
          || getRol == codigosCatalogoRol.subAdministradorGeneral
          || getRol == codigosCatalogoRol.coordinadorGazelook
          ) {
          return true;
        }

      }
    }

    return false;

  }

  //VALIDA LAS ACCIONES DE LOS ROLES ADMINISTRADOR
  async validarAccionesRolAdmin(user: any, accion: any): Promise<any> {

    if (user.rolSistema.length > 0) {
      for (const getRolSistema of user.rolSistema) {
        let getRol = getRolSistema.rol;
        //Verifica si el rol es administrador
        if (getRol == codigosCatalogoRol.administrador) {
          if (getRolSistema.rolesEspecificos) {
            const getAcciones = getRolSistema.rolesEspecificos.acciones
            if (getAcciones.length > 0) {
              for (const acciones of getAcciones) {
                if (acciones.codigo == accion) {
                  return true
                }
              }
            }
          }
        }
      }
    }

    return false;

  }

}
