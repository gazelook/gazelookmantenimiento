import { HttpService, Inject, Injectable } from '@nestjs/common';
import { resolve } from 'path';
import { appendFile, createWriteStream, mkdirSync, rmdir, stat, unlink } from 'fs';
import { ObtenerEmailService } from '../../emails/casos_de_uso/obtener-email.service';
const archiver = require('archiver');
import * as moment from 'moment';
import { ActualizarEstadoEmailService } from '../../emails/casos_de_uso/actualizar-estado-email.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import { nombrecatalogoEstados, nombreEntidades } from '../../../shared/enum-sistema';
import { StorageInformacionCuentaService } from '../../../drivers/amazon_s3/services/storage-informacion-cuenta.service';


@Injectable()
export class DescargarInformacioService {

  constructor(
    private obtenerEmailService: ObtenerEmailService,
    private actualizarEstadoEmailService: ActualizarEstadoEmailService,
    private nombreEntidad: CatalogoEntidadService,
    private nombreAccion: CatalogoAccionService,
    private nombreEstado: CatalogoEstadoService,
    private storageInformacionCuentaService: StorageInformacionCuentaService
  ) { }

  async descargarInformacion(token: string): Promise<any> {
    console.log('*****************************************************')
    console.log('DESCARGA INFORMACION')
    try {
      const entidad = await this.nombreEntidad.obtenerNombreEntidad(nombreEntidades.email);
      let codEntidad = entidad.codigo;

      const estado = await this.nombreEstado.obtenerNombreEstado(nombrecatalogoEstados.activa, codEntidad);

      const getEmail = await this.obtenerEmailService.obtenerEmailByTokenSinEstado(token);
      

      // fecha actual
      const fechaActualValidar = moment().format('YYYY-MM-DD');
      let formatFechaActual = new Date(fechaActualValidar);

      const filename = getEmail.datosProceso.nombreArchivo
      const zipPath = resolve(__dirname, `../../temp_files/${filename}`);
      
      //const verificarFile = await this.verificarArchivo(zip);
      const verificarFile = await this.storageInformacionCuentaService.verificarArchivoStorage(filename);


      if (moment(formatFechaActual) <= moment(getEmail.fechaValidacion)) {

        if (verificarFile) {

          const getFileStorage = await this.storageInformacionCuentaService.getArchivo(filename);
          await this.crearArchivo(zipPath, getFileStorage.Body);
          const dataResult = {
            archivo: zipPath,
            nombreArchivo: filename
          }
      
          return dataResult;
        } else {
          // no hay archivo
          return null;
        }
      } else {
        //link no valido ha expirado, bloquear email token, y eliminar el archivo
        if (verificarFile) {
          await this.actualizarEstadoEmailService.actualizarEstadoEmailFechaExpirada(token);
        }

        return null
      }

    } catch (error) {
      throw error;
    }
  }


  async verificarArchivo(path) {
    return new Promise(async function (resolve, reject) {
      setTimeout(function () {
        stat(path, function (err) {
          if (!err) {
            console.log('file or directory exists');
            resolve(true);
          }
          else if (err.code === 'ENOENT') {
            console.log('file or directory does not exist');
            resolve(false);
          }
        });
      }, 100);
    });
  }

  async eliminarArchivo(path) {
    return new Promise(async function (resolve, reject) {
      setTimeout(function () {
        unlink(path, async (err) => {
          if (err) reject(err);
          resolve(true);
        });
      }, 50);
    });
  }

  async crearArchivo(path, data) {
    return new Promise(async function (resolve, reject) {
      setTimeout(function () {
        appendFile(path, data, (error) => {
          if (error) {
            throw error;
          } else {
            resolve(true);
          }
        })
      }, 100);
    });
  }
}
