
import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { idiomas, codIdiomas, codigoCatalogosTipoEmail, nombreAcciones, nombreEntidades, estadosUsuario } from 'src/shared/enum-sistema';
import { Usuario } from 'src/drivers/mongoose/interfaces/usuarios/usuario.interface';
import { CrearEmailNotificacionRechazoCuentaService } from '../../emails/casos_de_uso/crear-email-notificacion-rechazo-cuenta.service';
import { CatalogoIdiomasService } from '../../catalogos/casos_de_uso/catalogo-idiomas.service';

const mongoose = require('mongoose');

@Injectable()
export class RechazoCuentaResponsableService {

  constructor(
    @Inject('USUARIO_MODEL') private readonly usuarioModel: Model<Usuario>,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private crearHistoricoService: CrearHistoricoService,
    private crearEmailNotificacionRechazoCuentaService: CrearEmailNotificacionRechazoCuentaService,
    private catalogoIdiomasService: CatalogoIdiomasService
  ) {

  }
  getCodIdiomas = codIdiomas;
  getIdiomas = idiomas;

  //Metodo para actualizar contraseña
  async rechazoCuenta(usuario) {

    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    await session.startTransaction();
    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };


      //Obtiene el codigo de la accion a realizarse
      const accion = await this.catalogoAccionService.obtenerNombreAccion(nombreAcciones.modificar)
      let getAccion = accion.codigo;

      //Obtiene la entidad usuarios
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(nombreEntidades.usuarios)
      let getCodEntUsuarios = entidad.codigo;


      //Actualizar estado usuario
      await this.usuarioModel.findOneAndUpdate({ _id: usuario }, {
        $set: {
          estado: estadosUsuario.rechazadoResponsable,
          responsableVerificado: true,
          emailVerificado: true,
          fechaActualizacion: new Date()
        }
      }, opts)

      const getUsuario = await this.usuarioModel.findOne({ _id: usuario }).populate(
        {
          path: 'perfiles'
        }
      ).session(opts.session)

      //Objeto para guardar el historico de la recuperacion del proyecto
      let datosUsuario = {
        _id: getUsuario._id,
        email: getUsuario.email,
        contrasena: getUsuario.contrasena,
        fechaActualizacion: getUsuario.fechaActualizacion
      }

      let newHistoricoActualizarPass: any = {
        datos: datosUsuario,//Datos de la actualizacion de contraseña de usuario
        usuario: getUsuario._id,
        accion: getAccion,
        entidad: getCodEntUsuarios
      }

      // await this.crearHistoricoService.crearHistoricoTransaccion(newHistoricoActualizarPass, opts);
      this.crearHistoricoService.crearHistoricoServer(newHistoricoActualizarPass);

      const getIdiomaUser = await this.catalogoIdiomasService.obtenerIdiomaByCodigo(getUsuario.idioma)
      const dataEmail = {
        usuario: getUsuario._id,
        nombre: getUsuario.perfiles[0].nombre,
        nombreResponsable: getUsuario.nombreResponsable,
        idioma: getIdiomaUser.codNombre,
        emailDestinatario: getUsuario.email
      }

      // SI RECHAZO LA CUENTA, NOTIFICARLE AL USUARIO MENOR EDAD
      await this.crearEmailNotificacionRechazoCuentaService.crearEmailNotificacionRechazoCuenta(dataEmail)

      //Confirma los cambios de la transaccion
      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();
      return true;

    } catch (error) {
      await session.abortTransaction();
      await session.endSession();
      throw error
    }
  }

}