import { ApiProperty } from "@nestjs/swagger";

export class TipoPerfilDto {
    @ApiProperty()
    codigo: string;
    @ApiProperty()
    estado: string;
}