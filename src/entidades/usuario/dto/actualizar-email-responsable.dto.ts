import { ApiProperty } from "@nestjs/swagger";

export class ActualizarEmailResponsableDto {
    @ApiProperty({description: 'id del usuario'})
    usuario: string;
    @ApiProperty({description: 'email del responsable'})
    email: string;
    @ApiProperty({description: 'Código nombre del idioma'})
    codIdioma: string;

}