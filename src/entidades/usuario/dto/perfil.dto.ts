import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";

export class PerfilDto {
    @ApiProperty()
    perfil: string;
    @ApiProperty()
    nombre: string;
    @ApiProperty()
    descripcion: string;
    @ApiProperty()
    idioma: string;
    @ApiProperty()
    codigoTipoPerfil: string;
}


export class PerfilNoticiaDto {
    @ApiProperty()
    _id: string;
}

export class PerfilNoticiaUpdateDto {
    @ApiProperty()
    _id: string;
    @ApiProperty()
    nombre: string;
}

export class PerfilProyectoDto {
    @ApiProperty({required: true, description:'identificador del perfil', example:'5f4d0f02ecea1c15acb66d64'})
    _id: string;
}

export class PerfilProyectoUnicoDto {
    @ApiProperty({description:'identificador del perfil', example:'5f4d0f02ecea1c15acb66d64'})
    _id: string;

    @ApiProperty({description:'Nombre del perfil', example:'Leonardo'})
    nombre: string;
}

export class idPerfilDto {
    @ApiProperty({required: true, description:'Identificador del perfil', example:'5f57a7a1e446d64158ef85f1'})
    @IsNotEmpty()
    _id: string;
}


export class PerfilesDto {
    @ApiProperty({description:'identificador del perfil', example:'5f4d0f02ecea1c15acb66d64'})
    _id: string;

    @ApiProperty({description:'Nombre del perfil', example:'Leonardo'})
    nombre: string;

    @ApiProperty({description:'Nombre contacto del perfil', example:'Leonardo'})
    nombreContacto: string;

    @ApiProperty({description:'Nombre contacto traducido del perfil', example:'Leonardoasad'})
    nombreContactoTraducido: string;
}