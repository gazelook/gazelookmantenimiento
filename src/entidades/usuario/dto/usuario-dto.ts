import { ApiProperty } from "@nestjs/swagger";
import { NodeCompatibleEventEmitter } from "rxjs/internal/observable/fromEvent";
import { PerfilesDto } from "./perfil.dto";



export class UsuarioDto {
    @ApiProperty()
    email: string;
    @ApiProperty()
    contrasena: string;
    @ApiProperty()
    aceptoTerminosCondiciones:boolean;
    @ApiProperty()
    perfilGrupo:boolean;
    @ApiProperty()
    emailVerificado:boolean;
    @ApiProperty()
    menorEdad:boolean;
    @ApiProperty()
    emailResponsable:string;
    @ApiProperty()
    idioma:string;
    @ApiProperty()
    estado:string;
    // @ApiProperty()
    // fechaCreacion: Date;
    // @ApiProperty()
    // fecha_actualizacion: Date;

    @ApiProperty()
    nombreContacto: string;
    @ApiProperty()
    nombre: string;
    @ApiProperty()
    listaTipoPerfiles: Array<string>;
}

export class IdUsuarioDto {
    @ApiProperty({type:String})
    _id: string;

}

export class EstadoUsuarioDto {
    @ApiProperty({type:String})
    codigo: string;

}

export class ActualizarEstadoUsuarioDto {
    @ApiProperty({type:String})
    _id: string;

    @ApiProperty({type:EstadoUsuarioDto})
    estado: EstadoUsuarioDto;

}

export class CodigoCatalogoRolDto {
    @ApiProperty({type:String})
    codigo: string;
}

export class RolSistemaDto {
    @ApiProperty({type:String})
    _id: string;

    @ApiProperty({type:String})
    nombre: string;

    @ApiProperty({type:CodigoCatalogoRolDto})
    rol: CodigoCatalogoRolDto;
}

export class RespObtenerTodosUsuariosDto {
    @ApiProperty({type:String})
    _id: string;

    @ApiProperty({type:[RolSistemaDto]})
    rolSistema: Array<RolSistemaDto>;

    @ApiProperty({type:String})
    email: string;

    @ApiProperty({type:[PerfilesDto]})
    perfiles: Array<PerfilesDto>;

    @ApiProperty({type:EstadoUsuarioDto})
    estado: EstadoUsuarioDto;

}