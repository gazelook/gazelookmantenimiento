import { estadosModelo } from './../../../drivers/mongoose/modelos/catalogoEstado/catalogoEstadoModelo';
import { Connection } from 'mongoose';
import { usuarioModelo } from '../../../drivers/mongoose/modelos/usuarios/usuario.schema';
import { perfilModelo } from '../../../drivers/mongoose/modelos/perfil/perfil.schema';
import { accionModelo } from '../../../drivers/mongoose/modelos/catalogoAccion/catalogoAccionModelo';
import { entidadesModelo } from '../../../drivers/mongoose/modelos/catalogoEntidades/catalogoEntidadesModelo';
import { tipoPerfilModelo } from '../../../drivers/mongoose/modelos/catalogo-tipo-perfil/catalogo-tipo-perfil.schema';
import { traduccionTipoPerfilModelo } from '../../../drivers/mongoose/modelos/traduccion_catalogo_tipo_perfil/traduccion-catalogo-tipo-perfil.schema';
import { albumModelo } from '../../../drivers/mongoose/modelos/album/album.schema';
import { MediaModelo } from '../../../drivers/mongoose/modelos/media/mediaModelo';
import { catalogoIdiomasModelo } from '../../../drivers/mongoose/modelos/catalogoIdiomas/catalogoIdiomasModelo';
import { TransaccionModelo } from '../../../drivers/mongoose/modelos/transaccion/transaccion.schema';
import { ProyectoModelo } from '../../../drivers/mongoose/modelos/proyectos/proyecto.schema';
import { ParticipanteProyectoModelo } from '../../../drivers/mongoose/modelos/participante_proyecto/participante_proyecto.schema';
import { PensamientoModelo } from '../../../drivers/mongoose/modelos/pensamientos/pensamiento.schema';
import { noticiaModelo } from '../../../drivers/mongoose/modelos/noticias/noticia-schema';
import { traduccionNoticiaSchema } from '../../../drivers/mongoose/modelos/traduccion_noticias/traduccion-noticias-modelo';
import { votoNoticiaSchema } from '../../../drivers/mongoose/modelos/voto_noticia/voto-noticia-schema';
import { traduccionVotoNoticiaSchema } from '../../../drivers/mongoose/modelos/traduccion_voto_noticia/traduccion-noticias-modelo';
import { CatalogoTipoMediaModelo } from '../../../drivers/mongoose/modelos/catalogoTipoMedia/catalogoTipoMediaModelo';
import { archivoModelo } from '../../../drivers/mongoose/modelos/archivo/archivo.schema';
import { CatalogoArchivoDefaultModelo } from '../../../drivers/mongoose/modelos/catalogo_archivo_default/catalogo-archivo-default.schema';
import { AsociacionModelo } from '../../../drivers/mongoose/modelos/asociaciones/asociacion.schema';
import { CatalogoTipoAsociacionModelo } from '../../../drivers/mongoose/modelos/catalogo_tipo_asociacion/catalogo-tipo-asociacion.schema';
import { TraduccionMediaModelo } from '../../../drivers/mongoose/modelos/traducion_media/traduccion-media.schema';
import { DireccionModelo } from '../../../drivers/mongoose/modelos/direcciones/direccion.schema';
import { TelefonoModelo } from '../../../drivers/mongoose/modelos/telefonos/telefono.schema';
import { catalogoAlbumModelo } from '../../../drivers/mongoose/modelos/catalogo_album/catalogo-album.schema';
import { TraduccionProyectoModelo } from 'src/drivers/mongoose/modelos/traduccion_proyecto/traduccion-proyecto.schema';
import { TraduccionCatalogoTipoProyectoModelo } from 'src/drivers/mongoose/modelos/traduccion_catalogo_tipo_proyecto/traduccion-catalogo-tipo-proyecto.schema';
import { CatalogoLocalidadModelo } from 'src/drivers/mongoose/modelos/catalogo_localidad/catalogo-localidad.schema';
import { RolEntidadModelo } from 'src/drivers/mongoose/modelos/rol_entidad/rol-entidad.schema';
import { VotoProyectoModelo } from 'src/drivers/mongoose/modelos/voto_proyecto/voto-proyecto.schema';
import { CatalogoTipoVotoModelo } from 'src/drivers/mongoose/modelos/catalogo_tipo_voto/catalogo-tipo-voto.schema';
import { TraduccionPensamientoModelo } from 'src/drivers/mongoose/modelos/traduccion_pensamientos/traduccion-pensamiento.schema';
import { dispositivoSchema } from '../../../drivers/mongoose/modelos/dispositivos/dispositivo.schema';
import { CatalogoPaisModelo } from '../../../drivers/mongoose/modelos/catalogo_pais/catalogo-pais.schema';
import { TraduccionPaisModelo } from '../../../drivers/mongoose/modelos/traducciones/traducion_pais/traduccion-pais.schema';
import { RolSistemaModelo } from 'src/drivers/mongoose/modelos/rol_sistema/rol-sistema.schema';
import { tokenUsuarioModelo } from '../../../drivers/mongoose/modelos/token_usuario/token_usuario.schema';
import { ParticipanteAsociacionModelo } from '../../../drivers/mongoose/modelos/participante_asociacion/participante-asociacion.schema';

export const usuarioProviders = [
  {
    provide: 'USUARIO_MODEL',
    useFactory: (connection: Connection) => connection.model('usuario', usuarioModelo, 'usuario'),
    inject: ['DB_CONNECTION']
  },
  {
    provide: 'PERFIL_MODEL',
    useFactory: (connection: Connection) => connection.model('perfil', perfilModelo, 'perfil'),
    inject: ['DB_CONNECTION']
  },

  {
    provide: 'TIPO_PERFIL_MODEL',
    useFactory: (connection: Connection) => connection.model('catalogo_tipo_perfil', tipoPerfilModelo, 'catalogo_tipo_perfil'),
    inject: ['DB_CONNECTION']
  },
  {
    provide: 'TRADUCCION_TIPO_PERFIL_MODEL',
    useFactory: (connection: Connection) => connection.model('traduccion_catalogo_tipo_perfil', traduccionTipoPerfilModelo, 'traduccion_catalogo_tipo_perfil'),
    inject: ['DB_CONNECTION']
  },
  {
    provide: 'TRANSACCION_MODEL',
    useFactory: (connection: Connection) => connection.model('transaccion', TransaccionModelo, 'transaccion'),
    inject: ['DB_CONNECTION']
  },
  {
    provide: 'PROYECTO_MODEL',
    useFactory: (connection: Connection) => connection.model('proyecto', ProyectoModelo, 'proyecto'),
    inject: ['DB_CONNECTION']
  },
  {
    provide: 'TRADUCCION_PROYECTO_MODEL',
    useFactory: (connection: Connection) => connection.model('traduccion_proyecto', TraduccionProyectoModelo, 'traduccion_proyecto'),
    inject: ['DB_CONNECTION']
  },
  {
    provide: 'TRADUCCION_CATALOGO_TIPO_PROYECTO_MODEL',
    useFactory: (connection: Connection) => connection.model('traduccion_catalogo_tipo_proyecto', TraduccionCatalogoTipoProyectoModelo, 'traduccion_catalogo_tipo_proyecto'),
    inject: ['DB_CONNECTION']
  },
  {
    provide: 'PARTICIPANTE_PROYECTO_MODEL',
    useFactory: (connection: Connection) => connection.model('participante_proyecto', ParticipanteProyectoModelo, 'participante_proyecto'),
    inject: ['DB_CONNECTION']
  },
  {
    provide: 'PENSAMIENTO_MODEL',
    useFactory: (connection: Connection) => connection.model('pensamiento', PensamientoModelo, 'pensamiento'),
    inject: ['DB_CONNECTION']
  },
  {
    provide: 'NOTICIA_MODEL',
    useFactory: (connection: Connection) => connection.model('noticia', noticiaModelo, 'noticia'),
    inject: ['DB_CONNECTION']
  },
  {
    provide: 'TRADUCCION_NOTICIA_MODEL',
    useFactory: (connection: Connection) => connection.model('traduccion_noticia', traduccionNoticiaSchema, 'traduccion_noticia'),
    inject: ['DB_CONNECTION']
  },
  {
    provide: 'TRADUCCION_VOTO_NOTICIA_MODEL',
    useFactory: (connection: Connection) => connection.model('traduccion_voto_noticia', traduccionVotoNoticiaSchema, 'traduccion_voto_noticia'),
    inject: ['DB_CONNECTION']
  },
  {
    provide: 'ALBUM_MODEL',
    useFactory: (connection: Connection) => connection.model('album', albumModelo, 'album'),
    inject: ['DB_CONNECTION']
  },
  {
    provide: 'ARCHIVO_MODEL',
    useFactory: (connection: Connection) => connection.model('archivo', archivoModelo, 'archivo'),
    inject: ['DB_CONNECTION']
  },
  {
    provide: 'TIPO_MEDIA_MODEL',
    useFactory: (connection: Connection) => connection.model('catalogo_tipo_media', CatalogoTipoMediaModelo, 'catalogo_tipo_media'),
    inject: ['DB_CONNECTION']
  },
  {
    provide: 'CATALOGO_ARCHIVO_DEFAULT_MODEL',
    useFactory: (connection: Connection) => connection.model('catalogo_archivo_default', CatalogoArchivoDefaultModelo, 'catalogo_archivo_default'),
    inject: ['DB_CONNECTION']
  },
  {
    provide: 'ASOCIACION_MODEL',
    useFactory: (connection: Connection) => connection.model('asociacion', AsociacionModelo, 'asociacion'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'TIPO_ASO_MODEL',
    useFactory: (connection: Connection) => connection.model('catalogo_tipo_asociacion', CatalogoTipoAsociacionModelo, 'catalogo_tipo_asociacion'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'TRADUCCION_MEDIA_MODEL',
    useFactory: (connection: Connection) => connection.model('traduccion_media', TraduccionMediaModelo, 'traduccion_media'),
    inject: ['DB_CONNECTION']
  },
  {
    provide: 'MEDIA_MODEL',
    useFactory: (connection: Connection) => connection.model('media', MediaModelo, 'media'),
    inject: ['DB_CONNECTION']
  },
  {
    provide: 'DIRECCION_MODEL',
    useFactory: (connection: Connection) => connection.model('direccion', DireccionModelo, 'direccion'),
    inject: ['DB_CONNECTION']
  },
  {
    provide: 'TELEFONO_MODEL',
    useFactory: (connection: Connection) => connection.model('telefono', TelefonoModelo, 'telefono'),
    inject: ['DB_CONNECTION']
  },
  {
    provide: 'CATALOGO_IDIOMAS',
    useFactory: (connection: Connection) => connection.model('catalogo_idiomas', catalogoIdiomasModelo, 'catalogo_idiomas'),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'CATALOGO_ALBUM_MODEL',
    useFactory: (connection: Connection) => connection.model('catalogo_album', catalogoAlbumModelo, 'catalogo_album'),
    inject: ['DB_CONNECTION']
  },
  {
    provide: 'CATALOGO_LOCALIDAD_MODEL',
    useFactory: (connection: Connection) => connection.model('catalogo_localidad', CatalogoLocalidadModelo, 'catalogo_localidad'),
    inject: ['DB_CONNECTION']
  },
  {
    provide: 'ROL_ENTIDAD_MODEL',
    useFactory: (connection: Connection) => connection.model('rol_entidad', RolEntidadModelo, 'rol_entidad'),
    inject: ['DB_CONNECTION']
  },
  {
    provide: 'VOTO_PROYECTO_MODEL',
    useFactory: (connection: Connection) => connection.model('voto_proyecto', VotoProyectoModelo, 'voto_proyecto'),
    inject: ['DB_CONNECTION']
  },
  {
    provide: 'CATALOGO_TIPO_VOTO_MODEL',
    useFactory: (connection: Connection) => connection.model('catalogo_tipo_voto', CatalogoTipoVotoModelo, 'catalogo_tipo_voto'),
    inject: ['DB_CONNECTION']
  },
  {
    provide: 'TRADUCCION_PENSAMIENTO_MODEL',
    useFactory: (connection: Connection) => connection.model('traduccion_pensamiento', TraduccionPensamientoModelo, 'traduccion_pensamiento'),
    inject: ['DB_CONNECTION']
  },
  {
    provide: 'VOTO_NOTICIA_MODEL',
    useFactory: (connection: Connection) => connection.model('voto_noticia', votoNoticiaSchema, 'voto_noticia'),
    inject: ['DB_CONNECTION']
  },
  {
    provide: 'DISPOSTIVO_MODEL',
    useFactory: (connection: Connection) => connection.model('dispositivo', dispositivoSchema),
    inject: ['DB_CONNECTION'],
  },
  {
    provide: 'CATALOGO_PAIS_MODEL',
    useFactory: (connection: Connection) => connection.model('catalogo_pais', CatalogoPaisModelo, 'catalogo_pais'),
    inject: ['DB_CONNECTION']
  },
  {
    provide: 'TRADUCCION_PAIS_MODEL',
    useFactory: (connection: Connection) => connection.model('traduccion_catalogo_pais', TraduccionPaisModelo, 'traduccion_catalogo_pais'),
    inject: ['DB_CONNECTION']
  },
  {
    provide: 'ROL_SISTEMA_MODEL',
    useFactory: (connection: Connection) => connection.model('rol_sistema', RolSistemaModelo, 'rol_sistema'),
    inject: ['DB_CONNECTION']
  },
  {
    provide: 'TOKEN_USUARIO_MODEL',
    useFactory: (connection: Connection) => connection.model('token_usuario', tokenUsuarioModelo, 'token_usuario'),
    inject: ['DB_CONNECTION']
  },
  {
    provide: 'PARTI_ASOC_MODEL',
    useFactory: (connection: Connection) => connection.model('participante_asociacion', ParticipanteAsociacionModelo, 'participante_asociacion'),
    inject: ['DB_CONNECTION'],
  },
];
