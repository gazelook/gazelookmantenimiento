import { UsuarioControllerModule } from './controladores/usuario-controller.module';
import { Module } from '@nestjs/common';
import { UsuarioServicesModule } from './casos_de_uso/usuario-services.module';

@Module({
  imports: [UsuarioControllerModule, 
  ],
  providers: [
  ],
  controllers: [],
  exports: [UsuarioControllerModule]
})
export class UsuarioModule {}

