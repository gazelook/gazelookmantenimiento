import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { LogSistema } from '../../../drivers/mongoose/interfaces/log_sistema/log-sistema.interface';
import { LogSistemaDto } from '../dtos/log.dto';



@Injectable()
export class CrearLogSistemaService {

    constructor(
        @Inject('LOG_SISTEMA_MODEL') private readonly logSistemaModel: Model<LogSistema>,
    ) {
    }

    async crearLogSistema(dataLogSistema: LogSistemaDto): Promise<LogSistema> {

        try {

            const saveLog = new this.logSistemaModel(dataLogSistema).save();
            return saveLog;

        } catch (error) {
            throw error;
        }
    }
}