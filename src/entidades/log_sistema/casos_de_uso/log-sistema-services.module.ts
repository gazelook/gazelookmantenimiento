import { Global, Module } from '@nestjs/common';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { logSistemaProviders } from '../drivers/log-sistema.provider';
import { CrearLogSistemaService } from './crear-log-sistema.service';
import { ObtenerLogSistemaService } from './obtener-log-sistema.service';

@Global()
@Module({
  imports: [
    DBModule,
  ],
  providers: [
    ...logSistemaProviders,
    CrearLogSistemaService,
    ObtenerLogSistemaService,
],
  exports: [
    CrearLogSistemaService,
    ObtenerLogSistemaService,
  ],
  controllers: [],
})
export class LogSistemaServicesModule {}
