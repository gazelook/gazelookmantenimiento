import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { LogSistema } from '../../../drivers/mongoose/interfaces/log_sistema/log-sistema.interface';
import { numeroDias } from '../../../shared/enum-sistema';
import { FilterLogsDto } from '../dtos/log.dto';



@Injectable()
export class ObtenerLogSistemaService {

    numeroDia = numeroDias

    constructor(
        @Inject('LOG_SISTEMA_MODEL') private readonly logSistemaModel: Model<LogSistema>,
    ) {
    }

    async getLogSistema(params: FilterLogsDto): Promise<Array<LogSistema>> {

        try {
            let fechaInit = this.getFormatDateUTC(params.fechaInicial);
            let fechaFin = this.getFormatDateUTC(params.fechaFinal);

            fechaFin = fechaFin + 'T23:59:59.999Z';
            const final = new Date(fechaFin);

            fechaInit = fechaInit + 'T00:00:00.000Z';
            const inicio = new Date(fechaInit);

            console.log("inicio", inicio);
            console.log("final", final);



            const logs = await this.logSistemaModel.find({
                $and: [
                    {
                        fechaCreacion: {
                            $gte: new Date(
                                new Date(params.fechaInicial).setUTCHours(0, 0, 0, 0),
                            ),
                            $lte: new Date(
                                new Date(params.fechaFinal).setUTCHours(23, 59, 59, 999),
                            ),
                        },
                    }
                ],
            })
            return logs;

        } catch (error) {
            throw error;
        }
    }

    getFormatDateUTC(fecha: string) {
        const getFecha = new Date(fecha);
        //Obtiene el dia de la fecha enviada
        let dia = getFecha.getUTCDay();

        const getDia = this.seleccionarDia(dia);

        
        //Establece la fecha final para la busqueda
        // const fechaF = getFecha.setUTCDate(getFecha.getUTCDate() - getDia);
        const fechaF = getFecha
        // getFecha.setUTCDate(getFecha.getUTCDate());

        const fecFinal = new Date(fechaF);
        console.log('FECHA ENVIADA: ', fecFinal)
        let formatFinal = fecFinal.getUTCFullYear() + '-' + (fecFinal.getUTCMonth() + 1) + '-' + (fecFinal.getUTCDate())

        //Si el mes de la fecha final es menor a 10 y el dia de la fecha final es mayor o igual a 10
        if (((fecFinal.getUTCMonth() + 1) < 10) && ((fecFinal.getUTCDate()) >= 10)) {

            formatFinal = fecFinal.getUTCFullYear() + '-0' + (fecFinal.getUTCMonth() + 1) + '-' + (fecFinal.getUTCDate())
        }

        //Si el mes de la fecha final es menor a 10 y el dia de la fecha final es igual a 32
        if (((fecFinal.getUTCMonth() + 1) < 10) && ((fecFinal.getUTCDate()) === 32)) {

            formatFinal = fecFinal.getUTCFullYear() + '-0' + (fecFinal.getUTCMonth() + 2) + '-01';
        }

        //Si el mes de la fecha final es menor a 10 y el dia de la fecha final es menor a 10
        if (((fecFinal.getUTCMonth() + 1) < 10) && ((fecFinal.getUTCDate()) < 10)) {

            formatFinal = fecFinal.getUTCFullYear() + '-0' + (fecFinal.getUTCMonth() + 1) + '-0' + (fecFinal.getUTCDate())

        }
        return formatFinal;
    }

    seleccionarDia(numeroDia) {
        switch (numeroDia) {
            case this.numeroDia.domingo:
                return 0;
            case this.numeroDia.lunes:
                return 1;
            case this.numeroDia.martes:
                return 2;
            case this.numeroDia.miercoles:
                return 3;
            case this.numeroDia.jueves:
                return 4;
            case this.numeroDia.viernes:
                return 5;
            case this.numeroDia.sabado:
                return 6;
            default:
                return 1;
        }
    }
}