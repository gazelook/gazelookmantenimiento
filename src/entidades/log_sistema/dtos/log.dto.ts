import { IsNotEmpty } from "class-validator";

export class LogSistemaDto {
     method: string;
     date: Date;
     status: number;
     url: string;
     ip: string;
     browser: string;
     content: any;
     timeRequest: number;
}

export class FilterLogsDto {
     @IsNotEmpty()
     fechaInicial: string;
   
     @IsNotEmpty()
     fechaFinal: string;
   }