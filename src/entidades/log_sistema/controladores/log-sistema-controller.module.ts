import { TraduccionEstaticaController } from './../../../multiIdioma/controladores/traduccion-estatica-controller';
import { Module, HttpModule } from '@nestjs/common';
import { Funcion } from 'src/shared/funcion';
import { LogSistemaServicesModule } from '../casos_de_uso/log-sistema-services.module';
import { FilterLogsController } from './filter-logs.controller';


@Module({
  imports: [LogSistemaServicesModule],
  providers: [TraduccionEstaticaController, Funcion],
  exports: [],
  controllers: [
    FilterLogsController
  ],
})
export class LogsistemaControllerModule {}
