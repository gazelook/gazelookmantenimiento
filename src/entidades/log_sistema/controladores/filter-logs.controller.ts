import {
  Controller, Get,
  Headers, HttpStatus, Query, Res, UseGuards
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiHeader, ApiOperation, ApiQuery, ApiResponse,
  ApiTags
} from '@nestjs/swagger';
import { Response } from 'express';
import { Funcion } from 'src/shared/funcion';
import { ObtenerLogSistemaService } from '../casos_de_uso/obtener-log-sistema.service';
import { FilterLogsDto, LogSistemaDto } from '../dtos/log.dto';
import { TraduccionEstaticaController } from './../../../multiIdioma/controladores/traduccion-estatica-controller';


@ApiTags('Logs')
@Controller('api/logs-sistema')
export class FilterLogsController {
  constructor(
    private readonly obtenerLogSistemaService: ObtenerLogSistemaService,
    private readonly traduccionEstaticaController: TraduccionEstaticaController,
    private readonly funcion: Funcion
  ) { }
  @Get('/')
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiOperation({ summary: 'Listar logs del sistema por rango de fecha' })
  @ApiResponse({ status: 201, type: LogSistemaDto, description: 'Creado' })
  @ApiResponse({
    status: 404,
    description: 'No se pudo obtener los documentos',
  })
  @ApiQuery({ name: 'fechaInicial', required: false })
  @ApiQuery({ name: 'fechaFinal', required: false })
  @UseGuards(AuthGuard('jwt'))
  public async getLogsSystem(
    @Headers() headers,
    @Query() params: FilterLogsDto,
    @Res() response: Response
  ) {

    try {
      const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma)


      const documentos = await this.obtenerLogSistemaService.getLogSistema(params);

      if (documentos.length === 0) {
        //llama al metodo de traduccion estatica
        const COLECCION_BACIA = await this.traduccionEstaticaController.traduccionEstatica(
          codIdioma,
          'COLECCION_BACIA',
        );
        const respuesta = this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.OK, mensaje: COLECCION_BACIA, datos: [] })
        response.send(respuesta);
        return respuesta;

      } else {

        const respuesta = this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.OK, datos: documentos })
        response.send(respuesta);
        return respuesta;
      }
    } catch (e) {
      const respuesta = this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR, mensaje: e.message })
      response.set(respuesta);
      return respuesta;
    }
  }
}
