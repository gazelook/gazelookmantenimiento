import { loginControllerModule } from './controladores/login-controller.module';
import { Module } from '@nestjs/common';

@Module({

  imports: [
    loginControllerModule,
  ],

})
export class LoginModule { }
