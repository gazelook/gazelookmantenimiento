import {
  Controller, Headers,
  HttpStatus, Post, Query
} from '@nestjs/common';
import {
  ApiHeader,
  ApiOperation, ApiResponse,
  ApiTags
} from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import { JwtStrategy } from '../drivers/jwt.strategy';
import { LogoutService } from './../casos_de_uso/logout.service';

@ApiTags('Autenticación')
@Controller('api/logout')
export class LogoutController {
  constructor(
    private readonly logoutService: LogoutService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
    private jwtStrategy: JwtStrategy
  ) { }

  // @ApiBody({ type: LoginDto })
  @ApiOperation({ summary: 'se debe enviar dispositivo y usuario' })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
  })
  @ApiResponse({
    status: 201,
    description: 'Logout OK',
  })
  //@ApiResponse({ status: 401, description: 'Unauthorized' })
  //@UseGuards(LocalAuthGuard)
  //@UseGuards(AuthGuard('jwt'))
  @Post('/')
  async logout(
    @Headers() headers,
    @Query('idDispositivo') idDispositivo: string,
    @Query('idUsuario') idUsuario: string
  ) {

    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {
      if (
        mongoose.isValidObjectId(idDispositivo) &&
        mongoose.isValidObjectId(idUsuario)
      ) {
        const logout = await this.logoutService.logout(
          idUsuario,
          idDispositivo,
        );
        if (logout) {
          const SESION_CERRADA = await this.i18n.translate(
            codIdioma.concat('.SESION_CERRADA'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: SESION_CERRADA,
          });
        } else {
          const ERROR_SALIR = await this.i18n.translate(
            codIdioma.concat('.ERROR_SALIR'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.BAD_REQUEST,
            mensaje: ERROR_SALIR,
          });
        }
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (e) {
      return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR, mensaje: e.message });
    }
  }

}
