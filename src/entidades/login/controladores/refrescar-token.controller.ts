import { RefrescarTokenService } from '../casos_de_uso/refrescar-token.service';
import { TokenRefreshResponseDto, TokenRefreshDto } from '../dto/token-refresh-responde-dto';

import { Controller, Request, Post, UseGuards, Body, Header, Headers, HttpStatus } from '@nestjs/common';
import { ApiResponse, ApiTags, ApiBody, ApiHeader, ApiOperation, ApiBearerAuth } from '@nestjs/swagger';
import { I18nService } from 'nestjs-i18n'
import { Funcion } from '../../../shared/funcion';

@ApiTags('Autenticación')
@Controller('api/auth')
export class RefrescarTokenController {

  constructor(
    private readonly refrescarToken: RefrescarTokenService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion

  ) { }

  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas', required: true })
  @ApiBody({ type: TokenRefreshDto })
  @ApiOperation({ summary: 'envio de refresh token para generar un nuevo token' })
  @ApiResponse({ status: 201, type: TokenRefreshResponseDto, description: 'Created' })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  @Post('/refrescar-token')
  async refreshToken(@Request() req, @Body() dataToken: any, @Headers() headers) {

    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {
      const user = await this.refrescarToken.refreshToken(dataToken);
      if (user.accessToken !== 'null') {
        const datos = {
          tokenAccess: user.accessToken,
          tokenRefresh: user.tokenRefresh
        }
        return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.CREATED, datos: datos });
      } else {
        const CREDENCIALES_INCORRECTAS = await this.i18n.translate(codIdioma.concat('.CREDENCIALES_INCORRECTAS'), {
          lang: codIdioma
        });
        return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.UNAUTHORIZED, token: null, mensaje: CREDENCIALES_INCORRECTAS });

      }
    } catch (error) {
      if (error?.codigoNombre) {
        const MENSAJE = await this.i18n.translate(codIdioma.concat(`.${error.codigoNombre}`), {
          lang: codIdioma
        });
        return this.funcion.enviarRespuestaOptimizada({ codigoEstado: error.codigo, mensaje: MENSAJE });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: error.message
        });
      }
    }
  }

}



