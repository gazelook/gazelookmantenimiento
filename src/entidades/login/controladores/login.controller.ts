import { Controller, Request, Get, Post, UseGuards, Body, Header, Headers, HttpStatus } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiResponse, ApiTags, ApiBody, ApiHeader, ApiSecurity, ApiOperation, ApiBearerAuth } from '@nestjs/swagger';
import { LoginService } from '../casos_de_uso/login.service';
import { LoginDto } from '../dto/login-dto';
import { LocalAuthGuard } from '../drivers/local-auth.guard';
import { I18nService } from 'nestjs-i18n'
import { Funcion } from 'src/shared/funcion';
import { estadosUsuario } from '../../../shared/enum-sistema';
import { RetornoValidarCuentaDto } from '../dto/retorno-validar-cuenta.dto';

@ApiTags('Autenticación')
@Controller('api/auth')
export class LoginController {

  constructor(
    private readonly authService: LoginService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion

  ) { }

  // funcion = new Funcion(this.i18n);

  @ApiBody({ type: LoginDto })
  @ApiOperation({ summary: 'se debe enviar usuario y contraseña para obtencion de token, el contrasena es el email' })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas', required: true })
  @ApiHeader({ name: 'idioma', description: 'codigo del idioma. Ejm: es, en, fr' })
  @ApiResponse({ status: 201, type: RetornoValidarCuentaDto, description: 'created' })
  @ApiResponse({ status: 401, description: 'Unauthorized' })
  @UseGuards(LocalAuthGuard)
  @Post('/iniciar-sesion')
  async login(@Request() req, @Headers() headers) {
    let codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    
    try {
      const os = require('os');
      
      // console.log("hostnamePeticion:", req.hostname);
      // console.log("os.homedir():", os.homedir());
      // console.log("os.hostname():", os.hostname());
      
      /* 
      
      console.log("os.networkInterfaces():", os.networkInterfaces());
      console.log("os.type(): ",os.type());
      console.log("userAgent: headers['user-agent'],: ", headers['user-agent']) */

      let dispo = {
        ip: req.ip,
        userAgent: headers['user-agent'],
      }
      // console.log('codIdioma: ', codIdioma)
      // console.log('req.user: ', req.user)
      const user = await this.authService.login(req.user, codIdioma, dispo);

      if (user.accessToken !== 'null') {
        let mensaje;
        if (user.usuario.estado.codigo === estadosUsuario.inactivaPago) {
          const INACTIVA_PAGO = await this.i18n.translate(codIdioma.concat('.INACTIVA_PAGO'), {
            lang: codIdioma
          });
          mensaje = INACTIVA_PAGO
        }
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          mensaje: mensaje,
          datos: {
            usuario: user.usuario,
            tokenAccess: user.accessToken,
            tokenRefresh: user.tokenRefresh,
            rolSistema: user.rolSistema
          }
        });
        // return this.funcion.enviarRespuesta(HttpStatus.OK, null, {
        //   usuario: user.usuario,
        //   tokenAccess: user.accessToken,
        //   tokenRefresh: user.tokenRefresh,
        //   pago: user.pago || "OK"
        // });
      } else {
        const CREDENCIALES_INCORRECTAS = await this.i18n.translate(codIdioma.concat('.CREDENCIALES_INCORRECTAS'), {
          lang: codIdioma
        });
        return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.UNAUTHORIZED, mensaje: CREDENCIALES_INCORRECTAS });
      }
    } catch (e) {
      if (e?.codigoNombre) {
        const MENSAJE = await this.i18n.translate(codIdioma.concat(`.${e.codigoNombre}`), {
          lang: codIdioma
        });
        return this.funcion.enviarRespuestaOptimizada({ codigoEstado: e.codigo, mensaje: MENSAJE });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message
        });
      }
    }
  }
}