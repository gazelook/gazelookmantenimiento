import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsNotEmpty } from 'class-validator';
import { NodeCompatibleEventEmitter } from "rxjs/internal/observable/fromEvent";



export class LoginDto {
    @ApiProperty()
    @IsNotEmpty()
    email: string;
    @ApiProperty()
    @IsNotEmpty()
    contrasena: string;
}