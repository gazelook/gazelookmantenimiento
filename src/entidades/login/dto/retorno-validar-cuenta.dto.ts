import { Matches, IsNotEmpty, IsMongoId } from 'class-validator';
import { IsEmail } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class TraduccionPerfil {

  @ApiProperty()
  _id: string
  @ApiProperty()
  nombre: string
  @ApiProperty()
  descripcion: string
}

export class PerfilUsuario {

  @ApiProperty()
  _id: string
  @ApiProperty({description: 'username o una alias del usuario'})
  nombre: string
  @ApiProperty({description: 'nombre del usuario'})
  nombreContacto: string
  @ApiProperty({description: 'estado de perfil de usuario'})
  estado: string
}

export class Perfil {

  @ApiProperty()
  _id: string
  @ApiProperty()
  codigo: string
  @ApiProperty()
  traducciones: string
  @ApiProperty()
  perfil: PerfilUsuario
}

export class RetornoValidarCuentaDto {

  @ApiProperty()
  perfil: Perfil
  @ApiProperty()
  tokenAccess: string
  @ApiProperty()
  tokenRefresh: string
}
