import { EliminarDispositivoService } from './../../dispositivos/casos_de_uso/eliminar-dispositivo.service';
import { Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';

@Injectable()
export class LogoutService {
  constructor(
    private eliminarDispositivoService: EliminarDispositivoService

  ) { }


  async logout(idUsuario: string, idDispositivo: string) {
   
    const session = await mongoose.startSession()
    await session.startTransaction();

    try {
      const opts = { session };

      // const usuario = await  this.obtenerIdUsuarioService.obtenerUsuarioById(idUsuario);
      const dispositivo = this.eliminarDispositivoService.eliminarDispositivo(idDispositivo, idUsuario, opts);

      await session.commitTransaction();
      session.endSession();

      return dispositivo;
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw error;
    }

  }





}