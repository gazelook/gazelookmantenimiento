import { LogoutService } from './logout.service';
import { UsuarioServicesModule } from './../../usuario/casos_de_uso/usuario-services.module';
import { LocalStrategy } from './../drivers/local.strategy';
import { JwtStrategy } from './../drivers/jwt.strategy';

import { LoginService } from './login.service';
import { RefrescarTokenService } from './refrescar-token.service';
import { tokenProviders } from './../drivers/token.provider';

import { Module } from '@nestjs/common';
import { DBModule } from 'src/drivers/db_conection/db.module';

import { CatalogosServiceModule } from 'src/entidades/catalogos/casos_de_uso/catalogos-services.module';
import { HistoricoServiceModule } from 'src/entidades/historico/casos_de_uso/historico-services.module';

import { JwtModule } from '@nestjs/jwt';
import { EmailServicesModule } from '../../emails/casos_de_uso/email-services.module';
import { DispositivoServicesModule } from '../../dispositivos/casos_de_uso/dispositivo-services.module';
import { ConfigService } from '../../../config/config.service';
import { expiracionToken } from '../../../shared/enum-sistema';



@Module({
  imports: [
    DBModule,
    JwtModule.registerAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => {
        return {
          secret: configService.get<string>('JWT_SECRET'),
          signOptions: {
            expiresIn: expiracionToken.segundos,
          },
        };
      },
    }),
    CatalogosServiceModule,
    HistoricoServiceModule,
    DispositivoServicesModule,
    EmailServicesModule,
    UsuarioServicesModule

  ],
  providers: [
    ...tokenProviders,
    RefrescarTokenService,
    LogoutService,
    LoginService,
    JwtStrategy,
    LocalStrategy,


  ],
  exports: [
    RefrescarTokenService,
    LogoutService,
    LoginService,
    RefrescarTokenService,
    LogoutService,
    LoginService,
    JwtStrategy,
    LocalStrategy


  ],
  controllers: [],
})
export class LoginServicesModule { }
