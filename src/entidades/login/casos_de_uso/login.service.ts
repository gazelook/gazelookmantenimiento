import { CrearDispositivoService } from './../../dispositivos/casos_de_uso/crear-dispositivo.service';
import { CatalogoEntidadService } from './../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoAccionService } from './../../catalogos/casos_de_uso/catalogo-accion.service';
import { CrearHistoricoService } from './../../historico/casos_de_uso/crear-historico.service';
import { Injectable, Inject, HttpStatus } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { TokenInterface } from 'src/drivers/mongoose/interfaces/token_usuario/token.interface';
import { Model } from 'mongoose';
import { uid } from 'rand-token';
import { ObtenerIdUsuarioService } from '../../usuario/casos_de_uso/obtener-id-usuario.service';
import { codigoEntidades, codigosCatalogoRol, estadosUsuario, nombreAcciones, nombreEntidades, nombreRolSistema } from '../../../shared/enum-sistema';
import * as mongoose from 'mongoose';
import { erroresGeneral, erroresLogin } from '../../../shared/enum-errores';
import { ObtenerUsuarioLoginService } from '../../usuario/casos_de_uso/obtener-usuario-login.service';
import { ObtenerPerfilUsuarioService } from '../../usuario/casos_de_uso/obtener-perfil-usuario.service';
import { Usuario } from '../../../drivers/mongoose/interfaces/usuarios/usuario.interface';

@Injectable()
export class LoginService {
  constructor(private readonly loginService: ObtenerUsuarioLoginService, private readonly jwtService: JwtService,
    @Inject('TOKEN_USUARIO_MODEL') private readonly tokenUsuarioModel: Model<TokenInterface>,
    private obtenerIdUsuarioService: ObtenerIdUsuarioService,
    private readonly perfilService: ObtenerPerfilUsuarioService,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private dispositivoService: CrearDispositivoService,
  ) { }

  async validateUser(email: string, pass: string): Promise<Usuario> {
    const user = await this.loginService.encontrarUsuario(email);

    if (user) {
      const isMatch = await bcrypt.compare(pass, user.contrasena);
      if (isMatch) {
        return user;
      }
    }
    return null;
  }



  async login(user: any, codIdioma: any, dispositivo: any, opts?: any) {

    let session;
    let optsLocal;
    if (!opts) {
      session = await mongoose.startSession()
      await session.startTransaction();
    }

    try {
      if (!opts) {
        optsLocal = true;
        opts = { session }
      }

      this.validarEstadoUsuario(user);

      const accion = await this.catalogoAccionService.obtenerNombreAccion(nombreAcciones.crear)
      let getAccion = accion.codigo;

      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(nombreEntidades.tokenUsuario)

      const usuario = await this.obtenerIdUsuarioService.obtenerUsuario(user._id, opts);


      let ingreso = false;
      
      if (user._id != undefined) {

        if (usuario) {
          if (usuario.rolSistema.length > 0) {
            if (usuario.rolSistema[0] === null) {
              session.endSession();
              return { accessToken: 'null' };
            }
            const arrayRolSistema = [];
            let objRolSistema: any;
            for (const getUserRol of usuario.rolSistema) {
              const nomRolSistema = getUserRol.nombre;
              const rol = getUserRol.rol;
              if (rol == codigosCatalogoRol.administrador
                || rol == codigosCatalogoRol.coordinadorGazelook
                || rol == codigosCatalogoRol.administradorAnuncios
                || rol == codigosCatalogoRol.administradorTraductorAnuncios
                || rol == codigosCatalogoRol.administradorFinca
                || rol == codigosCatalogoRol.administradorProgramadores
                || rol == codigosCatalogoRol.administradorVoluntariado
                || rol == codigosCatalogoRol.alojamientoPersonalOficina
                || rol == codigosCatalogoRol.contabilidad
                || rol == codigosCatalogoRol.subAdministradorGeneral
                || rol == codigosCatalogoRol.subAdministradorProgramadores
              ) {
                ingreso = true;
                const arrayRolEspecifico = [];
                let objRolEspecifico: any;
                if (getUserRol.rolesEspecificos.length > 0) {

                  for (const getRolEspecifico of getUserRol.rolesEspecificos) {
                    const getEntidad = getRolEspecifico.entidad;
                    const getRol = getRolEspecifico.rol;
                    objRolEspecifico = {
                      rol: getRol,
                      entidad: getEntidad
                    };
                    if (getRolEspecifico.acciones.length > 0) {
                      const arrayAcciones = [];
                      let objAcciones: any;
                      for (const getAcciones of getRolEspecifico.acciones) {
                        const getCodigoAcciones = getAcciones.codigo;
                        arrayAcciones.push(getCodigoAcciones);
                      }
                      objRolEspecifico.acciones = arrayAcciones;
                    }
                    arrayRolEspecifico.push(objRolEspecifico)
                  }
                }
                objRolSistema = {
                  nombre: nomRolSistema,
                  rol:rol,
                  rolesEspecificos: arrayRolEspecifico
                }
                arrayRolSistema.push(objRolSistema);

              }
            }

            if (ingreso) {
              const idDispositivo = new mongoose.mongo.ObjectId();
              const payload = {
                // id: user._id,
                email: user.email,
                dispositivo: idDispositivo
              };

              const token = this.jwtService.sign(payload);
              const tokenRefresh = uid(256);
              const tokenUsuario = {
                emailUsuario: user.email,
                //  usuario: user._id,
                token: token,
                tokenRefresh: tokenRefresh,

              }

              const saveToken = await new this.tokenUsuarioModel(tokenUsuario).save(opts);
              const dataTokens = JSON.parse(JSON.stringify(saveToken))
              delete dataTokens.fechaCreacion
              delete dataTokens.fechaActualizacion
              delete dataTokens.__v

              let newHistoricoTokens: any = {
                datos: dataTokens,
                usuario: user._id,
                accion: getAccion,
                entidad: entidad.codigo
              }


              this.crearHistoricoService.crearHistoricoServer(newHistoricoTokens);

              let getPerfil = await this.perfilService.obtenerDatosUsuarioWithPerfiles(user._id);


              const result: any = {
                accessToken: token,
                tokenRefresh: tokenRefresh,
                usuario: getPerfil,
                rolSistema: arrayRolSistema
              }

              if (dispositivo) {
                dispositivo._id = idDispositivo;
                dispositivo.tokenUsuario = saveToken._id;
                const dispo = await this.dispositivoService.crearDispositivo(user, dispositivo, opts);
                result.usuario.dispositivos = [
                  {
                    _id: dispo._id
                  }
                ]
              }

              // FINISH TRANSACTION
              if (optsLocal) {
                await session.commitTransaction();
                await session.endSession();
              }

              return result;
            } else {
              session.endSession();
              return { accessToken: 'null' };
            }

          } else {
            session.endSession();
            return { accessToken: 'null' };
          }
        } else {
          session.endSession();
          return { accessToken: 'null' };
        }

      } else {
        session.endSession();
        return { accessToken: 'null' };
      }
    } catch (error) {
      if (optsLocal) {
        await session.abortTransaction();
        session.endSession();
      }
      throw error;
    }
  }

  // verifica los usuarios con el pago en estado pendiente

  validarEstadoUsuario(usuario) {
    /* if (usuario.estado === estadosUsuario.activaNoVerificado){
      throw { codigo: HttpStatus.UNAUTHORIZED, codigoNombre: erroresLogin.VALIDAR_CUENTA };
    } */

    if (usuario.estado === estadosUsuario.rechazadoResponsable) {
      throw { codigo: HttpStatus.UNAUTHORIZED, codigoNombre: erroresLogin.CUENTA_RECHAZADA_RESPONSABLE };
    }

    /* if (usuario.estado === estadosUsuario.inactivaPago) {
      throw { codigo: HttpStatus.UNAUTHORIZED, codigoNombre: erroresLogin.INACTIVA_PAGO };
    } */

    if (usuario.estado === estadosUsuario.inactivaPagoPaymentez) {
      throw {
        codigo: HttpStatus.UNAUTHORIZED,
        codigoNombre: erroresGeneral.NO_AUTORIZADO,
      };
    }
  }



}