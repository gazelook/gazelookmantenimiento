
import { Injectable, Inject, HttpStatus } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { TokenInterface } from 'src/drivers/mongoose/interfaces/token_usuario/token.interface';
import { Model } from 'mongoose';
import { uid } from 'rand-token';
import * as mongoose from 'mongoose';
import { ObtenerDispositivoUsuarioService } from '../../dispositivos/casos_de_uso/obtener-dispositivo-usuario.service';
import { estadosDispositivo } from '../../../shared/enum-sistema';
import { erroresGeneral } from '../../../shared/enum-errores';

@Injectable()
export class RefrescarTokenService {
  constructor(
    private readonly jwtService: JwtService,
    @Inject('TOKEN_USUARIO_MODEL') private readonly tokenUsuarioModel: Model<TokenInterface>,
    private obtenerDispositivoUsuarioService: ObtenerDispositivoUsuarioService
  ) { }


  async refreshToken(dataToken: TokenInterface) {

    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    try {

      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      const userToken = await this.tokenUsuarioModel.findOne({ tokenRefresh: dataToken.tokenRefresh });
      const getDispositivo = await this.obtenerDispositivoUsuarioService.getDispositivoByIdToken(userToken._id);

      if (getDispositivo.estado === estadosDispositivo.eliminado) {
        throw { codigo: HttpStatus.NOT_ACCEPTABLE, codigoNombre: erroresGeneral.ERROR_ACTUALIZAR };
      }

      if (userToken) {

        const payload = {
          email: userToken.emailUsuario,
          dispositivo: getDispositivo._id
        };
        const token = this.jwtService.sign(payload);
        const tokenRefresh = uid(256);
        const dataUpdate = {
          token: token,
          tokenRefresh: tokenRefresh,
        }
        await this.tokenUsuarioModel.findByIdAndUpdate(userToken._id, dataUpdate, opts);

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return { accessToken: token, tokenRefresh: tokenRefresh };

      } else {
        //Aborta la transaccion
        await session.abortTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return { accessToken: 'null' };
      }
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error
    }
  }

}