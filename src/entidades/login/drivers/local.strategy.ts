import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { LoginService } from '../casos_de_uso/login.service';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly authService: LoginService) {
    super({
      usernameField: 'email',
      passwordField: 'contrasena',
    });
  }

  async validate(email: string, contrasena: string) {

    const user = await this.authService.validateUser(email, contrasena);
    if (!user) {
      throw new UnauthorizedException('not allow');
    }
    return user;
  }
}