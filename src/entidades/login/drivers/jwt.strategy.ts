import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';

import { ObtenerIdUsuarioService } from 'src/entidades/usuario/casos_de_uso/obtener-id-usuario.service';
import { ObtenerDispositivoUsuarioService } from '../../dispositivos/casos_de_uso/obtener-dispositivo-usuario.service';
import { estadosDispositivo } from '../../../shared/enum-sistema';
import { ValidaRolUsuarioService } from 'src/entidades/usuario/casos_de_uso/validar-rol-usuario.service';

import { ConfigService } from '../../../config/config.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {


  constructor(private readonly obIdUsService: ObtenerIdUsuarioService,
    private readonly obtenerDispositivoUsuarioService: ObtenerDispositivoUsuarioService,
    private validaRolUsuarioService: ValidaRolUsuarioService,
    private config: ConfigService
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: config.get<string>('JWT_SECRET'),
    });

  }

  async validate(payload: any) {

    console.log('payload: ', payload)
    //Obtiene usuario por el email
    const user = await this.obIdUsService.obtenerUsuarioByEmail(payload.email);

    //Obtiene dispositivo
    const dispositivo = await this.obtenerDispositivoUsuarioService.getDispositivoById(payload.dispositivo);

    //Valida que exista el usuario
    if (user) {
      //Valida rol administrador
      let validaRolAdmin;
      validaRolAdmin = await this.validaRolUsuarioService.validarRolAdmin(user);
      if (validaRolAdmin && dispositivo.estado === estadosDispositivo.activo) {
        return { user };
      }
    }

    return null;
  }
}


