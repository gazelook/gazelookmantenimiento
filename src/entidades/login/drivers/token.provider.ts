import { traduccionNoticiaSchema } from '../../../drivers/mongoose/modelos/traduccion_noticias/traduccion-noticias-modelo';
import { Connection } from 'mongoose';
import { noticiaModelo } from 'src/drivers/mongoose/modelos/noticias/noticia-schema';
import { tokenUsuarioModelo } from 'src/drivers/mongoose/modelos/token_usuario/token_usuario.schema';

export const tokenProviders = [
    {
      provide: 'TOKEN_USUARIO_MODEL',
      useFactory: (connection: Connection) => connection.model('token_usuario', tokenUsuarioModelo, 'token_usuario'),
      inject: ['DB_CONNECTION']
    },
 
  ];
