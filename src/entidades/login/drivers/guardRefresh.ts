import { ExtractJwt } from 'passport-jwt';
import { Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { TokenExpiredError } from 'jsonwebtoken';
import { ObtenerIdUsuarioService } from 'src/entidades/usuario/casos_de_uso/obtener-id-usuario.service';
@Injectable()
export class GuardRefresh extends AuthGuard('jwt') {
    JwtService: any;
    constructor(private readonly obIdUsService: ObtenerIdUsuarioService) {
        super();
    }


    handleRequest(err, user, info: Error) {

        if (info instanceof TokenExpiredError) {
            // do stuff when token is expired
            // const a = this.JwtService.sign('hola'

        }

        return user;
    }
}
