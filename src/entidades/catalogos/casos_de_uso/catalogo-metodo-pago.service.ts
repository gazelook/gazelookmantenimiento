import {
  Inject,
  Injectable,
  HttpException,
  HttpStatus,
  ConflictException,
} from '@nestjs/common';
import { Model } from 'mongoose';
import { CatalogoMetodoPago } from '../../../drivers/mongoose/interfaces/catalogo_metodo_pago/catalogo-metodo-pago.interface';
import { CatalogoEstadoService } from './catalogo-estado.service';
import { CatalogoEntidadService } from './catalogo-entidad.service';
import { async } from 'rxjs';
import { codIdiomas, estadoMetodosPago, estadosTraduccionMetodosPago, nombrecatalogoEstados, nombreEntidades } from 'src/shared/enum-sistema';
import { ActivaDesactivaCatalogoMetodoPagoDto, CambiaNombreCatalogoMetodoPagoDto } from '../dto/catalogo-metodo-pago.dto';
import { CatalogoIdiomasService } from './catalogo-idiomas.service';
import { TraduccionCatalogoMetodoPago } from 'src/drivers/mongoose/interfaces/traduccion_catalogo_metodo_pago/traduccion-catalogo-metodo-pago.interface';
const mongoose = require('mongoose');

@Injectable()
export class CatalogoMetodoPagoService {

  constructor(
    @Inject('CATALOGO_METODO_PAGO')
    private catalogoMetodoPagoModel: Model<CatalogoMetodoPago>,
    @Inject('CATALOGO_TRADUCCION_METODO_PAGO')
    private readonly catalogoTraduccionMetodoPagoModel: Model<TraduccionCatalogoMetodoPago>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoIdiomasService: CatalogoIdiomasService

  ) { }

  async obtenerListaMetodoPago(codigoIdioma: string): Promise<any> {
    //Obtiene la entidad
    const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
      nombreEntidades.metodoPago,
    );

    const estadoActivo: any = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.activa,
      entidad.codigo,
    );
    const estadoPendiente: any = await this.catalogoEstadoService.obtenerNombreEstado(
      nombrecatalogoEstados.pendiente,
      entidad.codigo,
    );

    try {
      console.log('codigoIdioma: ', codigoIdioma)
      let catalogoMetodoPago = await this.catalogoMetodoPagoModel
        .find({ estado: { $in: [estadoActivo.codigo, estadoPendiente.codigo] } })
        .select('-fechaActualizacion -fechaCreacion')
        .populate({
          path: 'traducciones',
          select: 'nombre descripcion idioma ',
          match: {
            idioma: codigoIdioma,
          },
        })
        .populate({
          path: 'icono',
          select: 'principal -_id',
          populate: { path: 'principal', select: 'url -_id' },
        });

      let result: any = [];
      for (let index = 0; index < catalogoMetodoPago.length; index++) {
        result.push(catalogoMetodoPago[index]);
        let catMetodoPago = await this.catalogoEstadoService.obtenerEstadoByCodigo(catalogoMetodoPago[index].estado);
        result[index].estado = catMetodoPago;
      };
      return result;
    } catch (error) {
      throw error;
    }
  }

  async existeTraduccionMetodoPago(codigoIdioma: string): Promise<any> {
    try {
      const existeTraducion = await this.catalogoTraduccionMetodoPagoModel.findOne(
        { idioma: codigoIdioma },
      );
      return existeTraducion ? true : false;
    } catch (error) {
      throw error;
    }
  }

  //Activar/Desactivar metodos de pago
  async activaDesactivaMetodoPago(activaDesactiva: ActivaDesactivaCatalogoMetodoPagoDto, codIdioma: string): Promise<any> {

    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    //Obtiene el idioma por el codigo
    const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(codIdioma);
    let codigoIdioma = idioma.codigo;

    try {

      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      let catalogoMetodoPago = await this.catalogoMetodoPagoModel
        .findOne({ codigo: activaDesactiva.codigo })
        .select('-fechaActualizacion -fechaCreacion')
        .populate({
          path: 'traducciones',
          select: 'nombre descripcion idioma ',
          match: {
            idioma: codigoIdioma,
          },
        })
        .populate({
          path: 'icono',
          select: 'principal -_id',
          populate: { path: 'principal', select: 'url -_id' },
        });

        console.log('catalogoMetodoPago: ', catalogoMetodoPago)
      if (catalogoMetodoPago) {
        if (catalogoMetodoPago.estado === estadoMetodosPago.activa) {
          //Actualiza el estado del metodo de pago a desactivado
          await this.catalogoMetodoPagoModel.updateOne({ _id: catalogoMetodoPago._id },
            { estado: estadoMetodosPago.desactivado, fechaActualizacion: new Date() }, opts);
        } else if (catalogoMetodoPago.estado === estadoMetodosPago.desactivado) {
          //Actualiza el estado del metodo de pago a activo
          await this.catalogoMetodoPagoModel.updateOne({ _id: catalogoMetodoPago._id },
            { estado: estadoMetodosPago.activa, fechaActualizacion: new Date() }, opts);
        }

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return true;

      } else {
        //Finaliza la transaccion
        await session.endSession();
        return false
      }

    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }

  //Actualiza el nombre de un metodo de pago
  async modificaNombreMetodoPago(cambiaNombreCatalogoMetodoPago: CambiaNombreCatalogoMetodoPagoDto, codIdioma: string): Promise<any> {

    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    //Obtiene el idioma por el codigo
    const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(codIdioma);
    let codigoIdioma = idioma.codigo;

    try {

      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      let catalogoMetodoPago = await this.catalogoMetodoPagoModel
        .findOne({ codigo: cambiaNombreCatalogoMetodoPago.codigo })
        .select('-fechaActualizacion -fechaCreacion')
        .populate({
          path: 'traducciones',
          select: 'nombre descripcion idioma ',
          match: {
            idioma: codigoIdioma,
          },
        })
        .populate({
          path: 'icono',
          select: 'principal -_id',
          populate: { path: 'principal', select: 'url -_id' },
        });

      if (catalogoMetodoPago) {
        console.log('catalogoMetodoPago: ', catalogoMetodoPago)
        //Actualiza el estado del metodo de pago
        await this.catalogoTraduccionMetodoPagoModel.updateMany({ codigo: cambiaNombreCatalogoMetodoPago.codigo },
          { $set: { estado: estadosTraduccionMetodosPago.eliminado }, fechaActualizacion: new Date() }, opts);

        let arrayTraducciones = [];
        for (const metodoPago of cambiaNombreCatalogoMetodoPago.traducciones) {

          let original = false;
          if (metodoPago.idioma.codigo === codIdiomas.ingles) {
            original = true;
          }
          let descripcion: string;
          if (!metodoPago.descripcion) {
            descripcion = metodoPago.nombre;
          }else{
            descripcion = metodoPago.descripcion;
          }

          let obMetodoPago = {
            codigo: cambiaNombreCatalogoMetodoPago.codigo,
            nombre: metodoPago.nombre,
            descripcion: descripcion,
            idioma: metodoPago.idioma.codigo,
            original: original,
            estado: estadosTraduccionMetodosPago.activa,
            fechaCreacion: new Date(),
            fechaActualizacion: new Date(),
          }

          //Guarda la traduccion
          const creartraduccion = await new this.catalogoTraduccionMetodoPagoModel(obMetodoPago).save(opts);
          arrayTraducciones.push(creartraduccion)

        }
        //Actualiza el estado del metodo de pago
        await this.catalogoMetodoPagoModel.updateOne({ codigo: cambiaNombreCatalogoMetodoPago.codigo },
          { $set: { traducciones: arrayTraducciones }, fechaActualizacion: new Date() }, opts);

        //Confirma los cambios de la transaccion
        await session.commitTransaction();
        //Finaliza la transaccion
        await session.endSession();
        return true;

      } else {
        //Finaliza la transaccion
        await session.endSession();
        return false
      }

    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }

}
