import { Inject, Injectable, HttpException, HttpStatus, ConflictException, BadRequestException } from '@nestjs/common';
import { Model } from 'mongoose'
import { CatalogoTipoArchivo } from 'src/drivers/mongoose/interfaces/catalogo_tipo_archivo/catalogo-tipo-archivo.interface';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import { CatalogoArchivoDefault } from 'src/drivers/mongoose/interfaces/catalogo_archivo_default/catalogo-archivo-default.interface';

@Injectable()
export class CatalogoArchivoDefaultService {

  constructor(@Inject('CATALOGO_ARCHIVO_DEFAULT') private readonly catalogoArchivoDefault: Model<CatalogoArchivoDefault>) { }

  async obtenerCatalogoArchivoDefault(): Promise<any> {

    try {
      const catalogoidiomas = await this.catalogoArchivoDefault.find({},{fechaCreacion:0, fechaActualizacion:0, estado:0});

      return catalogoidiomas;

    } catch (error) {
      throw error;
    }

  }
}