import { Inject, Injectable } from '@nestjs/common';

import { Model } from 'mongoose'
import { CatalogoConfiguracion } from 'src/drivers/mongoose/interfaces/catalogo_configuracion/catalogo-configuracion.interface';
import { CatalogoPorcentajeEsperaFondos } from 'src/drivers/mongoose/interfaces/catalogo_porcentaje_espera_fondos/catalogo-porcentaje-espera-fondos.interface';
import { CatalogoPorcentajeProyecto } from 'src/drivers/mongoose/interfaces/catalogo_porcentaje_proyecto/catalogo-porcentaje-proyecto.interface';
import { codigosEstadosCatalogoPorcentajeEsperaFondos, codigosEstadosCatalogoPorcentajeProyecto } from 'src/shared/enum-sistema';
import { ActualizarCatalogoPorcentajeEsperaFondosDto } from '../dto/catalogo-porcentaje-espera-fondos';

const mongoose = require('mongoose');

@Injectable()
export class CatalogoPorcentajeEsperaFondosService {

  constructor(@Inject('CATALOGO_PORCENTAJE_ESPERA_FONDOS_MODEL') private readonly catalogoPorcentajeEsperaFondosModel: Model<CatalogoPorcentajeEsperaFondos>,
  ) { }

  async obtenerCatalogoPorcentajeEsperaFondos(): Promise<any> {

    try {
      const catalogoPorcentajeEsperaFondos = await this.catalogoPorcentajeEsperaFondosModel.findOne({ estado: codigosEstadosCatalogoPorcentajeEsperaFondos.activa });

      return catalogoPorcentajeEsperaFondos;

    } catch (error) {
      throw error;
    }
  }

  async actualizarCatalogoPorcentajeEsperaFondos(actualizarCatalogoPorcentajeEsperaFondosDto: ActualizarCatalogoPorcentajeEsperaFondosDto): Promise<any> {

    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    try {

      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      //Obtiene el catalogo porcentaje espera de fondos por el codigo
      let getCatalogo = await this.catalogoPorcentajeEsperaFondosModel.findOne({ _id: actualizarCatalogoPorcentajeEsperaFondosDto._id, estado: codigosEstadosCatalogoPorcentajeEsperaFondos.activa });
      if (!getCatalogo) {
        //Obtiene el catalogo porcentaje espera de fondos por el ID
        let getCatalogoByCodigo = await this.catalogoPorcentajeEsperaFondosModel.find({ codigo: actualizarCatalogoPorcentajeEsperaFondosDto.codigo });
        if (getCatalogoByCodigo.length > 0) {
          for (const catalogo of getCatalogoByCodigo) {
            //Actualizo el estado a eliminado logico
            await this.catalogoPorcentajeEsperaFondosModel.updateOne({ _id: catalogo._id }, { estado: codigosEstadosCatalogoPorcentajeEsperaFondos.eliminado }, opts);
          }
        }

      } else {
        //Actualizo el estado a eliminado logico
        await this.catalogoPorcentajeEsperaFondosModel.updateOne({ _id: actualizarCatalogoPorcentajeEsperaFondosDto._id }, { estado: codigosEstadosCatalogoPorcentajeEsperaFondos.eliminado }, opts);
      }

      //Objeto de catalogo porcentaje espera de fondos
      let objCatalogoPorcentajeEsperaFondos = {
        codigo: actualizarCatalogoPorcentajeEsperaFondosDto.codigo,
        porcentaje: actualizarCatalogoPorcentajeEsperaFondosDto.porcentaje,
        estado: codigosEstadosCatalogoPorcentajeEsperaFondos.activa,
        fechaCreacion: new Date(),
        fechaActualizacion: null
      }

      //Crea el nuevo registro del catalogo porcentaje espera de fondos
      const crearCatalogo = await new this.catalogoPorcentajeEsperaFondosModel(objCatalogoPorcentajeEsperaFondos).save(opts);

      //Confirma los cambios de la transaccion
      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();
      return crearCatalogo;

    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }


}