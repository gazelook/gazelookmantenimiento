import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CatalogoTipoMoneda } from 'src/drivers/mongoose/interfaces/catalogo_tipo_moneda/catalogo-tipo-moneda.interface';
import { CatalogoIdiomasService } from './catalogo-idiomas.service';
import { codIdiomas } from 'src/shared/enum-sistema';


@Injectable()
export class CatalogoTipoMonedaService {

  constructor(@Inject('CATALOGO_TIPO_MONEDA') private readonly catalogoTipoMonedaModel: Model<CatalogoTipoMoneda>,
    private catalogoIdiomasService: CatalogoIdiomasService) { }

  async obtenerCatalogoTipoMoneda(codIdioma): Promise<any> {

    try {

      //Obtener el codigo del idioma
      const idioma = await this.catalogoIdiomasService.obtenerIdiomaByCodigoNombre(codIdioma)
      let codigoIdioma = idioma.codigo;

      //Obtiene todos los tipos de monedas en el idioma en que se envia
      const catalogoTipoMoneda = await this.catalogoTipoMonedaModel.find({ idioma: codigoIdioma }, { '_id': 0, 'codigo': 1, 'nombre': 1, 'codNombre': 1, 'idioma': 1, 'predeterminado': 1 }).sort('nombre');
      if (catalogoTipoMoneda.length > 0) {

        return catalogoTipoMoneda;

      } else {
        //Obtiene todos los tipos de monedas en idioma ingles por defecto
        const catalogoTipoMoneda = await this.catalogoTipoMonedaModel.find({ idioma: codIdiomas.ingles }, { '_id': 0, 'codigo': 1, 'nombre': 1, 'codNombre': 1, 'idioma': 1, 'predeterminado': 1 }).sort('nombre');
        return catalogoTipoMoneda;
      }

    } catch (error) {
      throw error;
    }

  }

  async obtenerCatalogoTipoMonedaByCodNombre(codNombre: string): Promise<any> {

    try {
      //Obtiene la moneda segun su codigo nombre (USD, EUR, etc)
      const catalogoTipoMoneda = await this.catalogoTipoMonedaModel.findOne({ codNombre: codNombre }, { 'codNombre': 1 });
      return catalogoTipoMoneda;
    } catch (error) {
      throw error;
    }
  }

}