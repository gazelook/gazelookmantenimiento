import { CatalogoAccion } from './../../../drivers/mongoose/interfaces/accion/accion.interface';
import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';

@Injectable() 
export class CatalogoAccionService {

  constructor(@Inject('ACCION_MODEL') private readonly accionModel: Model<CatalogoAccion>) { }

  async obtenerNombreAccion(nombreAccion: string): Promise<any> {
    const nombreAc= await this.accionModel.findOne({ nombre: nombreAccion});
    return nombreAc;
  }

  async obtenerAcciones(): Promise<any> {
    const acciones= await this.accionModel.find({ }).select('_id codigo nombre');
    return acciones;
  }

}
