
import { CatalogoIdiomasService } from './catalogo-idiomas.service';
import { Module } from '@nestjs/common';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { CatalogoProviders } from '../drivers/catalogo.provider';
import { CatalogoAccionService } from './catalogo-accion.service';
import { CatalogoEntidadService } from './catalogo-entidad.service';
import { CatalogoEstadoService } from './catalogo-estado.service';
import { CatalogoArchivoDefaultService } from './catalogo-archivo-default.service';
import { CatalogoTipoEmailService } from './catalogo-tipo-email.service';
import { CatalogoPorcentajeProyectoService } from './catalogo-porcentaje-proyecto.service';
import { CatalogoPorcentajeFinanciacionService } from './catalogo-porcentaje-financiacion.service';
import { CatalogoPorcentajeEsperaFondosService } from './catalogo-porcentaje-espera-fondos.service';
import { CatalogoTipoMonedaService } from './catalogo-tipo-moneda.service';
import { CatalogoMetodoPagoService } from './catalogo-metodo-pago.service';


@Module({
  imports: [DBModule],
  providers: [
    ...CatalogoProviders,
    CatalogoIdiomasService,
    CatalogoAccionService,
    CatalogoEntidadService,
    CatalogoEstadoService,
    CatalogoArchivoDefaultService,
    CatalogoTipoEmailService,
    CatalogoPorcentajeProyectoService,
    CatalogoPorcentajeFinanciacionService,
    CatalogoPorcentajeEsperaFondosService,
    CatalogoTipoMonedaService,
    CatalogoMetodoPagoService
  ],
  exports: [
    CatalogoIdiomasService,
    CatalogoAccionService,
    CatalogoEntidadService,
    CatalogoEstadoService,
    CatalogoArchivoDefaultService,
    CatalogoTipoEmailService,
    CatalogoPorcentajeProyectoService,
    CatalogoPorcentajeFinanciacionService,
    CatalogoPorcentajeEsperaFondosService,
    CatalogoTipoMonedaService,
    CatalogoMetodoPagoService
  ],
  controllers: [],
})
export class CatalogosServiceModule {}
