import { Inject, Injectable } from '@nestjs/common';

import { Model } from 'mongoose'
import { CatalogoConfiguracion } from 'src/drivers/mongoose/interfaces/catalogo_configuracion/catalogo-configuracion.interface';
import { CatalogoPorcentajeProyecto } from 'src/drivers/mongoose/interfaces/catalogo_porcentaje_proyecto/catalogo-porcentaje-proyecto.interface';
import { codigosEstadosCatalogoPorcentajeProyecto } from 'src/shared/enum-sistema';
import { ActualizaCatalogoPorcentajeProyectoDto } from '../dto/catalogo-porcentaje-proyecto';

const mongoose = require('mongoose');

@Injectable()
export class CatalogoPorcentajeProyectoService {

  constructor(@Inject('CATALOGO_PORCENTAJE_PROYECTO_MODEL') private readonly catalogoPorcentajeProyectoModel: Model<CatalogoPorcentajeProyecto>,
  ) { }

  //Obtiene el catalogo porcentaje proyecto
  async obtenerCatalogoPorcentajeProyecto(): Promise<any> {

    try {
      const catalogoPorcentajeProyecto = await this.catalogoPorcentajeProyectoModel.find({ estado: codigosEstadosCatalogoPorcentajeProyecto.activa }).sort('prioridad');

      return catalogoPorcentajeProyecto;

    } catch (error) {
      throw error;
    }
  }

  //Actualiza el catalogo porcentaje proyecto
  async actualizarCatalogoPorcentajeProyecto(actualizaCatalogoPorcentajeProyectoDto: ActualizaCatalogoPorcentajeProyectoDto): Promise<any> {

    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    try {

      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      //Obtiene el catalogo porcentaje proyecto por el ID
      let getCatalogo = await this.catalogoPorcentajeProyectoModel.findOne({ _id: actualizaCatalogoPorcentajeProyectoDto._id, estado: codigosEstadosCatalogoPorcentajeProyecto.activa });
      let getTipo;
      let getPrioridad;
      if (!getCatalogo) {
        //Obtiene el catalogo porcentaje proyecto por el codigo
        let getCatalogoByCodigo = await this.catalogoPorcentajeProyectoModel.find({ codigo: actualizaCatalogoPorcentajeProyectoDto.codigo });
        if (getCatalogoByCodigo.length > 0) {
          for (const catalogo of getCatalogoByCodigo) {
            getTipo = catalogo.tipo;
            getPrioridad = catalogo.prioridad;
            //Actualizo el estado a eliminado logico
            await this.catalogoPorcentajeProyectoModel.updateOne({ _id: catalogo._id }, { estado: codigosEstadosCatalogoPorcentajeProyecto.eliminado }, opts);
          }
        }
      } else {
        getTipo = getCatalogo.tipo;
        getPrioridad = getCatalogo.prioridad;
        //Actualizo el estado a eliminado logico
        await this.catalogoPorcentajeProyectoModel.updateOne({ _id: actualizaCatalogoPorcentajeProyectoDto._id }, { estado: codigosEstadosCatalogoPorcentajeProyecto.eliminado }, opts);
      }


      //Objeto de catalogo porcentaje proyecto
      let objCatalogoPorcentajeProyecto = {
        codigo: actualizaCatalogoPorcentajeProyectoDto.codigo,
        tipo: getTipo,
        porcentaje: actualizaCatalogoPorcentajeProyectoDto.porcentaje,
        prioridad: getPrioridad,
        estado: codigosEstadosCatalogoPorcentajeProyecto.activa,
        fechaCreacion: new Date(),
        fechaActualizacion: null
      }

      //Crea el nuevo registro del catalogo porcentaje proyecto
      const crearCatalogo = await new this.catalogoPorcentajeProyectoModel(objCatalogoPorcentajeProyecto).save(opts);

      //Confirma los cambios de la transaccion
      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();
      return crearCatalogo;

    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }


}