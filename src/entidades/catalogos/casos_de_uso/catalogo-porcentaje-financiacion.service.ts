import { Inject, Injectable } from '@nestjs/common';

import { Model } from 'mongoose'
import { CatalogoConfiguracion } from 'src/drivers/mongoose/interfaces/catalogo_configuracion/catalogo-configuracion.interface';
import { CatalogoPorcentajeFinanciacion } from 'src/drivers/mongoose/interfaces/catalogo_porcentaje_financiacion/catalogo-porcentaje-financiacion.interface';
import { CatalogoPorcentajeProyecto } from 'src/drivers/mongoose/interfaces/catalogo_porcentaje_proyecto/catalogo-porcentaje-proyecto.interface';
import { codigosEstadosCatalogoPorcentajeFinanciacion, codigosEstadosCatalogoPorcentajeProyecto } from 'src/shared/enum-sistema';
import { ActualizaCatalogoPorcentajeFinanciacionDto } from '../dto/catalogo-porcentaje-financiacion';

const mongoose = require('mongoose');

@Injectable()
export class CatalogoPorcentajeFinanciacionService {

  constructor(@Inject('CATALOGO_PORCENTAJE_FINANCIACION_MODEL') private readonly catalogoPorcentajeFinanciacionModel: Model<CatalogoPorcentajeFinanciacion>,
  ) { }

  async obtenerCatalogoPorcentajeFinanciacion(): Promise<any> {

    try {
      //Obtiene los tipos de colores por el codigo
      const catalogoPorcentajeFinanciacion = await this.catalogoPorcentajeFinanciacionModel.find({ estado: codigosEstadosCatalogoPorcentajeFinanciacion.activa }).sort('prioridad');

      return catalogoPorcentajeFinanciacion;

    } catch (error) {
      throw error;
    }
  }

  async actualizaCatalogoPorcentajeFinanciacion(actualizaCatalogoPorcentajeFinanciacionDto: ActualizaCatalogoPorcentajeFinanciacionDto): Promise<any> {


    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    try {

      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      //Obtiene el catalogo porcentaje proyecto por el ID
      let getCatalogo = await this.catalogoPorcentajeFinanciacionModel.findOne({ _id: actualizaCatalogoPorcentajeFinanciacionDto._id, estado: codigosEstadosCatalogoPorcentajeFinanciacion.activa });
      let getPrioridad;
      if (!getCatalogo) {
        //Obtiene el catalogo porcentaje proyecto por el codigo
        let getCatalogoByCodigo = await this.catalogoPorcentajeFinanciacionModel.find({ codigo: actualizaCatalogoPorcentajeFinanciacionDto.codigo});
        if (getCatalogoByCodigo.length > 0) {
          for (const catalogo of getCatalogoByCodigo) {
            getPrioridad = catalogo.prioridad;
            //Actualizo el estado a eliminado logico
            await this.catalogoPorcentajeFinanciacionModel.updateOne({ _id: catalogo._id }, { estado: codigosEstadosCatalogoPorcentajeFinanciacion.eliminado }, opts);
          }
        }
      } else {
        getPrioridad = getCatalogo.prioridad;
        //Actualizo el estado a eliminado logico
        await this.catalogoPorcentajeFinanciacionModel.updateOne({ _id: actualizaCatalogoPorcentajeFinanciacionDto._id }, { estado: codigosEstadosCatalogoPorcentajeFinanciacion.eliminado }, opts);

      }

      //Objeto de catalogo porcentaje financiacion
      let objCatalogoPorcentajeFinanciacion = {
        codigo: actualizaCatalogoPorcentajeFinanciacionDto.codigo,
        porcentaje: actualizaCatalogoPorcentajeFinanciacionDto.porcentaje,
        prioridad: getPrioridad,
        estado: codigosEstadosCatalogoPorcentajeFinanciacion.activa,
        fechaCreacion: new Date(),
        fechaActualizacion: null
      }

      //Crea el nuevo registro del catalogo porcentaje financiacion
      const crearCatalogo = await new this.catalogoPorcentajeFinanciacionModel(objCatalogoPorcentajeFinanciacion).save(opts);

      //Confirma los cambios de la transaccion
      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();
      return crearCatalogo;

    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }

}