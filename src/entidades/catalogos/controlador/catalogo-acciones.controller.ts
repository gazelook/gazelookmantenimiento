import { ApiResponse, ApiTags, ApiOperation, ApiHeader } from '@nestjs/swagger';
import { Controller, Post, Body, Headers, Get, HttpStatus, Put, UseGuards } from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import { CatalogoArchivoDefaultService } from '../casos_de_uso/catalogo-archivo-default.service';
import { CatalogoArchivoDefaultDto } from '../dto/catalogo-archivo-default.dto';
import { Funcion } from 'src/shared/funcion';
import { CatalogoPorcentajeProyectoService } from '../casos_de_uso/catalogo-porcentaje-proyecto.service';
import { CatalogoPorcentajeFinanciacionService } from '../casos_de_uso/catalogo-porcentaje-financiacion.service';
import { CatalogoPorcentajeEsperaFondosService } from '../casos_de_uso/catalogo-porcentaje-espera-fondos.service';
import { ActualizarCatalogoPorcentajeEsperaFondosDto, CatalogoPorcentajeEsperaFondosDto } from '../dto/catalogo-porcentaje-espera-fondos';
import { AuthGuard } from '@nestjs/passport';
import { CatalogoEntidadService } from '../casos_de_uso/catalogo-entidad.service';
import { CatalogoEntidadesDto } from '../dto/catalogo-entidades.dto';
import { CatalogoAccionService } from '../casos_de_uso/catalogo-accion.service';

@ApiTags('Catalogos')
@Controller('api/catalogos')
export class CatalogoAccionesControlador {

    constructor(
        private readonly catalogoAccionService: CatalogoAccionService,
        private readonly i18n: I18nService
    ) { }
    funcion = new Funcion(this.i18n);

    @Get('/catalogo-acciones')
    @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
    @ApiHeader({ name: 'idioma', description: 'codigo del idioma. Ejm: es, en, fr' })
    @ApiResponse({
        status: 200,
        type: CatalogoEntidadesDto
    })
    @ApiOperation({ summary: 'Este método devuelve el catalogo de acciones' })
    @ApiResponse({ status: 404, description: 'Error al obtener el catalogo' })
    @UseGuards(AuthGuard('jwt'))

    public async obtenerCatalogoAcciones(@Headers() headers) {
        //Verifica si el idioma llega vacio
        const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
        try {

            const catalogoEntidades = await this.catalogoAccionService.obtenerAcciones();

            return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.OK, datos: catalogoEntidades });

        } catch (e) {
            return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR, mensaje: e.message });
        }
    }
}

