import { ApiResponse, ApiTags, ApiOperation, ApiHeader } from '@nestjs/swagger';
import { Controller, Post, Body, Headers, Get, Put, HttpStatus, UseGuards } from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import { CatalogoArchivoDefaultService } from '../casos_de_uso/catalogo-archivo-default.service';
import { CatalogoArchivoDefaultDto } from '../dto/catalogo-archivo-default.dto';
import { Funcion } from 'src/shared/funcion';
import { CatalogoPorcentajeProyectoService } from '../casos_de_uso/catalogo-porcentaje-proyecto.service';
import { CatalogoPorcentajeFinanciacionService } from '../casos_de_uso/catalogo-porcentaje-financiacion.service';
import { ActualizaCatalogoPorcentajeFinanciacionDto, CatalogoPorcentajeFinanciacionDto } from '../dto/catalogo-porcentaje-financiacion';
import { AuthGuard } from '@nestjs/passport';

@ApiTags('Catalogos')
@Controller('api/catalogos')
export class CatalogoPorcentajeFinanciacionControlador {

    constructor(private readonly catalogoPorcentajeFinanciacionService: CatalogoPorcentajeFinanciacionService,
        private readonly i18n: I18nService
    ) { }
    funcion = new Funcion(this.i18n);

    @Get('/catalogo-porcentaje-financiacion')
    @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
    @ApiHeader({ name: 'idioma', description: 'codigo del idioma. Ejm: es, en, fr' })
    @ApiResponse({ status: 200, type: CatalogoPorcentajeFinanciacionDto })
    @ApiOperation({ summary: 'Este método devuelve el catálogo porcentaje de financiacion' })
    @ApiResponse({ status: 404, description: 'Error al obtener el catalogo' })
    @UseGuards(AuthGuard('jwt'))

    public async obtenerCatalogoPorcentajeFinanciacion(@Headers() headers) {
        //Verifica si el idioma llega vacio
        const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
        try {

            const catalogoPorcentajeFinanciacion = await this.catalogoPorcentajeFinanciacionService.obtenerCatalogoPorcentajeFinanciacion();
            if (!catalogoPorcentajeFinanciacion || catalogoPorcentajeFinanciacion.length === 0) {
                return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.OK, mensaje: await this.funcion.obtenerTraduccionEstatica(codIdioma, 'ERROR_OBTENER') });
            } else {
                return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.OK, datos: catalogoPorcentajeFinanciacion });
            }
        } catch (e) {
            return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR, mensaje: e.message });

        }

    }


    @Put('/catalogo-porcentaje-financiacion')
    @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
    @ApiHeader({ name: 'idioma', description: 'codigo del idioma. Ejm: es, en, fr' })
    @ApiResponse({ status: 200, type: CatalogoPorcentajeFinanciacionDto })
    @ApiOperation({ summary: 'Este metodo actualiza el catálogo porcentaje de financiacion según los parámetros enviados' })
    @ApiResponse({ status: 404, description: 'Error al actualizar' })
    @UseGuards(AuthGuard('jwt'))
    
    public async actualizaCatalogoPorcentajeFinanciacion(@Headers() headers, @Body() actualizaCatalogoPorcentajeFinanciacionDto: ActualizaCatalogoPorcentajeFinanciacionDto) {
        //Verifica si el idioma llega vacio
        const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
        try {

            const catalogoPorcentajeFinanciacion = await this.catalogoPorcentajeFinanciacionService.actualizaCatalogoPorcentajeFinanciacion(actualizaCatalogoPorcentajeFinanciacionDto);
            if (!catalogoPorcentajeFinanciacion) {
                return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.CONFLICT, mensaje: await this.funcion.obtenerTraduccionEstatica(codIdioma, 'ERROR_ACTUALIZAR') });
            } else {
                return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.OK, datos: catalogoPorcentajeFinanciacion });
            }
        } catch (e) {
            return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR, mensaje: e.message });

        }

    }

}

