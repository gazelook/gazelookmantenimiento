import { ApiResponse, ApiTags, ApiOperation, ApiHeader } from '@nestjs/swagger';
import { Controller, Post, Body, Headers, Get, HttpStatus, Put, UseGuards } from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { AuthGuard } from '@nestjs/passport';
import { CatalogoEntidadService } from '../casos_de_uso/catalogo-entidad.service';
import { CatalogoEntidadesDto } from '../dto/catalogo-entidades.dto';

@ApiTags('Catalogos')
@Controller('api/catalogos')
export class CatalogoEntidadesControlador {

    constructor(private readonly catalogoEntidadService: CatalogoEntidadService,
        private readonly i18n: I18nService
    ) { }
    funcion = new Funcion(this.i18n);

    @Get('/catalogo-entidades')
    @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
    @ApiHeader({ name: 'idioma', description: 'codigo del idioma. Ejm: es, en, fr' })
    @ApiResponse({
        status: 200,
        type: CatalogoEntidadesDto
    })
    @ApiOperation({ summary: 'Este método devuelve el catalogo de entidades' })
    @ApiResponse({ status: 404, description: 'Error al obtener el catalogo' })
    @UseGuards(AuthGuard('jwt'))

    public async obtenerCatalogoEntidades(@Headers() headers) {
        //Verifica si el idioma llega vacio
        const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
        try {

            const catalogoEntidades = await this.catalogoEntidadService.obtenerAllEntidades();

            return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.OK, datos: catalogoEntidades });

        } catch (e) {
            return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR, mensaje: e.message });
        }
    }
}

