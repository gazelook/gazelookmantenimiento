import { ApiResponse, ApiTags, ApiOperation, ApiHeader } from '@nestjs/swagger';
import { Controller, Post, Body, Headers, Get, HttpStatus, Put, UseGuards } from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import { CatalogoArchivoDefaultService } from '../casos_de_uso/catalogo-archivo-default.service';
import { CatalogoArchivoDefaultDto } from '../dto/catalogo-archivo-default.dto';
import { Funcion } from 'src/shared/funcion';
import { CatalogoPorcentajeProyectoService } from '../casos_de_uso/catalogo-porcentaje-proyecto.service';
import { CatalogoPorcentajeFinanciacionService } from '../casos_de_uso/catalogo-porcentaje-financiacion.service';
import { CatalogoPorcentajeEsperaFondosService } from '../casos_de_uso/catalogo-porcentaje-espera-fondos.service';
import { ActualizarCatalogoPorcentajeEsperaFondosDto, CatalogoPorcentajeEsperaFondosDto } from '../dto/catalogo-porcentaje-espera-fondos';
import { AuthGuard } from '@nestjs/passport';

@ApiTags('Catalogos')
@Controller('api/catalogos')
export class CatalogoPorcentajeEsperaFondosControlador {

    constructor(private readonly catalogoPorcentajeEsperaFondosService: CatalogoPorcentajeEsperaFondosService,
        private readonly i18n: I18nService
    ) { }
    funcion = new Funcion(this.i18n);

    @Get('/catalogo-porcentaje-espera-fondos')
    @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
    @ApiHeader({ name: 'idioma', description: 'codigo del idioma. Ejm: es, en, fr' })
    @ApiResponse({ status: 200, type: CatalogoPorcentajeEsperaFondosDto })
    @ApiOperation({ summary: 'Este método devuelve el catalogo porcentaje de espera de fondos' })
    @ApiResponse({ status: 404, description: 'Error al obtener el catalogo' })
    @UseGuards(AuthGuard('jwt'))

    public async obtenerCatalogoPorcentajeEsperaFondos(@Headers() headers) {
        //Verifica si el idioma llega vacio
        const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
        try {

            const catalogoPorcentajeFinanciacion = await this.catalogoPorcentajeEsperaFondosService.obtenerCatalogoPorcentajeEsperaFondos();
            if (!catalogoPorcentajeFinanciacion || catalogoPorcentajeFinanciacion.length === 0) {
                return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.OK, mensaje: await this.funcion.obtenerTraduccionEstatica(codIdioma, 'ERROR_OBTENER') });
            } else {
                return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.OK, datos: catalogoPorcentajeFinanciacion });
            }
        } catch (e) {
            return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR, mensaje: e.message });
        }
    }

    @Put('/catalogo-porcentaje-espera-fondos')
    @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
    @ApiHeader({ name: 'idioma', description: 'codigo del idioma. Ejm: es, en, fr' })
    @ApiResponse({ status: 200, type: CatalogoPorcentajeEsperaFondosDto })
    @ApiOperation({ summary: 'Este metodo actualiza el catálogo porcentaje de espera de fondos según los parámetros enviados' })
    @ApiResponse({ status: 404, description: 'Error al actualizar' })
    @UseGuards(AuthGuard('jwt'))
    public async actualizarCatalogoPorcentajeEsperaFondos(@Headers() headers, @Body() actualizarCatalogoPorcentajeEsperaFondosDto: ActualizarCatalogoPorcentajeEsperaFondosDto) {
        //Verifica si el idioma llega vacio
        const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
        try {

            const catalogoPorcentajeEspeaFondos = await this.catalogoPorcentajeEsperaFondosService.actualizarCatalogoPorcentajeEsperaFondos(actualizarCatalogoPorcentajeEsperaFondosDto);
            if (!catalogoPorcentajeEspeaFondos) {
                return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.CONFLICT, mensaje: await this.funcion.obtenerTraduccionEstatica(codIdioma, 'ERROR_ACTUALIZAR') });
            } else {
                return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.OK, datos: catalogoPorcentajeEspeaFondos });
            }
        } catch (e) {
            return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR, mensaje: e.message });
        }
    }
}

