import { ApiResponse, ApiTags, ApiOperation, ApiHeader } from '@nestjs/swagger';
import { Controller, Post, Body, Headers, Get, Put, HttpStatus, UseGuards } from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { CatalogoPorcentajeProyectoService } from '../casos_de_uso/catalogo-porcentaje-proyecto.service';
import { ActualizaCatalogoPorcentajeProyectoDto, CatalogoPorcentajeProyectoDto } from '../dto/catalogo-porcentaje-proyecto';
import { AuthGuard } from '@nestjs/passport';

@ApiTags('Catalogos')
@Controller('api/catalogos')
export class CatalogoPorcentajeProyectoControlador {

    constructor(private readonly catalogoPorcentajeProyectoService: CatalogoPorcentajeProyectoService,
        private readonly i18n: I18nService
    ) { }
    funcion = new Funcion(this.i18n);

    @Get('/catalogo-porcentaje-proyecto')
    @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
    @ApiHeader({ name: 'idioma', description: 'codigo del idioma. Ejm: es, en, fr' })
    @ApiResponse({ status: 200, type: CatalogoPorcentajeProyectoDto })
    @ApiOperation({ summary: 'Este metodo devuelve el catalogo porcentaje proyecto' })
    @ApiResponse({ status: 404, description: 'Error al obtener el catalogo' })
    @UseGuards(AuthGuard('jwt'))
    
    public async obtenerCatalogoPorcentajeProyecto(@Headers() headers) {
        
        //Verifica si el idioma llega vacio
        const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
        try {

            const catalogoPorcentajeProyecto = await this.catalogoPorcentajeProyectoService.obtenerCatalogoPorcentajeProyecto();
            if (!catalogoPorcentajeProyecto || catalogoPorcentajeProyecto.length === 0) {
                return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.OK, mensaje: await this.funcion.obtenerTraduccionEstatica(codIdioma, 'ERROR_OBTENER') });
            } else {
                return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.OK, datos: catalogoPorcentajeProyecto });
            }
        } catch (e) {
            return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR, mensaje: e.message });
        }
    }


    @Put('/catalogo-porcentaje-proyecto')
    @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
    @ApiHeader({ name: 'idioma', description: 'codigo del idioma. Ejm: es, en, fr' })
    @ApiResponse({ status: 200, type: CatalogoPorcentajeProyectoDto})
    @ApiOperation({ summary: 'Este método actualiza el catálogo porcentaje proyecto según los parámetros enviados' })
    @ApiResponse({ status: 404, description: 'Error al actualizar' })
    @UseGuards(AuthGuard('jwt'))

    public async actualizarCatalogoPorcentajeProyecto(@Headers() headers, @Body() actualizaCatalogoPorcentajeProyectoDto: ActualizaCatalogoPorcentajeProyectoDto) {
        //Verifica si el idioma llega vacio
        const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
        try {

            const catalogoPorcentajeProyecto = await this.catalogoPorcentajeProyectoService.actualizarCatalogoPorcentajeProyecto(actualizaCatalogoPorcentajeProyectoDto);
            if (!catalogoPorcentajeProyecto) {
                return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.CONFLICT, mensaje: await this.funcion.obtenerTraduccionEstatica(codIdioma, 'ERROR_ACTUALIZAR') });
            } else {
                return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.OK, datos: catalogoPorcentajeProyecto });
            }
        } catch (e) {
            return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR, mensaje: e.message });
        }
    }
}

