import { Module, Delete } from '@nestjs/common';

import { CatalogosServiceModule } from '../casos_de_uso/catalogos-services.module';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { CatalogoPorcentajeProyectoControlador } from './catalogo-porcentaje-proyecto.controller';
import { CatalogoPorcentajeFinanciacionControlador } from './catalogo-porcentaje-financiacion.controller';
import { CatalogoPorcentajeEsperaFondosControlador } from './catalogo-porcentaje-espera-fondos.controller';
import { CatalogoEntidadesControlador } from './catalogo-entidades.controller';
import { CatalogoAccionesControlador } from './catalogo-acciones.controller';
import { CatalogoMetodoPagoControlador } from './catalogo-metodo-pago.controller';


@Module({
  imports: [CatalogosServiceModule],
  providers: [TraduccionEstaticaController],
  exports: [],
  controllers: [
    CatalogoPorcentajeProyectoControlador,
    CatalogoPorcentajeFinanciacionControlador,
    CatalogoPorcentajeEsperaFondosControlador,
    CatalogoEntidadesControlador,
    CatalogoAccionesControlador,
    CatalogoMetodoPagoControlador
  ],
})
export class CatalogosControllerModule { }
