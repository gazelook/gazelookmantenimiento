import { ApiResponse, ApiTags, ApiOperation, ApiHeader } from '@nestjs/swagger';
import { Controller, Post, Body, Headers, Get, HttpStatus, Put, UseGuards } from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import { AuthGuard } from '@nestjs/passport';
import { CatalogoEntidadService } from '../casos_de_uso/catalogo-entidad.service';
import { CatalogoEntidadesDto } from '../dto/catalogo-entidades.dto';
import { Funcion } from 'src/shared/funcion';
import { CatalogoMetodoPagoService } from '../casos_de_uso/catalogo-metodo-pago.service';
import { ActivaDesactivaCatalogoMetodoPagoDto, CambiaNombreCatalogoMetodoPagoDto } from '../dto/catalogo-metodo-pago.dto';

@ApiTags('Catalogos')
@Controller('api/catalogos')
export class CatalogoMetodoPagoControlador {

    constructor(private readonly catalogoMetodoPagoService: CatalogoMetodoPagoService,
        private readonly i18n: I18nService
    ) { }
    funcion = new Funcion(this.i18n);

    @Put('/catalogo-metodo-pago/activa-desactiva')
    @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
    @ApiHeader({ name: 'idioma', description: 'codigo del idioma. Ejm: es, en, fr' })
    @ApiResponse({
        status: 200,
        type: CatalogoEntidadesDto
    })
    @ApiOperation({ summary: 'Este método activa/desactiva un metodo de pago' })
    @ApiResponse({ status: 200, description: 'Actualización correcta' })
    @ApiResponse({ status: 404, description: 'Error al actualizar' })
    @ApiResponse({ status: 406, description: 'Parámetros no validos' })
    @ApiResponse({ status: 401, description: 'No autorizado' })
    @UseGuards(AuthGuard('jwt'))

    public async activaDesactivaMetodoPago(
        @Headers() headers,
        @Body() activaDesactiva: ActivaDesactivaCatalogoMetodoPagoDto
    ) {

        console.log('crontrollerrrrr')
        //Verifica si el idioma llega vacio
        const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
        try {
            if (activaDesactiva.codigo) {
                const metodoPago = await this.catalogoMetodoPagoService.activaDesactivaMetodoPago(activaDesactiva, codIdioma);

                if (metodoPago) {
                    const ACTUALIZACION_CORRECTA = await this.i18n.translate(codIdioma.concat('.ACTUALIZACION_CORRECTA'), {
                        lang: codIdioma
                    });
                    return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.OK, mensaje: ACTUALIZACION_CORRECTA })

                } else {
                    const ERROR_ACTUALIZAR = await this.i18n.translate(codIdioma.concat('.ERROR_ACTUALIZAR'), {
                        lang: codIdioma
                    });
                    return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.NOT_FOUND, mensaje: ERROR_ACTUALIZAR })

                }
            } else {
                const PARAMETROS_NO_VALIDOS = await this.i18n.translate(codIdioma.concat('.PARAMETROS_NO_VALIDOS'), {
                    lang: codIdioma
                });
                return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.NOT_ACCEPTABLE, mensaje: PARAMETROS_NO_VALIDOS })
            }



        } catch (e) {
            return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR, mensaje: e.message });
        }
    }

    @Put('/catalogo-metodo-pago/actualiza-traducciones')
    @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
    @ApiHeader({ name: 'idioma', description: 'codigo del idioma. Ejm: es, en, fr' })
    @ApiResponse({
        status: 200,
        type: CatalogoEntidadesDto
    })
    @ApiOperation({ summary: 'Este método actualiza las traducciones de un metodo de pago' })
    @ApiResponse({ status: 200, description: 'Actualización correcta' })
    @ApiResponse({ status: 404, description: 'Error al actualizar' })
    @ApiResponse({ status: 406, description: 'Parámetros no validos' })
    @ApiResponse({ status: 401, description: 'No autorizado' })
    @UseGuards(AuthGuard('jwt'))

    public async modificaNombreMetodoPago(
        @Headers() headers,
        @Body() cambiaNombreCatalogoMetodoPago: CambiaNombreCatalogoMetodoPagoDto
    ) {
        //Verifica si el idioma llega vacio
        const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
        try {
            if (cambiaNombreCatalogoMetodoPago.codigo && cambiaNombreCatalogoMetodoPago.traducciones.length === 6) {
                const metodoPago = await this.catalogoMetodoPagoService.modificaNombreMetodoPago(cambiaNombreCatalogoMetodoPago, codIdioma);

                if (metodoPago) {
                    const ACTUALIZACION_CORRECTA = await this.i18n.translate(codIdioma.concat('.ACTUALIZACION_CORRECTA'), {
                        lang: codIdioma
                    });
                    return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.OK, mensaje: ACTUALIZACION_CORRECTA })

                } else {
                    const ERROR_ACTUALIZAR = await this.i18n.translate(codIdioma.concat('.ERROR_ACTUALIZAR'), {
                        lang: codIdioma
                    });
                    return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.NOT_FOUND, mensaje: ERROR_ACTUALIZAR })

                }
            } else {
                const PARAMETROS_NO_VALIDOS = await this.i18n.translate(codIdioma.concat('.PARAMETROS_NO_VALIDOS'), {
                    lang: codIdioma
                });
                return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.NOT_ACCEPTABLE, mensaje: PARAMETROS_NO_VALIDOS })
            }



        } catch (e) {
            return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR, mensaje: e.message });
        }
    }
}

