import { TipoComentarioModelo } from './../../../drivers/mongoose/modelos/catalogo_tipo_comentario/catalogo-tipo-comentario.schema';
import { TraduccionCatalogoMetodoPagoModelo } from '../../../drivers/mongoose/modelos/traduccion_catalogo_metodo_pago/traduccion-catalogo-metodo-pago.schema';
import { CatalogoSuscripcionModelo } from './../../../drivers/mongoose/modelos/catalogo_suscripcion/catalogo_suscripcion.schema';
import { Connection } from 'mongoose';
import { CatalogoMediaModelo } from '../../../drivers/mongoose/modelos/catalogoMedia/catalogoMediaModelo';
import { catalogoAlbumModelo } from '../../../drivers/mongoose/modelos/catalogo_album/catalogo-album.schema';
import { CatalogoTipoMediaModelo } from '../../../drivers/mongoose/modelos/catalogoTipoMedia/catalogoTipoMediaModelo';
import { catalogoIdiomasModelo } from '../../../drivers/mongoose/modelos/catalogoIdiomas/catalogoIdiomasModelo';
import { accionModelo } from '../../../drivers/mongoose/modelos/catalogoAccion/catalogoAccionModelo';
import { entidadesModelo } from '../../../drivers/mongoose/modelos/catalogoEntidades/catalogoEntidadesModelo';
import { CatalogoMetodoPagoModelo } from '../../../drivers/mongoose/modelos/catalogo_metodo_pago/catalogo-metodo-pago.schema';
import { CatalogoTipoEmailSchema } from 'src/drivers/mongoose/modelos/catalogo_tipo_email/catalogoTipoEmail';
import { CatalogoArchivoDefaultModelo } from 'src/drivers/mongoose/modelos/catalogo_archivo_default/catalogo-archivo-default.schema';
import { TipoMonedaModelo } from 'src/drivers/mongoose/modelos/catalogo_tipo_moneda/catalogo-tipo-moneda.schema';
import { CatalogoTipoArchivoModelo } from 'src/drivers/mongoose/modelos/catalogo_tipo_archivo/catalogo-tipo-archivo.schema';
import { ConfiguracionEventoModelo } from 'src/drivers/mongoose/modelos/configuracion_evento/configuracion-evento.schema';
import { FormulaEventoModelo } from 'src/drivers/mongoose/modelos/formula_evento/formula-evento.schema';
import { CatalogoPorcentajeProyectoModelo } from 'src/drivers/mongoose/modelos/catalogo_porcentaje_proyecto/catalogo-porcentaje-proyecto.schema';
import { CatalogoPorcentajeFinanciacionModelo } from 'src/drivers/mongoose/modelos/catalogo_porcentaje_financiacion/catalogo-porcentaje-financiacion.schema';
import { CatalogoPorcentajeEsperaFondosModelo } from 'src/drivers/mongoose/modelos/catalogo_porcentaje_espera_fondos/catalogo-porcentaje-espera-fondos.schema';


export const CatalogoProviders = [
    {
        provide: 'ACCION_MODEL',
        useFactory: (connection: Connection) => connection.model('catalogo_acciones', accionModelo, 'catalogo_acciones'),
        inject: ['DB_CONNECTION']
    },
    {
        provide: 'ENTIDADES_MODEL',
        useFactory: (connection: Connection) => connection.model('catalogo_entidades', entidadesModelo, 'catalogo_entidades'),
        inject: ['DB_CONNECTION']
    },
    {
        provide: 'ESTADOS_MODEL',
        useFactory: (connection: Connection) => connection.model('catalogo_estados', entidadesModelo, 'catalogo_estados'),
        inject: ['DB_CONNECTION']
    },
    {
        provide: 'CATALOGO_IDIOMAS',
        useFactory: (connection: Connection) => connection.model('catalogo_idiomas', catalogoIdiomasModelo, 'catalogo_idiomas'),
        inject: ['DB_CONNECTION'],
    },
    {
        provide: 'CATALOGO_MEDIA',
        useFactory: (connection: Connection) => connection.model('catalogo_media', CatalogoMediaModelo, 'catalogo_media'),
        inject: ['DB_CONNECTION'],
    },
    {
        provide: 'CATALOGO_AlBUM',
        useFactory: (connection: Connection) => connection.model('catalogo_album', catalogoAlbumModelo, 'catalogo_album'),
        inject: ['DB_CONNECTION'],
    },
    {
        provide: 'CATALOGO_TIPO_MEDIA',
        useFactory: (connection: Connection) => connection.model('catalogo_tipo_media', CatalogoTipoMediaModelo, 'catalogo_tipo_media'),
        inject: ['DB_CONNECTION'],
    },
    {
        provide: 'CATALOGO_METODO_PAGO',
        useFactory: (connection: Connection) => connection.model('catalogo_metodo_pago', CatalogoMetodoPagoModelo, 'catalogo_metodo_pago'),
        inject: ['DB_CONNECTION'],
    },
    {
        provide: 'CATALOGO_TRADUCCION_METODO_PAGO',
        useFactory: (connection: Connection) => connection.model('traduccion_catalogo_metodo_pago', TraduccionCatalogoMetodoPagoModelo, 'traduccion_catalogo_metodo_pago'),
        inject: ['DB_CONNECTION'],
    },
    {
        provide: 'CATALOGO_TIPO_ARCHIVO',
        useFactory: (connection: Connection) => connection.model('catalogo_tipo_archivo', CatalogoTipoArchivoModelo, 'catalogo_tipo_archivo'),
        inject: ['DB_CONNECTION'],
    },
    {
        provide: 'CATALOGO_SUSCRIPCION',
        useFactory: (connection: Connection) => connection.model('catalogo_suscripcion',CatalogoSuscripcionModelo , 'catalogo_suscripcion'),
        inject: ['DB_CONNECTION'],
    },
    {
        provide: 'TIPO_EMAIL_MODEL',
        useFactory: (connection: Connection) => connection.model('catalogo_tipo_email', CatalogoTipoEmailSchema, 'catalogo_tipo_email'),
        inject: ['DB_CONNECTION'],
    },
    {
        provide: 'CATALOGO_ARCHIVO_DEFAULT',
        useFactory: (connection: Connection) => connection.model('catalogo_archivo_default', CatalogoArchivoDefaultModelo, 'catalogo_archivo_default'),
        inject: ['DB_CONNECTION'],
    },

    {
        provide: 'CATALOGO_TIPO_COMENTARIO',
        useFactory: (connection: Connection) => connection.model('catalogo_tipo_comentario', TipoComentarioModelo, 'catalogo_tipo_comentario'),
        inject: ['DB_CONNECTION'],
    },
    {
        provide: 'CATALOGO_TIPO_MONEDA',
        useFactory: (connection: Connection) => connection.model('catalogo_tipo_moneda', TipoMonedaModelo, 'catalogo_tipo_moneda'),
        inject: ['DB_CONNECTION'],
    },
    {
        provide: 'CONFIGURACION_EVENTO_MODEL',
        useFactory: (connection: Connection) => connection.model('configuracion_evento', ConfiguracionEventoModelo, 'configuracion_evento'),
        inject: ['DB_CONNECTION'],
    },
    {
        provide: 'FORMULA_EVENTO_MODEL',
        useFactory: (connection: Connection) => connection.model('formula_evento', FormulaEventoModelo, 'formula_evento'),
        inject: ['DB_CONNECTION'],
    },
    {
        provide: 'CATALOGO_PORCENTAJE_PROYECTO_MODEL',
        useFactory: (connection: Connection) => connection.model('catalogo_porcentaje_proyecto', CatalogoPorcentajeProyectoModelo, 'catalogo_porcentaje_proyecto'),
        inject: ['DB_CONNECTION']
    },
    {
        provide: 'CATALOGO_PORCENTAJE_FINANCIACION_MODEL',
        useFactory: (connection: Connection) => connection.model('catalogo_porcentaje_financiacion', CatalogoPorcentajeFinanciacionModelo, 'catalogo_porcentaje_financiacion'),
        inject: ['DB_CONNECTION']
    },
    {
        provide: 'CATALOGO_PORCENTAJE_ESPERA_FONDOS_MODEL',
        useFactory: (connection: Connection) => connection.model('catalogo_porcentaje_espera_fondos', CatalogoPorcentajeEsperaFondosModelo, 'catalogo_porcentaje_espera_fondos'),
        inject: ['DB_CONNECTION']
    }


];
