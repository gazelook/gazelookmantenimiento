import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsString } from "class-validator";
import { NodeCompatibleEventEmitter } from "rxjs/internal/observable/fromEvent";

export class CatalogoTipoMonedaDto {

    @ApiProperty()
    codigo: string;
    @ApiProperty()
    nombre: string;
    @ApiProperty()
    codNombre: string;
    @ApiProperty()
    predeterminado: boolean;

}

export class CatalogoTipoMonedaCodigoDto {

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    codNombre: string;

}

export class CodCatalogoTipoMonedaDto {

    @ApiProperty({ required: false, description: 'Codigo del catalogo tipo moneda', example: 'TIPMON_1' })
    codigo: string;
}