import { ApiProperty } from "@nestjs/swagger";
import { NodeCompatibleEventEmitter } from "rxjs/internal/observable/fromEvent";

export class CatalogoPorcentajeProyectoDto {
   
    @ApiProperty()
    _id: string;
    @ApiProperty()
    codigo: string;
    @ApiProperty()
    tipo: string;
    @ApiProperty()
    porcentaje: number;
    @ApiProperty()
    prioridad: number;
    @ApiProperty()
    estado: string;
    @ApiProperty()
    fechaCreacion: Date;
    @ApiProperty()
    fechaActualizacion: Date;
    
}

export class ActualizaCatalogoPorcentajeProyectoDto {
   
    @ApiProperty()
    _id: string;
    @ApiProperty()
    codigo: string;
    @ApiProperty()
    porcentaje: number;
    
}