import { ApiProperty } from "@nestjs/swagger";

export class CodigosCatalogoAccionesDto {

    @ApiProperty({ type: String, description: 'ids de las acciones', required: true })
    _id: string;

}