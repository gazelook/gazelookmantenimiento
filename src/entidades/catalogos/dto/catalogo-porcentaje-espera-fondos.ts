import { ApiProperty } from "@nestjs/swagger";

export class CatalogoPorcentajeEsperaFondosDto {
   
    @ApiProperty()
    _id: string;
    @ApiProperty()
    codigo: string;
    @ApiProperty()
    porcentaje: number;
    @ApiProperty()
    estado: string;
    @ApiProperty()
    fechaCreacion: Date;
    @ApiProperty()
    fechaActualizacion: Date;
    
}

export class ActualizarCatalogoPorcentajeEsperaFondosDto {
   
    @ApiProperty()
    _id: string;
    @ApiProperty()
    codigo: string;
    @ApiProperty()
    porcentaje: number;
    
}