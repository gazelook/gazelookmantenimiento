import { ApiProperty } from "@nestjs/swagger";

export class CodigoCatalogoEntidadesDto {

    @ApiProperty({description: 'Codigo del catalogo entidades', required: true, example: "ENT_115"})
    codigo: string;

}

export class CatalogoEntidadesDto {

    @ApiProperty({description: 'ID del catalogo entidades', required: true, example: "ObjectId('5f524c2262150c3d97faf052')"})
    _id: string;
    @ApiProperty({description: 'Codigo del catalogo entidades', required: true, example: "ENT_76"})
    codigo: string;
    @ApiProperty({description: 'Nombre del catalogo entidades', required: true, example: "rolEntidad"})
    nombre: string;

}