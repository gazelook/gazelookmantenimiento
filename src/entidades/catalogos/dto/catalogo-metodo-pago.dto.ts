import { ApiProperty } from "@nestjs/swagger";
import { CodigoCatalogoIdiomasDto } from "./catalogo-idiomas.dto";

export class CatalogoMetodoPagoDto {

    @ApiProperty({description: 'Código catálogo generado automaticamente', required: false, example: 'METPAG_1 | METPAG_2'})
    codigo: string;

    @ApiProperty({description: 'Lista traducciones disponibles', required: true, example: '[METPAG_ES1 | METPAG_EN1 | METPAG_IT1]'})
    traducciones: string[];
    // @ApiProperty({description: 'Nombre catálogo', required: true, example: 'tajeta de credito | debito | tranferecia | trueque'})
    // nombre: string;
    // @ApiProperty({description: 'Descripción del método de pago', required: false})
    // descripcion: string;
    @ApiProperty({description: 'Código estado catálogo', required: true, example: 'EST_13(Activa) | EST_14(eliminado)'})
    estado: string;
    @ApiProperty({description: 'Refencia a la media del método de pago', required: true})
    icono: string;

}

export class TraduccionesCatalogoMetodoPagoDto {

    @ApiProperty({type: CodigoCatalogoIdiomasDto})
    idioma: CodigoCatalogoIdiomasDto;

    @ApiProperty({})
    nombre: string;

    @ApiProperty({})
    descripcion: string;

}

export class ActivaDesactivaCatalogoMetodoPagoDto {

    @ApiProperty({description: 'Código catálogo', required: true, example: 'METPAG_1'})
    codigo: string;


}

export class CambiaNombreCatalogoMetodoPagoDto {

    @ApiProperty({description: 'Código catálogo', required: true, example: 'METPAG_1'})
    codigo: string;

    @ApiProperty({type:[TraduccionesCatalogoMetodoPagoDto]})
    traducciones: Array<TraduccionesCatalogoMetodoPagoDto>;

}