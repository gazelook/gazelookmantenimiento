import { ApiProperty } from "@nestjs/swagger";
import { NodeCompatibleEventEmitter } from "rxjs/internal/observable/fromEvent";

export class CatalogoPorcentajeFinanciacionDto {
   
    @ApiProperty()
    _id: string;
    @ApiProperty()
    codigo: string;
    @ApiProperty()
    porcentaje: number;
    @ApiProperty()
    prioridad: number;
    @ApiProperty()
    estado: string;
    @ApiProperty()
    fechaCreacion: Date;
    @ApiProperty()
    fechaActualizacion: Date;
    
}

export class ActualizaCatalogoPorcentajeFinanciacionDto {
   
    @ApiProperty()
    _id: string;
    @ApiProperty()
    codigo: string;
    @ApiProperty()
    porcentaje: number;
    
}