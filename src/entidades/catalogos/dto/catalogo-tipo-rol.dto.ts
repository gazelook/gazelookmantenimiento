import { ApiProperty } from "@nestjs/swagger";

export class CodigoCatalogoTipoRolDto {

    @ApiProperty({description: 'Codigo del catalogo tipo Rol', required: true, example: "CATIPROL_1"})
    codigo: string;

}