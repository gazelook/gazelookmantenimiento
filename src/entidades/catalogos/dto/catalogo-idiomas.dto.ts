import { ApiProperty } from "@nestjs/swagger";
import { NodeCompatibleEventEmitter } from "rxjs/internal/observable/fromEvent";

export class CatalogoIdiomasDto {
   
    @ApiProperty()
    codigo: string;
    @ApiProperty()
    nombre: string;
    @ApiProperty()
    codNombre: string;
    @ApiProperty()
    idiomaSistema: boolean;
    
}

export class CodigoCatalogoIdiomasDto {
   
    @ApiProperty()
    codigo: string;
    
}