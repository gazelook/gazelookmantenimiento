import { ApiProperty } from "@nestjs/swagger";

export class CatalogoTipoMediaDto {

    @ApiProperty()
    codigo: string;
    @ApiProperty()
    nombre: string;
    @ApiProperty()
    estado: string;

}