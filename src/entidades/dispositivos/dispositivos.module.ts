import { DispositivoServicesModule } from './casos_de_uso/dispositivo-services.module';
import { Module } from '@nestjs/common';
import { DispositivoControllerModule } from './controladores/dispositivo-controller.module';
import { UsuarioModule } from '../usuario/usuario.module';
@Module({
    imports: [DispositivoControllerModule],
    providers: [DispositivoServicesModule],
    controllers: [],
    exports: []
})
export class DispositivosModule { }
