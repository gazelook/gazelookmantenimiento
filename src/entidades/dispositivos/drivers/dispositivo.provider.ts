import { dispositivoSchema } from './../../../drivers/mongoose/modelos/dispositivos/dispositivo.schema';


import { Connection } from 'mongoose';
import { tokenUsuarioModelo } from 'src/drivers/mongoose/modelos/token_usuario/token_usuario.schema';

export const dispositivoProviders = [
    {
      provide: 'DISPOSTIVO_MODEL',
      useFactory: (connection: Connection) => connection.model('dispositivo', dispositivoSchema),
      inject: ['DB_CONNECTION'],
    },
    {
      provide: 'TOKEN_USUARIO_MODEL',
      useFactory: (connection: Connection) =>
        connection.model('token_usuario', tokenUsuarioModelo, 'token_usuario'),
      inject: ['DB_CONNECTION'],
    },
  ];
