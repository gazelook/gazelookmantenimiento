import { DispositivoDto } from '../dto/dispositivo-dto';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { Controller, Post, Body, Res, HttpStatus, Get, Headers, Query} from '@nestjs/common';
import { CrearDispositivoService } from '../casos_de_uso/crear-dispositivo.service';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { Funcion } from 'src/shared/funcion';


@Controller('api/dispositivo')
export class CrearDispositivoControlador {

    constructor(private readonly crearDispositivoService: CrearDispositivoService,
        private readonly traduccionEstaticaController: TraduccionEstaticaController,
        private readonly funcion: Funcion) { }

    @Post('/crearDispositivo')
    @ApiResponse({ status: 201, description: 'Creado' })

    public async crearDispositivo(@Res() res, @Body() createDispoDTO: DispositivoDto, @Query() usuario, @Headers() headers) {
        const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma)
        
        const dispositivoCreado = await this.crearDispositivoService.crearDispositivo(usuario,createDispoDTO);

        try {

            if (!dispositivoCreado) {
                
                 //llama al metodo de traduccion estatica
                const ERROR_CREACION = await this.traduccionEstaticaController.traduccionEstatica(
                    codIdioma,
                    'ERROR_CREACION',
                );
                return this.funcion.enviarRespuestaOptimizada({codigoEstado : HttpStatus.NOT_FOUND, mensaje: ERROR_CREACION})
                
            } else {
                //llama al metodo de traduccion estatica
                const CREACION_CORRECTA = await this.traduccionEstaticaController.traduccionEstatica(
                    codIdioma,
                    'CREACION_CORRECTA',
                ); 
                return this.funcion.enviarRespuestaOptimizada({codigoEstado : HttpStatus.OK, mensaje: CREACION_CORRECTA})
                
            }

        } catch (e) {
            return this.funcion.enviarRespuestaOptimizada({codigoEstado : HttpStatus.INTERNAL_SERVER_ERROR, mensaje: e.message})
            
        }
    }

}

