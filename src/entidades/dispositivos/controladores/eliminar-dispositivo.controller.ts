import { EliminarDispositivoService } from './../casos_de_uso/eliminar-dispositivo.service';
import { DispositivoDto } from '../dto/dispositivo-dto';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import {
  Controller,
  Post,
  Body,
  Res,
  HttpStatus,
  Get,
  Delete,
  Query,
  Headers
} from '@nestjs/common';
import { CrearDispositivoService } from '../casos_de_uso/crear-dispositivo.service';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { idiomas } from 'src/shared/enum-sistema';
import { Funcion } from 'src/shared/funcion';

@Controller('api/dispositivo')
export class EliminarDispositivoControlador {
  constructor(
    private readonly eliminarDispositivoService: EliminarDispositivoService,
    private readonly traduccionEstaticaController: TraduccionEstaticaController,
    private readonly funcion: Funcion
  ) { }

  @Delete('/')
  @ApiResponse({ status: 200, description: 'Eliminado' })
  public async EliminarDispositivo(
    @Query('idDispositivo') idDispositivo: string,
    @Query('idUsuario') idUsuario: string,
    @Headers() headers
  ) {

    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma)
    try {
      const dispositivoEliminado = await this.eliminarDispositivoService.eliminarDispositivo(
        idDispositivo,
        idUsuario,
      );
      if (dispositivoEliminado) {
        //llama al metodo de traduccion estatica
        const ELIMINACION_CORRECTA = await this.traduccionEstaticaController.traduccionEstatica(
          codIdioma,
          'ELIMINACION_CORRECTA',
        );
        return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.OK, mensaje: ELIMINACION_CORRECTA })
      } else {
        //llama al metodo de traduccion estatica
        const ERROR_ELIMINAR = await this.traduccionEstaticaController.traduccionEstatica(
          codIdioma,
          'ERROR_ELIMINAR',
        );
        return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.NOT_FOUND, mensaje: ERROR_ELIMINAR })
      }

    } catch (error) {
      return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR, mensaje: error.message })

    }
  }
}
