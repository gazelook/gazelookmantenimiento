import { CrearDispositivoControlador } from './crear-dispositivo.controller';
import { Module } from '@nestjs/common';
import { DispositivoServicesModule } from '../casos_de_uso/dispositivo-services.module';
import { ObtenerDispositivoUsuarioControlador } from './obtener-dispositivo-usuario.controller';
import { EliminarDispositivoControlador } from './eliminar-dispositivo.controller';
import { Funcion } from 'src/shared/funcion';
import { CerrarSesionDispositivosController } from './cerrar-sesion-dispositivos-usuario.controller';




@Module({
  imports: [DispositivoServicesModule],
  providers: [Funcion],
  exports: [],
  controllers: [
    //ObtenerDispositivoUsuarioControlador
    //CrearDispositivoControlador,
    //EliminarDispositivoControlador
    CerrarSesionDispositivosController
  ],
})
export class DispositivoControllerModule { }
