import { ObtenerDispositivoUsuarioService } from './../casos_de_uso/obtener-dispositivo-usuario.service';
import { ApiHeader, ApiOperation, ApiResponse, ApiSecurity, ApiTags } from '@nestjs/swagger';
import { Controller, Get, HttpStatus, Param, UseGuards, Headers } from '@nestjs/common';
import { Funcion } from 'src/shared/funcion';
import { AuthGuard } from '@nestjs/passport';
import { isValidObjectId } from 'mongoose';
import { I18nService } from 'nestjs-i18n';


@ApiTags('Dispositivo')
@Controller('api/dispositivo')
export class ObtenerDispositivoUsuarioControlador {

  constructor(private readonly obtenerDispositivoService: ObtenerDispositivoUsuarioService,
    private readonly funcion: Funcion,
    private readonly i18n: I18nService,
  ) { }

  @Get('/listarDispositivosUsuario/:usuario')
  @ApiSecurity('Authorization')
  @ApiResponse({ status: 200, type: Object, description: 'Dispositivos de sesión' })
  @ApiResponse({ status: 404, description: 'Error al obtener los dispositivos' })
  @ApiOperation({ summary: 'Este Endpoint retorna los dispositivos en los que ingreso el usuario' })
  @ApiHeader({ name: 'idioma', description: 'codigo del idioma. Ejm: es, en, fr', required: false })
  @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas', required: true })
  @UseGuards(AuthGuard('jwt'))
  async getDispositivoUsuario(@Param('usuario') usuario, @Headers() headers) {
    try {
      const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
      if(isValidObjectId(usuario)){
        const dispositivos = await this.obtenerDispositivoService.getDispositivoUsuario(usuario);
        return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.OK, datos: dispositivos });
      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(codIdioma.concat('.PARAMETROS_NO_VALIDOS'), {
          lang: codIdioma
      });
      return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.NOT_ACCEPTABLE, mensaje: PARAMETROS_NO_VALIDOS });
      }
    } catch (error) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: error.message
    });
    }
  }
}

