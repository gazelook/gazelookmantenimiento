import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Dispositivo } from 'src/drivers/mongoose/interfaces/dispositivo/dispositivo.interface';
import { estadosDispositivo, nombreAcciones, nombreEntidades } from '../../../shared/enum-sistema';
import * as mongoose from 'mongoose';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import { CrearHistoricoService } from '../../historico/casos_de_uso/crear-historico.service';

@Injectable()
export class CerrarSesionDispositivosService {


  constructor(@Inject('DISPOSTIVO_MODEL') private readonly dispositivoModel: Model<Dispositivo>,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
    private crearHistoricoService: CrearHistoricoService,
  ) { }

  async cerrarSesionDispositivos(idDispositivo: string): Promise<Dispositivo> {
    const session = await mongoose.startSession()
    await session.startTransaction();

    try {

      const accion = await this.catalogoAccionService.obtenerNombreAccion(nombreAcciones.crear);
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(nombreEntidades.perfiles);

      const opts = { session };

      const getDispositivo = await this.dispositivoModel.findById(idDispositivo);

      const dispositivo = await this.dispositivoModel.updateMany(
        {
          usuario: getDispositivo.usuario.toString(),
          estado: estadosDispositivo.activo,
          _id: {$ne: idDispositivo}
        },
        { estado: estadosDispositivo.eliminado },
        opts
      );


      const dataHistorico = JSON.parse(JSON.stringify(getDispositivo));
      //eliminar parametro no necesarios
      delete dataHistorico.fechaCreacion
      delete dataHistorico.fechaActualizacion
      delete dataHistorico.__v

      const dataHistoricoMedia: any = {
        datos: dataHistorico,
        usuario: getDispositivo.usuario.toString(),
        accion: accion.codigo,
        entidad: entidad.codigo
      }
      // crear el historico del archivo
      this.crearHistoricoService.crearHistoricoServer(dataHistoricoMedia);
      
      await session.commitTransaction();
      session.endSession();

      return dispositivo;

    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw error;
    }
  }
}