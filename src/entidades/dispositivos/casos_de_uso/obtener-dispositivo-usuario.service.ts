import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Dispositivo } from 'src/drivers/mongoose/interfaces/dispositivo/dispositivo.interface';
import { estadosDispositivo } from '../../../shared/enum-sistema';

@Injectable()
export class ObtenerDispositivoUsuarioService {


  constructor(@Inject('DISPOSTIVO_MODEL') private readonly dispositivoModel: Model<Dispositivo>) { }

  // Post a single user
  async getDispositivoUsuario(usuario: string): Promise<Dispositivo[]> {
    const dispositivos = await this.dispositivoModel.find({ usuario: usuario, estado: estadosDispositivo.activo })
      .select('-fechaCreacion -fechaActualizacion -codigo -__v');
    return dispositivos;
  }
  async getDispositivoById(idDispositivo: string): Promise<Dispositivo> {
    const dispositivo = await this.dispositivoModel.findOne({ _id: idDispositivo }).populate(
      {
        path: 'tokenUsuario',
        select: '_id token ultimoAcceso'
      }
    );
    return dispositivo;
  }

  async getDispositivoByIdToken(idTokenUsuario: string): Promise<Dispositivo> {
    const dispositivo = await this.dispositivoModel.findOne({ tokenUsuario: idTokenUsuario }).populate(
      {
        path: 'tokenUsuario',
        select: '_id token ultimoAcceso'
      }
    );
    return dispositivo;
  }
}