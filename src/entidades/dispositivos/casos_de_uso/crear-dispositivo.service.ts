import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { v4 as uuidv4 } from 'uuid';
import { Dispositivo } from 'src/drivers/mongoose/interfaces/dispositivo/dispositivo.interface';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { codigoDispositivo, nombreAcciones, nombrecatalogoEstados, nombreEntidades } from 'src/shared/enum-sistema';

@Injectable()
export class CrearDispositivoService {


    constructor(@Inject('DISPOSTIVO_MODEL') private readonly dispositivoModel: Model<Dispositivo>,
        private nombreEntidad: CatalogoEntidadService,
        private nombreAccion: CatalogoAccionService,
        private nombreEstado: CatalogoEstadoService,
        private crearHistoricoService: CrearHistoricoService) { }

    // Post a single user
    async crearDispositivo(user: any, crearDispositivoDto: any, opts?: any): Promise<Dispositivo> {

        try {

            // Obtiene el codigo de la accion a realizarse
            const accion = await this.nombreAccion.obtenerNombreAccion(nombreAcciones.crear);
            let getAccion = accion.codigo;

            // Obtiene el codigo de la entidad
            const entidad = await this.nombreEntidad.obtenerNombreEntidad(nombreEntidades.dispositivo);
            let codEntidad = entidad.codigo;


            // Obtiene el estado de la entidad
            const estado = await this.nombreEstado.obtenerNombreEstado(nombrecatalogoEstados.activa, codEntidad);
            let codEstado = estado.codigo;

            //datos para guardar el dispositivo
            let nuevoDispostivo = {
                _id: crearDispositivoDto._id,
                nombre: crearDispositivoDto.userAgent,
                tipo: crearDispositivoDto.userAgent,
                codigo: codigoDispositivo.codigo.concat(uuidv4()),
                usuario: user._id,
                estado: codEstado,
                // localidad: crearDispositivoDto.localidad,
                accion: getAccion,
                entidad: codEntidad,
                ipDispositivo: crearDispositivoDto.ip,
                tokenUsuario: crearDispositivoDto.tokenUsuario
            };
            const dispositivo = await new this.dispositivoModel(nuevoDispostivo).save(opts);
            //datos para guardar el historico
            const dataMensaje = JSON.parse(JSON.stringify(dispositivo))
            delete dataMensaje.__v

            let newHistorico: any = {
                datos: dataMensaje,
                usuario: user._id,
                accion: getAccion,
                entidad: codEntidad
            }

            this.crearHistoricoService.crearHistoricoServer(newHistorico);
            return dispositivo;
        } catch (error) {
            throw error;
        }

    }


}