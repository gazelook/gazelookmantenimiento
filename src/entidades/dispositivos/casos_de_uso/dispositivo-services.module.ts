import { CrearDispositivoService } from './crear-dispositivo.service';
import { Module } from '@nestjs/common';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { dispositivoProviders } from '../drivers/dispositivo.provider';
import { ObtenerDispositivoUsuarioService } from './obtener-dispositivo-usuario.service';
import { CatalogosServiceModule } from 'src/entidades/catalogos/casos_de_uso/catalogos-services.module';
import { HistoricoServiceModule } from 'src/entidades/historico/casos_de_uso/historico-services.module';
import { EliminarDispositivoService } from './eliminar-dispositivo.service';
import { CerrarSesionDispositivosService } from './cerrar-sesion-dispositivos.service';


@Module({
  imports: [DBModule, CatalogosServiceModule, HistoricoServiceModule],
  providers: [
    ...dispositivoProviders,
    CrearDispositivoService,
    ObtenerDispositivoUsuarioService,
    EliminarDispositivoService,
    CerrarSesionDispositivosService
  ],
  exports: [
    ...dispositivoProviders,
    CrearDispositivoService,
    ObtenerDispositivoUsuarioService,
    EliminarDispositivoService,
    CerrarSesionDispositivosService
  ],
  controllers: [],
})
export class DispositivoServicesModule {}
