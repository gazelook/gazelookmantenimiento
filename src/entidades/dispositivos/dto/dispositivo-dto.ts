import { ApiProperty } from "@nestjs/swagger";



export class DispositivoDto {
    @ApiProperty()
    nombre: string;
    @ApiProperty()
    tipo:string;
    @ApiProperty()
    localidad:string;
    @ApiProperty()
    ipDispositivo:string;
  
}