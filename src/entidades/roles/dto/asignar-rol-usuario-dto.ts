import { ApiProperty } from "@nestjs/swagger";
import { IdUsuarioDto } from "src/entidades/usuario/dto/usuario-dto";
import { IdRolEntidadDto } from "./rol-entidad-dto";

export class AsignarRolUsuarioDto {

    @ApiProperty({ type: String })
    perfilAdmin: string;
    @ApiProperty({ type: IdUsuarioDto })
    usuarioAsignar: IdUsuarioDto;
    @ApiProperty({ type: IdRolEntidadDto })
    rol: IdRolEntidadDto;
}

export class QuitarRolUsuarioDto {

    @ApiProperty({ type: String })
    perfilAdmin: string;
    @ApiProperty({ type: IdUsuarioDto })
    usuarioQuitar: IdUsuarioDto;
    @ApiProperty({ type: IdRolEntidadDto })
    rol: IdRolEntidadDto;
}