import { ApiProperty } from "@nestjs/swagger";
import { CodigosCatalogoAccionesDto } from "src/entidades/catalogos/dto/catalogo-acciones.dto";
import { CodigoCatalogoEntidadesDto } from "src/entidades/catalogos/dto/catalogo-entidades.dto";
import { idPerfilDto } from "src/entidades/usuario/dto/perfil.dto";
import { CodigoCatalogoRolDto } from "./catalogo-rol-dto";

export class RolEntidadDto {

    @ApiProperty({ type: idPerfilDto })
    perfil: idPerfilDto;

    @ApiProperty()
    nombre: string;

    @ApiProperty({ type: CodigoCatalogoRolDto })
    rol: CodigoCatalogoRolDto;

    @ApiProperty({ type: CodigoCatalogoEntidadesDto })
    entidad: CodigoCatalogoEntidadesDto;

    @ApiProperty({ type: [CodigosCatalogoAccionesDto] })
    acciones: Array<CodigosCatalogoAccionesDto>;

}

export class IdRolEntidadDto {

    @ApiProperty({ type: String })
    _id: string;

}