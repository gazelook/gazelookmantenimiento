import { ApiProperty } from "@nestjs/swagger";
import { CodigosCatalogoAccionesDto } from "src/entidades/catalogos/dto/catalogo-acciones.dto";
import { CodigoCatalogoEntidadesDto } from "src/entidades/catalogos/dto/catalogo-entidades.dto";
import { idPerfilDto } from "src/entidades/usuario/dto/perfil.dto";
import { CodigoCatalogoRolDto } from "./catalogo-rol-dto";
import { IdRolEntidadDto } from "./rol-entidad-dto";

export class RolSistemaDto {

    @ApiProperty({ type: idPerfilDto })
    perfil: idPerfilDto;

    @ApiProperty()
    nombre: string;

    @ApiProperty({ type: CodigoCatalogoRolDto })
    rol: CodigoCatalogoRolDto;
    
    @ApiProperty({ type: [IdRolEntidadDto] })
    rolesEspecificos: Array<IdRolEntidadDto>;

}