import { ApiProperty } from "@nestjs/swagger";
import { CodigoCatalogoTipoRolDto } from "src/entidades/catalogos/dto/catalogo-tipo-rol.dto";
import { idPerfilDto } from "src/entidades/usuario/dto/perfil.dto";

export class CatalogoRolDto {

    @ApiProperty({ type: idPerfilDto })
    perfil: idPerfilDto;
    @ApiProperty({ type: String })
    nombre: string;
    @ApiProperty({ type: String })
    descripcion: string;
    @ApiProperty({ type: CodigoCatalogoTipoRolDto })
    tipo: CodigoCatalogoTipoRolDto;
}

export class CodigoCatalogoRolDto {

    @ApiProperty({ type: String, required: true, example: 'CATROL_12' })
    codigo: string;
}