import { RolesControllerModule } from './controladores/roles-controller.module';
import { Module } from '@nestjs/common';

@Module({
  imports: [RolesControllerModule, 
  ],
  providers: [
  ],
  controllers: [],
  exports: [RolesControllerModule]
})
export class RolesModule {}

