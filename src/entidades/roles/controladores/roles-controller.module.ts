import { Module, Delete } from '@nestjs/common';
import { RolesServicesModule } from '../casos_de_uso/roles-services.module';
import { HttpErrorFilter } from 'src/shared/filters/http-error.filter';
import { Funcion } from '../../../shared/funcion';
import { CatalogoRolControlador } from './catalogo_rol.controller';
import { TraduccionEstaticaController } from '../../../multiIdioma/controladores/traduccion-estatica-controller';
import { CatalogosServiceModule } from '../../catalogos/casos_de_uso/catalogos-services.module';
import { RolEntidadControlador } from './rol_entidad.controller';
import { ObtenerCatalogoRolControlador } from './obtener-catalogo_rol.controller';
import { RolSistemaControlador } from './rol_sistema.controller';
import { AsignarRolUsuarioControlador } from './asignar-rol-usuario.controller';
import { ObtenerRolSistemaControlador } from './obtener_rol_sistema.controller';
import { QUitarRolUsuarioControlador } from './quitar-rol-usuario.controller';



@Module({
  imports: [RolesServicesModule, CatalogosServiceModule],
  providers: [HttpErrorFilter, Funcion, TraduccionEstaticaController],
  exports: [RolesServicesModule],
  controllers: [
    CatalogoRolControlador,
    RolEntidadControlador,
    ObtenerCatalogoRolControlador,
    RolSistemaControlador,
    AsignarRolUsuarioControlador,
    ObtenerRolSistemaControlador,
    QUitarRolUsuarioControlador
  ],
})
export class RolesControllerModule { }
