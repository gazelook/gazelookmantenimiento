
import { AuthGuard } from '@nestjs/passport';
import { ApiResponse, ApiTags, ApiOperation, ApiHeader } from '@nestjs/swagger';
import { Controller, Post, Body, Res, HttpStatus, Headers } from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { CatalogoRolService } from '../casos_de_uso/catalogo_rol.service';
import { RolEntidadDto } from '../dto/rol-entidad-dto';
import { RolEntidadService } from '../casos_de_uso/rol-entidad.service';
import * as mongoose from 'mongoose';

@ApiTags('Roles')
@Controller('api/rol-entidad')
export class RolEntidadControlador {

    constructor(
        private readonly rolEntidadService: RolEntidadService,
        private readonly i18n: I18nService,
        private readonly funcion: Funcion
    ) { }

    //funcion = new Funcion(this.i18n);

    @Post('/')
    @ApiHeader({ name: 'idioma', description: 'codigo del idioma. Ejm: es, en, fr' })
    @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
    @ApiOperation({ summary: 'Crea un nuevo Rol Entidad' })
    @ApiResponse({ status: 201, description: 'Creacion Correcta' })
    @ApiResponse({ status: 404, description: 'Error al crear' })
    @ApiResponse({ status: 401, description: 'No autorizado' })
    @ApiResponse({ status: 406, description: 'Parámetros no validos' })
    //@UseGuards(AuthGuard('jwt'))

    public async crearRolEntidad(
        @Headers() headers,
        @Body() rolEntidadDto: RolEntidadDto,
        @Res() res
    ) {

        const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

        try {
            if (mongoose.isValidObjectId(rolEntidadDto.perfil._id) && rolEntidadDto.entidad.codigo &&
                rolEntidadDto.nombre && rolEntidadDto.rol.codigo && rolEntidadDto.acciones.length > 0) {
                const crear = await this.rolEntidadService.crearRolEntidad(rolEntidadDto);

                if (crear) {

                    const CREACION_CORRECTA = await this.i18n.translate(codIdioma.concat('.CREACION_CORRECTA'), {
                        lang: codIdioma
                    });
                    return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.CREATED, mensaje: CREACION_CORRECTA, datos: { _id: crear._id } })

                } else {
                    const ERROR_CREACION = await this.i18n.translate(codIdioma.concat('.ERROR_CREACION'), {
                        lang: codIdioma
                    });
                    return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.NOT_FOUND, mensaje: ERROR_CREACION })
                }

            } else {
                const PARAMETROS_NO_VALIDOS = await this.i18n.translate(codIdioma.concat('.PARAMETROS_NO_VALIDOS'), {
                    lang: codIdioma
                });
                return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.NOT_ACCEPTABLE, mensaje: PARAMETROS_NO_VALIDOS })
            }
        } catch (e) {
            if (e?.codigoNombre) {
                const MENSAJE = await this.i18n.translate(codIdioma.concat(`.${e.codigoNombre}`), {
                    lang: codIdioma
                });
                return this.funcion.enviarRespuestaOptimizada({ codigoEstado: e.codigo, mensaje: MENSAJE });
            } else {
                return this.funcion.enviarRespuestaOptimizada({
                    codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
                    mensaje: e.message
                });
            }
        }
    }
}

