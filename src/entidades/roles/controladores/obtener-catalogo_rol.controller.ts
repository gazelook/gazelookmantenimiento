
import { AuthGuard } from '@nestjs/passport';
import { ApiResponse, ApiTags, ApiOperation, ApiHeader } from '@nestjs/swagger';
import { Controller, Post, Body, Res, HttpStatus, Headers, Get, UseGuards } from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { CatalogoRolDto } from '../dto/catalogo-rol-dto';
import { CatalogoRolService } from '../casos_de_uso/catalogo_rol.service';
import * as mongoose from 'mongoose';
import { ObtenerCatalogoRolService } from '../casos_de_uso/obtener-catalogo-rol.service';

@ApiTags('Catalogos')
@Controller('api/catalogos')
export class ObtenerCatalogoRolControlador {

    constructor(
        private readonly obtenerCatalogoRolService: ObtenerCatalogoRolService,
        private readonly i18n: I18nService,
        private readonly funcion: Funcion
    ) { }

    //funcion = new Funcion(this.i18n);

    @Get('/catalogo-roles')
    @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
    @ApiHeader({ name: 'idioma', description: 'codigo del idioma. Ejm: es, en, fr' })
    @ApiResponse({
        status: 200,
        // type: CatalogoEntidadesDto
    })
    @ApiOperation({ summary: 'Este método devuelve el catalogo roles' })
    @ApiResponse({ status: 404, description: 'Error al obtener el catalogo' })
    @UseGuards(AuthGuard('jwt'))

    public async obtenerCatalogoRol(
        @Headers() headers,
    ) {

        const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

        try {

            const catalogoEntidades = await this.obtenerCatalogoRolService.obtenerCatalogoRol();

            return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.OK, datos: catalogoEntidades });

        } catch (e) {

            return this.funcion.enviarRespuestaOptimizada({
                codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
                mensaje: e.message
            });

        }
    }
}

