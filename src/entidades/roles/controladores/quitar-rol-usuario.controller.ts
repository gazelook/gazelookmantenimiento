
import { AuthGuard } from '@nestjs/passport';
import { ApiResponse, ApiTags, ApiOperation, ApiHeader, ApiBody, ApiSecurity } from '@nestjs/swagger';
import { Controller, Post, Body, Res, HttpStatus, Headers, Delete, UseGuards } from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { CatalogoRolDto } from '../dto/catalogo-rol-dto';
import { CatalogoRolService } from '../casos_de_uso/catalogo_rol.service';
import * as mongoose from 'mongoose';
import { AsignarRolUsuarioDto, QuitarRolUsuarioDto } from '../dto/asignar-rol-usuario-dto';
import { AsignarRolUsuarioService } from '../casos_de_uso/asignar-rol-usuario.service';
import { QuitarRolUsuarioService } from '../casos_de_uso/quitar-rol-usuario.service';

@ApiTags('Roles')
@Controller('api/quitar-rol-usuario')
export class QUitarRolUsuarioControlador {

    constructor(
        private readonly quitarRolUsuarioService: QuitarRolUsuarioService,
        private readonly i18n: I18nService,
        private readonly funcion: Funcion
    ) { }

    //funcion = new Funcion(this.i18n);

    @Delete('/')
    @ApiSecurity('Authorization')
    @ApiBody({ type: QuitarRolUsuarioDto })
    @ApiHeader({ name: 'idioma', description: 'codigo del idioma. Ejm: es, en, fr' })
    @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
    @ApiOperation({ summary: 'Elimina rol a usuarios' })
    @ApiResponse({
        status: 200,
        description: 'Eliminacion correcta'
    })
    @ApiResponse({ status: 404, description: 'Error al eliminar' })
    @ApiResponse({ status: 406, description: 'Parámetros no validos' })
    @ApiResponse({ status: 401, description: 'No autorizado' })
    @UseGuards(AuthGuard('jwt'))

    public async quitarRolUsuario(
        @Headers() headers,
        @Body() quitarRolUsuarioDto: QuitarRolUsuarioDto
    ) {

        const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

        try {
            if (mongoose.isValidObjectId(quitarRolUsuarioDto.perfilAdmin) && quitarRolUsuarioDto.usuarioQuitar._id &&
                quitarRolUsuarioDto.rol._id) {
                const eliminar = await this.quitarRolUsuarioService.quitarRolUsuario(quitarRolUsuarioDto);

                console.log('eliminar: ', eliminar)
                if (eliminar) {
                    const ELIMINACION_CORRECTA = await this.i18n.translate(codIdioma.concat('.ELIMINACION_CORRECTA'), {
                        lang: codIdioma
                    });
                    return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.OK, mensaje: ELIMINACION_CORRECTA })

                } else {
                    const ERROR_ELIMINAR = await this.i18n.translate(codIdioma.concat('.ERROR_ELIMINAR'), {
                        lang: codIdioma
                    });
                    return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.NOT_FOUND, mensaje: ERROR_ELIMINAR })

                }

            } else {
                const PARAMETROS_NO_VALIDOS = await this.i18n.translate(codIdioma.concat('.PARAMETROS_NO_VALIDOS'), {
                    lang: codIdioma
                });
                return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.NOT_ACCEPTABLE, mensaje: PARAMETROS_NO_VALIDOS })
            }
        } catch (e) {
            if (e?.codigoNombre) {
                const MENSAJE = await this.i18n.translate(codIdioma.concat(`.${e.codigoNombre}`), {
                    lang: codIdioma
                });
                return this.funcion.enviarRespuestaOptimizada({ codigoEstado: e.codigo, mensaje: MENSAJE });
            } else {
                return this.funcion.enviarRespuestaOptimizada({
                    codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
                    mensaje: e.message
                });
            }
        }
    }
}

