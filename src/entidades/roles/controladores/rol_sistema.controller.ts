
import { AuthGuard } from '@nestjs/passport';
import { ApiResponse, ApiTags, ApiOperation, ApiHeader } from '@nestjs/swagger';
import { Controller, Post, Body, Res, HttpStatus, Headers, UseGuards } from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import * as mongoose from 'mongoose';
import { RolSistemaDto } from '../dto/rol-sistema-dto';
import { RolSistemaService } from '../casos_de_uso/rol-sistema.service';

@ApiTags('Roles')
@Controller('api/rol-sistema')
export class RolSistemaControlador {

    constructor(
        private readonly rolSistemaService: RolSistemaService,
        private readonly i18n: I18nService,
        private readonly funcion: Funcion
    ) { }

    //funcion = new Funcion(this.i18n);

    @Post('/')
    @ApiHeader({ name: 'idioma', description: 'codigo del idioma. Ejm: es, en, fr' })
    @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
    @ApiOperation({ summary: 'Crea un nuevo Rol Sistema' })
    @ApiResponse({ status: 201, description: 'Creacion Correcta' })
    @ApiResponse({ status: 404, description: 'Error al crear' })
    @ApiResponse({ status: 401, description: 'No autorizado' })
    @ApiResponse({ status: 406, description: 'Parámetros no validos' })
    @UseGuards(AuthGuard('jwt'))

    public async crearRolSistema(
        @Headers() headers,
        @Body() rolSistemaDto: RolSistemaDto
    ) {

        const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

        try {
            if (mongoose.isValidObjectId(rolSistemaDto.perfil._id) && rolSistemaDto.nombre 
            && rolSistemaDto.rol.codigo && rolSistemaDto.rolesEspecificos.length > 0) {
                const crear = await this.rolSistemaService.crearRolSistema(rolSistemaDto);

                if (crear) {

                    const CREACION_CORRECTA = await this.i18n.translate(codIdioma.concat('.CREACION_CORRECTA'), {
                        lang: codIdioma
                    });
                    return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.CREATED, mensaje: CREACION_CORRECTA, datos: { _id: crear._id } })

                } else {
                    const ERROR_CREACION = await this.i18n.translate(codIdioma.concat('.ERROR_CREACION'), {
                        lang: codIdioma
                    });
                    return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.NOT_FOUND, mensaje: ERROR_CREACION })
                }

            } else {
                const PARAMETROS_NO_VALIDOS = await this.i18n.translate(codIdioma.concat('.PARAMETROS_NO_VALIDOS'), {
                    lang: codIdioma
                });
                return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.NOT_ACCEPTABLE, mensaje: PARAMETROS_NO_VALIDOS })
            }
        } catch (e) {
            if (e?.codigoNombre) {
                const MENSAJE = await this.i18n.translate(codIdioma.concat(`.${e.codigoNombre}`), {
                    lang: codIdioma
                });
                return this.funcion.enviarRespuestaOptimizada({ codigoEstado: e.codigo, mensaje: MENSAJE });
            } else {
                return this.funcion.enviarRespuestaOptimizada({
                    codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
                    mensaje: e.message
                });
            }
        }
    }
}

