
import { AuthGuard } from '@nestjs/passport';
import { ApiResponse, ApiTags, ApiOperation, ApiHeader } from '@nestjs/swagger';
import { Controller, Post, Body, Res, HttpStatus, Headers, UseGuards, Query, Get } from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import * as mongoose from 'mongoose';
import { RolSistemaDto } from '../dto/rol-sistema-dto';
import { RolSistemaService } from '../casos_de_uso/rol-sistema.service';

@ApiTags('Roles')
@Controller('api/rol-sistema')
export class ObtenerRolSistemaControlador {

    constructor(
        private readonly rolSistemaService: RolSistemaService,
        private readonly i18n: I18nService,
        private readonly funcion: Funcion
    ) { }

    //funcion = new Funcion(this.i18n);

    @Get('/')
    @ApiHeader({ name: 'idioma', description: 'codigo del idioma. Ejm: es, en, fr' })
    @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
    @ApiOperation({ summary: 'Obtiene los roles del Sistema' })
    @ApiResponse({ status: 200 })
    @ApiResponse({ status: 401, description: 'No autorizado' })
    @ApiResponse({ status: 404, description: 'No se ha podido obtener los datos' })
    @ApiResponse({ status: 406, description: 'Parámetros no validos' })
    @UseGuards(AuthGuard('jwt'))

    public async obtenerRolSistema(
        @Headers() headers,
        @Query('perfil') perfil: string
    ) {

        const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

        try {
            if (mongoose.isValidObjectId(perfil)) {
                const rolSistema = await this.rolSistemaService.obtenerRolSistema(perfil);

                return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.OK, datos: rolSistema })


            } else {
                const PARAMETROS_NO_VALIDOS = await this.i18n.translate(codIdioma.concat('.PARAMETROS_NO_VALIDOS'), {
                    lang: codIdioma
                });
                return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.NOT_ACCEPTABLE, mensaje: PARAMETROS_NO_VALIDOS })
            }
        } catch (e) {
            if (e?.codigoNombre) {
                const MENSAJE = await this.i18n.translate(codIdioma.concat(`.${e.codigoNombre}`), {
                    lang: codIdioma
                });
                return this.funcion.enviarRespuestaOptimizada({ codigoEstado: e.codigo, mensaje: MENSAJE });
            } else {
                return this.funcion.enviarRespuestaOptimizada({
                    codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
                    mensaje: e.message
                });
            }
        }
    }
}

