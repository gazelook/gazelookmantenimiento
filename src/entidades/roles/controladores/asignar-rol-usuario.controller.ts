
import { AuthGuard } from '@nestjs/passport';
import { ApiResponse, ApiTags, ApiOperation, ApiHeader, ApiSecurity, ApiBody } from '@nestjs/swagger';
import { Controller, Post, Body, Res, HttpStatus, Headers, UseGuards } from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from 'src/shared/funcion';
import { CatalogoRolDto } from '../dto/catalogo-rol-dto';
import { CatalogoRolService } from '../casos_de_uso/catalogo_rol.service';
import * as mongoose from 'mongoose';
import { AsignarRolUsuarioDto } from '../dto/asignar-rol-usuario-dto';
import { AsignarRolUsuarioService } from '../casos_de_uso/asignar-rol-usuario.service';

@ApiTags('Roles')
@Controller('api/asignar-rol-usuario')
export class AsignarRolUsuarioControlador {

    constructor(
        private readonly asignarRolUsuarioService: AsignarRolUsuarioService,
        private readonly i18n: I18nService,
        private readonly funcion: Funcion
    ) { }

    //funcion = new Funcion(this.i18n);

    @Post('/')
    @ApiSecurity('Authorization')
    @ApiBody({ type: AsignarRolUsuarioDto })
    @ApiHeader({ name: 'idioma', description: 'codigo del idioma. Ejm: es, en, fr' })
    @ApiHeader({ name: 'apiKey', description: 'key para usar las rutas' })
    @ApiOperation({ summary: 'Asigna rol a usuarios' })
    @ApiResponse({
        status: 200, //type: ProyectoDto, 
        description: 'Actualización correcta'
    })
    @ApiResponse({ status: 404, description: 'Error al actualizar' })
    @ApiResponse({ status: 406, description: 'Parámetros no validos' })
    @ApiResponse({ status: 401, description: 'No autorizado' })
    @UseGuards(AuthGuard('jwt'))

    public async asignarRolUsuario(
        @Headers() headers,
        @Body() asignarRolUsuarioDto: AsignarRolUsuarioDto
    ) {

        const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

        try {
            if (mongoose.isValidObjectId(asignarRolUsuarioDto.perfilAdmin) && asignarRolUsuarioDto.usuarioAsignar._id &&
                asignarRolUsuarioDto.rol._id) {
                const actualizar = await this.asignarRolUsuarioService.asignarRolUsuario(asignarRolUsuarioDto);

                if (actualizar) {
                    const ACTUALIZACION_CORRECTA = await this.i18n.translate(codIdioma.concat('.ACTUALIZACION_CORRECTA'), {
                        lang: codIdioma
                    });
                    return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.OK, mensaje: ACTUALIZACION_CORRECTA })

                } else {
                    const ERROR_ACTUALIZAR = await this.i18n.translate(codIdioma.concat('.ERROR_ACTUALIZAR'), {
                        lang: codIdioma
                    });
                    return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.NOT_FOUND, mensaje: ERROR_ACTUALIZAR })

                }

            } else {
                const PARAMETROS_NO_VALIDOS = await this.i18n.translate(codIdioma.concat('.PARAMETROS_NO_VALIDOS'), {
                    lang: codIdioma
                });
                return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.NOT_ACCEPTABLE, mensaje: PARAMETROS_NO_VALIDOS })
            }
        } catch (e) {
            if (e?.codigoNombre) {
                const MENSAJE = await this.i18n.translate(codIdioma.concat(`.${e.codigoNombre}`), {
                    lang: codIdioma
                });
                return this.funcion.enviarRespuestaOptimizada({ codigoEstado: e.codigo, mensaje: MENSAJE });
            } else {
                return this.funcion.enviarRespuestaOptimizada({
                    codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
                    mensaje: e.message
                });
            }
        }
    }
}

