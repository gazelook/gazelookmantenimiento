
import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { UsuarioServicesModule } from 'src/entidades/usuario/casos_de_uso/usuario-services.module';
import { Funcion } from 'src/shared/funcion';
import { StorageModule } from '../../../drivers/amazon_s3/storage.module';
import { CatalogosServiceModule } from '../../catalogos/casos_de_uso/catalogos-services.module';
import { EmailServicesModule } from '../../emails/casos_de_uso/email-services.module';
import { HistoricoServiceModule } from '../../historico/casos_de_uso/historico-services.module';
import { ServicioEmailModule } from '../../servicio_email/servicio-email.module';
import { rolesProviders } from '../drivers/roles.provider';
import { AsignarRolUsuarioService } from './asignar-rol-usuario.service';
import { CatalogoRolService } from './catalogo_rol.service';
import { ObtenerCatalogoRolService } from './obtener-catalogo-rol.service';
import { QuitarRolUsuarioService } from './quitar-rol-usuario.service';
import { RolEntidadService } from './rol-entidad.service';
import { RolSistemaService } from './rol-sistema.service';
import { ValidaRolUsuarioService } from './validar-rol-usuario.service';
import { VerificarPerfilService } from './verificar-perfil.service';



@Module({
  imports: [
    DBModule,
    HttpModule,
    StorageModule,
    ServicioEmailModule,
    CatalogosServiceModule,
    HistoricoServiceModule,
    EmailServicesModule,
    UsuarioServicesModule
  ],
  providers: [
    ...rolesProviders,
    CatalogoRolService,
    ValidaRolUsuarioService,
    Funcion,
    VerificarPerfilService,
    RolEntidadService,
    ObtenerCatalogoRolService,
    RolSistemaService,
    AsignarRolUsuarioService,
    QuitarRolUsuarioService
  ],
  exports: [
    CatalogoRolService,
    ValidaRolUsuarioService,
    VerificarPerfilService,
    RolEntidadService,
    ObtenerCatalogoRolService,
    RolSistemaService,
    AsignarRolUsuarioService,
    QuitarRolUsuarioService
  ],
  controllers: [

  ],
})
export class RolesServicesModule { }
