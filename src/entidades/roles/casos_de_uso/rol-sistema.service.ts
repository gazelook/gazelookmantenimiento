import { Injectable, Inject, HttpStatus } from '@nestjs/common';
import { Model } from 'mongoose';
import { Funcion } from 'src/shared/funcion';
import { CrearHistoricoService } from '../../historico/casos_de_uso/crear-historico.service';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import { codigoEntidades, codigosEstadoCatalogoRol, codigosEstadoRolEntidad, codigosEstadoRolSistema, nombreAcciones, nombreEntidades } from '../../../shared/enum-sistema';
import { CatalogoRol } from 'src/drivers/mongoose/interfaces/catalogo_rol/catalogo-rol.interface';
import { CatalogoRolDto } from '../dto/catalogo-rol-dto';
import { VerificarPerfilService } from './verificar-perfil.service';
import { erroresGeneral } from 'src/shared/enum-errores';
import { RolEntidadDto } from '../dto/rol-entidad-dto';
import { RolEntidad } from 'src/drivers/mongoose/interfaces/rol_entidad/rol-entidad.interface';
import { RolSistemaDto } from '../dto/rol-sistema-dto';
import { RolSistema } from 'src/drivers/mongoose/interfaces/rol_sistema/rol-sistema.interface';
const mongoose = require('mongoose');

@Injectable()
export class RolSistemaService {
  constructor(
    @Inject('ROL_SISTEMA_MODEL') private readonly rolSistemaModel: Model<RolSistema>,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoAccionService: CatalogoAccionService,
    private verificarPerfilService: VerificarPerfilService
  ) { }

  async crearRolSistema(rolSistemaDto: RolSistemaDto): Promise<any> {

    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      let perfilAdminGazelook = await this.verificarPerfilService.verificarPerfilAdministradorGazelook(rolSistemaDto.perfil._id);

      if (!perfilAdminGazelook) {
        throw {
          codigo: HttpStatus.UNAUTHORIZED,
          codigoNombre: erroresGeneral.NO_PERMISO_ACCION,
        };
      }
      //Obtiene el codigo de la accion a realizarse
      const accion = await this.catalogoAccionService.obtenerNombreAccion(nombreAcciones.crear)
      let getAccion = accion.codigo;

      let arrayRolesEspecificos = [];
      for (const listRolesEspecificos of rolSistemaDto.rolesEspecificos) {
        arrayRolesEspecificos.push(listRolesEspecificos._id);
      }

      let dataRolSistema = {
        nombre: rolSistemaDto.nombre,
        rol: rolSistemaDto.rol.codigo,
        rolesEspecificos: arrayRolesEspecificos,
        estado: codigosEstadoRolSistema.activa
      }
      const crear = await new this.rolSistemaModel(dataRolSistema).save(opts);
      const dataRol = JSON.parse(JSON.stringify(crear))
      delete dataRol.__v

      let newHistoricoRolSistema: any = {
        descripcion: dataRol,
        usuario: rolSistemaDto.perfil._id,
        accion: getAccion,
        entidad: codigoEntidades.entidadRolSistema
      }

      this.crearHistoricoService.crearHistoricoServer(newHistoricoRolSistema);

      //Confirma los cambios de la transaccion
      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();

      return crear;
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }

  async obtenerRolSistema(perfil: string): Promise<any> {

    try {

      let perfilAdminGazelook = await this.verificarPerfilService.verificarPerfilAdministradorGazelook(perfil);

      if (!perfilAdminGazelook) {
        throw {
          codigo: HttpStatus.UNAUTHORIZED,
          codigoNombre: erroresGeneral.NO_PERMISO_ACCION,
        };
      }

      const rolSistema = await this.rolSistemaModel.find({ estado: codigosEstadoRolSistema.activa }).select('_id nombre rol rolesEspecificos');
      let arrayRolSistema = [];
      if (rolSistema.length > 0) {
        for (const getRol of rolSistema) {
          let dataRolSistema = {
            _id: getRol._id,
            nombre: getRol.nombre,
            rol: getRol.rol,
            rolesEspecificos: getRol.rolesEspecificos,
          }
          arrayRolSistema.push(dataRolSistema);
        }
      }
  
      return arrayRolSistema;

    } catch (error) {
      throw error;
    }
  }

}

