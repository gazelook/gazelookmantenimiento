import { CatalogoEntidad } from '../../../drivers/mongoose/interfaces/entidad/entidad.interface';
import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CatalogoRol } from 'src/drivers/mongoose/interfaces/catalogo_rol/catalogo-rol.interface';
import { codigosEstadoCatalogoRol } from 'src/shared/enum-sistema';

@Injectable()
export class ObtenerCatalogoRolService {

  constructor(
    @Inject('CATALOGO_ROL_MODEL') private readonly catalogoRolModel: Model<CatalogoRol>) { }


  async obtenerCatalogoRol(): Promise<any> {
    const catalogoRol = await this.catalogoRolModel.find({ estado: codigosEstadoCatalogoRol.activa }).select('_id codigo nombre descripcion estado tipo');
    let arrayCatalogo = [];
    if (catalogoRol.length > 0) {
      for (const getCatalogo of catalogoRol) {
        let dataCatalogo = {
          _id: getCatalogo._id,
          codigo: getCatalogo.codigo,
          nombre: getCatalogo.nombre,
          descripcion: getCatalogo.descripcion,
          tipo: {
            codigo: getCatalogo.tipo
          }
        }
        arrayCatalogo.push(dataCatalogo);
      }
    }

    return arrayCatalogo;
  }

}
