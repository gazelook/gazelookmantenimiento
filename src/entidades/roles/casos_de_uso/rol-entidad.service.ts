import { Injectable, Inject, HttpStatus } from '@nestjs/common';
import { Model } from 'mongoose';
// import * {} from '../../../shared/funcion';
import { Funcion } from 'src/shared/funcion';
import { CrearHistoricoService } from '../../historico/casos_de_uso/crear-historico.service';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import { codigoEntidades, codigosEstadoCatalogoRol, codigosEstadoRolEntidad, nombreAcciones, nombreEntidades } from '../../../shared/enum-sistema';
import { CatalogoRol } from 'src/drivers/mongoose/interfaces/catalogo_rol/catalogo-rol.interface';
import { CatalogoRolDto } from '../dto/catalogo-rol-dto';
import { VerificarPerfilService } from './verificar-perfil.service';
import { erroresGeneral } from 'src/shared/enum-errores';
import { RolEntidadDto } from '../dto/rol-entidad-dto';
import { RolEntidad } from 'src/drivers/mongoose/interfaces/rol_entidad/rol-entidad.interface';
const mongoose = require('mongoose');

@Injectable()
export class RolEntidadService {
  constructor(
    @Inject('ROL_ENTIDAD_MODEL') private readonly rolEntidadModel: Model<RolEntidad>,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
    private readonly funcion: Funcion,
    private verificarPerfilService: VerificarPerfilService
  ) { }

  async crearRolEntidad(rolEntidadDto: RolEntidadDto): Promise<any> {

    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      let perfilAdminGazelook = await this.verificarPerfilService.verificarPerfilAdministradorGazelook(rolEntidadDto.perfil._id);

      if (!perfilAdminGazelook) {
        throw {
          codigo: HttpStatus.UNAUTHORIZED,
          codigoNombre: erroresGeneral.NO_PERMISO_ACCION,
        };
      }
      //Obtiene el codigo de la accion a realizarse
      const accion = await this.catalogoAccionService.obtenerNombreAccion(nombreAcciones.crear)
      let getAccion = accion.codigo;

      let arrayAcciones = [];
      for (const listAcciones of rolEntidadDto.acciones) {
        arrayAcciones.push(listAcciones._id);
      }

      let dataRolEntidad = {
        nombre: rolEntidadDto.nombre,
        rol: rolEntidadDto.rol.codigo,
        entidad: rolEntidadDto.entidad.codigo,
        acciones: arrayAcciones,
        estado: codigosEstadoRolEntidad.activa
      }
      const crear = await new this.rolEntidadModel(dataRolEntidad).save(opts);
      const dataRol = JSON.parse(JSON.stringify(crear))
      delete dataRol.__v

      let newHistoricoCatalogoRol: any = {
        descripcion: dataRol,
        usuario: rolEntidadDto.perfil._id,
        accion: getAccion,
        entidad: codigoEntidades.entidadRolEntidad
      }

      
      this.crearHistoricoService.crearHistoricoServer(newHistoricoCatalogoRol);

      //Confirma los cambios de la transaccion
      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();

      return crear;
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }

}

