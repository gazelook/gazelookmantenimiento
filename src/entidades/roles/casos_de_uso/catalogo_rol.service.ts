import { Injectable, Inject, HttpStatus } from '@nestjs/common';
import { Model } from 'mongoose';
// import * {} from '../../../shared/funcion';
import { Funcion } from 'src/shared/funcion';
import { CrearHistoricoService } from '../../historico/casos_de_uso/crear-historico.service';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import { codigoEntidades, codigosEstadoCatalogoRol, nombreAcciones, nombrecatalogoEstados, nombreEntidades } from '../../../shared/enum-sistema';
import { CatalogoRol } from 'src/drivers/mongoose/interfaces/catalogo_rol/catalogo-rol.interface';
import { CatalogoRolDto } from '../dto/catalogo-rol-dto';
import { VerificarPerfilService } from './verificar-perfil.service';
import { erroresGeneral } from 'src/shared/enum-errores';
const mongoose = require('mongoose');

@Injectable()
export class CatalogoRolService {
  constructor(
    @Inject('CATALOGO_ROL_MODEL') private readonly catalogoRolModel: Model<CatalogoRol>,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoAccionService: CatalogoAccionService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoEstadoService: CatalogoEstadoService,
    private readonly funcion: Funcion,
    private verificarPerfilService: VerificarPerfilService
  ) { }

  async crearCatalogoRol(catalogoRolDto: CatalogoRolDto): Promise<any> {

    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      let perfilAdminGazelook = await this.verificarPerfilService.verificarPerfilAdministradorGazelook(catalogoRolDto.perfil._id);

      if (!perfilAdminGazelook) {
        throw {
          codigo: HttpStatus.UNAUTHORIZED,
          codigoNombre: erroresGeneral.NO_PERMISO_ACCION,
        };
      }
      //Obtiene el codigo de la accion a realizarse
      const accion = await this.catalogoAccionService.obtenerNombreAccion(nombreAcciones.crear)
      let getAccion = accion.codigo;

      const getUltimoCatalogoRol = await this.catalogoRolModel.findOne(
        {
          estado: codigosEstadoCatalogoRol.activa
        }
      ).sort({ fechaCreacion: -1 }).session(opts.session);

      let getCodigo = await this.funcion.codigoAutoincrementableCatalogoRol(getUltimoCatalogoRol);

      let dataCatalogoRol = {
        codigo: getCodigo,
        nombre: catalogoRolDto.nombre,
        descripcion: catalogoRolDto.descripcion,
        tipo: catalogoRolDto.tipo.codigo,
        estado: codigosEstadoCatalogoRol.activa
      }
      const crear = await new this.catalogoRolModel(dataCatalogoRol).save(opts);
      const dataCatalogo = JSON.parse(JSON.stringify(crear))
      delete dataCatalogo.__v

      let newHistoricoCatalogoRol: any = {
        descripcion: dataCatalogo,
        usuario: catalogoRolDto.perfil._id,
        accion: getAccion,
        entidad: codigoEntidades.entidadCatalogoRol
      }

      // await this.crearHistoricoService.crearHistoricoTransaccion(newHistoricoCatalogoRol, opts);
      this.crearHistoricoService.crearHistoricoServer(newHistoricoCatalogoRol);

      //Confirma los cambios de la transaccion
      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();

      return crear;
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }

}

