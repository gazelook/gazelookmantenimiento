import { Injectable, Inject, HttpStatus } from '@nestjs/common';
import { Model } from 'mongoose';
import { CrearHistoricoService } from '../../historico/casos_de_uso/crear-historico.service';
import { CatalogoAccionService } from '../../catalogos/casos_de_uso/catalogo-accion.service';
import { codigoEntidades, codigosCatalogoRol, nombreAcciones } from '../../../shared/enum-sistema';
import { VerificarPerfilService } from './verificar-perfil.service';
import { erroresGeneral } from 'src/shared/enum-errores';
import { RolSistema } from 'src/drivers/mongoose/interfaces/rol_sistema/rol-sistema.interface';
import { Usuario } from 'src/drivers/mongoose/interfaces/usuarios/usuario.interface';
import { AsignarRolUsuarioDto, QuitarRolUsuarioDto } from '../dto/asignar-rol-usuario-dto';
const mongoose = require('mongoose');

@Injectable()
export class QuitarRolUsuarioService {
  constructor(
    @Inject('ROL_SISTEMA_MODEL') private readonly rolSistemaModel: Model<RolSistema>,
    @Inject('USUARIO_MODEL') private readonly usuarioModel: Model<Usuario>,
    private crearHistoricoService: CrearHistoricoService,
    private catalogoAccionService: CatalogoAccionService,
    private verificarPerfilService: VerificarPerfilService
  ) { }

  async quitarRolUsuario(quitarRolUsuarioDto: QuitarRolUsuarioDto): Promise<any> {

    console.log('quitarRolUsuarioDto: ', quitarRolUsuarioDto)
    const usuarioAdmin = quitarRolUsuarioDto.perfilAdmin;
    const usuarioQuitar = quitarRolUsuarioDto.usuarioQuitar._id;
    const rol = quitarRolUsuarioDto.rol._id;
    //Inicia la transaccion
    //(si ocurre algun error durante la transaccion hace un rollback a todas las acciones)
    const session = await mongoose.startSession();
    session.startTransaction();

    try {
      //constante que guarda la transaccion (este debe agregarse en todos los CRUD para que funcione la transaccion)
      const opts = { session };

      let rolSistema = await this.rolSistemaModel.findOne({ _id: rol });
      let getRol = rolSistema.rol;

      let usuarioVerificado;
      console.log('getRol: ', getRol)
      //Si el rol que se va a quitar es de administrador de anuncios se verifica que el usuario sea administrador gazelook o administrador general de anuncios
      if (getRol === codigosCatalogoRol.administradorAnuncios) {
        usuarioVerificado = await this.verificarPerfilService.verificarUsuarioAdminGazelookGeneralAnuncios(usuarioAdmin);
      }

      //Si el rol que se va a quitar es de administrador traductor de anuncios, se verifica que el usuario sea administrador gazelook, administrador general de anuncios, o administrador normal de anuncios
      if (getRol === codigosCatalogoRol.administradorTraductorAnuncios) {
        usuarioVerificado = await this.verificarPerfilService.verificarUsuarioAdminGazelookGeneralNormalAnuncios(usuarioAdmin);
      }

      //Si el rol que se va a quitar es de administrador general de anuncios, se verifica que el usuario sea administrador gazelook
      if (getRol === codigosCatalogoRol.administradorGeneralAnuncios) {
        usuarioVerificado = await this.verificarPerfilService.verificarPerfilAdministradorGazelook(usuarioAdmin);
      }

      //Si el rol que se va a quitar es de administrador gazelook, se verifica que el usuario sea administrador gazelook
      if (getRol === codigosCatalogoRol.administrador) {
        usuarioVerificado = await this.verificarPerfilService.verificarPerfilAdministrador(usuarioAdmin);
      }

      //Si el rol que se va a quitar es sub administrador general, se verifica que el usuario sea administrador gazelook
      if (getRol === codigosCatalogoRol.subAdministradorGeneral) {
        usuarioVerificado = await this.verificarPerfilService.verificarPerfilAdministrador(usuarioAdmin);
      }

      console.log('usuarioVerificado: ', usuarioVerificado)

      if (!usuarioVerificado) {
        throw {
          codigo: HttpStatus.UNAUTHORIZED,
          codigoNombre: erroresGeneral.NO_PERMISO_ACCION,
        };
      }
      //Obtiene el codigo de la accion a realizarse
      const accion = await this.catalogoAccionService.obtenerNombreAccion(nombreAcciones.eliminar)
      let getAccion = accion.codigo;

      await this.usuarioModel.updateOne({ _id: usuarioQuitar },
        { $pull: { rolSistema: rol }, fechaActualizacion: new Date() }, opts);

      let newHistoricoAsignarRol: any = {
        descripcion: quitarRolUsuarioDto,
        usuario: usuarioAdmin,
        accion: getAccion,
        entidad: codigoEntidades.entidadUsuarios
      }

      // await this.crearHistoricoService.crearHistoricoTransaccion(newHistoricoAsignarRol, opts);
      this.crearHistoricoService.crearHistoricoServer(newHistoricoAsignarRol);

      //Confirma los cambios de la transaccion
      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();

      return true;
    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }

}

