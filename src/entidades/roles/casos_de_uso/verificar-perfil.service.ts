import { codigosCatalogoRol } from '../../../shared/enum-sistema';

import { Inject, Injectable } from '@nestjs/common';
import { PaginateModel } from 'mongoose';
import { Proyecto } from 'src/drivers/mongoose/interfaces/proyectos/proyecto.interface';
import { ObtenerPerfilUsuarioService } from 'src/entidades/usuario/casos_de_uso/obtener-perfil-usuario.service';
import { ObtenerIdUsuarioService } from 'src/entidades/usuario/casos_de_uso/obtener-id-usuario.service';


@Injectable()
export class VerificarPerfilService {

  getProyectos: any;
  constructor(
    private obtenerPerfilUsuarioService: ObtenerPerfilUsuarioService,
    private obtenerIdUsuarioService: ObtenerIdUsuarioService
  ) {
  }

  async verificarPerfilAdministradorGazelook(idPerfil: string): Promise<any> {


    try {

      let getUsuario = await this.obtenerPerfilUsuarioService.obtenerPerfilById(idPerfil);

      let adminGazelook = false;
      //Verifica si el perfil que se envia es usuario administrador del sistema
      if (getUsuario.usuario) {
        if (getUsuario.usuario.rolSistema.length > 0) {
          for (const getRolSistema of getUsuario.usuario.rolSistema) {
            if (getRolSistema) {
              if (getRolSistema.rol == codigosCatalogoRol.administrador ||
                getRolSistema.rol == codigosCatalogoRol.subAdministradorGeneral) {
                adminGazelook = true;
                break;
              }
            }
          }
        }
      }

      return adminGazelook;

    } catch (error) {
      throw error;
    }
  }

  async verificarPerfilAdministrador(idPerfil: string): Promise<any> {


    try {

      let getUsuario = await this.obtenerPerfilUsuarioService.obtenerPerfilById(idPerfil);

      let adminGazelook = false;
      //Verifica si el perfil que se envia es usuario administrador del sistema
      if (getUsuario.usuario) {
        if (getUsuario.usuario.rolSistema.length > 0) {
          for (const getRolSistema of getUsuario.usuario.rolSistema) {
            if (getRolSistema) {
              if (getRolSistema.rol == codigosCatalogoRol.administrador) {
                adminGazelook = true;
                break;
              }
            }
          }
        }
      }

      return adminGazelook;

    } catch (error) {
      throw error;
    }
  }

  async verificarUsuarioAdminGazelookGeneralAnuncios(idUsuario: string): Promise<any> {


    try {

      let getUsuario = await this.obtenerIdUsuarioService.obtenerUsuario(idUsuario);

      let adminGazelook = false;
      //Verifica si el perfil que se envia es usuario administrador del sistema
      if (getUsuario) {
        if (getUsuario.rolSistema.length > 0) {
          for (const getRolSistema of getUsuario.rolSistema) {
            if (getRolSistema) {
              if (getRolSistema.rol == codigosCatalogoRol.administrador ||
                getRolSistema.rol == codigosCatalogoRol.subAdministradorGeneral) {
                adminGazelook = true;
                break;
              }
            }
          }
        }
      }

      return adminGazelook;

    } catch (error) {
      throw error;
    }
  }

  async verificarUsuarioAdminGazelookGeneralNormalAnuncios(idUsuario: string): Promise<any> {


    try {

      let getUsuario = await this.obtenerIdUsuarioService.obtenerUsuario(idUsuario);

      let adminGazelook = false;
      //Verifica si el perfil que se envia es usuario administrador del sistema
      if (getUsuario) {
        if (getUsuario.rolSistema.length > 0) {
          for (const getRolSistema of getUsuario.rolSistema) {
            if (getRolSistema) {
              if (getRolSistema.rol == codigosCatalogoRol.administrador ||
                getRolSistema.rol == codigosCatalogoRol.administradorGeneralAnuncios ||
                getRolSistema.rol == codigosCatalogoRol.administradorAnuncios ||
                getRolSistema.rol == codigosCatalogoRol.subAdministradorGeneral) {
                adminGazelook = true;
                break;
              }
            }
          }
        }
      }

      return adminGazelook;

    } catch (error) {
      throw error;
    }
  }


  //Verifica que el perfil sea administrador de traductor de anuncios
  async verificarPerfilAdministradorTraductorAnuncios(idPerfil: string): Promise<any> {


    try {

      let getUsuario = await this.obtenerPerfilUsuarioService.obtenerPerfilById(idPerfil);

      let adminAnunciosGazelook = false;
      //Verifica si el perfil que se envia es usuario administrador del sistema
      if (getUsuario.usuario) {
        if (getUsuario.usuario.rolSistema.length > 0) {
          for (const getRolSistema of getUsuario.usuario.rolSistema) {
            if (getRolSistema) {
              if (getRolSistema.rol == codigosCatalogoRol.administradorTraductorAnuncios) {
                adminAnunciosGazelook = true;
                break;
              }
            }
          }
        }
      }

      return adminAnunciosGazelook;

    } catch (error) {
      throw error;
    }
  }

}