import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoEstadoService } from './../../catalogos/casos_de_uso/catalogo-estado.service';

import {
  Inject,
  Injectable,
} from '@nestjs/common';
import { Model } from 'mongoose';

import { Transaccion } from 'src/drivers/mongoose/interfaces/transaccion/transaccion.interface';
import { nombreAcciones, nombrecatalogoEstados, nombreEntidades } from 'src/shared/enum-sistema';
import { UpdateTransaccionPagoDto } from '../dtos/transaccion.dto';


@Injectable()
export class ActualizarTransaccionService {
  constructor(@Inject('TRANSACCION_MODEL') private readonly transaccionModel: Model<Transaccion>,
    @Inject('INFORMACION_PAGO_MODEL') private readonly informacionPagoModel: Model<Transaccion>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private crearHistoricoService: CrearHistoricoService,
  ) { }

  async actualizarTransaccion(idTransaccion: string, opts: any, dataUpdate: UpdateTransaccionPagoDto): Promise<Transaccion> {

    try {
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.transaccion,
      );

      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );
      const accionMofificar = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.modificar,
      );
      let transaccion: Transaccion;

      const dataTransaccionUpdate = {
        ...dataUpdate,
        estado: estado.codigo
      }
      transaccion = await this.transaccionModel.updateOne({ _id: idTransaccion }, dataTransaccionUpdate, opts);



      //datos para guardar el historico
      const newHistorico: any = {
        datos: transaccion,
        usuario: transaccion.usuario,
        accion: accionMofificar.codigo,
        entidad: entidad.codigo
      };

      //Crear historico para transacción
      this.crearHistoricoService.crearHistoricoServer(newHistorico);

      return transaccion;
    } catch (error) {
      throw error;
    }
  }

}
