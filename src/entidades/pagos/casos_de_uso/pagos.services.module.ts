
import { Module } from '@nestjs/common';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { TransaccionServicesModule } from 'src/entidades/transaccion/casos_de_uso/transaccion-services.module';
import { StripeModule } from '../../../drivers/stripe/stripe.module';
import { CatalogosServiceModule } from '../../catalogos/casos_de_uso/catalogos-services.module';
import { EmailServicesModule } from '../../emails/casos_de_uso/email-services.module';
import { MonedaServicesModule } from '../../moneda/casos_de_uso/moneda.services.module';
import { UsuarioServicesModule } from '../../usuario/casos_de_uso/usuario-services.module';
import { PagosProviders } from '../drivers/pagos.provider';
import { ActualizarConversionTransaccionService } from './actualizar-conversion-transaccion.service';
import { ActualizarTransaccionService } from './actualizar-transaccion.service';
import { CrearConversionTransaccionService } from './crear-conversion-transaccion.service';
import { CrearinformacionPagoService } from './crear-informacion-pago.service';
import { CrearPagoStripeService } from './crear-pago-stripe.service';
import { CrearPagoService } from './crear-pago.service';
import { CrearTransaccionService } from './crear-transaccion.service';
import { GestionPagoStripeService } from './gestion-pago-stripe.service';
import { ObtenerTransaccionService } from './obtener-transaccion.service';
import { ValidarPagoService } from './validar-pago.service';


@Module({
  imports: [
    DBModule,
    CatalogosServiceModule,
    UsuarioServicesModule,
    MonedaServicesModule,
    CatalogosServiceModule,
    StripeModule,
    EmailServicesModule,
    TransaccionServicesModule
  ],
  providers: [
    ...PagosProviders,
    CrearTransaccionService,
    CrearinformacionPagoService,
    CrearConversionTransaccionService,
    CrearPagoStripeService,
    CrearPagoService,
    ActualizarConversionTransaccionService,
    ObtenerTransaccionService,
    ActualizarTransaccionService,
    GestionPagoStripeService,
    ValidarPagoService,
  ],
  exports: [
    CrearTransaccionService,
    CrearinformacionPagoService,
    CrearConversionTransaccionService,
    CrearPagoStripeService,
    CrearPagoService,
    ActualizarConversionTransaccionService,
    ObtenerTransaccionService,
    ActualizarTransaccionService,
    GestionPagoStripeService,
    ValidarPagoService,
  ],
  controllers: [],
})
export class PagosServiceModule { }
