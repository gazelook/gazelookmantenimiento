import {
  Inject,
  Injectable
} from '@nestjs/common';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { Transaccion } from 'src/drivers/mongoose/interfaces/transaccion/transaccion.interface';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { catalogoOrigen, descripcionTransPagosGazelook, nombreAcciones, nombrecatalogoEstados, nombreEntidades } from 'src/shared/enum-sistema';
import { ActualizarUsuarioService } from '../../usuario/casos_de_uso/actualizar-usuario.service';
import { ConversionTransaccionDto } from '../dtos/conversion-transaccion.dto';
import { PagoDto } from '../dtos/pago.dto';
import { CatalogoEstadoService } from './../../catalogos/casos_de_uso/catalogo-estado.service';
import { CrearConversionTransaccionService } from './crear-conversion-transaccion.service';
import { CrearinformacionPagoService } from './crear-informacion-pago.service';


@Injectable()
export class CrearTransaccionService {
  constructor(@Inject('TRANSACCION_MODEL') private readonly transaccionModel: Model<Transaccion>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private crearHistoricoService: CrearHistoricoService,
    private crearinformacionPagoService: CrearinformacionPagoService,
    private actualizarUsuarioService: ActualizarUsuarioService,
    private crearConversionTransaccionService: CrearConversionTransaccionService,
  ) { }

  async crearTransaccion(
    datosPago: PagoDto,
    idPago: string,
    numeroRecibo: string,
    opts: any
  ): Promise<Transaccion> {

    try {
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.transaccion,
      );

      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.pendiente,
        entidad.codigo,
      );
      const accionCrear = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.crear,
      );


      // guardar informacion de pago
      let datosInfoPago = {
        nombres: datosPago.datosFacturacion.nombres,
        telefono: datosPago.datosFacturacion.telefono,
        direcccion: datosPago.datosFacturacion.direccion,
        email: datosPago.datosFacturacion.email,
      };
      const infoPago = await this.crearinformacionPagoService.crearInformacionPago(
        datosInfoPago,
        idPago,
        opts
      );

      let transaccion
      for (const pagoCuenta of datosPago.transacciones) {

        const idTransaccion = new mongoose.mongo.ObjectId();

        // -------------------------------- Crear Conversion ---------------------------
        const dataConversion: ConversionTransaccionDto = {
          monedaUsuario: datosPago.monedaRegistro.codNombre,
          monedaDefault: pagoCuenta.moneda.codNombre,
          montoDefault: pagoCuenta.monto,
          idTransaccion: idTransaccion.toHexString()
        }
        const conversiones = await this.crearConversionTransaccionService.crearConversionTransaccion(dataConversion, opts)

        let descripcion
        if (pagoCuenta.origen.codigo === catalogoOrigen.valor_extra) {
          descripcion = descripcionTransPagosGazelook.valorExtra
        }
        if (pagoCuenta.destino.codigo === catalogoOrigen.fondos_reservados) {
          descripcion = descripcionTransPagosGazelook.fondosReservados
        }
        if (pagoCuenta.destino.codigo === catalogoOrigen.gastos_operativos) {
          descripcion = descripcionTransPagosGazelook.gastoOperacional
        }

        const transaccionDto = {
          _id: idTransaccion,
          estado: estado.codigo,
          monto: pagoCuenta.monto,
          moneda: pagoCuenta.moneda.codNombre,
          descripcion: descripcion,
          origen: pagoCuenta.origen.codigo,
          destino: pagoCuenta.destino.codigo,
          balance: [],
          metodoPago: datosPago.metodoPago.codigo,
          informacionPago: infoPago._id,
          usuario: datosPago.usuario._id,
          conversionTransaccion: conversiones,
          numeroRecibo: numeroRecibo
        };

        transaccion = await new this.transaccionModel(transaccionDto).save(opts);



        // Agregar referencia usuario
        await this.actualizarUsuarioService.agregarUsuarioTransaccion(datosPago.usuario._id, transaccion._id, opts);

        //datos para guardar el historico
        const newHistorico: any = {
          datos: transaccion,
          usuario: datosPago.usuario._id,
          accion: accionCrear.codigo,
          entidad: entidad.codigo
        };

        //Crear historico para transacción
        this.crearHistoricoService.crearHistoricoServer(newHistorico);

      }

      return transaccion;
    } catch (error) {
      throw error;
    }
  }


  generarNumeroRecibo() {
    try {
      const numGenerate = `${Math.floor(new Date().valueOf() * Math.random())}`;

      return numGenerate;
    } catch (error) {
      throw error;
    }
  }

}
