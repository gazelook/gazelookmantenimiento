import { HttpStatus, Inject, Injectable } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { Usuario } from '../../../drivers/mongoose/interfaces/usuarios/usuario.interface';
import { erroresCuenta } from '../../../shared/enum-errores';
import { codigosMetodosPago } from '../../../shared/enum-sistema';
import { CatalogoIdiomasService } from '../../catalogos/casos_de_uso/catalogo-idiomas.service';
import { CrearEmailReciboTransaccionService } from '../../emails/casos_de_uso/crear-email-recibo-transaccion.service';
import { ObtenerIdUsuarioService } from '../../usuario/casos_de_uso/obtener-id-usuario.service';
import { ActualizarConversionTransaccionService } from './actualizar-conversion-transaccion.service';
import { ActualizarTransaccionService } from './actualizar-transaccion.service';
import { GestionPagoStripeService } from './gestion-pago-stripe.service';
import { ObtenerTransaccionService } from './obtener-transaccion.service';

@Injectable()
export class ValidarPagoService {
  codigoMetodoPagoStripe = codigosMetodosPago.stripe;

  constructor(
    private readonly obtenerIdUsuarioService: ObtenerIdUsuarioService,
    private obtenerTransaccionService: ObtenerTransaccionService,
    private actualizarTransaccionService: ActualizarTransaccionService,
    private gestionPagoStripeService: GestionPagoStripeService,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private actualizarConversionTransaccionService: ActualizarConversionTransaccionService,
    private crearEmailReciboTransaccionService: CrearEmailReciboTransaccionService,
    @Inject('USUARIO_MODEL') private readonly userModel: Model<Usuario>,
  ) { }

  async validarPago(idTransaccion: string): Promise<any> {

    const session = await mongoose.startSession();
    await session.startTransaction();

    try {
      const opts = { session };

      const transaccionQuery: any = await this.obtenerTransaccionService.obtenerTransaccionById(idTransaccion);
      if (!transaccionQuery) {
        await session.abortTransaction();
        session.endSession();
        throw { codigo: HttpStatus.EXPECTATION_FAILED, codigoNombre: erroresCuenta.TRANSACCION_NO_VALIDA };
      }

      const informacionPago = transaccionQuery.informacionPago
      const getUsuario = await this.obtenerIdUsuarioService.obtenerUsuarioWithTransaccion(transaccionQuery.usuario._id);

      if (getUsuario) {
        // ___________________________________________ Verificar Pago OK____________________________________________
        // pago Stripe
        let verificarCuentaStripe;
        if (transaccionQuery.metodoPago === this.codigoMetodoPagoStripe) {
          verificarCuentaStripe = await this.gestionPagoStripeService.obtenerEstadoPagoStripe(informacionPago.idPago);
          if (!verificarCuentaStripe) {
            await session.abortTransaction();
            session.endSession();
            throw { codigo: HttpStatus.EXPECTATION_FAILED, codigoNombre: erroresCuenta.ERROR_REGISTRO_PAGO };
          }
        }

        if (verificarCuentaStripe) {
          const detallePagoStripe = await this.gestionPagoStripeService.obtenerDetallePagoStripe(informacionPago.idPago);
          detallePagoStripe.amount = detallePagoStripe.amount / 100;
          detallePagoStripe.fee = detallePagoStripe.fee / 100;
          detallePagoStripe.net = detallePagoStripe.net / 100;


          // ______________________________actualizar transaccion (dos origenes)________________________________
          //__________________Calcular valor total por transaccion________________
          const getNewValor = this.gestionPagoStripeService.calcularValorTotalComisionStripe(
            detallePagoStripe.amount,
            transaccionQuery.monto,
            detallePagoStripe.fee
          );

          const dataUpdate = {
            comisionTransferencia: getNewValor.newFee,
            totalRecibido: getNewValor.newAmountReceived,
            origenPais: detallePagoStripe.country
          }

          await this.actualizarTransaccionService.actualizarTransaccion(transaccionQuery._id, opts, dataUpdate);


          //__________________Actualizar conversion transaccion________________
          await this.actualizarConversionTransaccionService.actualizarConversionStripe(transaccionQuery._id, dataUpdate, opts);

          // agregar transaccion al usuario
          await this.userModel.findOneAndUpdate(
            { _id: getUsuario._id },
            { $push: { transacciones: idTransaccion } },
            opts
          );

          const idiomaUsuario = await this.catalogoIdiomasService.obtenerIdiomaByCodigo(getUsuario.idioma);

          // ___________________Cierra el proceso de transaccion______________________
          await session.commitTransaction();
          session.endSession();

          //_______________________________enviar recibo pago___________________________________
          const dataEmailPago = {
            idInformacionPago: informacionPago._id as string,
            usuario: getUsuario._id as string,
            nombre: getUsuario.perfiles[0].nombre as string,
            emailDestinatario: getUsuario.email as string,
            idioma: idiomaUsuario.codNombre as string
          }
          await this.crearEmailReciboTransaccionService.crearEmailReciboPagoTransaccion(dataEmailPago);

          const result = true;

          // proceso exitoso
          return result;
        }
      } else {
        return null;
      }
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw error;
    }
  }

  validarCodigosMetodosPago(codigo): Boolean {
    const codigos = [
      codigosMetodosPago.stripe
    ];
    if (codigos.indexOf(codigo) !== -1) {
      return true;
    } else {
      return false;
    }
  }
}
