import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';

import { Transaccion } from 'src/drivers/mongoose/interfaces/transaccion/transaccion.interface';
import { estadosTransaccion } from '../../../shared/enum-sistema';

@Injectable()
export class ObtenerTransaccionService {
  constructor(@Inject('TRANSACCION_MODEL') private readonly transaccionModel: Model<Transaccion>,
  ) { }

  async obtenerTransaccionById(idTransaccion, opts?): Promise<any> {
    try {
      if (opts) {
        const transaccion = await this.transaccionModel
          .findById(idTransaccion)
          .populate('informacionPago')
          .populate('usuario')
          .session(opts.session);

        return transaccion;
      } else {
        const transaccion = await this.transaccionModel
          .findById(idTransaccion)
          .populate('informacionPago')
          .populate('usuario');

        return transaccion;
      }
    } catch (error) {
      throw error;
    }
  }
  async obtenerTransaccionesUsuario(idUsuario: string): Promise<any> {
    try {

      const transaccion = await this.transaccionModel.find({ usuario: idUsuario, estado: estadosTransaccion.pendiente })
        .populate([
          {
            path: 'informacionPago'
          },
          {
            path: 'usuario'
          },
        ]);

      return transaccion;
    } catch (error) {
      throw error;
    }
  }

  async obtenerTransaccionByUsuarioByPago(idUsuario, informacionPago, opts?): Promise<any> {
    try {
      if (opts) {
        const transaccion = await this.transaccionModel
          .find({
            usuario: idUsuario,
            informacionPago: informacionPago
          })
          .populate('informacionPago')
          .populate('usuario')
          .session(opts.session);

          
        return transaccion;
      } else {
        const transaccion = await this.transaccionModel
          .find({
            usuario: idUsuario,
            informacionPago: informacionPago
          })
          .populate('informacionPago')
          .populate('usuario');

        return transaccion;
      }
    } catch (error) {
      throw error;
    }
  }
  
}
