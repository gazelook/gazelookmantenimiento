import { InformacionPago } from './../../../drivers/mongoose/interfaces/informacion_pago/informacion-pago.interface';
import {
  Inject,
  Injectable,
} from '@nestjs/common';
import { Model } from 'mongoose';

@Injectable()
export class CrearinformacionPagoService {
  constructor(
    @Inject('INFORMACION_PAGO_MODEL')
    private readonly informacionPago: Model<InformacionPago>,
  ) { }
  async crearInformacionPago(datos: any, idPago, opts: any): Promise<any> {
    try {
      // Guardar informacion de pago
      const infoPago = await new this.informacionPago({
        datos: datos,
        idPago: idPago
      }).save(opts);

      return infoPago;
    } catch (error) {
      throw error;
    }
  }

}