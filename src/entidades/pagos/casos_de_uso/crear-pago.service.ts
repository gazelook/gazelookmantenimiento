import {
  HttpStatus,
  Inject,
  Injectable
} from '@nestjs/common';
import * as mongoose from 'mongoose';
import { Model } from 'mongoose';
import { Usuario } from 'src/drivers/mongoose/interfaces/usuarios/usuario.interface';
import { CatalogoIdiomasService } from 'src/entidades/catalogos/casos_de_uso/catalogo-idiomas.service';
import { CrearEmailReciboPagoService } from 'src/entidades/emails/casos_de_uso/crear-email-recibo-pago.service';
import { CrearPagoPaymentezService } from 'src/entidades/transaccion/casos_de_uso/crear-pago-paymentez.service';
import { GestionPagoPaymentezService } from 'src/entidades/transaccion/casos_de_uso/gestion-pago-paymentez.service';
import { ObtenerIdUsuarioService } from 'src/entidades/usuario/casos_de_uso/obtener-id-usuario.service';
import { listaMonedasCripto } from 'src/money/enum-lista-money';
import { erroresCuenta, erroresGeneral } from 'src/shared/enum-errores';
import { codigosMetodosPago, descripcionTransPagosGazelook, porcentajeComisionPaymentez } from 'src/shared/enum-sistema';
import { PagoDto } from '../dtos/pago.dto';
import { UpdateTransaccionPagoDto } from '../dtos/transaccion.dto';
import { ActualizarConversionTransaccionService } from './actualizar-conversion-transaccion.service';
import { ActualizarTransaccionService } from './actualizar-transaccion.service';
import { CrearPagoStripeService } from './crear-pago-stripe.service';
import { CrearTransaccionService } from './crear-transaccion.service';
import { ObtenerTransaccionService } from './obtener-transaccion.service';

@Injectable()
export class CrearPagoService {
  constructor(
    @Inject('USUARIO_MODEL') private readonly userModel: Model<Usuario>,
    private crearPagoStripeService: CrearPagoStripeService,
    private crearTransaccionService: CrearTransaccionService,
    private obtenerIdUsuarioService: ObtenerIdUsuarioService,
    private crearPagoPaymentezService: CrearPagoPaymentezService,
    private obtenerTransaccionService: ObtenerTransaccionService,
    private gestionPagoPaymentezService: GestionPagoPaymentezService,
    private actualizarTransaccionService: ActualizarTransaccionService,
    private actualizarConversionTransaccionService: ActualizarConversionTransaccionService,
    private catalogoIdiomasService: CatalogoIdiomasService,
    private crearEmailReciboPagoService: CrearEmailReciboPagoService
  ) { }

  async crearPago(dataPago: PagoDto): Promise<any> {

    const session = await mongoose.startSession();
    session.startTransaction();

    try {


      const opts = { session };
      let getUser = await this.obtenerIdUsuarioService.obtenerPerfilesUsuarioSessionById(dataPago.usuario._id)

      if (getUser) {

        // _____________________ datos de pago _________________
        const tipoMonedaRegistro = dataPago.monedaRegistro.codNombre;
        let currency2
        if (dataPago.pagoCripto) {
          currency2 = dataPago.pagoCripto.simboloCripto
        }

        let datosPago: any = {
          email: dataPago.email,
          idUsuario: dataPago.usuario._id,
          metodoPago: dataPago.metodoPago.codigo,
          tipoMonedaRegistro: tipoMonedaRegistro,
          currency2: currency2,
          currency1: listaMonedasCripto.BTC
        };


        let total = 0;
        for (let transaccion of dataPago.transacciones) {
          total += transaccion.monto;
        }

        datosPago.nombres = dataPago.datosFacturacion.nombres;
        datosPago.telefono = dataPago.datosFacturacion.telefono;
        datosPago.direccion = dataPago.datosFacturacion?.direccion
        datosPago.monto = total;
        datosPago.transacciones = dataPago.transacciones;
        datosPago.creacionCuenta = true;
        datosPago.description = descripcionTransPagosGazelook.valorExtra;
        // datosPago.idProyecto = dataPago.idProyecto;
        datosPago.idPago = dataPago.datosFacturacion.idPago;
        datosPago.pagoCripto = dataPago.pagoCripto

        // ___________________________________________Crear Pago____________________________________________
        let cuentaOk: any = {};


        if (codigosMetodosPago.stripe === dataPago.metodoPago.codigo) {
          const pagoStripe = await this.crearPagoStripeService.crearPagoStripe(
            datosPago,
            opts,
            true
          );

          cuentaOk.idPago = pagoStripe.stripe.client_secret;
          cuentaOk.idTransaccion = pagoStripe.idTransaccion;

          // proceso exitoso
          await session.commitTransaction();
          session.endSession();

          console.log('TERMINAAAAAAAAAA PAGOOOOOOOOOOOOOOOOOOO STRIPE');
          
        }

        if (codigosMetodosPago.payments1 === dataPago.metodoPago.codigo
          || codigosMetodosPago.payments2 === dataPago.metodoPago.codigo) {
          const pagoPaymentez = await this.crearPagoPaymentezService.crearPagoPaymentez(
            datosPago,
            opts,
            true
          );
          cuentaOk.idPago = dataPago.datosFacturacion.idPago;
          cuentaOk.idTransaccion = pagoPaymentez.idTransaccion;


          const transaccionQuery: any = await this.obtenerTransaccionService.obtenerTransaccionById(
            cuentaOk.idTransaccion,
            opts,
          );

          if (!transaccionQuery) {
            throw {
              codigo: HttpStatus.EXPECTATION_FAILED,
              codigoNombre: erroresCuenta.TRANSACCION_NO_VALIDA,
            };
          }

          const informacionPago = transaccionQuery.informacionPago;

          // ______________________________actualizar transaccion (dos origenes)________________________________
          const transaccionUser: any = await this.obtenerTransaccionService.obtenerTransaccionByUsuarioByPago(
            dataPago.usuario._id,
            informacionPago._id,
            opts,
          );
          for (const transaccion of transaccionUser) {
            //__________________Calcular valor total por transaccion________________
            const getNewValor = this.gestionPagoPaymentezService.calcularValorTotalComisionPaymentez(
              // detallePagoStripe.amount,
              transaccion.monto,
              porcentajeComisionPaymentez.porcentaje,
            );

            const dataUpdate: UpdateTransaccionPagoDto = {
              comisionTransferencia: getNewValor.newFee, // valor de la comision
              totalRecibido: getNewValor.newAmountReceived, // total recibido
              origenPais: null,
            };


            // if (transaccion.informacionPago.idPago === transaccionQuery.informacionPago.idPago) {
            // transaccionDatabase = await session.withTransaction(async () => {
            const updatetransaccion = await this.actualizarTransaccionService.actualizarTransaccion(
              transaccion._id,
              opts,
              dataUpdate,
            );
            //   if (!updatetransaccion) {
            //     await session.abortTransaction();
            //     return;
            //   }
            // }, transactionOptions);
            // }

            //__________________Actualizar conversion transaccion________________
            // transaccionDatabase = await session.withTransaction(async () => {
            const updateConversion = await this.actualizarConversionTransaccionService.actualizarConversionStripe(
              transaccion._id,
              dataUpdate,
              opts,
            );
            //   if (!updateConversion) {
            //     await session.abortTransaction();
            //     return;
            //   }
            // }, transactionOptions);
          }

          const getUsuario = await this.obtenerIdUsuarioService.obtenerUsuarioWithID(
            transaccionQuery.usuario._id,
            opts,
          );
  
           // agregar transaccion al usuario
           await this.userModel.findOneAndUpdate(
            { _id: getUsuario._id },
            { $push: { transacciones: cuentaOk.idTransaccion } },
            opts
          );

          const idiomaUsuario = await this.catalogoIdiomasService.obtenerIdiomaByCodigo(
            getUsuario.idioma,
          );

          //_______________________________enviar recibo pago___________________________________
          const dataEmailPago = {
            idInformacionPago: informacionPago._id as string,
            usuario: getUsuario._id as string,
            nombre: getUsuario.perfiles[0].nombre as string,
            emailDestinatario: getUsuario.email as string,
            idioma: idiomaUsuario.codNombre as string,
            autorizacionCodePaymentez: dataPago.autorizacionCodePaymentez as string
          };
          // await this.crearEmailReciboPagoService.crearEmailReciboPago(
          //   dataEmailPago,
          // );

          // proceso exitoso
          await session.commitTransaction();
          session.endSession();

          // transaccionDatabase = await session.withTransaction(async () => {
          await this.crearEmailReciboPagoService.crearEmailReciboPago(
            dataEmailPago,
            // opts
          );
          // }, transactionOptions);
          console.log('TERMINAAAAAAAAAA PAGOOOOOOOOOOOOOOOOOOO PAYMENTEZ');



        }

        return cuentaOk;

      } else {
        throw {
          codigo: HttpStatus.UNAUTHORIZED,
          codigoNombre: erroresGeneral.NO_AUTORIZADO,
        };
      }


    } catch (error) {
      //Aborta la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }

}