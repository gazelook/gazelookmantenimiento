import {
  Inject,
  Injectable,
} from '@nestjs/common';
import { Model } from 'mongoose';
import { ConversionTransaccion } from '../../../drivers/mongoose/interfaces/convercion_transaccion/convercion-transaccion.interface';
import { listaCodigosMonedas } from '../../../shared/enum-lista-money';
import { ConvertirMontoMonedaService } from '../../moneda/casos_de_uso/convertir-monto-moneda.service';

@Injectable()
export class ActualizarConversionTransaccionService {
  constructor(
    @Inject('CONVERSION_TRANSACCION_MODEL')
    private readonly conversionTransaccionModel: Model<ConversionTransaccion>,
    private convertirMontoMonedaService: ConvertirMontoMonedaService
  ) { }
  async actualizarConversionStripe(idTransaccion: string, dataUpdateUsd: any, opts: any): Promise<any> {
    try {

      const listTransaccionConversion = await this.conversionTransaccionModel.find({ transaccion: idTransaccion }).session(opts.session);

      console.log('tra')
      for (const transaccionConversion of listTransaccionConversion) {
        // ____________update usd________________
        if (transaccionConversion.moneda === listaCodigosMonedas.USD) {
          console.log('actualiza conversion transaccion en dolares')
          const dataUpdateConversionTransaccion = {
            comisionTransferencia: dataUpdateUsd.comisionTransferencia,
            totalRecibido: dataUpdateUsd.totalRecibido
          };
          await this.conversionTransaccionModel.findByIdAndUpdate(transaccionConversion._id, dataUpdateConversionTransaccion, opts);
        }

        // ____________update eur________________
        if (transaccionConversion.moneda === listaCodigosMonedas.EUR) {

          const feeEur = await this.convertirMontoMonedaService.convertirMoney(
            dataUpdateUsd.comisionTransferencia,
            listaCodigosMonedas.USD,
            listaCodigosMonedas.EUR
          );
          
          const totalRecibido = transaccionConversion.monto - feeEur;

          const dataUpdateConversionTransaccion = {
            comisionTransferencia: parseFloat(feeEur.toFixed(2)),
            totalRecibido: parseFloat(totalRecibido.toFixed(2))
          }
          await this.conversionTransaccionModel.findByIdAndUpdate(transaccionConversion._id, dataUpdateConversionTransaccion, opts);
        }

        // ____________update moneda del usuario________________
        if (transaccionConversion.moneda !== listaCodigosMonedas.USD && transaccionConversion.moneda !== listaCodigosMonedas.EUR) {
          const feeMonedaUser = await this.convertirMontoMonedaService.convertirMoney(
            dataUpdateUsd.comisionTransferencia,
            listaCodigosMonedas.USD,
            transaccionConversion.moneda
          );

          const totalRecibido = transaccionConversion.monto - feeMonedaUser;
          const dataUpdateConversionTransaccion = {
            comisionTransferencia: parseFloat(feeMonedaUser.toFixed(2)),
            totalRecibido: parseFloat(totalRecibido.toFixed(2)),
          }
          await this.conversionTransaccionModel.findByIdAndUpdate(transaccionConversion._id, dataUpdateConversionTransaccion, opts);
        }
      }

      return true;
    } catch (error) {
      throw error;
    }
  }

}