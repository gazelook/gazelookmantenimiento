import { Injectable } from '@nestjs/common';
import { CrearTransaccionValorExtraService } from 'src/entidades/transaccion/casos_de_uso/crear-transaccion-valor-extra.service';
import { descripcionTransPagosGazelook } from 'src/shared/enum-sistema';
import Stripe from 'stripe';
import { CustomersService } from '../../../drivers/stripe/services/payment-customers.service';
import { PaymentIntentsService } from '../../../drivers/stripe/services/payment-intents.service';
import { PagoStripeDto } from '../dtos/pago-stripe.dto';
import { CrearTransaccionService } from './crear-transaccion.service';



@Injectable()
export class CrearPagoStripeService {

  constructor(
    private customersService: CustomersService,
    private paymentIntentsService: PaymentIntentsService,
    private crearTransaccionService: CrearTransaccionService,
    private crearTransaccionValorExtraService: CrearTransaccionValorExtraService
  ) { }


  async crearPagoStripe(datosPago: any, opts?: any, valorExtra?: any): Promise<any> {
    try {
      // Guardar cliente en stripe
      const nuevoCliente: any = {
        name: datosPago.nombres,
        description: datosPago.description || descripcionTransPagosGazelook.suscripcion,
        email: datosPago.email,
        phone: datosPago.telefono,
        address: {
          line1: datosPago.direccion,
          // country: dataUser.pais,
        },
        // currency: 'usd'
      };

      const customer = await this.customersService.createCustomer(nuevoCliente);

      

      const monto = Math.floor(datosPago.monto * 100);

      // crear orden de pago
      const paymentIntent = await this.paymentIntentsService.createPaymentsIntent({
        amount: monto,
        userEmail: datosPago.email,
        idCustomer: customer.id,
      });

      console.log('paymentIntenttttttttttttttt: ', paymentIntent)
      // guardar transacción y suscripcon con estado pendiente
      const numeroRecibo = this.crearTransaccionService.generarNumeroRecibo();

      let transaccion
      if (valorExtra) {

        transaccion = await this.crearTransaccionValorExtraService.crearTransaccionValorExtraService(
          datosPago,
          paymentIntent.id,
          numeroRecibo,
          opts,
        );
        console.log('valor extra SIIIII: ', transaccion)
      } else {
        transaccion = await this.crearTransaccionService.crearTransaccion(
          datosPago,
          paymentIntent.id,
          numeroRecibo,
          opts,
        );
      }

      const result = {
        stripe: paymentIntent,
        idTransaccion: transaccion._id,
      };
      return result;

    } catch (error) {
      throw error;
    }
  }

}

