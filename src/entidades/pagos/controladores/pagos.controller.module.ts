import { Module, HttpModule } from '@nestjs/common';
import { Funcion } from 'src/shared/funcion';
import { PagosServiceModule } from '../casos_de_uso/pagos.services.module';
import { CrearPagoValorExtraController } from './crear-pago-valor-extra.controller';
import { CrearPagoController } from './crear-pago.controller';
import { ValidarPagoValorExtraControlador } from './validar-pago-valor-extra.controller';
import { ValidarPagoControlador } from './validar-pago.controller';

@Module({
  imports: [PagosServiceModule, HttpModule],
  providers: [Funcion],
  exports: [],
  controllers: [
    CrearPagoController,
    ValidarPagoControlador,
    CrearPagoValorExtraController,
    ValidarPagoValorExtraControlador
  ],
})
export class PagosControllerModule {}
