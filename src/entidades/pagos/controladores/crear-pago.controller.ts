import {
  Body, Controller, Headers, HttpStatus, Post, UseGuards
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiHeader, ApiOperation, ApiResponse, ApiSecurity, ApiTags
} from '@nestjs/swagger';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from '../../../shared/funcion';
import { CrearPagoService } from '../casos_de_uso/crear-pago.service';
import { PagoDto, ResultPagoDto } from '../dtos/pago.dto';


@ApiTags('Pagos')
@Controller('api/pago')
export class CrearPagoController {
  constructor(

    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
    private crearPagoService: CrearPagoService
  ) { }

  @Post('/')
  @ApiSecurity('Authorization')
  @ApiResponse({ status: 200, type: ResultPagoDto, description: 'Pago en estado pendiente de confirmación' })
  @ApiResponse({
    status: 400,
    description: 'Error al registrar el pago',
  })
  @ApiOperation({
    summary:
      'Crear la orden de pago',
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
    required: false,
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @UseGuards(AuthGuard('jwt'))

  public async crearPago(@Body() pagoDto: PagoDto, @Headers() headers) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {
      const pago = await this.crearPagoService.crearPago(pagoDto);

      if (pago) {
        const CREACION_CORRECTA = await this.i18n.translate(codIdioma.concat('.CREACION_CORRECTA'), {
          lang: codIdioma
        });
        return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.OK, mensaje: CREACION_CORRECTA, datos: pago });
      } else {
        const ERROR_CREACION = await this.i18n.translate(codIdioma.concat('.ERROR_CREACION'), {
          lang: codIdioma
        });
        return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.NOT_FOUND, mensaje: ERROR_CREACION })

      }

    } catch (e) {
      if (e?.codigoNombre) {
        const MENSAJE = await this.i18n.translate(codIdioma.concat(`.${e.codigoNombre}`), {
          lang: codIdioma
        });
        return this.funcion.enviarRespuestaOptimizada({ codigoEstado: e.codigo, mensaje: MENSAJE });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message
        });
      }
    }
  }

}
