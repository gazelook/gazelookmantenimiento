import {
  Body, Controller, Headers, HttpStatus, Post, Request, UseGuards
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiHeader, ApiOperation, ApiResponse, ApiSecurity, ApiTags
} from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from '../../../shared/funcion';
import { ValidarPagoService } from '../casos_de_uso/validar-pago.service';
import { ValidarPagoDto } from '../dtos/validar-pago.dto';



@ApiTags('Pagos')
@Controller('api/pago/validar')
export class ValidarPagoControlador {
  constructor(
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
    private validarPagoService: ValidarPagoService
  ) { }
  @Post('/')
  @ApiSecurity('Authorization')
  @ApiResponse({ status: 200, description: 'Pago Verificado' })
  @ApiResponse({
    status: 404,
    description: 'Error al validar el pago',
  })
  @ApiOperation({
    summary:
      'Valida que el pago haya sido satisfactoriamente creado',
  })
  @ApiHeader({
    name: 'idioma',
    description: 'codigo del idioma. Ejm: es, en, fr',
    required: false,
  })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @UseGuards(AuthGuard('jwt'))
  public async validarCuenta(@Body() validar: ValidarPagoDto, @Headers() headers, @Request() req) {

    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {

      const dispositivo = {
        ip: req.ip,
        userAgent: headers['user-agent'],
      }

      if (mongoose.isValidObjectId(validar.idTransaccion)) {
        const cuentaVerificada = await this.validarPagoService.validarPago(validar.idTransaccion);
        if (cuentaVerificada) {
          const CREACION_CORRECTA = await this.i18n.translate(codIdioma.concat('.CREACION_CORRECTA'), {
            lang: codIdioma
          });
          return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.CREATED, mensaje: CREACION_CORRECTA, datos: cuentaVerificada });
        } else {
          const ERROR_CREACION = await this.i18n.translate(codIdioma.concat('.ERROR_CREACION'), {
            lang: codIdioma
          });
          return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.CONFLICT, mensaje: ERROR_CREACION });
        }

      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(codIdioma.concat('.PARAMETROS_NO_VALIDOS'), {
          lang: codIdioma
        });
        return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.NOT_ACCEPTABLE, mensaje: PARAMETROS_NO_VALIDOS });
      }

    } catch (e) {
      if (e?.codigoNombre) {
        const MENSAJE = await this.i18n.translate(codIdioma.concat(`.${e.codigoNombre}`), {
          lang: codIdioma
        });
        return this.funcion.enviarRespuestaOptimizada({ codigoEstado: e.codigo, mensaje: MENSAJE });
      } else {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
          mensaje: e.message
        });
      }
    }
  }
}
