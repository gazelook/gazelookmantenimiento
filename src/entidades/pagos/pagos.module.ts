import { Module } from '@nestjs/common';
import { PagosServiceModule } from './casos_de_uso/pagos.services.module';
import { PagosControllerModule } from './controladores/pagos.controller.module';

@Module({
    imports: [
        PagosControllerModule,
    ],
    providers: [
        PagosServiceModule
    ]
})
export class PagosModule {}
