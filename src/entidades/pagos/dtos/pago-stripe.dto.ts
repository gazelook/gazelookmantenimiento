import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";


export class PagoStripeDto {
    @ApiProperty()
    @IsNotEmpty()
    nombres: string;
    
    @ApiProperty()
    @IsNotEmpty()
    email: string;
    
    @ApiProperty()
    @IsNotEmpty()
    telefono: string;
    
    @ApiProperty()
    @IsNotEmpty()
    direccion: string;
    
    @ApiProperty()
    @IsNotEmpty()
    monto: number;
}