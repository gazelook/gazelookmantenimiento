import { ApiProperty } from "@nestjs/swagger";
import { Type } from "class-transformer";
import { IsEmail, IsMongoId, IsNotEmpty, IsNotEmptyObject, IsNumber, IsObject, IsOptional, IsString, ValidateNested } from "class-validator";
import { CatalogoOrigenDto } from "../../catalogos/dto/catalogo-origen.dto";
import { CatalogoTipoMonedaCodigoDto } from "../../catalogos/dto/catalogo-tipo-moneda.dto";

export class MetodoPago {
    @ApiProperty({
        description: 'Código catálogo método de pago',
        required: true,
        example: 'METPAG_1:stripe',
    })
    @IsNotEmpty()
    codigo: string;
}

export class TransaccionCuentaDto {
    @ApiProperty({ description: 'Cantidad transacción en dólares', required: true, example: '11.99' })
    @IsNotEmpty()
    @IsNumber()
    monto: number;

    @ApiProperty({ type: CatalogoTipoMonedaCodigoDto })
    @IsNotEmpty()
    @ValidateNested({ each: true })
    moneda: CatalogoTipoMonedaCodigoDto;

    @ApiProperty({ type: CatalogoOrigenDto })
    @IsNotEmpty()
    @ValidateNested({ each: true })
    origen: CatalogoOrigenDto;

    @ApiProperty({ type: CatalogoOrigenDto })
    @IsNotEmpty()
    @ValidateNested({ each: true })
    destino: CatalogoOrigenDto;
}

export class DatosFacturacion {
    @ApiProperty()
    @IsNotEmpty()
    nombres: string;

    @ApiProperty()
    @IsNotEmpty() @IsEmail()
    email: string;

    @ApiProperty()
    telefono: string;

    @ApiProperty()
    @IsNotEmpty()
    direccion: string;

    // @ApiProperty({ description: 'Monto a pagar de la suscripción' })
    // @IsNotEmpty()
    // monto: number;

    @ApiProperty()
    idPago: string;
}
export class MonedaRegistro {
    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    codNombre: string
}

export class UsuarioTransaccion {
    @ApiProperty()
    @IsNotEmpty()
    @IsMongoId()
    _id: string
}

export class PagoCripto {
    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    nombreCripto: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    cantidad: number;

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    simboloCripto: string;
}


export class PagoDto {
    @ApiProperty({ type: MonedaRegistro })
    @IsObject()
    @IsNotEmptyObject()
    @ValidateNested({ each: true })
    @Type(() => MonedaRegistro)
    monedaRegistro: MonedaRegistro;

    @ApiProperty({ type: MetodoPago })
    @IsNotEmpty()
    @ValidateNested({ each: true })
    @Type(() => MetodoPago)
    metodoPago: MetodoPago;


    //monto de pago en diferentes monedas
    @ApiProperty({ type: [TransaccionCuentaDto] })
    @IsNotEmpty()
    @ValidateNested({ each: true })
    transacciones: TransaccionCuentaDto[];

    @ApiProperty({ type: DatosFacturacion })
    @IsOptional() @IsObject()
    @ValidateNested({ each: true })
    @Type(() => DatosFacturacion)
    datosFacturacion: DatosFacturacion;

    @ApiProperty({ type: UsuarioTransaccion })
    @IsNotEmpty()
    @ValidateNested({ each: true })
    @Type(() => UsuarioTransaccion)
    usuario: UsuarioTransaccion;

    //usuario
    @ApiProperty()
    @IsNotEmpty()
    @IsEmail() //@Matches(/^\w+([.-]?\w+)@\w+([.-]?\w+)(.\w{2,4})+$/, { message: 'No valid Email!!' })
    email: string;

    @ApiProperty({ type: PagoCripto })
    pagoCripto: PagoCripto;

    //autorizacionCodePaymentez
    @ApiProperty({ type: String })
    autorizacionCodePaymentez: string;
}

export class ResultPagoDto {
    @ApiProperty()
    idPago?: string;
    @ApiProperty()
    idTransaccion?: string;
}