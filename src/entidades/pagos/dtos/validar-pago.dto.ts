import { ApiProperty } from '@nestjs/swagger';
import { IsMongoId, IsNotEmpty } from 'class-validator';


export class ValidarPagoDto {

  @ApiProperty()
  @IsNotEmpty() @IsMongoId()
  idTransaccion: string
}
