import { Injectable } from '@nestjs/common';
import { join } from 'path';
import { idiomas, servidor } from '../../../shared/enum-sistema';

import { readFileSync } from 'fs';
import { NodeMailerService } from '../../../drivers/lib_email/nodemailer.service';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import { ConfigService } from '../../../config/config.service';
const handlebars = require('handlebars');


@Injectable()
export class ConfirmacionUsuarioMenorEdadService {

  constructor(
    private nodeMailerService: NodeMailerService,
    private config: ConfigService
  ) { }

  async enviarEmailMenorEdad(formulario): Promise<any> {
    console.log('formulario', formulario);
    let mensaje = '';
    let htmlToSend = null;

    const filePath = join(__dirname, '../../../templates_email/menor-edad.html');
    const source = readFileSync(filePath, 'utf-8').toString();
    const template = handlebars.compile(source);

    const HOST_REMOTO = this.config.get('HOST_REMOTO');

    const urlCambiarEmail = `${HOST_REMOTO}/api/email/confirmar-cambio-email-responsable/${formulario.tokenEmail}/${formulario.idioma}`
    
    let parte1 = 'Confirmación Menor de Edad<';
    let parte2 = 'Estimado';
    let parte3 = 'Para completar la creación de la cuenta es necesario que el responsable confirme la misma';
    let parte4 = '¿Desea actualizar el email del responsable?';
    let parte5 = 'CAMBIAR EMAIL RESPONSABLE';
    let parte6 = 'Confirmación Cuenta Gazelook';

    if (formulario.idioma !== idiomas.espanol) {

      let textoUnido = parte1 +
        ' [-]' + parte2 +
        ' [-]' + parte3 +
        ' [-]' + parte4 +
        ' [-]' + parte5 +
        ' [-]' + parte6;

      let textoTraducido = await traducirTexto(formulario.idioma, textoUnido);
      const textS = textoTraducido.textoTraducido.split('[-]');

      parte1 = textS[0].trim();
      parte2 = textS[1].trim();
      parte3 = textS[2].trim();
      parte4 = textS[3].trim();
      parte5 = textS[4].trim();
      parte6 = textS[5].trim();

      console.log('-----------DATA TRADUCIDA-------------')
      console.log('textS[1].trim(): ', textS[0].trim())
      console.log('textS[0].trim(): ', textS[1].trim())
      console.log('textS[2].trim(): ', textS[2].trim())
      console.log('textS[3].trim(): ', textS[3].trim())
      console.log('textS[4].trim(): ', textS[4].trim())
      console.log('textS[5].trim(): ', textS[5].trim())
    }
    
    const replacements = {
      urlCambiarEmail: urlCambiarEmail,
      nombre: formulario.nombre,
      parte1: parte1,
      parte2: parte2,
      parte3: parte3,
      parte4: parte4,
      parte5: parte5,
    };
    htmlToSend = template(replacements);

    const mailOptions = {
      to: formulario.emailDestinatario,
      subject: parte6,
      html: htmlToSend,

    };

    const resp = await this.nodeMailerService.enviarEmail(mailOptions);
    return resp;

  }

}
