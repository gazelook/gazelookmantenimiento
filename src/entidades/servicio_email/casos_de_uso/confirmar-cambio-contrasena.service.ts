import { HttpService, Inject, Injectable } from '@nestjs/common';
import { resolve, join } from 'path';

import { readFileSync } from 'fs';
import { NodeMailerService } from '../../../drivers/lib_email/nodemailer.service';
import { idiomas, servidor } from '../../../shared/enum-sistema';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import { ConfigService } from '../../../config/config.service';
const handlebars = require('handlebars');


@Injectable()
export class ConfirmarCambioContrasenaService {

  constructor(
    private nodeMailerService: NodeMailerService,
    private config: ConfigService
  ) { }

  async confirmarCambioContrasena(formulario): Promise<any> {
    console.log('formulario', formulario);
    let htmlToSend = null;


    const filePath = join(__dirname, '../../../templates_email/confirmar-cambio-contrasena.html');
    const source = readFileSync(filePath, 'utf-8').toString();
    const template = handlebars.compile(source);

    let parte1 = 'Cambio de Contraseña';
    let parte2 = 'CONTRASEÑA CAMBIADA CON EXITO';
    let parte3 = 'Confirmacion Cambio Contraseña';

    if (formulario.idioma !== idiomas.espanol) {

      let textoUnido = parte1 +
        ' [-]' + parte2 +
        ' [-]' + parte3;

      let textoTraducido = await traducirTexto(formulario.idioma, textoUnido);
      const textS = textoTraducido.textoTraducido.split('[-]');

      parte1 = textS[0].trim();
      parte2 = textS[1].trim();
      parte3 = textS[2].trim();

      console.log('-----------DATA TRADUCIDA-------------')
      console.log('parte1: ', parte1)
      console.log('parte2: ', parte2)
      console.log('parte3: ', parte3)
    }
    
    const replacements = {
      emailDestinatario: formulario.emailDestinatario,
      parte1: parte1,
      parte2: parte2
    };
    htmlToSend = template(replacements);


    const mailOptions = {
      to: formulario.emailDestinatario,
      subject: parte3,
      html: htmlToSend,

    };
    const resp = await this.nodeMailerService.enviarEmail(mailOptions);
    return resp;

  }

}
