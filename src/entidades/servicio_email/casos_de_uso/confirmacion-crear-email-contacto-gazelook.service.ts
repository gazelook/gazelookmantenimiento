import { HttpService, Inject, Injectable } from '@nestjs/common';
import { resolve, join } from 'path';
import { codigoCatalogosTipoEmail, servidor } from '../../../shared/enum-sistema';

import { readFileSync } from 'fs';
import { NodeMailerService } from '../../../drivers/lib_email/nodemailer.service';
import { ConfigService } from '../../../config/config.service';
const handlebars = require('handlebars');


@Injectable()
export class ConfirmacionCrearEmailContactoService {

  constructor(
    private nodeMailerService: NodeMailerService,
    private config: ConfigService
  ) { }

  async confirmacionCrearEmailContactoGazelook(formulario): Promise<any> {

    let htmlToSend = null;

    const filePath = join(__dirname, '../../../templates_email/contacto-gazelook.html');
    const source = readFileSync(filePath, 'utf-8').toString();
    const template = handlebars.compile(source);

    let textoCorreo = "";
    textoCorreo += `<h1>Contacto Gazelook</h1>`;

    if (formulario.nombre) {
      let textoNombre = `<p><strong>Nombre:</strong>: ${formulario.nombre}</p>`;
      textoCorreo += textoNombre;
    }
  
    if (formulario.email) {
      let textoEmail = `<p><strong>Email</strong>: ${formulario.email}</p>`;
      textoCorreo += textoEmail;
    }
    if (formulario.pais) {
      let textoPais = `<p><strong>País</strong>: ${formulario.pais}</p>`;
      textoCorreo += textoPais;
    }
    if (formulario.direccion) {
      let textoDireccion = `<p><strong>Dirección</strong>: ${formulario.direccion}</p>`;
      textoCorreo += textoDireccion;
    }
    if (formulario.mensaje) {
      let textoMensaje = `<p><strong>Mensaje:</strong></p><p>${formulario.mensaje}</p>`;
      textoCorreo += textoMensaje;
    }

    //data para mostrar en vista html
    const replacements = {
      textoCorreo: textoCorreo
    };
    htmlToSend = template(replacements);

    const mailOptions = {
      to: formulario.emailDestinatario,
      subject: formulario.tema,
      html: textoCorreo
    };

    const resp = await this.nodeMailerService.enviarEmail(mailOptions);
    return resp;

  }

}
