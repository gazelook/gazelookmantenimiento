import { HttpService, Inject, Injectable } from '@nestjs/common';
import { resolve, join } from 'path';
import { servidor } from '../../../shared/enum-sistema';

import { readFileSync } from 'fs';
import { NodeMailerService } from '../../../drivers/lib_email/nodemailer.service';
import { ConfigService } from '../../../config/config.service';
const handlebars = require('handlebars');


@Injectable()
export class ActualizacionPoliticasLegalesService {

  constructor(
    private nodeMailerService: NodeMailerService,
    private config: ConfigService
  ) { }

  async actualizacionPoliticas(formulario): Promise<any> {
    console.log('formulario', formulario);
    let mensaje = '';
    let htmlToSend = null;

    const filePath = join(__dirname, '../../../templates_email/actualizacion-politicas-legales.html');

    const source = readFileSync(filePath, 'utf-8').toString();
    const template = handlebars.compile(source);
    const replacements = {
        link: formulario.link,
    };
    htmlToSend = template(replacements);


    const mailOptions = {
        to: formulario.emailDestinatarios,
        subject: "Cambios en la políticas de privacidad de Gazelook",
        html: htmlToSend,

    };

    const resp = await this.nodeMailerService.enviarEmail(mailOptions);
    return resp;

  }

}
