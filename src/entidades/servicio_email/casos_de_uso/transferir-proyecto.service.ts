import { Injectable } from '@nestjs/common';
import { join } from 'path';

import { readFileSync } from 'fs';
import { NodeMailerService } from '../../../drivers/lib_email/nodemailer.service';
import { idiomas, servidor } from '../../../shared/enum-sistema';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import { ConfigService } from '../../../config/config.service';
const handlebars = require('handlebars');


@Injectable()
export class TransferirProyectoService {

  constructor(
    private nodeMailerService: NodeMailerService,
    private config: ConfigService
  ) { }

  async transferirProyecto(formulario): Promise<any> {
    let htmlToSend = null;
    console.log("formulario", formulario);

    const datos = formulario.datosProceso;

    const filePath = join(__dirname, '../../../templates_email/transferir-proyecto.html');
    const source = readFileSync(filePath, 'utf-8').toString();
    const template = handlebars.compile(source);

    const HOST_REMOTO = this.config.get('HOST_REMOTO');

    const urlAceptar = `${HOST_REMOTO}/api/email/confirmar-email-trasferir-proyecto/${formulario.tokenEmail}/true/${datos.idioma}`
    const urlCancelar = `${HOST_REMOTO}/api/email/confirmar-email-trasferir-proyecto/${formulario.tokenEmail}/false/${datos.idioma}`

    let parte1 = 'Estimado';
    let parte2 = 'El propietario';
    let parte3 = 'desea asignarle como nuevo propietario, del proyecto:';
    let parte4 = 'Titulo Corto:';
    let parte5 = 'Descripcion:';
    let parte6 = 'ACEPTAR CAMBIO DE PROPIETARIO';
    let parte7 = 'ACEPTAR';
    let parte8 = 'CANCELAR';
    let parte9 = 'Caso contrario, ignorar el correo o cancele la transferencia';
    let parte10 = 'Transferencia de proyecto Gazelook';

    if (datos.idioma !== idiomas.espanol) {

      let textoUnido = parte1 +
        ', [-]' + parte2 +
        ' [-]' + parte3 +
        ' [-]' + parte4 +
        ' [-]' + parte5 +
        ' [-]' + parte6 +
        ' [-]' + parte7 +
        ' [-]' + parte8 +
        ' [-]' + parte9 +
        ' [-]' + parte10;

      let textoTraducido = await traducirTexto(datos.idioma, textoUnido);
      const textS = textoTraducido.textoTraducido.split('[-]');

      parte1 = textS[0].trim();
      parte2 = textS[1].trim();
      parte3 = textS[2].trim();
      parte4 = textS[3].trim();
      parte5 = textS[4].trim();
      parte6 = textS[5].trim();
      parte7 = textS[6].trim();
      parte8 = textS[7].trim();
      parte9 = textS[8].trim();
      parte10 = textS[9].trim();

      console.log('-----------DATA TRADUCIDA-------------')
      console.log('textS[1].trim(): ', textS[0].trim())
      console.log('textS[0].trim(): ', textS[1].trim())
      console.log('textS[2].trim(): ', textS[2].trim())
      console.log('textS[3].trim(): ', textS[3].trim())
      console.log('textS[4].trim(): ', textS[4].trim())
      console.log('textS[5].trim(): ', textS[5].trim())
      console.log('textS[6].trim(): ', textS[6].trim())
      console.log('textS[7].trim(): ', textS[7].trim())
      console.log('textS[8].trim(): ', textS[8].trim())
    }


    const replacements = {
      perfilPropietario: datos.perfilPropietario,
      nombrePropietario: datos.nombrePropietario,
      nombrePerfilNuevo: datos.nombrePerfilNuevo,
      perfilNuevo: datos.perfilNuevo,
      emailDestinatario: formulario.emailDestinatario,
      idProyecto: datos.idProyecto,
      //codigo: formulario.codigo,
      titulo: datos.titulo,
      tituloCorto: datos.tituloCorto,
      descripcion: datos.descripcion,
      idioma: datos.idioma,
      urlAceptar: urlAceptar,
      urlCancelar: urlCancelar,
      parte1: parte1,
      parte2: parte2,
      parte3: parte3,
      parte4: parte4,
      parte5: parte5,
      parte6: parte6,
      parte7: parte7,
      parte8: parte8,
      parte9: parte9,
      parte10:parte10
      // parte11:textS[10].trim(),
    };
    htmlToSend = template(replacements);


    const mailOptions = {
      to: formulario.emailDestinatario,
      subject: parte10,
      html: htmlToSend,

    };
    const resp = await this.nodeMailerService.enviarEmail(mailOptions);
    return resp;

  }

}
