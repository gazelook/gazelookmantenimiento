import { HttpService, Inject, Injectable } from '@nestjs/common';
import { resolve, join } from 'path';
import { codigoCatalogosTipoEmail, idiomas, servidor } from '../../../shared/enum-sistema';

import { readFileSync } from 'fs';
import { NodeMailerService } from '../../../drivers/lib_email/nodemailer.service';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import { ConfigService } from '../../../config/config.service';
const handlebars = require('handlebars');
import { I18nService } from 'nestjs-i18n';
import { contactEmail, recursosSistemaEN } from 'src/shared/enum-recursos-sistema';


@Injectable()
export class ConfirmacionEmailCuentaService {

  constructor(
    private nodeMailerService: NodeMailerService,
    private config: ConfigService,
    private readonly i18n: I18nService
  ) { }

  async confirmacionEmailCuenta(formulario): Promise<any> {
    console.log('formulario', formulario);
    let htmlToSend = null;

    const HOST_REMOTO = this.config.get('HOST_REMOTO');
    // const HOST_REMOTO = this.config.get('HOST');

    const urlNuevoEmail = `${HOST_REMOTO}/api/email/nuevo-email-cuenta/${formulario.tokenEmail}/${formulario.idioma}`

    let m10v2texto1;
    // if (formulario.codigo === codigoCatalogosTipoEmail.validacion_correo_normal) {
      
    const filePath = join(__dirname, '../../../templates_email/confirmacion-email-cuenta.html');
      const source = readFileSync(filePath, 'utf-8').toString();
      const template = handlebars.compile(source);

      const urlConfirmar = `${HOST_REMOTO}/api/email/confirmar-email-cuenta/${formulario.tokenEmail}/${formulario.idioma}`

      const codIdioma = formulario.idioma;
      const urlRecursoSistema = this.config.get<string>("URL_RECURSOS_SISTEMA");

      //OBTIENE EL NOMBRE DEL USUARIO
      const nombreUser = formulario.nombre.split('-');
      let nombre = nombreUser[0].trim().concat(' ').concat(nombreUser[1].trim());

      let urlHeaderEmail;
      let urlFooterEmail;
      let contact;

      if (codIdioma === idiomas.espanol) {
        urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_ES}`;
        urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_ES}`;
        contact = contactEmail.contactES;
      }
      if (codIdioma === idiomas.ingles) {
        urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_EN}`;
        urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_EN}`;
        contact = contactEmail.contactEN;
      }
      if (codIdioma === idiomas.frances) {
        urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_FR}`;
        urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_FR}`;
        contact = contactEmail.contactFR;
      }
      if (codIdioma === idiomas.italiano) {
        urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_IT}`;
        urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_IT}`;
        contact = contactEmail.contactIT;
      }
      if (codIdioma === idiomas.aleman) {
        urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_DE}`;
        urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_DE}`;
        contact = contactEmail.contactDE;
      }
      if (codIdioma === idiomas.portugues) {
        urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_PT}`;
        urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_PT}`;
        contact = contactEmail.contactPT;
      }


      //TRADUCCION DE TEXTOS ESTATCOS SEGUN EL IDIOMA DEL USUARIO
      m10v2texto1 = await this.i18n.translate(codIdioma.concat('.m10v2texto1'), {
        lang: codIdioma
      });
      const m10v2texto2 = await this.i18n.translate(codIdioma.concat('.m10v2texto2'), {
        lang: codIdioma
      });
      const m10v2texto3 = await this.i18n.translate(codIdioma.concat('.m10v2texto3'), {
        lang: codIdioma
      });
      const m10v2texto4 = await this.i18n.translate(codIdioma.concat('.m10v2texto4'), {
        lang: codIdioma
      });
      const m10v2texto5 = await this.i18n.translate(codIdioma.concat('.m10v2texto5'), {
        lang: codIdioma
      });
      const m10v2texto6 = await this.i18n.translate(codIdioma.concat('.m10v2texto6'), {
        lang: codIdioma
      });
      const m10v2texto7 = await this.i18n.translate(codIdioma.concat('.m10v2texto7'), {
        lang: codIdioma
      });

      const replacements = {
        tokenEmail: formulario.tokenEmail,
        urlConfirmar: urlConfirmar,
        urlNuevoEmail: urlNuevoEmail,
        m10v2texto1: m10v2texto1,
        m10v2texto2: m10v2texto2,
        m10v2texto3: m10v2texto3,
        m10v2texto4: m10v2texto4,
        m10v2texto5: m10v2texto5,
        m10v2texto6: m10v2texto6,
        m10v2texto7: m10v2texto7,
        urlHeaderEmail: urlHeaderEmail,
        urlFooterEmail: urlFooterEmail,
        contact: contact,
        nombreUser: nombre
      };
      htmlToSend = template(replacements);
    // }
    // else if (formulario.codigo === codigoCatalogosTipoEmail.validacion_correo_responsable) {
    //   const filePath = join(__dirname, '../../../templates_email/confirmacion-email-responsable.html');
    //   const source = readFileSync(filePath, 'utf-8').toString();
    //   const template = handlebars.compile(source);

    //   const urlConfirmar = `${HOST_REMOTO}/api/email/confirmar-email-cuenta/${formulario.tokenEmail}/${formulario.idioma}`
    //   const urlRechazo = `${HOST_REMOTO}/api/email/confirmar-rechazo-cuenta-responsable/${formulario.tokenEmail}/${formulario.idioma}`

    //   let parte1 = 'Confirmar E-mail Responsable';
    //   let parte2 = 'Cuenta de Gazelook Registrada';
    //   let parte3 = 'Su correo esta registrado como responsable de la cuenta del menor de edad';
    //   let parte4 = 'Desea confirmar la cuenta';
    //   let parte5 = 'CONFIRMAR CUENTA';
    //   let parte6 = 'RECHAZAR CUENTA';
    //   let parte7 = 'RECHAZAR'


    //   if (formulario.idioma !== idiomas.espanol) {

    //     let textoUnido = parte1 +
    //       ' [-]' + parte2 +
    //       ' [-]' + parte3 +
    //       ' [-]' + parte4 +
    //       ' [-]' + parte5 +
    //       ' [-]' + parte6 + 
    //       ' [-]' + parte7 +
    //       ' [-]' + parte8;

    //     let textoTraducido = await traducirTexto(formulario.idioma, textoUnido);
    //     const textS = textoTraducido.textoTraducido.split('[-]');

    //     parte1 = textS[0].trim();
    //     parte2 = textS[1].trim();
    //     parte3 = textS[2].trim();
    //     parte4 = textS[3].trim();
    //     parte5 = textS[4].trim();
    //     parte6 = textS[5].trim();
    //     parte7 = textS[6].trim();
    //     parte8 = textS[7].trim();

    //     console.log('-----------DATA TRADUCIDA-------------')
    //     console.log('parte1: ', parte1)
    //     console.log('parte2: ', parte2)
    //     console.log('parte3: ', parte3)
    //   }

    //   const replacements = {
    //     tokenEmail: formulario.tokenEmail,
    //     urlConfirmar: urlConfirmar,
    //     urlRechazo: urlRechazo,
    //     idioma: formulario.idioma,
    //     nombre: formulario.nombre,
    //     parte1: parte1,
    //     parte2: parte2,
    //     parte3: parte3,
    //     parte4: parte4,
    //     parte5: parte5,
    //     parte6: parte6,
    //     parte7: parte7
    //   };
    //   htmlToSend = template(replacements);
    // }

    const mailOptions = {
      to: formulario.emailDestinatario,
      subject: m10v2texto1,
      html: htmlToSend
    };

    const resp = await this.nodeMailerService.enviarEmail(mailOptions);
    return resp;

  }

}
