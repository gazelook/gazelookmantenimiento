import { Injectable } from '@nestjs/common';
import { join } from 'path';

import { readFileSync } from 'fs';
import { NodeMailerService } from '../../../drivers/lib_email/nodemailer.service';

import { ConfigService } from '../../../config/config.service';
const handlebars = require('handlebars');


@Injectable()
export class NotificarErrorEmailService {

  constructor(
    private nodeMailerService: NodeMailerService,
    private config: ConfigService
  ) { }

  async notificarError(formulario): Promise<any> {
    let htmlToSend = null;
    console.log("formulario", formulario);


    const filePath = join(__dirname, '../../../templates_email/errores-sistema.html');
    const source = readFileSync(filePath, 'utf-8').toString();
    const template = handlebars.compile(source);


    const replacements = {
      parte1: formulario.date,
      parte2: formulario.status,
      parte3: formulario.method,
      parte4: formulario.url,
      parte5: formulario.ip,
      parte6: formulario.browser,
      parte7: formulario.content,
    };
    htmlToSend = template(replacements);

    const mailOptions = {
      to: formulario.emailDestinatario,
      subject: "Errores del Sistema",
      html: htmlToSend,

    };
    const resp = await this.nodeMailerService.enviarEmail(mailOptions);
    return resp;

  }

}
