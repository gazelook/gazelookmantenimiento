import { Injectable } from '@nestjs/common';
import { readFileSync } from 'fs';
import { I18nService } from 'nestjs-i18n';
import { join } from 'path';
import { contactEmail, pdfBienvenida, recursosSistemaEN, pdfBienvenidaNombre } from 'src/shared/enum-recursos-sistema';
import { ConfigService } from '../../../config/config.service';
import { NodeMailerService } from '../../../drivers/lib_email/nodemailer.service';
import { idiomas } from '../../../shared/enum-sistema';

const handlebars = require('handlebars');


@Injectable()
export class CorreoBienvenidaService {

  constructor(
    private nodeMailerService: NodeMailerService,
    private config: ConfigService,
    private readonly i18n: I18nService
  ) { }

  async correoBienvenida(formulario): Promise<any> {
    let htmlToSend = null;

    const filePath = join(__dirname, '../../../templates_email/correo-bienvenida.html');
    const source = readFileSync(filePath, 'utf-8').toString();
    const template = handlebars.compile(source);

    const HOST_REMOTO = this.config.get('HOST_REMOTO');

    // const urlConfirmar = `${HOST_REMOTO}/api/email/confirmar-email-recuperar-contrasenia/${formulario.tokenEmail}/${formulario.idioma}`;

    const codIdioma = formulario.idioma;
    const urlRecursoSistema = this.config.get<string>("URL_RECURSOS_SISTEMA");


    //OBTIENE EL NOMBRE DEL USUARIO
    const nombreUser = formulario.usuario.split('-');
    let nombre = nombreUser[0].trim().concat(' ').concat(nombreUser[1].trim());

    let urlHeaderEmail
    let urlFooterEmail
    let pdf
    let contact
    let pdfName

    if (codIdioma === idiomas.espanol) {
      urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_ES}`;
      urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_ES}`;
      pdf = `${urlRecursoSistema}${pdfBienvenida.pdfES}`;
      contact = contactEmail.contactES;
      pdfName = pdfBienvenidaNombre.pdfES;
    }
    if (codIdioma === idiomas.ingles) {
      urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_EN}`;
      urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_EN}`;
      pdf = `${urlRecursoSistema}${pdfBienvenida.pdfEN}`;
      contact = contactEmail.contactEN;
      pdfName = pdfBienvenidaNombre.pdfEN;
    }
    if (codIdioma === idiomas.frances) {
      urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_FR}`;
      urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_FR}`;
      pdf = `${urlRecursoSistema}${pdfBienvenida.pdfFR}`;
      contact = contactEmail.contactFR;
      pdfName = pdfBienvenidaNombre.pdfFR;
      // nombre = nombre.concat(' ');
    }
    if (codIdioma === idiomas.italiano) {
      urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_IT}`;
      urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_IT}`;
      pdf = `${urlRecursoSistema}${pdfBienvenida.pdfIT}`;
      contact = contactEmail.contactIT;
      pdfName = pdfBienvenidaNombre.pdfIT;
    }
    if (codIdioma === idiomas.aleman) {
      urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_DE}`;
      urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_DE}`;
      pdf = `${urlRecursoSistema}${pdfBienvenida.pdfDE}`;
      contact = contactEmail.contactDE;
      pdfName = pdfBienvenidaNombre.pdfDE;
    }
    if (codIdioma === idiomas.portugues) {
      urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_PT}`;
      urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_PT}`;
      pdf = `${urlRecursoSistema}${pdfBienvenida.pdfPT}`;
      contact = contactEmail.contactPT;
      pdfName = pdfBienvenidaNombre.pdfPT;
    }

    //TRADUCCION DE TEXTOS ESTATCOS SEGUN EL IDIOMA DEL USUARIO
    const m10v3texto1 = await this.i18n.translate(codIdioma.concat('.m10v3texto1'), {
      lang: codIdioma
    });
    const m10v3texto2 = await this.i18n.translate(codIdioma.concat('.m10v3texto2'), {
      lang: codIdioma
    });
    const m10v3texto3 = await this.i18n.translate(codIdioma.concat('.m10v3texto3'), {
      lang: codIdioma
    });
    const m10v3texto4 = await this.i18n.translate(codIdioma.concat('.m10v3texto4'), {
      lang: codIdioma
    });
    const m10v3texto5 = await this.i18n.translate(codIdioma.concat('.m10v3texto5'), {
      lang: codIdioma
    });
    const m10v3texto6 = await this.i18n.translate(codIdioma.concat('.m10v3texto6'), {
      lang: codIdioma
    });
    const m10v3texto7 = await this.i18n.translate(codIdioma.concat('.m10v3texto7'), {
      lang: codIdioma
    });
    const m10v3texto8 = await this.i18n.translate(codIdioma.concat('.m10v3texto8'), {
      lang: codIdioma
    });



    const replacements = {
      // urlConfirmar: urlConfirmar,
      m10v3texto1: m10v3texto1,
      m10v3texto2: m10v3texto2,
      m10v3texto3: m10v3texto3,
      m10v3texto4: m10v3texto4,
      m10v3texto5: m10v3texto5,
      m10v3texto6: m10v3texto6,
      m10v3texto7: m10v3texto7,
      m10v3texto8: m10v3texto8,
      nombreUser: nombre,
      urlHeaderEmail: urlHeaderEmail,
      urlFooterEmail: urlFooterEmail,
      contact: contact,
      pdf: pdf
    };

    htmlToSend = template(replacements);

    const mailOptions = {
      to: formulario.emailDestinatario,
      subject: m10v3texto3,
      html: htmlToSend,
      attachments: [
        {   // utf-8 string as an attachment
          filename: pdfName,
          path: pdf
        }
      ]
    };
    const resp = await this.nodeMailerService.enviarEmail(mailOptions);
    return resp;

  }

}
