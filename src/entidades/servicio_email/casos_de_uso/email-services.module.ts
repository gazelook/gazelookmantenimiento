
import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { StorageModule } from '../../../drivers/amazon_s3/storage.module';
import { LibEmailModule } from '../../../drivers/lib_email/lib-email.module';
import { servicioEmailProviders } from '../drivers/servicio-email.provider';
import { EliminacionDatosCuentaService } from './eliminacion-datos-cuenta.service';
import { PeticionDatosUsuarioService } from './peticion-datos-usuario.service';
// import { EnvioDatosUsuarioService } from './envio-datos-usuario.service';
import { RecuperarContrasenaService } from './recuperar-contrasena.service';
import { TransferirProyectoService } from './transferir-proyecto.service';
import { ConfirmacionEmailCuentaService } from './confirmacion-email-cuenta.service';
import { ConfirmarCambioContrasenaService } from './confirmar-cambio-contrasena.service';
import { ActualizacionPoliticasLegalesService } from './actualizacion-politicas-legales.service';
import { ConfirmacionUsuarioMenorEdadService } from './usuario-menor-edad.service';
import { Funcion } from '../../../shared/funcion';

import { EmailServicesModule } from '../../emails/casos_de_uso/email-services.module';
import { ConfirmacionCrearEmailContactoService } from './confirmacion-crear-email-contacto-gazelook.service';
import { EnvioNotificacionRechazoCuentaService } from './envio-notificacion-rechazo-cuenta.service';
import { EstadoSuscripcionService } from './estado-suscripcion.service';
import { NotificarErrorEmailService } from './notificar-error-email.service';
import { NotificarActualizacionEmailService } from './notificar-actualizacion-email.service';
import { EnvioReciboPagoService } from './envio-recibo-pago.service';
import { EnvioReciboTransaccionService } from './envio-recibo-transaccion.service';
import { CorreoBienvenidaService } from './correo-bienvenida.service';
import { ConfirmarCambioEmailService } from './confirmar-cambio-email.service';



@Module({
  imports: [
    DBModule,
    HttpModule,
    StorageModule,
    LibEmailModule,
    forwardRef(() => EmailServicesModule),
  ],
  providers: [
    ...servicioEmailProviders,
    PeticionDatosUsuarioService,
    EliminacionDatosCuentaService,
    // EnvioDatosUsuarioService,
    RecuperarContrasenaService,
    TransferirProyectoService,
    ConfirmacionEmailCuentaService,
    ConfirmarCambioContrasenaService,
    ActualizacionPoliticasLegalesService,
    ConfirmacionUsuarioMenorEdadService,
    EnvioNotificacionRechazoCuentaService,
    Funcion,
    ConfirmacionCrearEmailContactoService,
    EstadoSuscripcionService,
    NotificarErrorEmailService,
    NotificarActualizacionEmailService,
    EnvioReciboPagoService,
    EnvioReciboTransaccionService,
    CorreoBienvenidaService,
    ConfirmarCambioEmailService
  ],
  exports: [
    PeticionDatosUsuarioService,
    EliminacionDatosCuentaService,
    // EnvioDatosUsuarioService,
    RecuperarContrasenaService,
    TransferirProyectoService,
    ConfirmacionEmailCuentaService,
    ConfirmarCambioContrasenaService,
    ActualizacionPoliticasLegalesService,
    ConfirmacionUsuarioMenorEdadService,
    ConfirmacionCrearEmailContactoService,
    EnvioNotificacionRechazoCuentaService,
    EstadoSuscripcionService,
    NotificarErrorEmailService,
    NotificarActualizacionEmailService,
    EnvioReciboPagoService,
    EnvioReciboTransaccionService,
    CorreoBienvenidaService,
    ConfirmarCambioEmailService
  ],
  controllers: [

  ],
})
export class ServicioEmailServicesModule { }
