// import { HttpService, Inject, Injectable } from '@nestjs/common';
// import { resolve, join } from 'path';

// import { readFileSync } from 'fs';
// import { NodeMailerService } from '../../../drivers/lib_email/nodemailer.service';
// import { idiomas, servidor } from '../../../shared/enum-sistema';
// import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
// import { ConfigService } from '../../../config/config.service';
// const handlebars = require('handlebars');


// @Injectable()
// export class EnvioDatosUsuarioService {

//   constructor(
//     private nodeMailerService: NodeMailerService,
//     private config: ConfigService
//   ) { }

//   async envioDatosUsuario(formulario): Promise<any> {

//     console.log('formulario:---------------> ', formulario)
//     let htmlToSend = null;


//     const filePath = join(__dirname, '../../../templates_email/envio-datos-usuario.html');
//     const source = readFileSync(filePath, 'utf-8').toString();
//     const template = handlebars.compile(source);

//     const HOST_REMOTO = this.config.get('HOST_REMOTO');

//     const urlDescargar = `${HOST_REMOTO}${servidor.path_descargar_informacion}/${formulario.tokenEmail}/${formulario.idioma}`

//     let parte1 = 'Descarga de Datos';
//     let parte2 = 'Estimado';
//     let parte3 = 'Sus datos estan listos'
//     let parte4 = 'Puede descargar con el siguiente enlace';
//     let parte5 = 'Enlace';
//     let parte6 = 'Datos de su cuenta';

//     if (formulario.idioma !== idiomas.espanol) {

//       let textoUnido = parte1 +
//         ' [-]' + parte2 +
//         ' [-]' + parte3 +
//         ' [-]' + parte4 +
//         ' [-]' + parte5 +
//         ' [-]' + parte6;

//       let textoTraducido = await traducirTexto(formulario.idioma, textoUnido);
//       const textS = textoTraducido.textoTraducido.split('[-]');

//       parte1 = textS[0].trim();
//       parte2 = textS[1].trim();
//       parte3 = textS[2].trim();
//       parte4 = textS[3].trim();
//       parte5 = textS[4].trim();
//       parte6 = textS[5].trim();

//       console.log('-----------DATA TRADUCIDA-------------')
//       console.log('parte1: ', parte1)
//       console.log('parte2: ', parte2)
//     }

//     const replacements = {
//       emailDestinatario: formulario.emailDestinatario,
//       nombre: formulario.nombre,
//       url: urlDescargar,
//       parte1: parte1,
//       parte2: parte2,
//       parte3: parte3,
//       parte4: parte4,
//       parte5: parte5
//     };
//     htmlToSend = template(replacements);


//     const mailOptions = {
//       to: formulario.emailDestinatario,
//       subject: parte6,
//       // attachments: [{
//       //     path: formulario.url
//       // }],
//       html: htmlToSend,

//     };
//     const resp = await this.nodeMailerService.enviarEmail(mailOptions);
//     return resp;



//   }

// }
