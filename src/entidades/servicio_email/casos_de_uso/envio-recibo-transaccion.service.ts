import { Inject, Injectable } from '@nestjs/common';
import { join } from 'path';

import { readFileSync } from 'fs';
import { NodeMailerService } from '../../../drivers/lib_email/nodemailer.service';
import { I18nService } from 'nestjs-i18n';
import { ReciboPagoEmailDto } from '../dto/servicio-correo.dto';
import { Model } from 'mongoose';
import { Transaccion } from '../../../drivers/mongoose/interfaces/transaccion/transaccion.interface';
import { ConversionTransaccion } from '../../../drivers/mongoose/interfaces/convercion_transaccion/convercion-transaccion.interface';
import { listaCodigosMonedas } from '../../../shared/enum-lista-money';
import { catalogoOrigen, idiomas } from '../../../shared/enum-sistema';
import { ConfigService } from 'src/config/config.service';
import { recursosSistemaEN, contactEmail } from 'src/shared/enum-recursos-sistema';

const handlebars = require('handlebars');


@Injectable()
export class EnvioReciboTransaccionService {

  constructor(
    private nodeMailerService: NodeMailerService,
    private readonly i18n: I18nService,
    @Inject('TRANSACCION_MODEL') private readonly transaccionModel: Model<Transaccion>,
    @Inject('CONVERSION_TRANSACCION_MODEL') private readonly conversionTransaccionModel: Model<ConversionTransaccion>,
    private readonly config: ConfigService
  ) { }

  async envioRecibo(reciboPagoEmailDto: ReciboPagoEmailDto): Promise<any> {
    const idioma = reciboPagoEmailDto.idioma;

    //OBTIENE EL NOMBRE DEL USUARIO
    const nombreUser = reciboPagoEmailDto.nombre.split('-');
    let nombre = nombreUser[0].trim().concat(' ').concat(nombreUser[1].trim());

    const m10v1texto1 = await this.i18n.translate(idioma.concat('.m10v1texto1'), { lang: idioma });
    const m10v1texto2 = await this.i18n.translate(idioma.concat('.m10v1texto2'), { lang: idioma });
    const m10v1texto3 = await this.i18n.translate(idioma.concat('.m10v1texto3'), { lang: idioma });
    const m10v1texto4 = await this.i18n.translate(idioma.concat('.m10v1texto4'), { lang: idioma });
    const m10v1texto5 = await this.i18n.translate(idioma.concat('.m10v1texto5'), { lang: idioma });
    const m10v1texto6 = await this.i18n.translate(idioma.concat('.m10v1texto6'), { lang: idioma });
    const m10v1texto7 = await this.i18n.translate(idioma.concat('.m10v1texto7'), { lang: idioma });
    const m10v1texto8 = await this.i18n.translate(idioma.concat('.m10v1texto8'), { lang: idioma });
    const m10v1texto9 = await this.i18n.translate(idioma.concat('.m10v1texto9'), { lang: idioma });
    const m10v1texto10 = await this.i18n.translate(idioma.concat('.m10v1texto10'), { lang: idioma });
    const m10v1texto11 = await this.i18n.translate(idioma.concat('.m10v1texto11'), { lang: idioma });
    const m10v1texto12 = await this.i18n.translate(idioma.concat('.m10v1texto12'), { lang: idioma });
    const m10v1texto13 = await this.i18n.translate(idioma.concat('.m10v1texto13'), { lang: idioma });
    const m10v1texto14 = await this.i18n.translate(idioma.concat('.m10v1texto14'), { lang: idioma });
    const m10v1texto15 = await this.i18n.translate(idioma.concat('.m10v1texto15'), { lang: idioma });
    const m10v1texto16 = await this.i18n.translate(idioma.concat('.m10v1texto16'), { lang: idioma });
    const m10v1texto17 = await this.i18n.translate(idioma.concat('.m10v1texto17'), { lang: idioma });

    let htmlToSend = null;

    const getTransacciones: any = await this.transaccionModel.find({
      informacionPago: reciboPagoEmailDto.informacionPago
    })
      .populate({
        path: 'conversionTransaccion'
      })



    const filePath = join(__dirname, '../../../templates_email/recibo-pago-transaccion.html');
    const source = readFileSync(filePath, 'utf-8').toString();
    const template = handlebars.compile(source);

    const codIdioma = idioma;
    const urlRecursoSistema = this.config.get<string>("URL_RECURSOS_SISTEMA");
    let urlHeaderEmail;
    let urlFooterEmail;
    let contact;
    if (codIdioma === idiomas.espanol) {
      urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_ES}`;
      urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_ES}`;
      contact = contactEmail.contactES;
    }
    if (codIdioma === idiomas.ingles) {
      urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_EN}`;
      urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_EN}`;
      contact = contactEmail.contactEN;
    }
    if (codIdioma === idiomas.frances) {
      urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_FR}`;
      urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_FR}`;
      contact = contactEmail.contactFR;
      // nombreUser = nombreUser.concat(' ');
    }
    if (codIdioma === idiomas.italiano) {
      urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_IT}`;
      urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_IT}`;
      contact = contactEmail.contactIT;
    }
    if (codIdioma === idiomas.aleman) {
      urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_DE}`;
      urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_DE}`;
      contact = contactEmail.contactDE;
    }
    if (codIdioma === idiomas.portugues) {
      urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_PT}`;
      urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_PT}`;
      contact = contactEmail.contactPT;
    }

    const getFechaTransaccion = new Date(getTransacciones[0].fechaActualizacion);
    const fechaTransaccion = `${getFechaTransaccion.toDateString()} ${getFechaTransaccion.toLocaleTimeString()}`;
    const numeroRecibo = getTransacciones[0].numeroRecibo;

    let montoMonedaUsuario = 0, transferAmountUSD = 0, transferAmountEUR = 0;
    let comisionUSD = 0, comisionEUR = 0;
    let extraUSD = 0, extraEUR = 0;
    let suscripcionUSD = 0, suscripcionEUR = 0;
    let monedaUser: string;
    for (const transaccion of getTransacciones) {
      for (const conversionTransaccion of transaccion.conversionTransaccion) {
        const getConversion = await this.conversionTransaccionModel.findById(conversionTransaccion._id);
        if (getConversion.moneda === listaCodigosMonedas.USD) {

          transferAmountUSD += getConversion.monto;
          comisionUSD += getConversion.comisionTransferencia;
          console.log('transferAmountUSD: ', transferAmountUSD)
          console.log('comisionUSD: ', comisionUSD)
        }
        if (getConversion.moneda === listaCodigosMonedas.EUR) {
          transferAmountEUR += getConversion.monto;
          comisionEUR += getConversion.comisionTransferencia;
        }
        if (getConversion.principal) {
          montoMonedaUsuario += getConversion.monto;
          monedaUser = getConversion.moneda;
        }

        if (transaccion.origen === catalogoOrigen.valor_extra && getConversion.moneda === listaCodigosMonedas.USD) {
          extraUSD = getConversion.totalRecibido;
        }
        if (transaccion.origen === catalogoOrigen.valor_extra && getConversion.moneda === listaCodigosMonedas.EUR) {
          extraEUR = getConversion.totalRecibido;
        }

        if (transaccion.origen === catalogoOrigen.suscripciones && getConversion.moneda === listaCodigosMonedas.USD) {
          suscripcionUSD = getConversion.totalRecibido;
        }
        if (transaccion.origen === catalogoOrigen.suscripciones && getConversion.moneda === listaCodigosMonedas.EUR) {
          suscripcionEUR = getConversion.totalRecibido;
        }
      }

    }

    const totalReceivedUSD = (transferAmountUSD - comisionUSD).toFixed(2);
    const totalReceivedEUR = (transferAmountEUR - comisionEUR).toFixed(2);

    const valor80SusucriptionUSD = (suscripcionUSD - (suscripcionUSD * 0.80)).toFixed(2);
    const valor20SusucriptionUSD = (suscripcionUSD - (suscripcionUSD * 0.20)).toFixed(2);

    const valor80SusucriptionEUR = (suscripcionEUR - (suscripcionEUR * 0.80)).toFixed(2);
    const valor20SusucriptionEUR = (suscripcionEUR - (suscripcionEUR * 0.20)).toFixed(2);


    const replacements = {
      montoMonedaUsuario: montoMonedaUsuario.toFixed(2),
      monedaUser: monedaUser,

      transferAmountUSD: transferAmountUSD.toFixed(2),
      comisionUSD: comisionUSD.toFixed(2),
      transferAmountEUR: transferAmountEUR.toFixed(2),
      comisionEUR: comisionEUR.toFixed(2),
      totalReceivedUSD: totalReceivedUSD,
      totalReceivedEUR: totalReceivedEUR,

      extraUSD: extraUSD,
      extraEUR: extraEUR,

      suscripcionUSD: suscripcionUSD,
      suscripcionEUR: suscripcionEUR,

      valor80SusucriptionUSD: valor80SusucriptionUSD,
      valor20SusucriptionUSD: valor20SusucriptionUSD,
      valor80SusucriptionEUR: valor80SusucriptionEUR,
      valor20SusucriptionEUR: valor20SusucriptionEUR,

      urlHeaderEmail: urlHeaderEmail,
      urlFooterEmail: urlFooterEmail,
      fechaTransaccion: fechaTransaccion,
      numeroRecibo: numeroRecibo,
      nombreUser: nombre,
      m10v1texto2: m10v1texto2,
      m10v1texto3: m10v1texto3,
      m10v1texto4: m10v1texto4,
      m10v1texto5: m10v1texto5,
      m10v1texto6: m10v1texto6,
      m10v1texto7: m10v1texto7,
      m10v1texto8: m10v1texto8,
      m10v1texto9: m10v1texto9,
      m10v1texto1: m10v1texto1,
      m10v1texto10: m10v1texto10,
      m10v1texto11: m10v1texto11,
      m10v1texto12: m10v1texto12,
      m10v1texto13: m10v1texto13,
      m10v1texto14: m10v1texto14,
      m10v1texto15: m10v1texto15,
      m10v1texto16: m10v1texto16,
      m10v1texto17: m10v1texto17,
      contact: contact,
    };

    htmlToSend = template(replacements);


    const mailOptions = {
      to: reciboPagoEmailDto.emailDestinatario,
      subject: m10v1texto1,
      html: htmlToSend,

    };

    const resp = await this.nodeMailerService.enviarEmail(mailOptions);
    return resp;

  }

}
