import { HttpService, Inject, Injectable } from '@nestjs/common';
import { resolve, join } from 'path';
import { idiomas, servidor } from '../../../shared/enum-sistema';

import { readFileSync } from 'fs';
import { NodeMailerService } from '../../../drivers/lib_email/nodemailer.service';

import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import { ConfigService } from '../../../config/config.service';
const handlebars = require('handlebars');
import { I18nService } from 'nestjs-i18n';
import { contactEmail, recursosSistemaEN } from 'src/shared/enum-recursos-sistema';

@Injectable()
export class PeticionDatosUsuarioService {

  constructor(
    private nodeMailerService: NodeMailerService,
    private config: ConfigService,
    private readonly i18n: I18nService
  ) { }

  async peticionDatosUsuario(formulario: any): Promise<any> {
    try {

      let htmlToSend = null;

      const filePath = join(__dirname, '../../../templates_email/informacion-completa-cuenta.html');
      const source = readFileSync(filePath, 'utf-8').toString();
      const template = handlebars.compile(source);

      const HOST_REMOTO = this.config.get('HOST_REMOTO');
      // const HOST_REMOTO = this.config.get('HOST');

      // const urlConfirmar = `${HOST_REMOTO}/api/email/confirmar-peticion-datos/${formulario.tokenEmail}/true/${formulario.idioma}`;

      const urlConfirmar = `${HOST_REMOTO}${servidor.path_descargar_informacion}/${formulario.tokenEmail}/${formulario.idioma}`

      const codIdioma = formulario.idioma;
      const urlRecursoSistema = this.config.get<string>("URL_RECURSOS_SISTEMA");

      let contact
      let urlHeaderEmail
      let urlFooterEmail
      //OBTIENE EL NOMBRE DEL USUARIO
      const nombreUser = formulario.nombre.split('-');
      let nombre = nombreUser[0].trim().concat(' ').concat(nombreUser[1].trim());

      if (codIdioma === idiomas.espanol) {
        urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_ES}`;
        urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_ES}`;
        contact = contactEmail.contactES;
      }
      if (codIdioma === idiomas.ingles) {
        urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_EN}`;
        urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_EN}`;
        contact = contactEmail.contactEN;
      }
      if (codIdioma === idiomas.frances) {
        urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_FR}`;
        urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_FR}`;
        contact = contactEmail.contactFR;
        // nombre = nombre.concat(' ');
      }
      if (codIdioma === idiomas.italiano) {
        urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_IT}`;
        urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_IT}`;
        contact = contactEmail.contactIT;
      }
      if (codIdioma === idiomas.aleman) {
        urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_DE}`;
        urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_DE}`;
        contact = contactEmail.contactDE;
      }
      if (codIdioma === idiomas.portugues) {
        urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_PT}`;
        urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_PT}`;
        contact = contactEmail.contactPT;
      }


      //TRADUCCION DE TEXTOS ESTATICOS SEGUN EL IDIOMA DEL USUARIO
      const m10v5texto1 = await this.i18n.translate(codIdioma.concat('.m10v5texto1'), {
        lang: codIdioma
      });
      const m10v5texto2 = await this.i18n.translate(codIdioma.concat('.m10v5texto2'), {
        lang: codIdioma
      });
      const m10v5texto3 = await this.i18n.translate(codIdioma.concat('.m10v5texto3'), {
        lang: codIdioma
      });
      const m10v5texto4 = await this.i18n.translate(codIdioma.concat('.m10v5texto4'), {
        lang: codIdioma
      });
      const m10v5texto5 = await this.i18n.translate(codIdioma.concat('.m10v5texto5'), {
        lang: codIdioma
      });

      const replacements = {
        nombreUser: nombre,
        urlConfirmar: urlConfirmar,
        m10v5texto1: m10v5texto1,
        m10v5texto2: m10v5texto2,
        m10v5texto3: m10v5texto3,
        m10v5texto4: m10v5texto4,
        m10v5texto5: m10v5texto5,
        contact: contact,
        urlHeaderEmail: urlHeaderEmail,
        urlFooterEmail: urlFooterEmail
      };
      htmlToSend = template(replacements);


      const mailOptions = {
        to: formulario.emailDestinatario,
        subject: m10v5texto1,
        // attachments: [{
        //     path: formulario.url
        // }],
        html: htmlToSend,

      };
      const resp: any = await this.nodeMailerService.enviarEmail(mailOptions);

      return resp;
    } catch (error) {
      throw error;
    }
  }

}
