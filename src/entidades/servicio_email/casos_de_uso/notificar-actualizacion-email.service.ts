import { Injectable } from '@nestjs/common';
import { join } from 'path';

import { readFileSync } from 'fs';
import { NodeMailerService } from '../../../drivers/lib_email/nodemailer.service';
import { idiomas, servidor } from '../../../shared/enum-sistema';
const handlebars = require('handlebars');
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import { ConfigService } from '../../../config/config.service';
import { contactEmail, recursosSistemaEN } from 'src/shared/enum-recursos-sistema';
import { I18nService } from 'nestjs-i18n';


@Injectable()
export class NotificarActualizacionEmailService {

  constructor(
    private nodeMailerService: NodeMailerService,
    private config: ConfigService,
    private readonly i18n: I18nService
  ) { }

  async notificarActualizacion(formulario): Promise<any> {
    let htmlToSend = null;
    console.log("formulario", formulario);


    const HOST_REMOTO = this.config.get('HOST_REMOTO');

    const urlConfirmar = `${HOST_REMOTO}/api/email/confirmar-actualizacion-data-email/${formulario.tokenEmail}/${formulario.idioma}`
    const urlNuevoEmail = `${HOST_REMOTO}/api/email/reenvio-confirmacion-actualizacion-email/${formulario.tokenEmail}/${formulario.idioma}`

    const codIdioma = formulario.idioma;
    const urlRecursoSistema = this.config.get<string>("URL_RECURSOS_SISTEMA");

    let urlHeaderEmail;
    let urlFooterEmail;
    let contact;

    if (codIdioma === idiomas.espanol) {
      urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_ES}`;
      urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_ES}`;
      contact = contactEmail.contactES;
    }
    if (codIdioma === idiomas.ingles) {
      urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_EN}`;
      urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_EN}`;
      contact = contactEmail.contactEN;
    }
    if (codIdioma === idiomas.frances) {
      urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_FR}`;
      urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_FR}`;
      contact = contactEmail.contactFR;
    }
    if (codIdioma === idiomas.italiano) {
      urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_IT}`;
      urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_IT}`;
      contact = contactEmail.contactIT;
    }
    if (codIdioma === idiomas.aleman) {
      urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_DE}`;
      urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_DE}`;
      contact = contactEmail.contactDE;
    }
    if (codIdioma === idiomas.portugues) {
      urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_PT}`;
      urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_PT}`;
      contact = contactEmail.contactPT;
    }


    //TRADUCCION DE TEXTOS ESTATCOS SEGUN EL IDIOMA DEL USUARIO
    const m10v8texto1 = await this.i18n.translate(codIdioma.concat('.m10v8texto1'), {
      lang: codIdioma
    });
    const m10v8texto2 = await this.i18n.translate(codIdioma.concat('.m10v8texto2'), {
      lang: codIdioma
    });
    const m10v8texto3 = await this.i18n.translate(codIdioma.concat('.m10v8texto3'), {
      lang: codIdioma
    });
    const m10v8texto4 = await this.i18n.translate(codIdioma.concat('.m10v8texto4'), {
      lang: codIdioma
    });
    const m10v8texto5 = await this.i18n.translate(codIdioma.concat('.m10v8texto5'), {
      lang: codIdioma
    });
    const m10v8texto6 = await this.i18n.translate(codIdioma.concat('.m10v8texto6'), {
      lang: codIdioma
    });
    const m10v8texto7 = await this.i18n.translate(codIdioma.concat('.m10v8texto7'), {
      lang: codIdioma
    });
    const m10v8texto8 = await this.i18n.translate(codIdioma.concat('.m10v8texto8'), {
      lang: codIdioma
    });
    const m10v8texto9 = await this.i18n.translate(codIdioma.concat('.m10v8texto9'), {
      lang: codIdioma
    });

    const filePath = join(__dirname, '../../../templates_email/confirmacion-cambio-email-cuenta.html');
    const source = readFileSync(filePath, 'utf-8').toString();
    const template = handlebars.compile(source);

    //OBTIENE EL NOMBRE DEL USUARIO
    const nombreUser = formulario.nombre.split('-');
    let nombre = nombreUser[0].trim().concat(' ').concat(nombreUser[1].trim());

    const replacements = {
      m10v8texto1: m10v8texto1,
      m10v8texto2: m10v8texto2,
      m10v8texto3: m10v8texto3,
      m10v8texto4: m10v8texto4,
      m10v8texto5: m10v8texto5,
      m10v8texto6: m10v8texto6,
      m10v8texto7: m10v8texto7,
      m10v8texto8: m10v8texto8,
      m10v8texto9: m10v8texto9,
      nombre: nombre,
      urlConfirmar: urlConfirmar,
      emailNuevo: formulario.nuevoEmail,
      urlNuevoEmail: urlNuevoEmail,
      urlHeaderEmail: urlHeaderEmail,
      urlFooterEmail: urlFooterEmail,
      contact: contact
    };
    htmlToSend = template(replacements);

    const mailOptions = {
      to: formulario.emailDestinatario,
      subject: m10v8texto1,
      html: htmlToSend,

    };
    const resp = await this.nodeMailerService.enviarEmail(mailOptions);
    return resp;

  }

}
