import { HttpService, Inject, Injectable } from '@nestjs/common';
import { resolve, join } from 'path';

import { readFileSync } from 'fs';
import { NodeMailerService } from '../../../drivers/lib_email/nodemailer.service';
import { idiomas, servidor } from '../../../shared/enum-sistema';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import { ConfigService } from '../../../config/config.service';
import { contactEmail, recursosSistemaEN } from 'src/shared/enum-recursos-sistema';
import { I18nService } from 'nestjs-i18n';
const handlebars = require('handlebars');


@Injectable()
export class EliminacionDatosCuentaService {

  constructor(
    private nodeMailerService: NodeMailerService,
    private config: ConfigService,
    private readonly i18n: I18nService
  ) { }

  async eliminacionDatosCuenta(formulario): Promise<any> {
    let htmlToSend = null;

    const HOST_REMOTO = this.config.get('HOST_REMOTO');


    const filePath = join(__dirname, '../../../templates_email/confirmacion-eliminacion-cuenta.html');
    const source = readFileSync(filePath, 'utf-8').toString();
    const template = handlebars.compile(source);

    const urlConfirmar = `${HOST_REMOTO}/api/email/confirmar-peticion-eliminacion-datos/${formulario.tokenEmail}/true/${formulario.idioma}`
    // const urlCancelar = `${HOST_REMOTO}/api/email/confirmar-peticion-eliminacion-datos/${formulario.tokenEmail}/false/${formulario.idioma}`

    const codIdioma = formulario.idioma;
    const urlRecursoSistema = this.config.get<string>("URL_RECURSOS_SISTEMA");

    let contact;
    let urlHeaderEmail
    let urlFooterEmail

    //OBTIENE EL NOMBRE DEL USUARIO
    const nombreUser = formulario.nombre.split('-');
    let nombre = nombreUser[0].trim().concat(' ').concat(nombreUser[1].trim());
    if (codIdioma === idiomas.espanol) {
      urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_ES}`;
      urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_ES}`;
      contact = contactEmail.contactES;
    }
    if (codIdioma === idiomas.ingles) {
      urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_EN}`;
      urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_EN}`;
      contact = contactEmail.contactEN;
    }
    if (codIdioma === idiomas.frances) {
      urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_FR}`;
      urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_FR}`;
      contact = contactEmail.contactFR;
      // nombre = nombre.concat(' ');
    }
    if (codIdioma === idiomas.italiano) {
      urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_IT}`;
      urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_IT}`;
      contact = contactEmail.contactIT;
    }
    if (codIdioma === idiomas.aleman) {
      urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_DE}`;
      urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_DE}`;
      contact = contactEmail.contactDE;
    }
    if (codIdioma === idiomas.portugues) {
      urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_PT}`;
      urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_PT}`;
      contact = contactEmail.contactPT;
    }


    //TRADUCCION DE TEXTOS ESTATCOS SEGUN EL IDIOMA DEL USUARIO
    const m10v6texto1 = await this.i18n.translate(codIdioma.concat('.m10v6texto1'), {
      lang: codIdioma
    });
    const m10v6texto2 = await this.i18n.translate(codIdioma.concat('.m10v6texto2'), {
      lang: codIdioma
    });
    const m10v6texto3 = await this.i18n.translate(codIdioma.concat('.m10v6texto3'), {
      lang: codIdioma
    });
    const m10v6texto4 = await this.i18n.translate(codIdioma.concat('.m10v6texto4'), {
      lang: codIdioma
    });
    const m10v6texto5 = await this.i18n.translate(codIdioma.concat('.m10v6texto5'), {
      lang: codIdioma
    });

    const replacements = {
      nombreUser: nombre,
      urlConfirmar: urlConfirmar,
      m10v6texto1: m10v6texto1,
      m10v6texto2: m10v6texto2,
      m10v6texto3: m10v6texto3,
      m10v6texto4: m10v6texto4,
      m10v6texto5: m10v6texto5,
      contact: contact,
      urlHeaderEmail: urlHeaderEmail,
      urlFooterEmail: urlFooterEmail
    };
    htmlToSend = template(replacements);

    // for (const iterator of formulario.usuario.perfiles) {
    //     console.log('iterator', iterator);
    // }


    const mailOptions = {
      to: formulario.emailDestinatario,
      subject: m10v6texto1,
      // attachments: [{
      //     path: formulario.url
      // }],
      html: htmlToSend,

    };
    const resp = await this.nodeMailerService.enviarEmail(mailOptions);
    return resp;


  }

}
