import { Injectable } from '@nestjs/common';
import { join } from 'path';
import { idiomas, servidor } from '../../../shared/enum-sistema';

import { readFileSync } from 'fs';
import { NodeMailerService } from '../../../drivers/lib_email/nodemailer.service';
import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica';
import { ConfigService } from '../../../config/config.service';
import { contactEmail, recursosSistemaEN } from 'src/shared/enum-recursos-sistema';
import { I18nService } from 'nestjs-i18n';
const handlebars = require('handlebars');

@Injectable()
export class EstadoSuscripcionService {

  constructor(
    private nodeMailerService: NodeMailerService,
    private config: ConfigService,
    private readonly i18n: I18nService
  ) { }

  async notificarEstadoSuscripcion(formulario: any): Promise<any> {
    try {
      console.log('formulario', formulario);
      let htmlToSend = null;


      const filePath = join(__dirname, '../../../templates_email/aviso-suspencion-cuenta.html');
      const source = readFileSync(filePath, 'utf-8').toString();
      const template = handlebars.compile(source);

      const codIdioma = formulario.idioma;
      const urlRecursoSistema = this.config.get<string>("URL_RECURSOS_SISTEMA");

      let urlHeaderEmail;
      let urlFooterEmail;
      let contact;

      if (codIdioma === idiomas.espanol) {
        urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_ES}`;
        urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_ES}`;
        contact = contactEmail.contactES;
      }
      if (codIdioma === idiomas.ingles) {
        urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_EN}`;
        urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_EN}`;
        contact = contactEmail.contactEN;
      }
      if (codIdioma === idiomas.frances) {
        urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_FR}`;
        urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_FR}`;
        contact = contactEmail.contactFR;
      }
      if (codIdioma === idiomas.italiano) {
        urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_IT}`;
        urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_IT}`;
        contact = contactEmail.contactIT;
      }
      if (codIdioma === idiomas.aleman) {
        urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_DE}`;
        urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_DE}`;
        contact = contactEmail.contactDE;
      }
      if (codIdioma === idiomas.portugues) {
        urlHeaderEmail = `${urlRecursoSistema}${recursosSistemaEN.HEADER_EMAIL_PT}`;
        urlFooterEmail = `${urlRecursoSistema}${recursosSistemaEN.FOOTER_EMAIL_PT}`;
        contact = contactEmail.contactPT;
      }


      //TRADUCCION DE TEXTOS ESTATCOS SEGUN EL IDIOMA DEL USUARIO
      const m10v7texto1 = await this.i18n.translate(codIdioma.concat('.m10v7texto1'), {
        lang: codIdioma
      });
      const m10v7texto2 = await this.i18n.translate(codIdioma.concat('.m10v7texto2'), {
        lang: codIdioma
      });
      const m10v7texto3 = await this.i18n.translate(codIdioma.concat('.m10v7texto3'), {
        lang: codIdioma
      });
      const m10v7texto4 = await this.i18n.translate(codIdioma.concat('.m10v7texto4'), {
        lang: codIdioma
      });


      const replacements = {
        nombre: formulario.nombre,
        m10v7texto1: m10v7texto1,
        m10v7texto2: m10v7texto2,
        m10v7texto3: m10v7texto3,
        m10v7texto4: m10v7texto4,
        urlHeaderEmail: urlHeaderEmail,
        urlFooterEmail: urlFooterEmail,
        contact: contact
      };
      htmlToSend = template(replacements);


      const mailOptions = {
        to: formulario.emailDestinatario,
        subject: m10v7texto1,
        // attachments: [{
        //     path: formulario.url
        // }],
        html: htmlToSend,

      };
      const resp: any = await this.nodeMailerService.enviarEmail(mailOptions);
      console.log("resp.code:", resp.code);

      return resp;
    } catch (error) {
      throw error;
    }
  }

}
