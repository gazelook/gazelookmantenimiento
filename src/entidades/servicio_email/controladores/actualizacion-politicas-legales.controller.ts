
import { AuthGuard } from '@nestjs/passport';
import { ApiResponse, ApiTags, ApiOperation, ApiHeader, ApiBody } from '@nestjs/swagger';
import { Controller, Post, Body, Res, HttpStatus, Request, HttpException, Inject, Get, Headers, Param, BadRequestException, UsePipes, ValidationPipe, UseGuards, Query } from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import * as mongoose from 'mongoose';
import { Funcion } from '../../../shared/funcion';
import { ActualizacionPoliticasLegalesService } from '../casos_de_uso/actualizacion-politicas-legales.service';
import { ActualizacionPoliticasDto } from '../dto/servicio-correo.dto';



@ApiTags('Servicio Correo')
@Controller('api/servicio-email/actualizacion-politicas-legales')
export class ActualizacionPoliticasLegalesControlador {

    constructor( private actualizacionPoliticasLegalesService: ActualizacionPoliticasLegalesService,
        private readonly i18n: I18nService,
        private readonly funcion: Funcion
    ) { }

    @Post('/')
    @ApiHeader({ name: 'idioma', description: 'codigo del idioma. Ejm: es, en, fr' })
    @ApiHeader({ name: 'apiKey', required: true, description: 'key para usar las rutas' })
    @ApiOperation({ summary: 'Envia un email al usuario notificandole que las politicas de la aplicación han sido actualizadas' })
    @ApiResponse({ status: 201, description: 'Email enviado' })
    @ApiResponse({ status: 401, description: 'No autorizado' })
    @ApiResponse({ status: 406, description: 'Operacion no permitida' })

    //@UseGuards(AuthGuard('jwt'))

    public async envioDatos(@Headers() headers,
    @Body() dataEmail: ActualizacionPoliticasDto
    ) {

        const respuesta = new RespuestaInterface;
        const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
        try {
            const informacion = await this.actualizacionPoliticasLegalesService.actualizacionPoliticas(dataEmail);
            if (informacion) {
                respuesta.codigoEstado = HttpStatus.OK;
                respuesta.respuesta = {
                    datos: informacion
                }

            } else {
                respuesta.codigoEstado = HttpStatus.NOT_FOUND;
                respuesta.respuesta = {
                    mensaje: 'Operacion no permitida',
                }
            }
            return respuesta;
        } catch (e) {
            respuesta.codigoEstado = HttpStatus.INTERNAL_SERVER_ERROR;
            respuesta.respuesta = {
                mensaje: e.message
            }
            return respuesta;
        }
    }
}

