import { Module, Delete } from '@nestjs/common';
import { ServicioEmailServicesModule } from '../casos_de_uso/email-services.module';
import { HttpErrorFilter } from 'src/shared/filters/http-error.filter';
import { Funcion } from '../../../shared/funcion';
import { PeticionDatosControlador } from './peticion-datos.controller';
import { EliminacionDatosCuentaControlador } from './eliminacion-datos-cuenta.controller';
import { RecuperarContrasenaControlador } from './recuperar-contrasena.controller';
import { TransferirProyectoControlador } from './transferir-proyecto.controller';
import { ConfirmacionEmailCuentaControlador } from './confirmacion-email-cuenta.controller';
import { ConfirmarCambioContrasenaControlador } from './confirmar-cambio-contrasena.controller';
import { ActualizacionPoliticasLegalesControlador } from './actualizacion-politicas-legales.controller';
import { ConfirmacionEmailMenorEdadControlador } from './usuario-menor-edad.controller';
import { NotificarErrorEmailController } from './notificar-error-email.controller';
import { NotificarActualizacionEmailController } from './notificar-actualizacion-email.controller';
import { EnvioReciboPagoControlador } from './email-recibo-pago.controller';
import { CorreoBienvenidaControlador } from './correo-bienvenida.controller';



@Module({
  imports: [ServicioEmailServicesModule],
  providers: [HttpErrorFilter, Funcion],
  exports: [ServicioEmailServicesModule],
  controllers: [
    PeticionDatosControlador,
    EliminacionDatosCuentaControlador,
    RecuperarContrasenaControlador,
    TransferirProyectoControlador,
    ConfirmacionEmailCuentaControlador,
    ConfirmarCambioContrasenaControlador,
    ActualizacionPoliticasLegalesControlador,
    ConfirmacionEmailMenorEdadControlador,
    NotificarErrorEmailController,
    NotificarActualizacionEmailController,
    EnvioReciboPagoControlador,
    CorreoBienvenidaControlador
  ],
})
export class ServicioEmailControllerModule {}
