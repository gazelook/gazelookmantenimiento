
import { AuthGuard } from '@nestjs/passport';
import { ApiResponse, ApiTags, ApiOperation, ApiHeader, ApiBody } from '@nestjs/swagger';
import { Controller, Post, Body, Res, HttpStatus, Request, HttpException, Inject, Get, Headers, Param, BadRequestException, UsePipes, ValidationPipe, UseGuards, Query } from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import * as mongoose from 'mongoose';
import { Funcion } from '../../../shared/funcion';
import { RecuperarContrasenaService } from '../casos_de_uso/recuperar-contrasena.service';
import { RecuperarContrasenaDto } from '../dto/servicio-correo.dto';



@ApiTags('Servicio Correo')
@Controller('api/servicio-email/recuperar-contrasena')
export class RecuperarContrasenaControlador {

    constructor(
        private readonly recuperarContrasenaService: RecuperarContrasenaService,
        private readonly i18n: I18nService,
        private readonly funcion: Funcion
    ) { }

    @Post('/')
    @ApiHeader({ name: 'idioma', description: 'codigo del idioma. Ejm: es, en, fr' })
    @ApiHeader({ name: 'apiKey', required: true, description: 'key para usar las rutas' })
    @ApiOperation({ summary: 'Envia un email al usuario para que pueda cambiar la contraseña' })
    @ApiResponse({ status: 201, description: 'Email enviado' })
    @ApiResponse({ status: 401, description: 'No autorizado' })
    @ApiResponse({ status: 406, description: 'Operacion no permitida' })
    //@UseGuards(AuthGuard('jwt'))

    public async envioDatos(@Headers() headers,
    @Body() dataEmail: RecuperarContrasenaDto
    ) {

        const respuesta = new RespuestaInterface;
        const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
        try {
            const informacion = await this.recuperarContrasenaService.recuperarContrasena(dataEmail);
            if (informacion) {
                respuesta.codigoEstado = HttpStatus.OK;
                respuesta.respuesta = {
                    datos: informacion
                }

            } else {
                respuesta.codigoEstado = HttpStatus.NOT_FOUND;
                respuesta.respuesta = {
                    mensaje: 'Operacion no permitida',
                }
            }
            return respuesta;
        } catch (e) {
            respuesta.codigoEstado = HttpStatus.INTERNAL_SERVER_ERROR;
            respuesta.respuesta = {
                mensaje: e.message
            }
            return respuesta;
        }
    }
}

