
import { Body, Controller, Headers, HttpStatus, Post } from '@nestjs/common';
import { ApiHeader, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { I18nService } from 'nestjs-i18n';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import { Funcion } from '../../../shared/funcion';
import { EnvioReciboPagoService } from '../casos_de_uso/envio-recibo-pago.service';
import { ReciboPagoEmailDto } from '../dto/servicio-correo.dto';



@ApiTags('Servicio Correo')
@Controller('api/servicio-email/envio-recibo-pago')
export class EnvioReciboPagoControlador {

    constructor( private envioReciboPagoService: EnvioReciboPagoService,
        private readonly i18n: I18nService,
        private readonly funcion: Funcion
    ) { }

    @Post('/')
    @ApiHeader({ name: 'idioma', description: 'codigo del idioma. Ejm: es, en, fr' })
    @ApiHeader({ name: 'apiKey', required: true, description: 'key para usar las rutas' })
    @ApiOperation({ summary: 'Envia un email al usuario el recibo del pago' })
    @ApiResponse({ status: 201, description: 'Email enviado' })
    @ApiResponse({ status: 401, description: 'No autorizado' })
    @ApiResponse({ status: 406, description: 'Operacion no permitida' })
    //@UseGuards(AuthGuard('jwt'))

    public async enviarRecibo(@Headers() headers,
    @Body() dataEmail: ReciboPagoEmailDto
    ) {

        const respuesta = new RespuestaInterface;
        const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
        try {
            const informacion = await this.envioReciboPagoService.envioRecibo(dataEmail);
            if (informacion) {
                respuesta.codigoEstado = HttpStatus.OK;
                respuesta.respuesta = {
                    datos: informacion
                }

            } else {
                respuesta.codigoEstado = HttpStatus.NOT_FOUND;
                respuesta.respuesta = {
                    mensaje: 'Operacion no permitida',
                }
            }
            return respuesta;
        } catch (e) {
            respuesta.codigoEstado = HttpStatus.INTERNAL_SERVER_ERROR;
            respuesta.respuesta = {
                mensaje: e.message
            }
            return respuesta;
        }
    }
}

