
import { ApiResponse, ApiTags, ApiOperation, ApiHeader, ApiBody } from '@nestjs/swagger';
import { Controller, Post, Body, HttpStatus, Headers } from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import { Funcion } from '../../../shared/funcion';
import { NotificarErrorEmailService } from '../casos_de_uso/notificar-error-email.service';
import { NotificarActualizacionEmailService } from '../casos_de_uso/notificar-actualizacion-email.service';
import { NotificarActualizacionEmailDto } from '../dto/servicio-correo.dto';



@ApiTags('Servicio Correo')
@Controller('api/servicio-email/notificar-actualizacion-email')
export class NotificarActualizacionEmailController {

    constructor(
        private readonly notificarActualizacionEmailService: NotificarActualizacionEmailService,
        private readonly i18n: I18nService,
        private readonly funcion: Funcion
    ) { }

    @Post('/')
    @ApiHeader({ name: 'idioma', description: 'codigo del idioma. Ejm: es, en, fr' })
    @ApiHeader({ name: 'apiKey', required: true, description: 'key para usar las rutas' })
    @ApiOperation({ summary: 'Envia un email al usuario para que confirme el cambio de email' })
    @ApiResponse({ status: 201, description: 'Email enviado' })
    @ApiResponse({ status: 401, description: 'No autorizado' })
    @ApiResponse({ status: 406, description: 'Operacion no permitida' })

    public async notificarActualizacionEmail(@Headers() headers,
        @Body() dataEmail: NotificarActualizacionEmailDto
    ) {

        const codIdioma = await this.funcion.obtenerIdiomaDefecto(headers.idioma);
        try {
            const savedEmail = await this.notificarActualizacionEmailService.notificarActualizacion(dataEmail);
            if (savedEmail) {
                return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.OK, datos: savedEmail })

            } else {
                const EMAIL_NO_PUEDE_CREARSE = await this.i18n.translate(codIdioma.concat('.EMAIL_NO_PUEDE_CREARSE'), {
                    lang: codIdioma
                });
                return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.NOT_FOUND, mensaje: EMAIL_NO_PUEDE_CREARSE })
            }
        } catch (e) {
            return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR, mensaje: e.message })
        }
    }
}

