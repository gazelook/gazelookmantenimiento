
import { AuthGuard } from '@nestjs/passport';
import { ApiResponse, ApiTags, ApiOperation, ApiHeader, ApiBody } from '@nestjs/swagger';
import { Controller, Post, Body, Res, HttpStatus, Request, HttpException, Inject, Get, Headers, Param, BadRequestException, UsePipes, ValidationPipe, UseGuards, Query } from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import * as mongoose from 'mongoose';
import { Funcion } from '../../../shared/funcion';
import { RecuperarContrasenaService } from '../casos_de_uso/recuperar-contrasena.service';
import { TransferirProyectoService } from '../casos_de_uso/transferir-proyecto.service';
import { TransferirProyectoDto } from '../dto/servicio-correo.dto';



@ApiTags('Servicio Correo')
@Controller('api/servicio-email/transferir-proyecto')
export class TransferirProyectoControlador {

    constructor(
        private readonly transferirProyectoService: TransferirProyectoService,
        private readonly i18n: I18nService,
        private readonly funcion: Funcion
    ) { }

    @Post('/')
    @ApiHeader({ name: 'idioma', description: 'codigo del idioma. Ejm: es, en, fr' })
    @ApiHeader({ name: 'apiKey', required: true, description: 'key para usar las rutas' })
    @ApiOperation({ summary: 'Envia un email al nuevo usuario para que acepte ser el dueño del proyecto' })
    @ApiResponse({ status: 201, description: 'Email enviado' })
    @ApiResponse({ status: 401, description: 'No autorizado' })
    @ApiResponse({ status: 406, description: 'Operacion no permitida' })
    //@UseGuards(AuthGuard('jwt'))

    public async transfeririProyecto(@Headers() headers,
        @Body() dataEmail: TransferirProyectoDto
    ) {

        const codIdioma = await this.funcion.obtenerIdiomaDefecto(headers.idioma);
        try {
            const informacion = await this.transferirProyectoService.transferirProyecto(dataEmail);
            if (informacion) {
                return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.OK, datos: informacion })

            } else {
                const EMAIL_NO_PUEDE_CREARSE = await this.i18n.translate(codIdioma.concat('.EMAIL_NO_PUEDE_CREARSE'), {
                    lang: codIdioma
                });
                return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.NOT_FOUND, mensaje: EMAIL_NO_PUEDE_CREARSE })
            }
        } catch (e) {
            return this.funcion.enviarRespuestaOptimizada({ codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR, mensaje: e.message })
        }
    }
}

