import { ServicioEmailControllerModule } from './controladores/email-controller.module';
import { Module } from '@nestjs/common';

@Module({
  imports: [ServicioEmailControllerModule],
  providers: [],
  controllers: [],
  exports: [ServicioEmailControllerModule]
})
export class ServicioEmailModule {}

