import { Connection } from 'mongoose';
import { ConversionTransaccionModelo } from '../../../drivers/mongoose/modelos/convercion_transaccion/convercion-transaccion.schema';
import { informacionPago } from '../../../drivers/mongoose/modelos/informacion_pago/informacion-pago.schema';
import { TransaccionModelo } from '../../../drivers/mongoose/modelos/transaccion/transaccion.schema';
import { usuarioModelo } from '../../../drivers/mongoose/modelos/usuarios/usuario.schema';

export const servicioEmailProviders = [
    {
        provide: 'CONVERSION_TRANSACCION_MODEL',
        useFactory: (connection: Connection) => connection.model('conversion_transaccion', ConversionTransaccionModelo, 'conversion_transaccion'),
        inject: ['DB_CONNECTION']
    },
    {
        provide: 'INFORMACION_PAGO_MODEL',
        useFactory: (connection: Connection) => connection.model('informacion_pago', informacionPago, 'informacion_pago'),
        inject: ['DB_CONNECTION']
      },
      {
        provide: 'TRANSACCION_MODEL',
        useFactory: (connection: Connection) => connection.model('transaccion', TransaccionModelo, 'transaccion'),
        inject: ['DB_CONNECTION']
      },
      {
        provide: 'USUARIO_MODEL',
        useFactory: (connection: Connection) => connection.model('usuario', usuarioModelo, 'usuario'),
        inject: ['DB_CONNECTION']
      },
];
