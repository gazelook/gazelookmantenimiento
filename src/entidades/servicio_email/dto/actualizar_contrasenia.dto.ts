import { ApiProperty } from "@nestjs/swagger";

export class ActualizarContraseniaDto {
    @ApiProperty()
    email: string;
    @ApiProperty()
    newPass: string;
    @ApiProperty()
    confirmPass: string;
}
