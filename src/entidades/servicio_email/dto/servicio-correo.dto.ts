import { ApiProperty } from "@nestjs/swagger";

export class ActualizacionPoliticasDto {
    @ApiProperty({description: 'Enlace de la documentación de las nuevas politicas de la App', required: true, example: ''})
    link: string;
    @ApiProperty({description: 'Lista de emails', required: true, example: '[ejemplo@gmail.com]'})
    emailDestinatarios: [];
}

export class ConfirmacionEmailCuentaDto {
    @ApiProperty({description: 'Codigo de validación de correo', required: true, example: 'CTEM_1: Validación normal | CTEM_2: Validación responsable'})
    codigo: string;
    @ApiProperty({description: 'Token del email', required: true, example: ''})
    tokenEmail: string;
    @ApiProperty({description: 'Email de quien se le va a enviar el correo', required: true, example: '[ejemplo@gmail.com]'})
    emailDestinatario: string;
    @ApiProperty({description: 'ID del usuario', required: true, example: ''})
    usuario: string;
    @ApiProperty({description: 'es, en', required: true, example: ''})
    idioma: string;
}

export class ConfirmarCambioContrasenaDto {
    @ApiProperty({description: 'Email del destinatario', required: true, example: '[ejemplo@gmail.com]'})
    emailDestinatario: string;
}

export class EliminacionDatosCuentaDto {
    @ApiProperty({description: 'Email del destinatario', required: true, example: '[ejemplo@gmail.com]'})
    emailDestinatario: string;
    @ApiProperty({description: '', required: true, example: ''})
    tokenEmail: string;
    @ApiProperty({description: 'Entidad usuario y su perfiles(populate nombre del usuario)', required: true, example: "{_id: '', email:'', perfiles: [{nombre:''}]"})
    usuario: {};
    @ApiProperty({description: 'es, en', required: true, example: ''})
    idioma: string;
}

export class PeticionDatosUsuarioDto {
    @ApiProperty({description: 'Email del destinatario', required: true, example: '[ejemplo@gmail.com]'})
    emailDestinatario: string;
    @ApiProperty({description: '', required: true, example: ''})
    tokenEmail: string;
    @ApiProperty({description: 'Entidad usuario y su perfiles(populate nombre del usuario)', required: true, example: "{_id: '', email:'', perfiles: [{nombre:''}]"})
    usuario: {};
    @ApiProperty({description: 'es, en', required: true, example: ''})
    idioma: string;
}

export class CorreoBienvenidaDto {
    @ApiProperty({description: 'Email del destinatario', required: true, example: '[ejemplo@gmail.com]'})
    emailDestinatario: string;
    @ApiProperty({description: '', required: true, example: ''})
    tokenEmail: string;
    @ApiProperty({description: 'Entidad usuario y su perfiles(populate nombre del usuario)', required: true, example: "{_id: '', email:'', perfiles: [{nombre:''}]"})
    usuario: {};
    @ApiProperty({description: 'es, en', required: true, example: ''})
    idioma: string;
}

export class RecuperarContrasenaDto {
    @ApiProperty({description: 'Email del destinatario', required: true, example: '[ejemplo@gmail.com]'})
    emailDestinatario: string;
    @ApiProperty({description: '', required: true, example: ''})
    tokenEmail: string;
    @ApiProperty({description: 'es, en', required: true, example: ''})
    idioma: string;
}

export class TransferirProyectoDto {
    @ApiProperty({description: 'Email del destinatario', required: true, example: '[ejemplo@gmail.com]'})
    emailDestinatario: string;
    @ApiProperty({description: 'Id del perfil propietario', required: true, example: ''})
    perfilPropietario: string;
    @ApiProperty({description: 'Nombre del propietario', required: true, example: ''})
    nombrePropietario: string;
    @ApiProperty({description: 'Id del perfil del nuevo propietario', required: true, example: ''})
    perfilNuevo: string;
    @ApiProperty({description: 'Nombre del nuevo propietario', required: true, example: ''})
    nombrePerfilNuevo: string;
    @ApiProperty({description: 'Id del proyecto', required: true, example: ''})
    idProyecto: string;
    @ApiProperty({description: 'codigo', required: true, example: ''})
    codigo: string;
    @ApiProperty({description: '', required: true, example: ''})
    titulo: string;
    @ApiProperty({description: '', required: true, example: ''})
    tituloCorto: string;
    @ApiProperty({description: '', required: true, example: ''})
    descripcion: string;
    @ApiProperty({description: '', required: true, example: ''})
    tokenEmail: string;
    @ApiProperty({description: 'es, en', required: true, example: ''})
    idioma: string;
}

export class ConfirmacionUsuarioMenorEdadDto {
    @ApiProperty({description: 'Email del destinatario', required: true, example: '[ejemplo@gmail.com]'})
    emailDestinatario: string;
    @ApiProperty({description: '', required: true, example: ''})
    tokenEmail: string;
    @ApiProperty({description: 'Id del usuario', required: true, example: ''})
    usuario: string;
    @ApiProperty({description: 'Nombre del usuario', required: true, example: ''})
    nombre: string;
    @ApiProperty({description: 'es, en', required: true, example: ''})
    idioma: string;
}

export class NotificarActualizacionEmailDto {
    @ApiProperty({description: 'Email del destinatario', required: true, example: '[ejemplo@gmail.com]'})
    emailDestinatario: string;
    @ApiProperty({description: '', required: true, example: ''})
    tokenEmail: string;
    @ApiProperty({description: 'Nuevo email del usuario', required: true, example: ''})
    nuevoEmail: string;
    @ApiProperty({description: 'Nombre del usuario', required: true, example: ''})
    nombre: string;
    @ApiProperty({description: 'es, en', required: true, example: ''})
    idioma: string;
}

export class ReciboPagoEmailDto {
    @ApiProperty({description: 'Email del destinatario', required: true, example: '[ejemplo@gmail.com]'})
    emailDestinatario: string;
    @ApiProperty({description: '', required: true, example: ''})
    tokenEmail: string;
    @ApiProperty({description
        : 'id informacionPago', required: true, example: ''})
    informacionPago: string;
    @ApiProperty({description: 'Nombre del usuario', required: true, example: ''})
    nombre: string;
    @ApiProperty({description: 'es, en', required: true, example: ''})
    idioma: string;
    @ApiProperty({})
    autorizacionCodePaymentez?: string;
}