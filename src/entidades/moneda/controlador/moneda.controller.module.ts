import { forwardRef, Module } from '@nestjs/common';
//import { CatalogoTipoMonedaService } from 'src/entidades/catalogos/casos_de_uso/catalogo-tipo-moneda.service';
import { CatalogosServiceModule } from 'src/entidades/catalogos/casos_de_uso/catalogos-services.module';
import { Funcion } from 'src/shared/funcion';
import { MonedaServicesModule } from '../casos_de_uso/moneda.services.module';
import { ConvertirMontoMonedaControlador } from './convertir-monto-moneda.controller';


@Module({
  imports: [MonedaServicesModule,
    forwardRef(() => CatalogosServiceModule),],
  providers: [Funcion],
  exports: [],
  controllers: [
    ConvertirMontoMonedaControlador
  ],
})
export class MonedaControllerModule {}
