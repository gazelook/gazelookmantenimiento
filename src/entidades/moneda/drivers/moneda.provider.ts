import { TipoComentarioModelo } from './../../../drivers/mongoose/modelos/catalogo_tipo_comentario/catalogo-tipo-comentario.schema';
import { TraduccionCatalogoMetodoPagoModelo } from '../../../drivers/mongoose/modelos/traduccion_catalogo_metodo_pago/traduccion-catalogo-metodo-pago.schema';
import { CatalogoSuscripcionModelo } from './../../../drivers/mongoose/modelos/catalogo_suscripcion/catalogo_suscripcion.schema';
import { Connection } from 'mongoose';
import { CatalogoMediaModelo } from '../../../drivers/mongoose/modelos/catalogoMedia/catalogoMediaModelo';
import { catalogoAlbumModelo } from '../../../drivers/mongoose/modelos/catalogo_album/catalogo-album.schema';
import { CatalogoTipoMediaModelo } from '../../../drivers/mongoose/modelos/catalogoTipoMedia/catalogoTipoMediaModelo';
import { catalogoIdiomasModelo } from '../../../drivers/mongoose/modelos/catalogoIdiomas/catalogoIdiomasModelo';
import { accionModelo } from '../../../drivers/mongoose/modelos/catalogoAccion/catalogoAccionModelo';
import { entidadesModelo } from '../../../drivers/mongoose/modelos/catalogoEntidades/catalogoEntidadesModelo';
import { CatalogoMetodoPagoModelo } from '../../../drivers/mongoose/modelos/catalogo_metodo_pago/catalogo-metodo-pago.schema';
import { CatalogoTipoEmailSchema } from 'src/drivers/mongoose/modelos/catalogo_tipo_email/catalogoTipoEmail';
import { CatalogoArchivoDefaultModelo } from 'src/drivers/mongoose/modelos/catalogo_archivo_default/catalogo-archivo-default.schema';
import { TipoMonedaModelo } from 'src/drivers/mongoose/modelos/catalogo_tipo_moneda/catalogo-tipo-moneda.schema';
import { CatalogoTipoArchivoModelo } from 'src/drivers/mongoose/modelos/catalogo_tipo_archivo/catalogo-tipo-archivo.schema';
import { ConfiguracionEventoModelo } from 'src/drivers/mongoose/modelos/configuracion_evento/configuracion-evento.schema';
import { FormulaEventoModelo } from 'src/drivers/mongoose/modelos/formula_evento/formula-evento.schema';
import { CatalogoMensajeModelo } from 'src/drivers/mongoose/modelos/catalogo_mensaje/catalogo-mensaje.schema';


export const MonedaProviders = [
   
];
