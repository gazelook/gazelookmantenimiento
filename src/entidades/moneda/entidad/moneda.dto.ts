import { ApiProperty } from "@nestjs/swagger";

export class ConvertirMontoMonedaDto {

    @ApiProperty({description: 'Conversion del monto $ de dolar a la moneda que se ingresa', example: "18"})
    monto: string;

}