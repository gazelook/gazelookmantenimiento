import { UsuarioModule } from 'src/entidades/usuario/usuario.module';
import { Module } from '@nestjs/common';
import { TransaccionControllerModule } from './controladores/transaccion-controller.module';

@Module({
  imports: [TransaccionControllerModule],
  providers: [],
  controllers: [],
})
export class TransaccionModule {}
