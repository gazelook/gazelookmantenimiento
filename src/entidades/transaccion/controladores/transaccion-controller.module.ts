import { CustomersService } from '../../../drivers/stripe/services/payment-customers.service';
import { Module, Delete } from '@nestjs/common';
import { TransaccionServicesModule } from '../casos_de_uso/transaccion-services.module';
import { Funcion } from 'src/shared/funcion';
import { FiltrarAportacionesControlador } from './filtrar-aportaciones.controller';
import { ObtenerGastosOperativosControlador } from './obtener-gastos-operativos.controller';
import { FondosReservadosGazelookControlador } from './fondos-reservados-gazelook.controller';
import { ObtenerGananciasGazelookControlador } from './obtener-ganancias-gazelook.controller';
import { CrearGastoOperativoControlador } from './crear-gasto-operativo.controller';
import { AportacionesMetodoPagoControlador } from './aportaciones-metodo-pago.controller';
import { ObtenerTransaccionesPaymentezControlador } from './obtener-transacciones-paymentez.controller';

@Module({
  imports: [TransaccionServicesModule],
  providers: [CustomersService, Funcion],
  exports: [],
  controllers: [
    FiltrarAportacionesControlador,
    ObtenerGastosOperativosControlador,
    FondosReservadosGazelookControlador,
    ObtenerGananciasGazelookControlador,
    CrearGastoOperativoControlador,
    AportacionesMetodoPagoControlador,
    ObtenerTransaccionesPaymentezControlador
  ],
})
export class TransaccionControllerModule {}
