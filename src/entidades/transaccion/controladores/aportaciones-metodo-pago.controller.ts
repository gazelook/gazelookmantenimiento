import { Controller, Get, Headers, HttpStatus, Query, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiHeader, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { I18nService } from 'nestjs-i18n';
import { filtroBusquedaAportacionesMetodoPago } from 'src/shared/enum-sistema';
//Metodos de pago
import { Funcion } from 'src/shared/funcion';
import { AportacionesMetodoPagoService } from '../casos_de_uso/aportaciones-metodo-pago.service';
import { AportacionesDto } from './../entidad/aportaciones-dto';

@ApiTags('Transacciones')
@Controller('api/aportaciones-metodo-pago')
export class AportacionesMetodoPagoControlador {
  constructor(
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
    private aportacionesMetodoPagoService: AportacionesMetodoPagoService,
  ) { }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  @Get('/')
  @ApiOperation({
    summary:
      'Este método obtiene las aportaciones segun el filtro que se envie que puede ser (mudial, pais) y segun el tipo de aportacion (suscripcion, valorExtra)',
  })
  @ApiResponse({ status: 200, type: [AportacionesDto], description: 'OK' })
  @ApiResponse({ status: 400, description: 'Datos inválidos' })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @UseGuards(AuthGuard('jwt'))

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public async filtrarAportaciones(
    @Headers() headers,
    @Query('filtro') filtro: string,
    @Query('fecha') fecha: Date,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {
      if (
        filtro &&
        (filtro === filtroBusquedaAportacionesMetodoPago.stripe ||
          filtro === filtroBusquedaAportacionesMetodoPago.paymentez ||
          filtro === filtroBusquedaAportacionesMetodoPago.cripto ||
          filtro === filtroBusquedaAportacionesMetodoPago.panama)
      ) {
        

        const aportaciones = await this.aportacionesMetodoPagoService.FiltrarAportaciones(
          filtro,
          fecha,
        );

        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: aportaciones,
        });

      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (err) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: err.message,
      });
    }
  }
}
