import { Controller, Get, Headers, HttpStatus, Query, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiHeader, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { I18nService } from 'nestjs-i18n';
import { filtroBusquedaGastoOperacional } from 'src/shared/enum-sistema';
//Metodos de pago
import { Funcion } from 'src/shared/funcion';
import { FiltrarAportacionesService } from '../casos_de_uso/filtrar-aportaciones.service';
import { AportacionesDto } from './../entidad/aportaciones-dto';

@ApiTags('Transacciones')
@Controller('api/aportaciones-admin-filtro')
export class FiltrarAportacionesControlador {
  constructor(
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
    private filtrarAportacionesService: FiltrarAportacionesService,
  ) { }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  @Get('/')
  @ApiOperation({
    summary:
      'Este método obtiene las aportaciones segun el filtro que se envie que puede ser (mudial, pais) y segun el tipo de aportacion (suscripcion, valorExtra)',
  })
  @ApiResponse({ status: 200, type: [AportacionesDto], description: 'OK' })
  @ApiResponse({ status: 400, description: 'Datos inválidos' })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @UseGuards(AuthGuard('jwt'))

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public async filtrarAportaciones(
    @Headers() headers,
    @Query('filtro') filtro: string,
    @Query('campo') campo: string,
    @Query('fecha') fecha: Date,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {
      if (
        filtro &&
        (filtro === filtroBusquedaGastoOperacional.PAIS ||
          filtro === filtroBusquedaGastoOperacional.MUNDIAL)
      ) {
        if (filtro === filtroBusquedaGastoOperacional.PAIS && !campo) {
          const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
            codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_ACCEPTABLE,
            mensaje: PARAMETROS_NO_VALIDOS,
          });
        }

        const aportaciones = await this.filtrarAportacionesService.FiltrarAportaciones(
          filtro,
          campo,
          fecha,
        );

        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: aportaciones,
        });

      } else {
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }
    } catch (err) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: err.message,
      });
    }
  }
}
