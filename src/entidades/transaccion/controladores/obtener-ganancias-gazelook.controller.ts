import { Controller, Get, Headers, HttpStatus, Query, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiHeader, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
//Metodos de pago
import { Funcion } from 'src/shared/funcion';
import { ObtenerGananciasGazelookService } from '../casos_de_uso/obtener-ganancias-gazelook.service';
import { AportacionesDto } from './../entidad/aportaciones-dto';

@ApiTags('Transacciones')
@Controller('api/ganancias-gazelook')
export class ObtenerGananciasGazelookControlador {
  constructor(
    private readonly obtenerGananciasGazelookService: ObtenerGananciasGazelookService,
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
  ) {}

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  @Get('/')
  @ApiOperation({
    summary:
      'Este método obtiene las las ganacias de gazelook es decir de todas las suscripciones coge el 20%',
  })
  @ApiResponse({ status: 200, type: [AportacionesDto], description: 'OK' })
  @ApiResponse({ status: 400, description: 'Datos inválidos' })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @UseGuards(AuthGuard('jwt'))

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public async obtenerGananciasGazelook(
    @Headers() headers,
    @Query('fecha') fecha: Date,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);
    try {

        const aportaciones = await this.obtenerGananciasGazelookService.obtenerGananciasGazelook(
          fecha
        );
        if (aportaciones) {
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            datos: aportaciones,
          });
        } else {
          const ERROR_OBTENER = await this.i18n.translate(
            codIdioma.concat('.ERROR_OBTENER'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.OK,
            mensaje: ERROR_OBTENER,
          });
        }

    } catch (err) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: err.message,
      });
    }
  }
}
