import { Body, Controller, Headers, HttpStatus, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBody, ApiHeader, ApiOperation, ApiResponse, ApiSecurity, ApiTags } from '@nestjs/swagger';
import * as mongoose from 'mongoose';
import { I18nService } from 'nestjs-i18n';
//Metodos de pago
import { Funcion } from 'src/shared/funcion';
import { CrearGastoOperativoService } from '../casos_de_uso/crear-transaccion.service';
import { CrearTransaccionGastoOperativoDto } from '../entidad/transaccion.dto';

@ApiTags('Transacciones')
@Controller('api/crear-gasto-operativo')
export class CrearGastoOperativoControlador {
  constructor(
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
    private crearGastoOperativoService: CrearGastoOperativoService,
  ) { }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  @Post('/')
  @ApiSecurity('Authorization')
  @ApiBody({ type: CrearTransaccionGastoOperativoDto })
  @ApiOperation({
    summary:
      'Este método crea un gasto operativo',
  })
  @ApiResponse({ status: 201, description: 'OK' })
  @ApiResponse({ status: 400, description: 'Datos inválidos' })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @UseGuards(AuthGuard('jwt'))

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public async crearGastosOperativos(
    @Headers() headers,
    @Body() transaccion: CrearTransaccionGastoOperativoDto,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {

      if(mongoose.isValidObjectId(transaccion.usuario._id) && transaccion.monto){

        const crearTransaccion = await this.crearGastoOperativoService.crearTransaccion(
          transaccion
        );
  
        if (crearTransaccion) {
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.CREATED,
            datos: crearTransaccion,
          });
        } else {
          const ERROR_CREACION = await this.i18n.translate(
            codIdioma.concat('.ERROR_CREACION'),
            {
              lang: codIdioma,
            },
          );
          return this.funcion.enviarRespuestaOptimizada({
            codigoEstado: HttpStatus.NOT_FOUND,
            mensaje: ERROR_CREACION,
          });
        }

      }else{
        const PARAMETROS_NO_VALIDOS = await this.i18n.translate(
          codIdioma.concat('.PARAMETROS_NO_VALIDOS'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.NOT_ACCEPTABLE,
          mensaje: PARAMETROS_NO_VALIDOS,
        });
      }

      
    } catch (err) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: err.message,
      });
    }
  }
}
