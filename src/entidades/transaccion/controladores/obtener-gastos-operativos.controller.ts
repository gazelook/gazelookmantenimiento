import { Controller, Get, Headers, HttpStatus, Query, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiHeader, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { I18nService } from 'nestjs-i18n';
import { filtroBusquedaGastoOperacional } from 'src/shared/enum-sistema';
//Metodos de pago
import { Funcion } from 'src/shared/funcion';
import { FiltrarAportacionesService } from '../casos_de_uso/filtrar-aportaciones.service';
import { ObtenerGastosOperativosService } from '../casos_de_uso/obtener-gastos-operativos.service';
import { AportacionesDto } from './../entidad/aportaciones-dto';

@ApiTags('Transacciones')
@Controller('api/obtener-gastos-operativos')
export class ObtenerGastosOperativosControlador {
  constructor(
    private readonly i18n: I18nService,
    private readonly funcion: Funcion,
    private obtenerGastosOperativosService: ObtenerGastosOperativosService,
  ) { }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  @Get('/')
  @ApiOperation({
    summary:
      'Este método obtiene las aportaciones segun el filtro que se envie que puede ser (mudial, pais) y segun el tipo de aportacion (suscripcion, valorExtra)',
  })
  @ApiResponse({ status: 200, type: [AportacionesDto], description: 'OK' })
  @ApiResponse({ status: 400, description: 'Datos inválidos' })
  @ApiHeader({
    name: 'apiKey',
    description: 'key para usar las rutas',
    required: true,
  })
  @UseGuards(AuthGuard('jwt'))

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public async gastosOperativos(
    @Headers() headers,
    @Query('fecha') fecha: Date,
  ) {
    const codIdioma = this.funcion.obtenerIdiomaDefecto(headers.idioma);

    try {

      const aportaciones = await this.obtenerGastosOperativosService.gastosOperativos(
        fecha
      );
      if (aportaciones) {
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          datos: aportaciones,
        });
      } else {
        const ERROR_OBTENER = await this.i18n.translate(
          codIdioma.concat('.ERROR_OBTENER'),
          {
            lang: codIdioma,
          },
        );
        return this.funcion.enviarRespuestaOptimizada({
          codigoEstado: HttpStatus.OK,
          mensaje: ERROR_OBTENER,
        });
      }

    } catch (err) {
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.INTERNAL_SERVER_ERROR,
        mensaje: err.message,
      });
    }
  }
}
