import { CatalogosServiceModule } from './../../catalogos/casos_de_uso/catalogos-services.module';
import { forwardRef, Module } from '@nestjs/common';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { transaccionProviders } from '../drivers/transaccion.provider';
import { UsuarioServicesModule } from '../../usuario/casos_de_uso/usuario-services.module';
import { ConvertirMontoMonedaService } from 'src/entidades/moneda/casos_de_uso/convertir-monto-moneda.service';
import { FiltrarAportacionesService } from './filtrar-aportaciones.service';
// import { ObtenerBeneficiarioTransaccionService } from './obtener-beneficiario-transaccion.service';
import { MonedaServicesModule } from '../../moneda/casos_de_uso/moneda.services.module';
import { StripeModule } from '../../../drivers/stripe/stripe.module';
import { ObtenerGastosOperativosService } from './obtener-gastos-operativos.service';
import { ObtenerFondosReservadosGazelookService } from './obtener-fondos-reservados-gazelook.service';
import { ObtenerGananciasGazelookService } from './obtener-ganancias-gazelook.service';
import { CrearGastoOperativoService } from './crear-transaccion.service';
import { CrearConversionTransaccionService } from './crear-conversion-transaccion.service';
import { AportacionesMetodoPagoService } from './aportaciones-metodo-pago.service';
import { Funcion } from 'src/shared/funcion';
import { ObtenerTrasaccionesPaymentezService } from './obtener-transacciones-paymentez.service';
import { CrearTransaccionValorExtraService } from './crear-transaccion-valor-extra.service';
import { CrearPagoCoinpaymentezService } from './crear-pago-coinpaymentez.service';
import { CrearPagoPaymentezService } from './crear-pago-paymentez.service';
import { CrearinformacionPagoService } from 'src/entidades/pagos/casos_de_uso/crear-informacion-pago.service';
import { GestionPagoPaymentezService } from './gestion-pago-paymentez.service';
import { PagosServiceModule } from 'src/entidades/pagos/casos_de_uso/pagos.services.module';
import { CoinpaymentezTransactionService } from 'src/drivers/coinpaymentez/services/coinpayments-transaction.service';

@Module({
  imports: [
    DBModule,
    CatalogosServiceModule,
    forwardRef(() => UsuarioServicesModule),
    MonedaServicesModule,
    StripeModule,
    forwardRef(() => PagosServiceModule),
  ],
  providers: [
    ...transaccionProviders,
    ConvertirMontoMonedaService,
    FiltrarAportacionesService,
    ObtenerGastosOperativosService,
    ObtenerFondosReservadosGazelookService,
    ObtenerGananciasGazelookService,
    CrearGastoOperativoService,
    CrearConversionTransaccionService,
    AportacionesMetodoPagoService,
    Funcion,
    ObtenerTrasaccionesPaymentezService,
    CrearTransaccionValorExtraService,
    CrearPagoCoinpaymentezService,
    CrearPagoPaymentezService,
    CrearinformacionPagoService,
    GestionPagoPaymentezService,
    CoinpaymentezTransactionService
  ],
  exports: [
    FiltrarAportacionesService,
    ObtenerGastosOperativosService,
    ObtenerFondosReservadosGazelookService,
    ObtenerGananciasGazelookService,
    CrearGastoOperativoService,
    CrearConversionTransaccionService,
    AportacionesMetodoPagoService,
    ObtenerTrasaccionesPaymentezService,
    CrearTransaccionValorExtraService,
    CrearPagoCoinpaymentezService,
    CrearPagoPaymentezService,
    CrearinformacionPagoService,
    GestionPagoPaymentezService
  ],
  controllers: [],
})
export class TransaccionServicesModule {}
