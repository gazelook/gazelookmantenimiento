import { Injectable } from '@nestjs/common';
import { CrearTransaccionService } from 'src/entidades/pagos/casos_de_uso/crear-transaccion.service';
import { CrearTransaccionValorExtraService } from './crear-transaccion-valor-extra.service';

@Injectable()
export class CrearPagoPaymentezService {
  constructor(private crearTransaccionService: CrearTransaccionService,
    private crearTransaccionValorExtraService: CrearTransaccionValorExtraService) {}

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  async crearPagoPaymentez(datosPago: any, opts?: any, valorExtra?: any): Promise<any> {
    try {
      const idPago = datosPago.idPago;

      // guardar transacción y suscripcon con estado pendiente
      const numeroRecibo = this.crearTransaccionService.generarNumeroRecibo();

      let transaccion
      if(valorExtra){
        
        transaccion = await this.crearTransaccionValorExtraService.crearTransaccionValorExtraService(
          datosPago,
          idPago,
          numeroRecibo,
          opts,
        );
        
      }else{
        transaccion = await this.crearTransaccionService.crearTransaccion(
          datosPago,
          idPago,
          numeroRecibo,
          opts,
        );
      }

      const result = {
        idTransaccion: transaccion._id,
      };

      return result;
    } catch (error) {
      throw error;
    }
  }
}
