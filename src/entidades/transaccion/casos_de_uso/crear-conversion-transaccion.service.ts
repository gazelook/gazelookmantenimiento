import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { ConversionTransaccion } from '../../../drivers/mongoose/interfaces/convercion_transaccion/convercion-transaccion.interface';
import { listaCodigosMonedas } from '../../../money/enum-lista-money';
import { ConvertirMontoMonedaService } from '../../moneda/casos_de_uso/convertir-monto-moneda.service';
import { ConversionTransaccionDto } from '../entidad/conversion-transaccion.dto';

@Injectable()
export class CrearConversionTransaccionService {
  constructor(
    @Inject('CONVERSION_TRANSACCION_MODEL')
    private readonly conversionTransaccionModel: Model<ConversionTransaccion>,
    private convertirMontoMonedaService: ConvertirMontoMonedaService,
  ) {}
  async crearConversionTransaccion(
    datosConversion: ConversionTransaccionDto,
    opts: any,
  ): Promise<Array<string>> {
    try {
      let listaIdsConversion = [];

      let listConvertir = [`${listaCodigosMonedas.EUR}`];

      // si la moneda del usuario es igual a la moneda por defecto del sistema (USD)
      if (datosConversion.monedaDefault === datosConversion.monedaUsuario) {
        const dataConversionTransaccion = {
          moneda: datosConversion.monedaUsuario,
          monto: datosConversion.montoDefault,
          principal: true,
          transaccion: datosConversion.idTransaccion,
          comisionTransferencia: 0,
          totalRecibido: datosConversion.montoDefault
        };

        const conversionDefault = await new this.conversionTransaccionModel(
          dataConversionTransaccion,
        ).save(opts);
        listaIdsConversion.push(conversionDefault._id);
      } else {
        // Guardar conversion en USD
        const dataConversionTransaccion = {
          moneda: datosConversion.monedaDefault,
          monto: datosConversion.montoDefault,
          transaccion: datosConversion.idTransaccion,
          comisionTransferencia: 0,
          totalRecibido: datosConversion.montoDefault
        };

        const conversionDefault = await new this.conversionTransaccionModel(
          dataConversionTransaccion,
        ).save(opts);
        listaIdsConversion.push(conversionDefault._id);
      }

      // agregar a la lista de conversion la moneda del usuario
      if (
        datosConversion.monedaUsuario !== listaCodigosMonedas.USD &&
        datosConversion.monedaUsuario !== listaCodigosMonedas.EUR
      ) {
        listConvertir.push(datosConversion.monedaUsuario);
      }

      // se ejecuta la conversion a EUR y la moneda del usuario (en el caso que sea diferente a USD)
      for (const moneda of listConvertir) {
        const montoConvertido = await this.convertirMontoMonedaService
          .convertirMoney(
            datosConversion.montoDefault,
            datosConversion.monedaDefault,
            moneda,
          )
          .catch(error => {
            throw error;
          });
        const dataConversionTransaccion = {
          moneda: moneda,
          monto: parseFloat(montoConvertido.toFixed(2)),
          principal: datosConversion.monedaUsuario === moneda ? true : false,
          transaccion: datosConversion.idTransaccion,
          comisionTransferencia: 0,
          totalRecibido: parseFloat(montoConvertido.toFixed(2))
        };
        const conversion = await new this.conversionTransaccionModel(
          dataConversionTransaccion,
        ).save(opts);
        listaIdsConversion.push(conversion._id);
      }

      return listaIdsConversion;
    } catch (error) {
      throw error;
    }
  }
}
