import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import * as mongoose from 'mongoose';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { CrearHistoricoService } from 'src/entidades/historico/casos_de_uso/crear-historico.service';
import { CatalogoEstadoService } from './../../catalogos/casos_de_uso/catalogo-estado.service';
import { Transaccion } from 'src/drivers/mongoose/interfaces/transaccion/transaccion.interface';
import {
  catalogoOrigen,
  descripcionTransPagosGazelook,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
} from 'src/shared/enum-sistema';
import { ActualizarUsuarioService } from '../../usuario/casos_de_uso/actualizar-usuario.service';
import { CrearConversionTransaccionService } from './crear-conversion-transaccion.service';
import { ConversionTransaccionDto } from '../entidad/conversion-transaccion.dto';
import { CrearTransaccionGastoOperativoDto } from '../entidad/transaccion.dto';
import { listaCodigosMonedas } from 'src/shared/enum-lista-money';

@Injectable()
export class CrearGastoOperativoService {
  constructor(
    @Inject('TRANSACCION_MODEL')
    private readonly transaccionModel: Model<Transaccion>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private crearHistoricoService: CrearHistoricoService,
    private actualizarUsuarioService: ActualizarUsuarioService,
    private crearConversionTransaccionService: CrearConversionTransaccionService,
  ) { }

  async crearTransaccion(
    dataTransaccion: CrearTransaccionGastoOperativoDto
  ): Promise<any> {
    // //Obtiene el codigo de la accion a realizarse
    // Inicia proceso de transaccion
    const session = await mongoose.startSession();
    await session.startTransaction();

    try {

      const opts = { session };

      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.transaccion,
      );

      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );
      const accionCrear = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.crear,
      );

      let transaccion

      const idTransaccion = new mongoose.mongo.ObjectId();

      console.log('idTransaccion: ', idTransaccion);
      // -------------------------------- Crear Conversion ---------------------------
      const dataConversion: ConversionTransaccionDto = {
        monedaUsuario: listaCodigosMonedas.USD,
        monedaDefault: listaCodigosMonedas.USD,
        montoDefault: dataTransaccion.monto,
        idTransaccion: idTransaccion.toHexString()
      };
      const conversiones = await this.crearConversionTransaccionService.crearConversionTransaccion(
        dataConversion,
        opts,
      );


      const transaccionDto = {
        _id: idTransaccion,
        estado: estado.codigo,
        monto: dataTransaccion.monto,
        moneda: listaCodigosMonedas.USD,
        descripcion: descripcionTransPagosGazelook.gastoOperacional,
        origen: catalogoOrigen.gastos_operativos,
        destino: catalogoOrigen.gastos_operativos,
        balance: [],
        metodoPago: null,
        informacionPago: null,
        usuario: dataTransaccion.usuario._id,
        conversionTransaccion: conversiones
      };

      transaccion = await new this.transaccionModel(transaccionDto).save(
        opts,
      );

      // Agregar referencia usuario
      await this.actualizarUsuarioService.agregarUsuarioTransaccion(
        dataTransaccion.usuario._id,
        transaccion._id,
        opts,
      );

      //datos para guardar el historico
      const newHistorico: any = {
        datos: transaccion,
        usuario: dataTransaccion.usuario._id,
        accion: accionCrear.codigo,
        entidad: entidad.codigo,
      };

      //Crear historico para transacción
      this.crearHistoricoService.crearHistoricoServer(newHistorico);


      console.log('finnnnnnnnnnn transaccion');

      // actualizar el nuevo pago
      await session.commitTransaction();
      session.endSession();

      return transaccion;
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      throw error;
    }
  }

  generarNumeroRecibo() {
    try {
      const numGenerate = `${Math.floor(new Date().valueOf() * Math.random())}`;

      return numGenerate;
    } catch (error) {
      throw error;
    }
  }
}
