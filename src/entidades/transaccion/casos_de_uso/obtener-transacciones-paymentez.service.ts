import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Transaccion } from 'src/drivers/mongoose/interfaces/transaccion/transaccion.interface';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { listaCodigosMonedas } from 'src/money/enum-lista-money';
import { Funcion } from 'src/shared/funcion';
import {
  codigosMetodosPago,
  filtroBusquedaAportacionesMetodoPago, nombrecatalogoEstados,
  nombreEntidades,
  numeroDias
} from './../../../shared/enum-sistema';
import { CatalogoEstadoService } from './../../catalogos/casos_de_uso/catalogo-estado.service';

@Injectable()
export class ObtenerTrasaccionesPaymentezService {
  constructor(
    @Inject('TRANSACCION_MODEL')
    private readonly transaccionModel: Model<Transaccion>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private readonly funcion: Funcion
  ) { }

  filtroBus = filtroBusquedaAportacionesMetodoPago;
  numeroDia = numeroDias;

  async transaccionesPaymentez(
    fechaInicial: any,
    fechaFinal: any,
  ): Promise<any> {
    // valor total de mis aportaciones en Gazelook. por rango de fecha
    try {
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.transaccion,
      );

      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );

      let transaccionesSuscripcion: any;
      let aportacionesSuscripcionUsd = 0;
      let aportacionesSuscripcionEur = 0;
      
      let arrayAportaciones = [];
      let objAportacionUsd: any = {
        aportaciones: 0,
        moneda: {
          codNombre: '',
        },
      };
      let objAportacionEur: any = {
        aportaciones: 0,
        moneda: {
          codNombre: '',
        },
      };

      // console.log('formatFinal: ', formatFinal)
      let formatFechaInicial = fechaInicial + 'T00:00:00.000Z';
      let formatFechaFinal = fechaFinal + 'T23:59:59.999Z';
      //Fecha inical y fianl
      let fInicial = new Date(formatFechaInicial);
      let fFinal = new Date(formatFechaFinal);
      console.log('fechaInicial: ', fInicial);
      console.log('fechaFinal: ', fFinal);



        //Obtener aportes por suscripcion
        transaccionesSuscripcion = await this.transaccionModel
          .find({
            $and: [
              { fechaActualizacion: { $gte: fInicial } },
              { fechaActualizacion: { $lte: fFinal } },
              { estado: estado.codigo },
              {
                $or: [
                  { metodoPago: codigosMetodosPago.payments1 },
                  { metodoPago: codigosMetodosPago.payments2 },
                ]
              }
            ],
          })
          .populate({
            path: 'conversionTransaccion',
          });


        if (transaccionesSuscripcion.length > 0) {
          for (const transaccionSuscripcion of transaccionesSuscripcion) {
            let objTransaccion = {
              _id:transaccionSuscripcion._id,
              totalRecibido: transaccionSuscripcion.totalRecibido,
              comisionTransferencia: transaccionSuscripcion.comisionTransferencia,
              origen: {
                codigo:transaccionSuscripcion.origen
              },
              // perfil
              descripcion:transaccionSuscripcion.descripcion,
              fechaCreacion:transaccionSuscripcion.fechaCreacion,
              fechaActualizacion: transaccionSuscripcion.fechaActualizacion
            }

            arrayAportaciones.push(objTransaccion)
            
          }
        }

        return arrayAportaciones;
      
    } catch (error) {
      throw error;
    }
  }
  
}
