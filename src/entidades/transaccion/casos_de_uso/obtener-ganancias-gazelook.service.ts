import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Transaccion } from 'src/drivers/mongoose/interfaces/transaccion/transaccion.interface';
import { CatalogoAccionService } from 'src/entidades/catalogos/casos_de_uso/catalogo-accion.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { listaCodigosMonedas } from 'src/money/enum-lista-money';
import { Funcion } from 'src/shared/funcion';
import {
  catalogoOrigen,
  filtroBusquedaAportaciones,
  nombreAcciones,
  nombrecatalogoEstados,
  nombreEntidades,
  numeroDias,
} from './../../../shared/enum-sistema';
import { CatalogoEstadoService } from './../../catalogos/casos_de_uso/catalogo-estado.service';

@Injectable()
export class ObtenerGananciasGazelookService {
  constructor(
    @Inject('TRANSACCION_MODEL')
    private readonly transaccionModel: Model<Transaccion>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private catalogoAccionService: CatalogoAccionService,
    private readonly funcion: Funcion
  ) {}

  filtroBus = filtroBusquedaAportaciones;
  numeroDia = numeroDias;
  
  async obtenerGananciasGazelook(
    fecha: any
  ): Promise<any> {
    // valor total de mis aportaciones en Gazelook. por rango de fecha
    try {
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.transaccion,
      );

      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );
      const accionCrear = await this.catalogoAccionService.obtenerNombreAccion(
        nombreAcciones.ver,
      );

      let transaccionesSuscripcion: any;
      let transaccionesValorExtra: any;

      let final;
      let getFecha
      //SI SE ENVIA LA FECHA
      if (fecha) {
        getFecha = new Date(fecha);
      } else {
        getFecha = new Date();
      }

      //Obtiene el dia de la fecha enviada
      let dia = getFecha.getUTCDay();
      //SI el dia de la fecha enviada es 6 (sabado) el dia se establece en 0 (domingo)
      // if (dia === this.numeroDia.sabado) {
      //   dia = this.numeroDia.domingo;
      // } else {
      //   //Caso contrario se le suma 1 al dia obtenido
      //   dia = dia + 1;
      // }
      //Obtiene el dia
      const getDia = this.funcion.seleccionarDia(dia);

      // console.log('FECHA ENVIADA: ', getFecha);
      //Establece la fecha final para la busqueda
      const fechaF = getFecha.setUTCDate(getFecha.getUTCDate() - getDia);

      const fecFinal = new Date(fechaF);

      // console.log('fecFinal: ', fecFinal)

      let formatFinal =
        fecFinal.getUTCFullYear() +
        '-' +
        (fecFinal.getUTCMonth() + 1) +
        '-' +
        fecFinal.getUTCDate();

      //Si el mes de la fecha final es menor a 10 y el dia de la fecha final es mayor o igual a 10
      if (fecFinal.getUTCMonth() + 1 < 10 && fecFinal.getUTCDate() >= 10) {
        formatFinal =
          fecFinal.getUTCFullYear() +
          '-0' +
          (fecFinal.getUTCMonth() + 1) +
          '-' +
          fecFinal.getUTCDate();
      }

      //Si el mes de la fecha final es menor a 10 y el dia de la fecha final es igual a 32
      if (fecFinal.getUTCMonth() + 1 < 10 && fecFinal.getUTCDate() === 32) {
        formatFinal =
          fecFinal.getUTCFullYear() +
          '-0' +
          (fecFinal.getUTCMonth() + 2) +
          '-01';
      }

      //Si el mes de la fecha final es menor a 10 y el dia de la fecha final es menor a 10
      if (fecFinal.getUTCMonth() + 1 < 10 && fecFinal.getUTCDate() < 10) {
        formatFinal =
          fecFinal.getUTCFullYear() +
          '-0' +
          (fecFinal.getUTCMonth() + 1) +
          '-0' +
          fecFinal.getUTCDate();
      }

      //Si el mes de la fecha final es mayor a 10 y el dia de la fecha final es menor a 10
      if (fecFinal.getUTCMonth() + 1 >= 10 && fecFinal.getUTCDate() < 10) {
        formatFinal =
          fecFinal.getUTCFullYear() +
          '-' +
          (fecFinal.getUTCMonth() + 1) +
          '-0' +
          fecFinal.getUTCDate();
      }


      // console.log('formatFinal: ', formatFinal)
      formatFinal = formatFinal + 'T23:59:59.999Z';
      //Fecha final
      final = new Date(formatFinal);
      console.log('fechaFin: ', final);

      //obtener aportes por suscripcion
      transaccionesSuscripcion = await this.transaccionModel
        .find({
          $and: [
            { fechaActualizacion: { $lte: final } },
            { estado: estado.codigo },
            { origen: catalogoOrigen.suscripciones },
          ],
        })
        .populate({
          path: 'conversionTransaccion',
        });
      // }

      
      let aportacionesSuscripcionUsd = 0;
      let aportacionesSuscripcionEur = 0;
      let arrayAportaciones = [];
      let objAportacionUsd: any = {
        aportacionesSuscripcion: 0,
        moneda: {
          codNombre: '',
        },
      };
      let objAportacionEur: any = {
        aportacionesSuscripcion: 0,
        moneda: {
          codNombre: '',
        },
      };

      if (transaccionesSuscripcion.length > 0) {
        for (const transaccionSuscripcion of transaccionesSuscripcion) {
          if (transaccionSuscripcion.conversionTransaccion.length > 0) {
            for (const conversion of transaccionSuscripcion.conversionTransaccion) {
              if (conversion.totalRecibido) {
                if (conversion.moneda === listaCodigosMonedas.USD) {
                  aportacionesSuscripcionUsd += Number(
                    conversion.totalRecibido,
                  );

                  objAportacionUsd.aportacionesSuscripcion = aportacionesSuscripcionUsd;
                  objAportacionUsd.moneda.codNombre = conversion.moneda;
                }
                if (conversion.moneda === listaCodigosMonedas.EUR) {
                  aportacionesSuscripcionEur += Number(
                    conversion.totalRecibido,
                  );
                  objAportacionEur.aportacionesSuscripcion = aportacionesSuscripcionEur;
                  objAportacionEur.moneda.codNombre = conversion.moneda;
                }
              }
            }
          }
        }
      }

      
      if (objAportacionUsd.moneda.codNombre != '') {
        objAportacionUsd.aportacionesSuscripcion =
          objAportacionUsd.aportacionesSuscripcion * 0.2;
        objAportacionUsd.aportacionesSuscripcion = parseFloat(
          objAportacionUsd.aportacionesSuscripcion.toFixed(2),
        );
        arrayAportaciones.push(objAportacionUsd);
      }
      if (objAportacionEur.moneda.codNombre != '') {
        objAportacionEur.aportacionesSuscripcion =
          objAportacionEur.aportacionesSuscripcion * 0.2;
        objAportacionEur.aportacionesSuscripcion = parseFloat(
          objAportacionEur.aportacionesSuscripcion.toFixed(2),
        );
        arrayAportaciones.push(objAportacionEur);
      }

      return arrayAportaciones;
    } catch (error) {
      throw error;
    }
  }

}
