import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Transaccion } from 'src/drivers/mongoose/interfaces/transaccion/transaccion.interface';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { listaCodigosMonedas } from 'src/money/enum-lista-money';
import { Funcion } from 'src/shared/funcion';
import {
  codigosMetodosPago,
  filtroBusquedaAportacionesMetodoPago, nombrecatalogoEstados,
  nombreEntidades,
  numeroDias
} from './../../../shared/enum-sistema';
import { CatalogoEstadoService } from './../../catalogos/casos_de_uso/catalogo-estado.service';

@Injectable()
export class AportacionesMetodoPagoService {
  constructor(
    @Inject('TRANSACCION_MODEL')
    private readonly transaccionModel: Model<Transaccion>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private readonly funcion: Funcion
  ) { }

  filtroBus = filtroBusquedaAportacionesMetodoPago;
  numeroDia = numeroDias;

  async FiltrarAportaciones(
    filtro: string,
    fecha: any,
  ): Promise<any> {
    // valor total de mis aportaciones en Gazelook. por rango de fecha
    try {
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.transaccion,
      );

      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );

      let transaccionesSuscripcion: any;
      let aportacionesSuscripcionUsd = 0;
      let aportacionesSuscripcionEur = 0;
      
      let arrayAportaciones = [];
      let objAportacionUsd: any = {
        aportaciones: 0,
        moneda: {
          codNombre: '',
        },
      };
      let objAportacionEur: any = {
        aportaciones: 0,
        moneda: {
          codNombre: '',
        },
      };

      const filtroSel = this.seleccionarFiltro(filtro);

      let final;
      let getFecha
      //SI SE ENVIA LA FECHA
      if (fecha) {
        getFecha = new Date(fecha);
      } else {
        getFecha = new Date();
      }

      //Obtiene el dia de la fecha enviada
      let dia = getFecha.getUTCDay();
      //SI el dia de la fecha enviada es 6 (sabado) el dia se establece en 0 (domingo)
      // if (dia === this.numeroDia.sabado) {
      //   dia = this.numeroDia.domingo;
      // } else {
      //   //Caso contrario se le suma 1 al dia obtenido
      //   dia = dia + 1;
      // }
      //Obtiene el dia
      const getDia = this.funcion.seleccionarDia(dia);

      // console.log('FECHA ENVIADA: ', getFecha);
      //Establece la fecha final para la busqueda
      const fechaF = getFecha.setUTCDate(getFecha.getUTCDate() - getDia);

      const fecFinal = new Date(fechaF);

      // console.log('fecFinal: ', fecFinal)

      let formatFinal =
        fecFinal.getUTCFullYear() +
        '-' +
        (fecFinal.getUTCMonth() + 1) +
        '-' +
        fecFinal.getUTCDate();

      //Si el mes de la fecha final es menor a 10 y el dia de la fecha final es mayor o igual a 10
      if (fecFinal.getUTCMonth() + 1 < 10 && fecFinal.getUTCDate() >= 10) {
        formatFinal =
          fecFinal.getUTCFullYear() +
          '-0' +
          (fecFinal.getUTCMonth() + 1) +
          '-' +
          fecFinal.getUTCDate();
      }

      //Si el mes de la fecha final es menor a 10 y el dia de la fecha final es igual a 32
      if (fecFinal.getUTCMonth() + 1 < 10 && fecFinal.getUTCDate() === 32) {
        formatFinal =
          fecFinal.getUTCFullYear() +
          '-0' +
          (fecFinal.getUTCMonth() + 2) +
          '-01';
      }

      //Si el mes de la fecha final es menor a 10 y el dia de la fecha final es menor a 10
      if (fecFinal.getUTCMonth() + 1 < 10 && fecFinal.getUTCDate() < 10) {
        formatFinal =
          fecFinal.getUTCFullYear() +
          '-0' +
          (fecFinal.getUTCMonth() + 1) +
          '-0' +
          fecFinal.getUTCDate();
      }

      //Si el mes de la fecha final es mayor a 10 y el dia de la fecha final es menor a 10
      if (fecFinal.getUTCMonth() + 1 >= 10 && fecFinal.getUTCDate() < 10) {
        formatFinal =
          fecFinal.getUTCFullYear() +
          '-' +
          (fecFinal.getUTCMonth() + 1) +
          '-0' +
          fecFinal.getUTCDate();
      }


      // console.log('formatFinal: ', formatFinal)
      formatFinal = formatFinal + 'T23:59:59.999Z';
      //Fecha final
      final = new Date(formatFinal);
      console.log('fechaFin: ', final);


      //Filtro Stripe
      if (filtroSel === 1) {

        //Obtener aportes por suscripcion
        transaccionesSuscripcion = await this.transaccionModel
          .find({
            $and: [
              // { fechaActualizacion: { $gte: inicio } },
              { fechaActualizacion: { $lte: final } },
              { estado: estado.codigo },
              { metodoPago: codigosMetodosPago.stripe },
            ],
          })
          .populate({
            path: 'conversionTransaccion',
          });


        if (transaccionesSuscripcion.length > 0) {
          for (const transaccionSuscripcion of transaccionesSuscripcion) {
            if (transaccionSuscripcion.conversionTransaccion.length > 0) {
              for (const conversion of transaccionSuscripcion.conversionTransaccion) {
                if (conversion.totalRecibido) {
                  if (conversion.moneda === listaCodigosMonedas.USD) {
                    aportacionesSuscripcionUsd += Number(
                      conversion.totalRecibido,
                    );

                    // objAportacionUsd.aportaciones = aportacionesSuscripcionUsd;
                    objAportacionUsd.moneda.codNombre = conversion.moneda;
                  }
                  if (conversion.moneda === listaCodigosMonedas.EUR) {
                    aportacionesSuscripcionEur += Number(
                      conversion.totalRecibido,
                    );
                    // objAportacionEur.aportaciones = aportacionesSuscripcionEur;
                    objAportacionEur.moneda.codNombre = conversion.moneda;
                  }
                }
              }
            }
          }
        }
        //USD
        objAportacionUsd.aportaciones = aportacionesSuscripcionUsd.toFixed(2)

        //EUR
        objAportacionEur.aportaciones = aportacionesSuscripcionEur.toFixed(2)

        if (objAportacionUsd.moneda.codNombre != '') {
          arrayAportaciones.push(objAportacionUsd);
        }
        if (objAportacionEur.moneda.codNombre != '') {
          arrayAportaciones.push(objAportacionEur);
        }
        return arrayAportaciones;
      }
      //Filtro de Paymentez
      if (filtroSel === 2) {


        //Obtener aportes por suscripcion
        transaccionesSuscripcion = await this.transaccionModel
          .find({
            $and: [
              // { fechaActualizacion: { $gte: inicio } },
              { fechaActualizacion: { $lte: final } },
              { estado: estado.codigo },
              {
                $or: [
                  { metodoPago: codigosMetodosPago.payments1 },
                  { metodoPago: codigosMetodosPago.payments2 },
                ]
              }
            ],
          })
          .populate({
            path: 'conversionTransaccion',
          });


        if (transaccionesSuscripcion.length > 0) {
          for (const transaccionSuscripcion of transaccionesSuscripcion) {
            if (transaccionSuscripcion.conversionTransaccion.length > 0) {
              for (const conversion of transaccionSuscripcion.conversionTransaccion) {
                if (conversion.totalRecibido) {
                  if (conversion.moneda === listaCodigosMonedas.USD) {
                    aportacionesSuscripcionUsd += Number(
                      conversion.totalRecibido,
                    );

                    // objAportacionUsd.aportaciones = aportacionesSuscripcionUsd;
                    objAportacionUsd.moneda.codNombre = conversion.moneda;
                  }
                  if (conversion.moneda === listaCodigosMonedas.EUR) {
                    aportacionesSuscripcionEur += Number(
                      conversion.totalRecibido,
                    );
                    // objAportacionEur.aportaciones = aportacionesSuscripcionEur;
                    objAportacionEur.moneda.codNombre = conversion.moneda;
                  }
                }
              }
            }
          }
        }

        //USD
        objAportacionUsd.aportaciones = aportacionesSuscripcionUsd.toFixed(2)

        //EUR
        objAportacionEur.aportaciones = aportacionesSuscripcionEur.toFixed(2)

        if (objAportacionUsd.moneda.codNombre != '') {
          arrayAportaciones.push(objAportacionUsd);
        }
        if (objAportacionEur.moneda.codNombre != '') {
          arrayAportaciones.push(objAportacionEur);
        }
        return arrayAportaciones;

      }

      //Filtro de Cripto
      if (filtroSel === 3) {


        //Obtener aportes por suscripcion
        transaccionesSuscripcion = await this.transaccionModel
          .find({
            $and: [
              // { fechaActualizacion: { $gte: inicio } },
              { fechaActualizacion: { $lte: final } },
              { estado: estado.codigo },
              { metodoPago: codigosMetodosPago.cripto }
            ],
          })
          .populate({
            path: 'conversionTransaccion',
          });

        if (transaccionesSuscripcion.length > 0) {
          for (const transaccionSuscripcion of transaccionesSuscripcion) {
            if (transaccionSuscripcion.conversionTransaccion.length > 0) {
              for (const conversion of transaccionSuscripcion.conversionTransaccion) {
                if (conversion.totalRecibido) {
                  if (conversion.moneda === listaCodigosMonedas.USD) {
                    aportacionesSuscripcionUsd += Number(
                      conversion.totalRecibido,
                    );

                    // objAportacionUsd.aportaciones = aportacionesSuscripcionUsd;
                    objAportacionUsd.moneda.codNombre = conversion.moneda;
                  }
                  if (conversion.moneda === listaCodigosMonedas.EUR) {
                    aportacionesSuscripcionEur += Number(
                      conversion.totalRecibido,
                    );
                    // objAportacionEur.aportaciones = aportacionesSuscripcionEur;
                    objAportacionEur.moneda.codNombre = conversion.moneda;
                  }
                }
              }
            }
          }
        }

        //USD
        objAportacionUsd.aportaciones = aportacionesSuscripcionUsd.toFixed(2)

        //EUR
        objAportacionEur.aportaciones = aportacionesSuscripcionEur.toFixed(2)

        if (objAportacionUsd.moneda.codNombre != '') {
          arrayAportaciones.push(objAportacionUsd);
        }
        if (objAportacionEur.moneda.codNombre != '') {
          arrayAportaciones.push(objAportacionEur);
        }
        return arrayAportaciones;

      }

      //Filtro de Cripto
      if (filtroSel === 4) {


        //Obtener aportes por suscripcion
        transaccionesSuscripcion = await this.transaccionModel
          .find({
            $and: [
              // { fechaActualizacion: { $gte: inicio } },
              { fechaActualizacion: { $lte: final } },
              { estado: estado.codigo },
              { metodoPago: codigosMetodosPago.panama }
            ],
          })
          .populate({
            path: 'conversionTransaccion',
          });

        if (transaccionesSuscripcion.length > 0) {
          for (const transaccionSuscripcion of transaccionesSuscripcion) {
            if (transaccionSuscripcion.conversionTransaccion.length > 0) {
              for (const conversion of transaccionSuscripcion.conversionTransaccion) {
                if (conversion.totalRecibido) {
                  if (conversion.moneda === listaCodigosMonedas.USD) {
                    aportacionesSuscripcionUsd += Number(
                      conversion.totalRecibido,
                    );

                    // objAportacionUsd.aportaciones = aportacionesSuscripcionUsd;
                    objAportacionUsd.moneda.codNombre = conversion.moneda;
                  }
                  if (conversion.moneda === listaCodigosMonedas.EUR) {
                    aportacionesSuscripcionEur += Number(
                      conversion.totalRecibido,
                    );
                    // objAportacionEur.aportaciones = aportacionesSuscripcionEur;
                    objAportacionEur.moneda.codNombre = conversion.moneda;
                  }
                }
              }
            }
          }
        }

        //USD
        objAportacionUsd.aportaciones = aportacionesSuscripcionUsd.toFixed(2)

        //EUR
        objAportacionEur.aportaciones = aportacionesSuscripcionEur.toFixed(2)

        if (objAportacionUsd.moneda.codNombre != '') {
          arrayAportaciones.push(objAportacionUsd);
        }
        if (objAportacionEur.moneda.codNombre != '') {
          arrayAportaciones.push(objAportacionEur);
        }
        return arrayAportaciones;

      }

    } catch (error) {
      throw error;
    }
  }

  seleccionarFiltro(filtro) {
    switch (filtro) {
      case this.filtroBus.stripe:
        return 1;
      case this.filtroBus.paymentez:
        return 2;
      case this.filtroBus.cripto:
        return 3;
      case this.filtroBus.panama:
        return 4;
    }
  }
  
}
