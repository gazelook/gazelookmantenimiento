import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Transaccion } from 'src/drivers/mongoose/interfaces/transaccion/transaccion.interface';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { listaCodigosMonedas } from 'src/money/enum-lista-money';
import { Funcion } from 'src/shared/funcion';
import {
  catalogoOrigen, nombrecatalogoEstados,
  nombreEntidades,
  numeroDias
} from './../../../shared/enum-sistema';
import { CatalogoEstadoService } from './../../catalogos/casos_de_uso/catalogo-estado.service';

@Injectable()
export class ObtenerGastosOperativosService {
  constructor(
    @Inject('TRANSACCION_MODEL')
    private readonly transaccionModel: Model<Transaccion>,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private readonly funcion: Funcion
  ) { }

  numeroDia = numeroDias;

  async gastosOperativos(
    fecha: any,
  ): Promise<any> {
    // valor total de mis aportaciones en Gazelook. por rango de fecha
    try {
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(
        nombreEntidades.transaccion,
      );

      const estado = await this.catalogoEstadoService.obtenerNombreEstado(
        nombrecatalogoEstados.activa,
        entidad.codigo,
      );

      let transaccionesGastosOperativos: any;
      let gastosOperativosUsd = 0;
      let objAportacion: any = {
        gastosOperativos: 0,
        moneda: {
          codNombre: '',
        },
      };

      let inicio;
      let final;
      let getFecha
      //SI SE ENVIA LA FECHA
      if (fecha) {
        getFecha = new Date(fecha);
      } else {
        getFecha = new Date();
      }

      //Obtiene el dia de la fecha enviada
      let dia = getFecha.getUTCDay();
      //SI el dia de la fecha enviada es 6 (sabado) el dia se establece en 0 (domingo)
      // if (dia === this.numeroDia.sabado) {
      //   dia = this.numeroDia.domingo;
      // } else {
      //   //Caso contrario se le suma 1 al dia obtenido
      //   dia = dia + 1;
      // }
      //Obtiene el dia
      const getDia = this.funcion.seleccionarDia(dia);

      // console.log('FECHA ENVIADA: ', getFecha);
      //Establece la fecha final para la busqueda
      const fechaF = getFecha.setUTCDate(getFecha.getUTCDate() - getDia);

      const fecFinal = new Date(fechaF);

      // console.log('fecFinal: ', fecFinal)

      let formatFinal =
        fecFinal.getUTCFullYear() +
        '-' +
        (fecFinal.getUTCMonth() + 1) +
        '-' +
        fecFinal.getUTCDate();

      //Si el mes de la fecha final es menor a 10 y el dia de la fecha final es mayor o igual a 10
      if (fecFinal.getUTCMonth() + 1 < 10 && fecFinal.getUTCDate() >= 10) {
        formatFinal =
          fecFinal.getUTCFullYear() +
          '-0' +
          (fecFinal.getUTCMonth() + 1) +
          '-' +
          fecFinal.getUTCDate();
      }

      //Si el mes de la fecha final es menor a 10 y el dia de la fecha final es igual a 32
      if (fecFinal.getUTCMonth() + 1 < 10 && fecFinal.getUTCDate() === 32) {
        formatFinal =
          fecFinal.getUTCFullYear() +
          '-0' +
          (fecFinal.getUTCMonth() + 2) +
          '-01';
      }

      //Si el mes de la fecha final es menor a 10 y el dia de la fecha final es menor a 10
      if (fecFinal.getUTCMonth() + 1 < 10 && fecFinal.getUTCDate() < 10) {
        formatFinal =
          fecFinal.getUTCFullYear() +
          '-0' +
          (fecFinal.getUTCMonth() + 1) +
          '-0' +
          fecFinal.getUTCDate();
      }

      //Si el mes de la fecha final es mayor a 10 y el dia de la fecha final es menor a 10
      if (fecFinal.getUTCMonth() + 1 >= 10 && fecFinal.getUTCDate() < 10) {
        formatFinal =
          fecFinal.getUTCFullYear() +
          '-' +
          (fecFinal.getUTCMonth() + 1) +
          '-0' +
          fecFinal.getUTCDate();
      }


      // console.log('formatFinal: ', formatFinal)
      formatFinal = formatFinal + 'T23:59:59.999Z';
      //Fecha final
      final = new Date(formatFinal);
      console.log('fechaFin: ', final);

      //Fecha inicial
      // const fechaI = getFecha.setDate(getFecha.getDate() - 6);
      // inicio = new Date(fechaI);
      // console.log('fechaInicial: ', inicio)

      //Gastos operativos
      transaccionesGastosOperativos = await this.transaccionModel
        .find({
          $and: [
            // { fechaActualizacion: { $gte: inicio } },
            { fechaActualizacion: { $lte: final } },
            { estado: estado.codigo },
            { destino: catalogoOrigen.gastos_operativos },
          ],
        })
        .populate({
          path: 'conversionTransaccion',
        });


      if (transaccionesGastosOperativos.length > 0) {
        for (const transacciongastosOperativos of transaccionesGastosOperativos) {
          if (transacciongastosOperativos.conversionTransaccion.length > 0) {
            for (const conversion of transacciongastosOperativos.conversionTransaccion) {
              if (conversion.totalRecibido) {
                if (conversion.moneda === listaCodigosMonedas.USD) {
                  gastosOperativosUsd += Number(conversion.totalRecibido);
                  objAportacion.moneda.codNombre =
                    conversion.moneda;
                }
              }
            }
          }
        }
      }

      objAportacion.gastosOperativos = gastosOperativosUsd.toFixed(2)
      
      return objAportacion

    } catch (error) {
      throw error;
    }
  }

}
