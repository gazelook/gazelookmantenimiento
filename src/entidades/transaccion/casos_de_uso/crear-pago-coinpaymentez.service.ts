import { Injectable } from '@nestjs/common';
import { CoinpaymentezTransactionService } from 'src/drivers/coinpaymentez/services/coinpayments-transaction.service';
import { CrearTransaccionService } from 'src/entidades/pagos/casos_de_uso/crear-transaccion.service';
import { CrearTransaccionValorExtraService } from './crear-transaccion-valor-extra.service';




@Injectable()
export class CrearPagoCoinpaymentezService {
  constructor(
    private crearTransaccionService: CrearTransaccionService,
    private coinpaymentezTransactionService: CoinpaymentezTransactionService,
    private crearTransaccionValorExtraService: CrearTransaccionValorExtraService,
  ) { }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  async crearPagoCoinpaymentez(datosPago: any, opts?: any, valorExtra?: any): Promise<any> {
    try {
      // GENERAR ORDEN DE PAGO TRANSACCION EN COINPAYMENTEZ
      const nuevoCliente: any = {
        currency1: datosPago.currency1,
        currency2: datosPago.currency2,
        buyer_email: datosPago.email,
        phone: datosPago.telefono,
        amount: datosPago.pagoCripto.cantidad,
        buyer_name: datosPago.nombres
        // currency: 'usd'
      };

      console.log('nuevoCliente----------->: ', nuevoCliente)
      const customer = await this.coinpaymentezTransactionService.createTransaction(nuevoCliente);


      // guardar transacción y suscripcon con estado pendiente
      const numeroRecibo = this.crearTransaccionService.generarNumeroRecibo();


      let transaccion
      if (valorExtra) {

        transaccion = await this.crearTransaccionValorExtraService.crearTransaccionValorExtraService(
          datosPago,
          customer.txn_id,
          numeroRecibo,
          opts,
        );

      } else {
        transaccion = await this.crearTransaccionService.crearTransaccion(
          datosPago,
          customer.txn_id,
          numeroRecibo,
          opts,
        );
      }

      console.log('termina transaccionnnnnnnnnnnnnnnnnnnnnn');
      const result = {
        coinpayments: customer,
        idTransaccion: transaccion._id,
      };

      return result;
    } catch (error) {
      throw error;
    }
  }
}
