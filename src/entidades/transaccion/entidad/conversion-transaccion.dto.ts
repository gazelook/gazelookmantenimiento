import { listaCodigosMonedas } from '../../../money/enum-lista-money';

export class ConversionTransaccionDto {
  monedaUsuario: string;
  montoDefault: number;
  monedaDefault: string;
  idTransaccion: string;
}

export class updateConversionTransaccionDto {
  monedaUsuario: string;
  montoDefault: number;
  monedaDefault: string;
  idTransaccion: string;
}
