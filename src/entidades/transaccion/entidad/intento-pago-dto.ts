import { ApiProperty } from '@nestjs/swagger';
import {
  IsNotEmpty,
  MinLength,
  IsString,
  MaxLength,
  Matches,
  IsEmpty,
  IsEmail,
} from 'class-validator';
import { isEmpty } from 'rxjs/operators';

export class IntentoPagoDto {
  @ApiProperty({ required: true })
  //@IsString({message: 'dsadadasdadasdasdas'})
  nombre: string;
  //     @ApiProperty({required: false})
  //     @IsEmpty()
  //     telefono: string;
  //     @ApiProperty({required: false})
  //    @IsString()
  //     direccion:string;
  //     @ApiProperty({required: false})
  //    @IsString()
  //     pais:string;
  @ApiProperty({ required: true })
  //@IsEmail()
  email: string;
}
