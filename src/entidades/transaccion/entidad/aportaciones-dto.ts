import { ApiProperty } from '@nestjs/swagger';
import { CatalogoTipoMonedaCodigoDto } from 'src/entidades/catalogos/dto/catalogo-tipo-moneda.dto';

export class AportacionesDto {
  @ApiProperty({ type: Number })
  aportacionesSuscripcion: number;
  @ApiProperty({ type: Number })
  aportacionesValorExtra: number;
  @ApiProperty({ type: CatalogoTipoMonedaCodigoDto })
  moneda: CatalogoTipoMonedaCodigoDto;
}
