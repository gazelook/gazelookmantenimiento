import {
  catalogoOrigen,
  codigosMetodosPago,
} from '../../../shared/enum-sistema';
import { TransaccionCuentaDto } from './transaccion.dto';

export class ActualizarTransaccionPagoDto {
  monto: number;
  origen: string;
  metodoPago: string;
}

export class ActualizarInformacionPagoDto {
  idPago: string;
  datos?: {
    nombres: string;
    telefono: string;
    direccion: string;
    email: string;
  };
}

export class NuevosDatosPagoDto {
  idPago?: string;
  nombres?: string;
  email?: string;
  telefono?: string;
  direccion?: string;
  monto?: number;
}

export class datosNuevaTransaccionDto {
  transaccion: TransaccionCuentaDto;
  informacionPago: string;
  usuario: string;
  metodoPago: string;
}
