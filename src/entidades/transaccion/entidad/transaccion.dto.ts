import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, ValidateNested } from 'class-validator';
import { CatalogoOrigenDto } from 'src/entidades/catalogos/dto/catalogo-origen.dto';
import { CatalogoTipoMonedaCodigoDto } from 'src/entidades/catalogos/dto/catalogo-tipo-moneda.dto';
import { IdUsuarioDto } from 'src/entidades/usuario/dto/usuario-dto';

export class TransaccionDto {
  @ApiProperty({
    description: 'Código catálogo estado',
    required: true,
    example: "'EST_7' | 'EST_8' | 'EST_9' | 'EST_10']",
  })
  estado: string;
  @ApiProperty({
    description: 'Cantidad transacción en dólares',
    required: true,
    example: '11.99',
  })
  monto: number;
  @ApiProperty({
    description: 'Código catálago tipo moneda',
    required: true,
    example: 'TIPMON_1 | TIPMON_2',
  })
  moneda: string;
  @ApiProperty({ description: 'Descripción transacción', required: false })
  descripcion: string;
  @ApiProperty({
    description: 'Código catálogo origen',
    required: true,
    example: 'CATORI_1 | CATORI_2 | CATORI_3 | CATORI_4 | CATORI_5',
  })
  origen: string;
  @ApiProperty()
  balance: string;
  @ApiProperty()
  beneficiario: string;
  @ApiProperty({
    description: 'Catálogo metodo pago',
    required: true,
    example: 'METPAG_1 | METPAG_2 | METPAG_3 | METPAG_4',
  })
  metodoPago: string;
}

export class TransaccionCuentaDto {
  @ApiProperty({
    description: 'Cantidad transacción en dólares',
    required: true,
    example: '11.99',
  })
  @IsNotEmpty()
  @IsNumber()
  monto: number;
  @ApiProperty({ type: CatalogoTipoMonedaCodigoDto })
  @IsNotEmpty()
  @ValidateNested({ each: true })
  moneda: CatalogoTipoMonedaCodigoDto;

  @ApiProperty({ type: CatalogoOrigenDto })
  @IsNotEmpty()
  @ValidateNested({ each: true })
  origen: CatalogoOrigenDto;

  @ApiProperty({ type: CatalogoOrigenDto })
  @IsNotEmpty()
  @ValidateNested({ each: true })
  destino: CatalogoOrigenDto;
}

export class UpdateTransaccionPagoDto {
  @IsNotEmpty()
  @IsNumber()
  comisionTransferencia: number;

  @IsNotEmpty()
  @IsNumber()
  totalRecibido: number;

  @IsNotEmpty()
  origenPais: string;
}

export class CrearTransaccionGastoOperativoDto {
  @ApiProperty({
    description: 'Cantidad transacción en dólares',
    required: true,
    example: '11.99',
  })
  monto: number;

  @ApiProperty({type: IdUsuarioDto})
  @IsNotEmpty()
  usuario: IdUsuarioDto;
}
