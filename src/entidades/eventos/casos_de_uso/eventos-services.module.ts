import { HistoricoServiceModule } from './../../historico/casos_de_uso/historico-services.module';
import { CatalogosServiceModule } from 'src/entidades/catalogos/casos_de_uso/catalogos-services.module';

import { Module, forwardRef } from '@nestjs/common';
import { DBModule } from 'src/drivers/db_conection/db.module';
import { eventosProviders } from '../drivers/eventos.provider';
import { UsuarioServicesModule } from '../../usuario/casos_de_uso/usuario-services.module';
import { EmailServicesModule } from '../../emails/casos_de_uso/email-services.module';
import { StorageModule } from '../../../drivers/amazon_s3/storage.module';
import { CronNotificarSuscripcionService } from './cron-notificar-suscripcion.service';
import { CronLimpiarArchivosService } from './cron-limpiar-archivos.service';




@Module({
  imports: [
    DBModule,
    HistoricoServiceModule,
    CatalogosServiceModule,
    UsuarioServicesModule,
    EmailServicesModule,
    StorageModule
  ],
  providers: [
    ...eventosProviders,
    CronLimpiarArchivosService,
    CronNotificarSuscripcionService,
  ],
  exports: [
    ...eventosProviders,
    CronLimpiarArchivosService,
    CronNotificarSuscripcionService,

  ],
  controllers: [],
})
export class EventosServicesModule { }
