import { Evento } from '../../../drivers/mongoose/interfaces/evento/evento.interface';
import { ConfiguracionEvento } from 'src/drivers/mongoose/interfaces/configuracion_evento/configuracion-evento.interface';

import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import * as moment from 'moment';
import * as mongoose from 'mongoose';
import { Cron, CronExpression } from '@nestjs/schedule';
import { Archivo } from '../../../drivers/mongoose/interfaces/archivo/archivo.interface';
import { TraduccionMedia } from '../../../drivers/mongoose/interfaces/traduccion_media/traduccion-media.interface';
import { Media } from '../../../drivers/mongoose/interfaces/media/media.interface';
import { StorageService } from '../../../drivers/amazon_s3/services/storage.service';
import { codigosEstadosMedia, nombrecatalogoEstados, nombreEntidades } from '../../../shared/enum-sistema';
import { CatalogoEstadoService } from '../../catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from '../../catalogos/casos_de_uso/catalogo-entidad.service';
import { StorageDocumentosUsuarioService } from '../../../drivers/amazon_s3/services/storage-documentos-usuario.service';



@Injectable()
export class CronLimpiarArchivosService {
  intervalo;
  configEvento;

  constructor(
    @Inject('EVENTO_MODEL') private readonly eventoModel: Model<Evento>,
    @Inject('CONFIGURACION_EVENTO_MODEL') private readonly configEventoModel: Model<ConfiguracionEvento>,
    @Inject('ARCHIVO_MODEL') private readonly archivoModel: Model<Archivo>,
    @Inject('TRADUCCION_MEDIA_MODEL') private readonly traduccionMediaModel: Model<TraduccionMedia>,
    @Inject('MEDIA_MODEL') private readonly mediaModel: Model<Media>,
    private readonly storageService: StorageService,
    private catalogoEstadoService: CatalogoEstadoService,
    private catalogoEntidadService: CatalogoEntidadService,
    private storageDocumentosUsuarioService: StorageDocumentosUsuarioService
  ) {
    this.configEvento = this.configEventoModel.findOne({
      //codigo: codigosConfiguracionEvento.eventoBloquearResponsableEmail,
    });
  }
  //@Cron(`30000 * * * * *`) // medio minuto prueba

  // todos los domingos a las 12 de la noche: elimina las medias en estado "sin asignar" de un dia anterior al Domingo
  //@Cron(`0 0 * * 0`)

  //Cada primer dia del mes a la media noche
  @Cron(CronExpression.EVERY_1ST_DAY_OF_MONTH_AT_MIDNIGHT)
  async cronLimpiarMediaSinAsignar() {

    const session = await mongoose.startSession()
    await session.startTransaction();

    try {

      const opts = { session };

      const configEvento = await this.configEventoModel.findOne({
        //codigo: codigosConfiguracionEvento.eventoBloquearResponsableEmail,
      });

      //Obtiene fecha actual
      const formatFechaActual = moment().format('YYYY-MM-DD');
      const fechaActual = new Date(formatFechaActual);

      const fechaAyer = new Date(formatFechaActual);
      fechaAyer.setDate(fechaActual.getDate() - 1);
      console.log("fechaAyer", fechaAyer);
      console.log("fechaActual", fechaActual);


      const getMedias = await this.mediaModel.find(
        {
          $or: [
            {
              estado: codigosEstadosMedia.sinAsignar
            }
          ]
        }
      ).populate([
        {
          path: 'traducciones'
        },
        {
          path: 'principal',
          match: {
            fileDefault: false
          }
        },
        {
          path: 'miniatura',
          match: {
            fileDefault: false
          }
        }
      ]);

      let i = 0
      for (const media of getMedias) {
        if (moment(fechaActual) > moment(media['fechaCreacion'])) {

          //----------PRINCIPAL-----------
          if (media.principal) {
            // _________________________eliminar el archivo del Storage S3______________________________
            const checkFileS3_01 = await this.storageService.verificarArchivoStorage(media.principal['filename']);
            if (checkFileS3_01) {
              console.log("eliminar principal filename storage");
              this.storageService.eliminarArchivo(media.principal['filename']);
            }

            const checkFileS3_02 = await this.storageService.verificarArchivoStorage(media.principal['filenameStorage']);
            if (checkFileS3_02) {
              console.log("eliminar principalfilenameStorage storage");
              this.storageService.eliminarArchivo(media.principal['filenameStorage']);
            }
            await this.archivoModel.deleteOne({ _id: media.principal['_id'] });
          }

          //----------MINIATURA-----------
          if (media?.miniatura) {
            const checkFileS3_01 = await this.storageService.verificarArchivoStorage(media.miniatura['filename']);
            if (checkFileS3_01) {
              console.log("eliminar miniatura filename storage");
              this.storageService.eliminarArchivo(media.miniatura['filename']);
            }
            const checkFileS3_02 = await this.storageService.verificarArchivoStorage(media.miniatura['filenameStorage']);
            if (checkFileS3_02) {
              console.log("eliminar miniatura filenameStorage storage");
              this.storageService.eliminarArchivo(media.miniatura['filenameStorage']);
            }
            await this.archivoModel.deleteOne({ _id: media.miniatura['_id'] });
          }

          // _______________________Eliminar Media____________________
          if (media.principal) {
            if (media.traducciones.length > 0) {
              for (const traduccion of media.traducciones) {
                await this.traduccionMediaModel.deleteOne({ _id: traduccion['_id'] });
              }
            }
            await this.mediaModel.deleteOne({ _id: media._id });
            i++;

            // if (i === 5) break;
          }
        }
      }

      console.log("Total eliminados: " + i);
      console.log("FIN");


      //Confirma los cambios de la transaccion
      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();

    } catch (error) {
      //Confirma los cambios de la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }


  // limpia los archivos de los documentos del usuario (cedula o pasaporte) (Archivo: estado sinAsignar) 
  //@Cron(`30000 * * * * *`)
  @Cron(CronExpression.EVERY_1ST_DAY_OF_MONTH_AT_MIDNIGHT)
  async cronLimpiarArchivoSinAsignar() {

    const session = await mongoose.startSession()
    await session.startTransaction();

    try {

      const opts = { session };

      const configEvento = await this.configEventoModel.findOne({
        
      });

      //Obtiene fecha actual
      const formatFechaActual = moment().format('YYYY-MM-DD');
      const fechaActual = new Date(formatFechaActual);

      const fechaAyer = new Date(formatFechaActual);
      fechaAyer.setDate(fechaActual.getDate() - 1);
      console.log("fechaAyer", fechaAyer);
      console.log("fechaActual", fechaActual);

      // entidad
      const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(nombreEntidades.archivo)
      const codEntidad = entidad['codigo'];
      //Obtiene el estado
      const estado = await this.catalogoEstadoService.obtenerNombreEstado(nombrecatalogoEstados.sinAsignar, codEntidad)
      const codEstado = estado['codigo'];


      const getArchivos = await this.archivoModel.find(
        {
          $or: [
            {
              estado: codEstado
            }
          ]
        }
      );

      let i = 0
      for (const archivo of getArchivos) {
        if (moment(fechaActual) > moment(archivo['fechaCreacion'])) {

          // _________________________eliminar el archivo del Storage S3______________________________
          const checkFileS3 = await this.storageDocumentosUsuarioService.verificarArchivoStorage(archivo.filenameStorage);
          if (checkFileS3) {
            this.storageDocumentosUsuarioService.eliminarArchivo(archivo.filename);
            await this.archivoModel.deleteOne({ _id: archivo._id });
            i++;
          }

        }
      }

      console.log("Total eliminados documentos usuario: " + i);
      console.log("FIN");


      //Confirma los cambios de la transaccion
      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();

    } catch (error) {
      //Confirma los cambios de la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }

}
