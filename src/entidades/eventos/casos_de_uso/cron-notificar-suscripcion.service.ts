import { Evento } from '../../../drivers/mongoose/interfaces/evento/evento.interface';
import { ConfiguracionEvento } from 'src/drivers/mongoose/interfaces/configuracion_evento/configuracion-evento.interface';

import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import * as moment from 'moment';
import * as mongoose from 'mongoose';
import { Cron, CronExpression } from '@nestjs/schedule';
import { Archivo } from '../../../drivers/mongoose/interfaces/archivo/archivo.interface';
import { TraduccionMedia } from '../../../drivers/mongoose/interfaces/traduccion_media/traduccion-media.interface';
import { Media } from '../../../drivers/mongoose/interfaces/media/media.interface';
import { StorageService } from '../../../drivers/amazon_s3/services/storage.service';
import { codigosEstadosMedia } from '../../../shared/enum-sistema';
import { Suscripcion } from '../../../drivers/mongoose/interfaces/suscripcion/suscripcion.interface';
import { CatalogoIdiomasService } from '../../catalogos/casos_de_uso/catalogo-idiomas.service';
import { CrearEmailEstadoSuscripcionService } from '../../emails/casos_de_uso/crear-email-estado-suscripcion.service';


@Injectable()
export class CronNotificarSuscripcionService {
  intervalo;
  configEvento;

  constructor(
    @Inject('EVENTO_MODEL') private readonly eventoModel: Model<Evento>,
    @Inject('CONFIGURACION_EVENTO_MODEL') private readonly configEventoModel: Model<ConfiguracionEvento>,
    @Inject('SUSCRIPCION_MODEL') private readonly suscripcionModel: Model<Suscripcion>,
    private catalogoIdiomas: CatalogoIdiomasService,
    private crearEmailEstadoSuscripcionService: CrearEmailEstadoSuscripcionService
  ) {
    this.configEvento = this.configEventoModel.findOne({
      //codigo: codigosConfiguracionEvento.eventoBloquearResponsableEmail,
    });
  }
  //@Cron(`30000 * * * * *`) // medio minuto prueba

  // todos los domingos a las 12 de la noche: elimina las medias en estado "sin asignar" de un dia anterior al Domingo
  //@Cron(`0 0 * * 0`)

  //Cada primer dia del mes a la media noche
  @Cron(CronExpression.EVERY_DAY_AT_2PM)
  async cronNotificarEstadoSuscripcion() {

    const session = await mongoose.startSession();
    await session.startTransaction();

    try {

      const opts = { session };

      // const configEvento = await this.configEventoModel.findOne({
      //   //codigo: codigosConfiguracionEvento.eventoBloquearResponsableEmail,
      // });

      //Obtiene fecha actual
      const formatFechaActual = moment().format('YYYY-MM-DD');
      const fechaActual = new Date(formatFechaActual);

      const fechaTerminaSuscripcion = new Date(formatFechaActual);
      fechaTerminaSuscripcion.setDate(fechaActual.getDate() - 15);

      // filtrar las suscripciones que no hayan renovado y que esten a 15 dias de vencer
      const suscripciones: any = await this.suscripcionModel.find({
        $and: [
          {
            fechaFinalizacion: {
              $gte: new Date(
                new Date(fechaTerminaSuscripcion).setUTCHours(0, 0, 0, 0),
              ),
              $lt: new Date(
                new Date(fechaTerminaSuscripcion).setUTCHours(23, 59, 59, 999),
              ),
            },
          },
          {
            referencia: { '$exists': false }
          }
        ],
      }).populate(
        {
          path: 'usuario',
          select: 'email idioma estado',
          populate: {
            path: 'perfiles',
            select: 'nombre'
          }
        }
      );

      console.log("suscripciones", suscripciones);

      if (suscripciones.length > 0 ) {
        for (const suscripcion of suscripciones) {
          const usuario = suscripcion.usuario
          const idiomaUsuario = await this.catalogoIdiomas.obtenerIdiomaByCodigo(usuario.idioma);
          const dataEmail = {
            idUsuario: usuario._id,
            nombre: usuario.perfiles[0].nombre,
            emailDestinatario: usuario.email,
            idioma: idiomaUsuario.codNombre
          }
          console.log("emailSuscripcion", dataEmail);
          // enviar email
          await this.crearEmailEstadoSuscripcionService.emailNotificarEstadoSuscripcion(dataEmail, opts);
        }
      }


      //Confirma los cambios de la transaccion
      await session.commitTransaction();
      //Finaliza la transaccion
      await session.endSession();

    } catch (error) {
      //Confirma los cambios de la transaccion
      await session.abortTransaction();
      //Finaliza la transaccion
      await session.endSession();
      throw error;
    }
  }

}
