import { Module } from '@nestjs/common';
import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller';
import { Funcion } from 'src/shared/funcion';
import { EventosServicesModule } from '../casos_de_uso/eventos-services.module';


@Module({
  imports: [EventosServicesModule],
  providers: [TraduccionEstaticaController, Funcion],
  exports: [],
  controllers: [
  ],
})
export class EventosControllerModule { }
