import { ConfiguracionEventoModelo } from 'src/drivers/mongoose/modelos/configuracion_evento/configuracion-evento.schema';
import { EventoModelo } from './../../../drivers/mongoose/modelos/evento/evento.schema';
import { entidadesModelo } from './../../../drivers/mongoose/modelos/catalogoEntidades/catalogoEntidadesModelo';
import { Connection } from 'mongoose';
import { TransaccionModelo } from 'src/drivers/mongoose/modelos/transaccion/transaccion.schema';
import { ProyectoModelo } from 'src/drivers/mongoose/modelos/proyectos/proyecto.schema';
import { MediaModelo } from '../../../drivers/mongoose/modelos/media/mediaModelo';
import { TraduccionMediaModelo } from '../../../drivers/mongoose/modelos/traducion_media/traduccion-media.schema';
import { archivoModelo } from '../../../drivers/mongoose/modelos/archivo/archivo.schema';
import { SuscripcionModelo } from '../../../drivers/mongoose/modelos/suscripcion/suscripcion.schema';


export const eventosProviders = [
  {
    provide: 'TRANSACCION_MODEL',
    useFactory: (connection: Connection) => connection.model('transaccion', TransaccionModelo, 'transaccion'),
    inject: ['DB_CONNECTION']
  },
  {
    provide: 'ESTADOS_MODEL',
    useFactory: (connection: Connection) => connection.model('catalogo_estados', entidadesModelo, 'catalogo_estados'),
    inject: ['DB_CONNECTION']
  },
  {
    provide: 'EVENTO_MODEL',
    useFactory: (connection: Connection) => connection.model('evento', EventoModelo, 'evento'),
    inject: ['DB_CONNECTION']
  },
  {
    provide: 'CONFIGURACION_EVENTO_MODEL',
    useFactory: (connection: Connection) => connection.model('configuracion_evento', ConfiguracionEventoModelo, 'configuracion_evento'),
    inject: ['DB_CONNECTION']
  },
  {
    provide: 'MEDIA_MODEL',
    useFactory: (connection: Connection) => connection.model('media', MediaModelo, 'media'),
    inject: ['DB_CONNECTION']
  },
  {
    provide: 'TRADUCCION_MEDIA_MODEL',
    useFactory: (connection: Connection) => connection.model('traduccion_media', TraduccionMediaModelo, 'traduccion_media'),
    inject: ['DB_CONNECTION']
  },
  {
    provide: 'ARCHIVO_MODEL',
    useFactory: (connection: Connection) => connection.model('archivo', archivoModelo, 'archivo'),
    inject: ['DB_CONNECTION']
  },
  {
    provide: 'SUSCRIPCION_MODEL',
    useFactory: (connection: Connection) => connection.model('suscripcion', SuscripcionModelo, 'suscripcion'),
    inject: ['DB_CONNECTION']
  },
];
