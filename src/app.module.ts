import { ConfigModule } from './config/config.module';
import { APP_FILTER, APP_GUARD, APP_INTERCEPTOR, APP_PIPE } from '@nestjs/core';
import { AuthModule } from './auth/auth.module';
import { HttpErrorFilter } from './shared/filters/http-error.filter';
import { LoggingInterceptor } from './shared/logging.interceptor';
import { Module, ValidationPipe, MiddlewareConsumer, RequestMethod, forwardRef } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsuarioModule } from './entidades/usuario/usuario.module';
import { DBModule } from './drivers/db_conection/db.module';
import { StorageModule } from './drivers/amazon_s3/storage.module';
import { I18nModule, I18nJsonParser } from 'nestjs-i18n';
import * as path from 'path';

import { AuthMiddleware } from './middleware/auth.middleware';

import { MongooseModule } from '@nestjs/mongoose'
import { MultiIdiomaModule } from './multiIdioma/multiIdioma.module';
import { CatalogosModule } from './entidades/catalogos/catalogos.module';
import { HistoricoModule } from './entidades/historico/historico.module';
import { LibEmailModule } from './drivers/lib_email/lib-email.module';
import { ServicioEmailModule } from './entidades/servicio_email/servicio-email.module';

import { ConfigService } from './config/config.service';
import { EmailModule } from './entidades/emails/email.module';
import { EventosModule } from './entidades/eventos/eventos.module';
import { LoginModule } from './entidades/login/login.module';
import { DispositivosModule } from './entidades/dispositivos/dispositivos.module';
import { ProyectosModule } from './entidades/proyectos/proyectos.module';
import { RolesModule } from './entidades/roles/roles.module';
import { PagosModule } from './entidades/pagos/pagos.module';
import { StripeModule } from './drivers/stripe/stripe.module';
import { MonedaModule } from './entidades/moneda/moneda.module';
import { LogSistemaModule } from './entidades/log_sistema/log-sistema.module';
import { TransaccionModule } from './entidades/transaccion/transaccion.module';
import { CoinpaymentezModule } from './drivers/coinpaymentez/coinpaymentez.module';

@Module({
  imports: [
    ConfigModule,
    MultiIdiomaModule,
    UsuarioModule,
    DBModule,
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        uri: configService.get<string>('MONGO_URI'),
      }),
      inject: [ConfigService],

    }),
    I18nModule.forRoot({
      fallbackLanguage: 'en',
      fallbacks: {
        'fr': 'fr',
        'es': 'es',
        'pt': 'pt',
        'it': 'it',
        'de': 'de',
      },
      parser: I18nJsonParser,
      parserOptions: {
        path: path.join(__dirname, '/i18n/'),
      },
    }),
    StorageModule,
    AuthModule,
    CatalogosModule,
    HistoricoModule,
    LibEmailModule,
    ServicioEmailModule,
    EmailModule,
    EventosModule,
    LoginModule,
    DispositivosModule,
    ProyectosModule,
    RolesModule,
    PagosModule,
    StripeModule,
    MonedaModule,
    LogSistemaModule,
    TransaccionModule,
    CoinpaymentezModule
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_FILTER,
      useClass: HttpErrorFilter
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: LoggingInterceptor
    },
    {
      provide: APP_PIPE,
      useClass: ValidationPipe,
    }
  ],
  exports: [
    AppService,
  ],
})


export class AppModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(AuthMiddleware).exclude(
      'api/email/confirmar-email(.*)',
      'api/email/nuevo-email(.*)',
      'api/email/crear-email(.*)',
      'api/email/confirmar-peticion(.*)',
      'api/transaccion(.*)',
      'api/actualizar-contrasenia(.*)',
      'api/informacion-usuario(.*)',
      'api/email/confirmar-cambio-email(.*)',
      'api/usuario/actualizar-email-responsable',
      'api/email/confirmar-rechazo-cuenta-responsable(.*)',
      'api/email/confirmar-actualizacion-data-email(.*)',
      'api/email/reenvio-confirmacion-actualizacion-email/(.*)',

    ).forRoutes('');
  }
}