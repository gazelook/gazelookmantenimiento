import { HttpStatus, Controller} from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import { RespuestaInterface } from 'src/shared/respuesta-interface';
import { idiomas } from '../../shared/enum-sistema';

@Controller('api/traduccionEstatica')
export class TraduccionEstaticaController {
    constructor(
                private readonly i18n: I18nService
        ){}


    public async traduccionEstatica(idioma:string, claveTexto:string){
      
      const respuesta = new RespuestaInterface;
        try {
          let textoTraducido:any

          if(!idioma){
            idioma=idiomas.ingles;
               textoTraducido =await this.i18n.translate(idioma.concat('.').concat(claveTexto), {
              lang: idioma
            });
          }else{
              textoTraducido =await this.i18n.translate(idioma.concat('.').concat(claveTexto), {
              lang: idioma
            });
          }
          
          return textoTraducido
            
        } catch (e) {
            respuesta.codigoEstado = HttpStatus.INTERNAL_SERVER_ERROR;
            respuesta.respuesta = {
                mensaje: e.message
            }
            return respuesta;
        }
    }
}

