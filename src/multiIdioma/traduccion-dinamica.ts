import { v4 as uuidv4 } from 'uuid';
const rp = require('request-promise');

  //clave del api de traduccion, acceso a la API de Cognitive Services
  let clave_translator = 'TRANSLATOR_TEXT_SUBSCRIPTION_KEY';
 
  //si no encuentra una variable de entorno devuelve error
  if (!process.env[clave_translator]) {
    throw new Error('Por favor, configure una variable  de entorno: ' + clave_translator);
  }

  let subscriptionKey = process.env[clave_translator];

  //url del api de traduccion, acceso a la API de Cognitive Services
  let endpoint_translator = 'TRANSLATOR_TEXT_ENDPOINT';

  if (!process.env[endpoint_translator]) {
    throw new Error('Por favor, configure una variable de entorno: ' + endpoint_translator);
  }

  let endpoint = process.env[endpoint_translator];


export async function traducirTexto(idioma:String, texto:String) : Promise<any>{

    //Configuracion del api de traduccion
    let options = {
      method: 'POST',
      baseUrl: endpoint,
      url: 'translate',
      qs: {
        'api-version': '3.0',
        'to': [idioma]
      },
      headers: {
        'Ocp-Apim-Subscription-Key': subscriptionKey,
        'Content-type': 'application/json',
        'X-ClientTraceId': uuidv4().toString(),
        // "Ocp-Apim-Subscription-Region": 'westeurope'
      },
      body: [{
        'text': texto
      }],
      json: true,
    }
    let idiomaOriginal: boolean;
      //retorna solo el texto traducido segun el idioma que recibe la funcion
      
      return rp(options).then(body => {

        //Codigo de idioma que detecta el api de traduccion
          let idiomaDetectado=JSON.parse(JSON.stringify(body[0]['detectedLanguage']['language'], null, 4));
        
          //Codigo de idioma que se envia
          let idiomaEnviado=JSON.parse(JSON.stringify(body[0]['translations'][0]['to'], null, 4));
          
          //Verifica si el idioma enviado es igual al idioma detectado para saber si es el idioma original o no
          if (idiomaDetectado==idiomaEnviado){
            idiomaOriginal= true
          }else{
            idiomaOriginal= false
          }
          
          //obtiene la traduccion del texto
          let textoTraducido = JSON.parse(JSON.stringify(body[0]['translations'][0]['text'], null, 4));
          
          //Objeto que se retornara
          let newTraduccion = {
            textoTraducido : textoTraducido,
            original: idiomaOriginal,
          }

          return newTraduccion;
      });
  };