import { Module } from '@nestjs/common';
import { firebaseProvider } from './drivers/firebase.provider';
import { FirebaseMessageService } from './services/firebase-mensajes.service';
import { FirebaseProyectosService } from './services/firebase-proyectos.service';

@Module({
    providers: [
        ...firebaseProvider,
        FirebaseProyectosService,
        FirebaseMessageService,
    ],
    exports: [
        FirebaseProyectosService,
        FirebaseMessageService,
    ]
})
export class FirebaseModule {}
