import { Inject, Injectable } from '@nestjs/common';
import * as admin from 'firebase-admin';
import { codigoEstadosComentario } from '../../../shared/enum-sistema';


@Injectable()
export class FirebaseProyectosService {

    fireDatabase: admin.database.Database;

    constructor(@Inject('FIREBASE_PROVIDER') private readonly firebase) {

        this.fireDatabase = admin.database();
    }


    // Obtener comentarios del proyecto
    async obtenerComentariosProyecto(idProyecto: string) {

        let listComentarios = [];

        await this.fireDatabase.ref(`comentarios/${idProyecto}`).once('value', function (snapshot) {
            snapshot.forEach(
                function (ChildSnapshot) {
                    listComentarios = [...listComentarios, ChildSnapshot.val()];
                }
            )
        });

        return listComentarios;
        //codigoEstadosComentario.activa
    }




}