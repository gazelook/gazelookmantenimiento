import { Inject, Injectable } from '@nestjs/common';
import * as admin from 'firebase-admin';


@Injectable()
export class FirebaseMessageService {

    fireDatabase: admin.database.Database;

    constructor(@Inject('FIREBASE_PROVIDER') private readonly firebase) {

        this.fireDatabase = admin.database();
    }


    // Obtener comentarios del proyecto
    async obtenerMensajesAsociacion(idAsociacion: string) {

        let listMensajes = [];

        await this.fireDatabase.ref(`mensajes/${idAsociacion}`).once('value', function (snapshot) {
            snapshot.forEach(
                function (ChildSnapshot) {
                    listMensajes = [...listMensajes, ChildSnapshot.val()];
                }
            )
        });

        return listMensajes;
        //codigoEstadosComentario.activa
    }




}