import { ServiceAccount } from "firebase-admin";
import { ConfigService } from "../../../config/config.service";
import * as admin from 'firebase-admin';
import { ConfigModule } from 'src/config/config.module';

export const firebaseProvider = [
    {
        provide: 'FIREBASE_PROVIDER',
        imports: [ConfigModule],
        inject: [ConfigService],
        useFactory: async (config: ConfigService): Promise<any> => {
            const adminConfig: ServiceAccount = {
                projectId: config.get<string>('FIREBASE_PROJECT_ID'),
                privateKey: config.get<string>('FIREBASE_PRIVATE_KEY').replace(/\\n/g, '\n'),
                clientEmail: config.get<string>('FIREBASE_CLIENT_EMAIL'),
            };
            // Initialize the firebase admin app
            await admin.initializeApp({
                credential: admin.credential.cert(adminConfig),
                databaseURL: config.get<string>('DATABASE_URL'),
            });
            return admin;
        }
    }

]