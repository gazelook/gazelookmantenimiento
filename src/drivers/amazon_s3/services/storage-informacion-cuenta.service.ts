import { S3 } from 'aws-sdk';
import { Injectable } from '@nestjs/common';
import { v4 as uuidv4 } from 'uuid';
import { extname } from 'path';
import { ConfigService } from '../../../config/config.service';
import { ArchivoInformacionCuenta, StorageArchivo } from '../models/storage-archivo';

@Injectable()
export class StorageInformacionCuentaService {
    private FOLDER;
    private s3: S3;
    private URL_BASE;
    private PATH_BASE;
    private BUCKET;
    private ACCESS_KEY_ID: string;
    private SECRET_ACCESS_KEY: string;
    private URL_S3_INFORMACION_CUENTA: string;


    constructor(private config: ConfigService) {
        this.ACCESS_KEY_ID = this.config.get<string>('S3_ACCESS_KEY');
        this.SECRET_ACCESS_KEY = this.config.get<string>('S3_SECTRET_ACCESS_KEY');

        this.URL_S3_INFORMACION_CUENTA = this.config.get<string>('URL_S3_INFORMACION_CUENTA');
        this.BUCKET = this.config.get<string>('S3_BUCKET_INFORMACION_CUENTA');
        this.FOLDER = this.config.get<string>('S3_FOLDER_DATOS_CUENTA');


        this.s3 = new S3({
            accessKeyId: this.ACCESS_KEY_ID,
            secretAccessKey: this.SECRET_ACCESS_KEY
        })

        this.URL_BASE = `${this.URL_S3_INFORMACION_CUENTA}/${this.FOLDER}`;
        this.PATH_BASE = `s3://${this.BUCKET}/${this.FOLDER}`;
    }

    async cargarArchivo(archivoInformacion: ArchivoInformacionCuenta): Promise<StorageArchivo> {
        const filename = `${archivoInformacion.userEmail}-${uuidv4().toString()}${extname(archivoInformacion.originalname)}`;
        const bucket = {
            Bucket: `${this.BUCKET}/${this.FOLDER}`,
            Key: filename,
            Body: archivoInformacion.buffer
        }

        try {
            const result = await this.s3.putObject(bucket).promise();

            const data: StorageArchivo = {
                url: `${this.URL_BASE}/${filename}`,
                etag: result.ETag,
                filename: filename,
                path: `${this.PATH_BASE}/${filename}`,
                size: archivoInformacion.size,
            }

            return data;

        } catch (err) {
            throw err;
        }
    }

    async getArchivo(filename: string): Promise<S3.GetObjectOutput> {
        const bucket = {
            Bucket: `${this.BUCKET}/${this.FOLDER}`,
            Key: `${filename}`
        }

        try {
            const file = await this.s3
                .getObject(bucket)
                .promise();

            return file;

        } catch (err) {
            throw err;
        }
    }

    async eliminarArchivo(filename: string) {
        const bucket = {
            Bucket: `${this.BUCKET}/${this.FOLDER}`,
            Key: filename
        }
        try {
            const result = await this.s3.deleteObject(bucket).promise();
            return result;
        } catch (err) {
            throw err;
        }
    }

    async verificarArchivoStorage(filename: string) {
        const bucket = {
            Bucket: `${this.BUCKET}/${this.FOLDER}`,
            Key: filename
        }

        try {
            const result = await this.s3.headObject(bucket).promise();
            return result;
        } catch (err) {
            //throw err;
            console.log("Error, el archivo no existe en el Storage: ", err)
            return false;
        }
    }
}