import { Module } from '@nestjs/common';
import { StorageDocumentosUsuarioService } from './services/storage-documentos-usuario.service';
import { StorageInformacionCuentaService } from './services/storage-informacion-cuenta.service';
import { StorageService } from './services/storage.service';

@Module({
    providers: [
        StorageService,
        StorageInformacionCuentaService,
        StorageDocumentosUsuarioService
    ],
    exports: [
        StorageService,
        StorageInformacionCuentaService,
        StorageDocumentosUsuarioService
    ]
})
export class StorageModule { }
