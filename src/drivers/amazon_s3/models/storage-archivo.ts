export class StorageArchivo {
    url: string;
    etag: string;
    filename: string;
    path: string;
    size: number;
}

export class ArchivoInformacionCuenta {
    buffer: Buffer;
    size: number;
    originalname: string;
    userEmail: string;
}