import { ApiProperty } from "@nestjs/swagger";

export class ConverCoinpaymentsDto {
  @ApiProperty({type:Number})
  amount : number;

  @ApiProperty({type:String})
  from: string;

  @ApiProperty({type:String})
  to: string;

  @ApiProperty({type:String})
  address: string;

  @ApiProperty({type:String})
  dest_tag?: string;
  
  
}
