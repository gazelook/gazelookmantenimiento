import { Injectable } from '@nestjs/common';
import Coinpayments from 'coinpayments';
import { CoinpaymentsCredentials } from 'coinpayments/dist/types/base';
import { CoinpaymentsCreateTransactionOpts } from 'coinpayments/dist/types/options';
import { ConfigService } from '../../../config/config.service';


@Injectable()
export class CoinpaymentezTransactionService {
  private coinpayments: Coinpayments;
  private coinpaymentsCredentials: CoinpaymentsCredentials;
  private COINPAYMENTEZ_KEY: string;
  private COINPAYMENTEZ_SECRET: string;

  constructor(private config: ConfigService
  ) {
    this.COINPAYMENTEZ_KEY = this.config.get<string>('COINPAYMENTEZ_KEY');
    this.COINPAYMENTEZ_SECRET = this.config.get<string>('COINPAYMENTEZ_SECRET');
    this.coinpaymentsCredentials = {
      key: this.COINPAYMENTEZ_KEY,
      secret: this.COINPAYMENTEZ_SECRET
    }
    this.coinpayments = new Coinpayments(this.coinpaymentsCredentials);

  }

  //metodo que crea una transaccion en Coinpaymentez
  async createTransaction(data: CoinpaymentsCreateTransactionOpts): Promise<any> {
    try {

      let transaction: CoinpaymentsCreateTransactionOpts = {
        currency1: data.currency1,
        currency2: data.currency2,
        amount: data.amount,
        buyer_email: data.buyer_email,
        // address: data.address,
        buyer_name: data.buyer_name,
        // item_name: data.item_name,
        // item_number: data.item_number,
        // invoice: data.invoice,
        // custom?: string,
        // ipn_url?: string,
        // success_url?: string,
        // cancel_url?: string
      }

      console.log('antessssssssssssssssssssssssssss')
      const coinpaymentezTransaction = await this.coinpayments.createTransaction(
        transaction
      );
      console.log('coinpaymentezTransaction: ', coinpaymentezTransaction)
      return coinpaymentezTransaction;

    } catch (error) {
      throw error;
    }
  }

}
