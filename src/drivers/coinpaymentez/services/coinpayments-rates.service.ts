import { Injectable } from '@nestjs/common';
import Coinpayments from 'coinpayments';
import { CoinpaymentsCredentials } from 'coinpayments/dist/types/base';
import { CoinpaymentsRatesOpts } from 'coinpayments/dist/types/options';
import { ConfigService } from '../../../config/config.service';


@Injectable()
export class CoinpaymentezRatesService {
  private coinpayments: Coinpayments;
  private coinpaymentsCredentials: CoinpaymentsCredentials;
  private COINPAYMENTEZ_KEY: string;
  private COINPAYMENTEZ_SECRET: string;

  constructor(private config: ConfigService
  ) {
    this.COINPAYMENTEZ_KEY = this.config.get<string>('COINPAYMENTEZ_KEY');
    this.COINPAYMENTEZ_SECRET = this.config.get<string>('COINPAYMENTEZ_SECRET');
    this.coinpaymentsCredentials = {
      key: this.COINPAYMENTEZ_KEY,
      secret: this.COINPAYMENTEZ_SECRET
    }
    this.coinpayments = new Coinpayments(this.coinpaymentsCredentials);

  }



  async obtenerRates(): Promise<any> {
    try {

      let coinpaymentsRatesOpts: CoinpaymentsRatesOpts = {
        accepted: 2
      }

      const coinpaymentezRates = await this.coinpayments.rates(
        coinpaymentsRatesOpts
      );

      return coinpaymentezRates;

    } catch (error) {
      throw error;
    }
  }

}
