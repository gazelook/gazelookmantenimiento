import { Injectable } from '@nestjs/common';
import Coinpayments from 'coinpayments';
import { CoinpaymentsCredentials } from 'coinpayments/dist/types/base';
import { CoinpaymentsConvertCoinsOpts, CoinpaymentsCreateTransactionOpts, CoinpaymentsGetTxOpts } from 'coinpayments/dist/types/options';
import { ConfigService } from '../../../config/config.service';
import { ConverCoinpaymentsDto } from '../dtos/convert-coinpayments.dto';


@Injectable()
export class ConverCoinpaymentezService {
  private coinpayments: Coinpayments;
  private coinpaymentsCredentials: CoinpaymentsCredentials;
  private COINPAYMENTEZ_KEY: string;
  private COINPAYMENTEZ_SECRET: string;

  constructor(private config: ConfigService
  ) {
    this.COINPAYMENTEZ_KEY = this.config.get<string>('COINPAYMENTEZ_KEY');
    this.COINPAYMENTEZ_SECRET = this.config.get<string>('COINPAYMENTEZ_SECRET');
    this.coinpaymentsCredentials = {
      key: this.COINPAYMENTEZ_KEY,
      secret: this.COINPAYMENTEZ_SECRET
    }
    this.coinpayments = new Coinpayments(this.coinpaymentsCredentials);

  }

  //metodo que crea una transaccion en Coinpaymentez
  async createTransaction(data: ConverCoinpaymentsDto): Promise<any> {
    try {

      let conver: CoinpaymentsConvertCoinsOpts = {
        amount: data.amount,
        from: data.from,
        to: data.to,
        address: data.address,
        dest_tag: data.dest_tag,
      }


      const converCoinpaymentez= await this.coinpayments.convertCoins(
        conver
      );
      console.log('converCoinpaymentez: ', converCoinpaymentez)
      let transaction: CoinpaymentsGetTxOpts = {
        txid: converCoinpaymentez.id,
        full: 2
      }

      const getTransaction = await this.coinpayments.getTx(
        transaction
      );
      console.log('getTransaction: ', getTransaction)

      return getTransaction;

    } catch (error) {
      throw error;
    }
  }

}
