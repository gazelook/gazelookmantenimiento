import { Body, Controller, Get, Module, Param, Post } from '@nestjs/common';
import { CoinpaymentezRatesService } from '../services/coinpayments-rates.service';

@Controller('coinpayments')
export class CoinPaymentsRatesController {
  constructor(
    private readonly coinpaymentezRatesService: CoinpaymentezRatesService
    ) {}

  @Get('/rates')
  async obtenerMonedasAdmitidas(
  ): Promise<any> {
    return this.coinpaymentezRatesService.obtenerRates();
  }
}

@Module({
  controllers: [CoinPaymentsRatesController],
  providers: [CoinpaymentezRatesService],
})
export class CoinpaymentsRatesModule {}
