import { Body, Controller, HttpStatus, Module, Post } from '@nestjs/common';
import { Funcion } from 'src/shared/funcion';
import { ConverCoinpaymentsDto } from '../dtos/convert-coinpayments.dto';
import { ConverCoinpaymentezService } from '../services/conver-coinpayments.service';

@Controller('coinpayments')
export class ConverCoinPaymentsController {
  constructor(
    private readonly converCoinpaymentezService: ConverCoinpaymentezService,
    private readonly funcion: Funcion,
    ) {}

  @Post('/conver')
  async convertirMonedas(
    @Body() transactionDto: ConverCoinpaymentsDto,
  ): Promise<any> {

    try {
      const transaccion = await this.converCoinpaymentezService.createTransaction(transactionDto)
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.OK,
        datos: transaccion,
      });
      // return transaccion
    } catch (error) {
      throw error
      
    }
    
  }
}

@Module({
  controllers: [ConverCoinPaymentsController],
  providers: [ConverCoinpaymentezService, Funcion],
})
export class CoinpaymentsTransactionModule {}
