import { Body, Controller, Get, HttpStatus, Module, Post } from '@nestjs/common';
import { Funcion } from 'src/shared/funcion';
import { CoinpaymentsDetalleTransactionDto } from '../dtos/coinpayments-detalle-transaction.dto';
import { CoinpaymentezInfoTransactionService } from '../services/coinpayments-info-transaction.service';

@Controller('coinpayments')
export class CoinPaymentsDetalleTransactionController {
  constructor(
    private readonly coinpaymentezInfoTransactionService: CoinpaymentezInfoTransactionService,
    private readonly funcion: Funcion,
  ) { }

  @Get('/detalle-transaction')
  async obtenerMonedasAdmitidas(
    @Body() transactionDto: CoinpaymentsDetalleTransactionDto,
  ): Promise<any> {

    try {
      const transaccion = await this.coinpaymentezInfoTransactionService.obtenerTransaction(transactionDto)
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.OK,
        datos: transaccion,
      });
      // return transaccion
    } catch (error) {
      throw error

    }

  }
}

@Module({
  controllers: [CoinPaymentsDetalleTransactionController],
  providers: [CoinpaymentezInfoTransactionService, Funcion],
})
export class CoinpaymentsTransactionModule { }
