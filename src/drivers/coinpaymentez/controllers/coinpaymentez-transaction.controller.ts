import { Body, Controller, HttpStatus, Module, Post } from '@nestjs/common';
import { Funcion } from 'src/shared/funcion';
import { CoinpaymentsTransactionDto } from '../dtos/coinpayments-transaction.dto';
import { CoinpaymentezTransactionService } from '../services/coinpayments-transaction.service';

@Controller('coinpayments')
export class CoinPaymentsTransactionController {
  constructor(
    private readonly coinpaymentezTransactionService: CoinpaymentezTransactionService,
    private readonly funcion: Funcion,
    ) {}

  @Post('/transaction')
  async obtenerMonedasAdmitidas(
    @Body() transactionDto: CoinpaymentsTransactionDto,
  ): Promise<any> {

    try {
      const transaccion = await this.coinpaymentezTransactionService.createTransaction(transactionDto)
      return this.funcion.enviarRespuestaOptimizada({
        codigoEstado: HttpStatus.OK,
        datos: transaccion,
      });
      // return transaccion
    } catch (error) {
      throw error
      
    }
    
  }
}

@Module({
  controllers: [CoinPaymentsTransactionController],
  providers: [CoinpaymentezTransactionService, Funcion],
})
export class CoinpaymentsTransactionModule {}
