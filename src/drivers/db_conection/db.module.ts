import { Module } from '@nestjs/common';
import { dbProviders } from './db.providers';
import { HttpErrorFilter } from 'src/shared/filters/http-error.filter';
import { LoggingInterceptor } from 'src/shared/logging.interceptor';
import { APP_INTERCEPTOR, APP_FILTER } from '@nestjs/core';

@Module({
  providers: [
    ...dbProviders,
    {
      provide: APP_FILTER,
      useClass: HttpErrorFilter
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: LoggingInterceptor
    }
  ],
  exports: [...dbProviders],
  // providers: [...dbSsh],
  // exports: [...dbSsh],
})
export class DBModule {}



// db.createUser({
//   user: 'gazelookDB',
//   pwd: 'Digital2020ServerGaze',
//   roles: [{ role: 'dbAdmin', db:'pruebaGaze'}]
// })
// db.createUser({
//   user: 'gazelookDB',
//   pwd: 'Digital2020ServerGaze',
//   roles: [{ role: 'readWrite', db:'gazeLook_dev'}]
// })
// mongo -u gazelookDB -p Digital2020ServerGaze pruebaGaze
// mongo -u ian -p secretPassword 123.45.67.89/cool_db

// db.createUser(
//   {
//     user: "adminAazelookDB",
//     pwd: "adminAazelookDB'Digital2020ServerGaze",
//     roles: [ { role: "userAdminAnyDatabase", db: "admin" } ]
//   }
// )

// db.createUser(
//   {
//     user: " adminAazelookDB ",
//     pwd: "Digital2020ServerGaze",
//     roles: [ { role: "userAdminAnyDatabase", db: "admin" } ]
//   }
//)
// mongo -u adminAazelookDB -p --authenticationDatabase admin

// db.createUser({user: "gazelook_dev_user", pwd:"gazelookdevpass_2123", roles:[ { role: "dbAdmin", db: "pruebaGaze" } ]})