import { Controller, Module, Post, Body } from '@nestjs/common';
import { PaymentMethodsService } from '../services/payment-methods.service';
import { Stripe } from '../models/stripe.iterface';
import { PaymentMethodsDto } from '../dtos/payment-methods.dto';

@Controller('payment_methods')
export class PaymentMethodsController {
  constructor(private readonly paymentMethodsService: PaymentMethodsService) {}
  @Post()
  async createPaymentMethods(
    @Body() paymentMethodsDto: PaymentMethodsDto
  ): Promise<any> {
    return this.paymentMethodsService.createPaymentMethods(paymentMethodsDto);
  }
}

@Module({
  controllers: [PaymentMethodsController],
  providers: [PaymentMethodsService],
})
export class PaymentMethodsModule {}
