export class PaymentIntentsDto {
  readonly amount: number;
  readonly userEmail: string;
  readonly idCustomer: string;
  readonly description?: string;
}
