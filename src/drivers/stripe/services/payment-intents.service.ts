import { Injectable } from '@nestjs/common';
import Stripe from 'stripe';

import { PaymentIntentsDto } from '../dtos/payment-intents.dto';
import { PaymentIntentResponse } from '../models/payment-intents.interface';
import { ConfigService } from '../../../config/config.service';
import { pagoStripe } from '../../../shared/enum-sistema';

@Injectable()
export class PaymentIntentsService {

  private readonly stripe: Stripe;
  private readonly STRIPE_KEY: string;

  constructor(private config: ConfigService) {
    this.STRIPE_KEY = this.config.get<string>('STRIPE_KEY');
    this.stripe = new Stripe(this.STRIPE_KEY, {
      apiVersion: pagoStripe.apiVersion,
    });
  }

  async createPaymentsIntent(dataPayment: PaymentIntentsDto): Promise<Stripe.PaymentIntent> {
    try {
      const paymentIntent = await this.stripe.paymentIntents.create({
        amount: dataPayment.amount,
        currency: pagoStripe.currency,
        payment_method_types: [pagoStripe.payment_method_types],
        receipt_email: dataPayment.userEmail,
        customer: dataPayment.idCustomer,
        description: dataPayment.description
        // confirmation_method: 'manual',
        // capture_method: 'manual',
        // confirm: true
      });
      return paymentIntent;
    } catch (error) {
      throw error;
    }
  }

  async obtenerEstadoPagoStripe(idPago: string): Promise<Boolean> {
    try {
      const paymentIntents = await this.stripe.paymentIntents.retrieve(idPago);


      if (paymentIntents.status === 'succeeded') {
        return true;
      }
      return false;
    } catch (error) {
      console.error('stripe:', error);
      throw { message: 'error al verificar el estado de la cuenta stripe' }
    }
  }
  async getStripeBalanceTransaction(idBalanceTransaction: string): Promise<Stripe.BalanceTransaction> {
    try {
      /* const paymentIntents = await this.stripe.paymentIntents.retrieve(idPago);
      const getBalanceTransaction = await this.stripe.balanceTransactions.retrieve(paymentIntents.charges.data[0].balance_transaction.toString()) */
      const getBalanceTransaction = await this.stripe.balanceTransactions.retrieve(idBalanceTransaction);

      return getBalanceTransaction;
    } catch (error) {
      console.error('stripe:', error);
      throw { message: 'error al verificar el estado de la cuenta stripe' }
    }
  }

  async retrievePaymentIntents(id: string): Promise<Stripe.PaymentIntent> {
    const paymentIntents = await this.stripe.paymentIntents.retrieve(id);

    return paymentIntents;
    //return this.generatePaymentResponse(paymentIntents);
  }

  async confirmPaymentsIntent(id: string): Promise<PaymentIntentResponse> {
    const paymentIntents = await this.stripe.paymentIntents.confirm(id);

    return this.generatePaymentResponse(paymentIntents);
  }

  generatePaymentResponse(intent: Stripe.PaymentIntent): PaymentIntentResponse {

    if (intent.status === 'requires_action') {
      if (intent.next_action.type === 'use_stripe_sdk') {
        return {
          payment_intent_id: intent.id,
          requires_action: true,
          payment_intent_client_secret: intent.client_secret,
        };
      }

      return {
        payment_intent_id: intent.id,
        requires_action: true,
        next_action: intent.next_action,
      };
    }

    if (intent.status === 'requires_confirmation') {
      return {
        payment_intent_id: intent.id,
        requires_action: true,
        next_action: {
          type: 'requires_confirmation',
        },
      };
    }

    if (intent.status === 'succeeded') {
      return {
        success: true,
      };
    }

    return {
      error: 'Invalid PaymentIntent status',
    };
  }
}
