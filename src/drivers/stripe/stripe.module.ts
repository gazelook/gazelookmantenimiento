import { Module } from '@nestjs/common';
import { CustomersService } from './services/payment-customers.service';
import { PaymentIntentsService } from './services/payment-intents.service';
import { PaymentMethodsService } from './services/payment-methods.service';

@Module({
    providers: [
        CustomersService,
        PaymentIntentsService,
        PaymentMethodsService,
    ],
    exports: [
        CustomersService,
        PaymentIntentsService,
        PaymentMethodsService,
    ],
    controllers: [

    ]
})
export class StripeModule { }
