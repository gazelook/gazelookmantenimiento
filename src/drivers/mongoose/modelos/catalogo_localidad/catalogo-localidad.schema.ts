import { Schema } from 'mongoose'

const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' }
}

export const CatalogoLocalidadModelo = new Schema({
    catalogoPais: {
        ref: 'catalogo_pais',
        type: Schema.Types.String
    },
    nombre: String,
    codigo: String,
    codigoPostal: String,
    estado: {
        ref: 'catalogo_estado',
        type: Schema.Types.String
    },
    /* traducciones:[{
        ref: 'traduccion_catalogo_localidad',
        type: Schema.Types.ObjectId
    }] */
}, schemaOptions);
