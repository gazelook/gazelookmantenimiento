import { Schema } from 'mongoose'

const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' }
}

export const CatalogoPaisModelo = new Schema({
    codigo: String,
    codigoNombre: String,
    estado: {
        ref: 'catalogo_estado',
        type: Schema.Types.String
    },
    codigoTelefono: [{
        type: Schema.Types.String
    }],
    traducciones:[{
        ref: 'traduccion_catalogo_pais',
        type: Schema.Types.ObjectId
    }]
}, schemaOptions);
