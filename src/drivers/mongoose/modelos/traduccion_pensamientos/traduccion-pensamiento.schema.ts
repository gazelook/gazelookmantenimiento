import {Schema} from 'mongoose'

const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
}
 
export const TraduccionPensamientoModelo = new Schema({
    pensamiento:{
        ref:'pensamiento',
        type:Schema.Types.ObjectId
    },
    texto:String,
    idioma:{
        ref:'catalogo_idiomas',
        type:Schema.Types.String
    },
    original: Boolean,
    estado:{
        ref:'catalogo_estado',
        type:Schema.Types.String
    },
}, schemaOptions);
 
// const PensamientoModelo = mongoose.model<mongoose.Document>('pensamiento', campos)
 
// export default PensamientoModelo