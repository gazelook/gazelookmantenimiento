import { Schema } from 'mongoose'

const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' }
}

export const LogSistema = new Schema({
    method: String,
    date: Date,
    status: Number,
    url: String,
    ip: String,
    browser: String,
    content: Schema.Types.Mixed,
    timeRequest: Number
}, schemaOptions);

