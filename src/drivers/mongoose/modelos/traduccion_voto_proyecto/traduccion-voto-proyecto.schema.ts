import {Schema} from 'mongoose'

const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' },
}
 
export const TraduccionVotoProyectoModelo = new Schema({

    descripcion:String,
    idioma:{
        ref:'catalogo_idiomas',
        type:Schema.Types.String
    },
    estado:{
        ref:'catalogo_estado',
        type:Schema.Types.String
    },
    original: Boolean,
    votoProyecto:{
        ref:'voto_proyecto',
        type:Schema.Types.ObjectId
    }
}, schemaOptions);