import {Schema} from 'mongoose'

const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' }
}
 
export const ConfiguracionModelo = new Schema({
    codigo: String,
    entidad:{
        ref:'catalogo_entidades',
        type:Schema.Types.String
    },
    silenciada:Boolean,
    tonoNotificacion:[{
        ref:'media',
        type:Schema.Types.ObjectId
    }],
    estilos:[{
        ref:'estilos',
        type:Schema.Types.ObjectId 
    }],
    tipo:{
        ref:'catalogo_configuracion',
        type:Schema.Types.ObjectId 
    },
    estado:{
        ref:'catalogo_estado',
        type:Schema.Types.String
    }
}, schemaOptions);