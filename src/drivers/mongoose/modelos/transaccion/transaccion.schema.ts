import { Schema } from 'mongoose'

const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' }
}

export const TransaccionModelo = new Schema({
    estado: {
        ref: 'catalogo_estados',
        type: Schema.Types.String
    },
    monto: Number,
    moneda: {
        ref: 'catalogo_tipo_moneda',
        type: Schema.Types.String
    },
    descripcion: String,
    origen: {
        ref: 'catalogo_origen',
        type: Schema.Types.String
    },
    balance: Array,
    beneficiario: {
        ref: 'beneficiario',
        type: Schema.Types.ObjectId
    },
    metodoPago: {
        ref: 'catalogo_metodo_pago',
        type: Schema.Types.String
    },
    informacionPago: {
        ref: 'informacion_pago',
        type: Schema.Types.ObjectId
    },
    usuario: {
        ref: 'usuario',
        type: Schema.Types.ObjectId
    },
    conversionTransaccion: [{
        ref: 'conversion_transaccion',
        type: Schema.Types.ObjectId
    }],
    destino: {
        ref: 'catalogo_origen',
        type: Schema.Types.String
    },
    numeroRecibo: String,
    comisionTransferencia: Number,
    totalRecibido: Number,
    origenPais: String,

}, schemaOptions);
