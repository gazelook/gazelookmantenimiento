import { Schema } from 'mongoose'

const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' }
}

export const CatalogoPorcentajeEsperaFondosModelo = new Schema({
    codigo:String,
    porcentaje:Number,
    estado:{
        ref:'catalogo_estados',
        type: Schema.Types.String
    },
}, schemaOptions);