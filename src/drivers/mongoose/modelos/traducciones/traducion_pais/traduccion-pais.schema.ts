import { Schema } from 'mongoose'

const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' }
}

export const TraduccionPaisModelo = new Schema({
    nombre: String,
    idioma: String,
    original: Boolean
}, schemaOptions);