import { Schema } from 'mongoose'

const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' }
}

export const TraduccionLocalidadModelo = new Schema({
    nombre: String,
    idioma: String,
    original: Boolean
}, schemaOptions);