import mongoose from 'mongoose'

const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' }
}
 
const CatalogoTipoMensajeModelo = new mongoose.Schema({
    codigo:String,
    nombre:String,
    descripcion: String,
    estado:{
        ref:'catalogo_estados',
        type: mongoose.Schema.Types.ObjectId
    }
}, schemaOptions);
 
