import {Schema} from 'mongoose'

const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' }
}

export const CatalogoOrigenDocumentoModelo = new Schema({
    estado:{
        ref:'catalogo_estados',
        type:Schema.Types.String
    },
    codigo:String,
    nombre:String,
    descripcion:String,
}, schemaOptions);
