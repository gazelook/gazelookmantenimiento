import {Schema} from 'mongoose'
import * as mongoosePaginate from 'mongoose-paginate-v2';

const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' }
}
 
export const ProyectoModelo = new Schema({
    perfil:{
        ref:'perfil',
        type:Schema.Types.ObjectId
    },
    tipo:{
        ref:'catalogo_tipo_proyecto',
        type:Schema.Types.String
    },
    localidad:{
        ref:'catalogo_localidad',
        type:Schema.Types.String
    },
    participantes:[{
        ref:'participante_proyecto',
        type:Schema.Types.ObjectId
    }],//lista de participantes del proyecto
    recomendadoAdmin: Boolean,
    valorEstimado: Number,
    adjuntos:[{
        ref:'album',
        type:Schema.Types.ObjectId
    }],
    medias:[{
        ref:'media',
        type:Schema.Types.ObjectId
    }],
    votos:[{
        ref: 'voto_proyecto',
        type: Schema.Types.ObjectId
    }],
    totalVotos:Number,
    traducciones: [{
        ref: 'traduccion_proyecto',
        type: Schema.Types.ObjectId
    }],
    estrategia: String,
    comentarios: [{
        ref: 'comentarios',
        type: Schema.Types.ObjectId 
    }],
    moneda:{
        ref:'catalogo_tipo_moneda',
        type:Schema.Types.String
    },
    estado:{
        ref:'catalogo_estado',
        type:Schema.Types.String
    },
}, schemaOptions)
ProyectoModelo.plugin(mongoosePaginate);