import {Schema} from 'mongoose'


const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' }
}

export const dispositivoSchema = new Schema({
    nombre:String,
    codigo:String,
    tipo:String,
    ciudad: String,
    pais: String,
    ipDispositivo: String,
    usuario:{
        ref:'usuario',
        type:Schema.Types.String
    },
    estado:{
        ref:'catalogo_estados',
        type:Schema.Types.String
    },
    localidad:{
        ref:'catalogo_localidad',
        type:Schema.Types.String
    },
    tokenUsuario: {
        ref: 'token_usuario',
        type: Schema.Types.ObjectId
    },
}, schemaOptions);



//const UsuarioModelo = mongoose.model<mongoose.Document>('usuario', campos, 'usuario')
 
//export default UsuarioModelo