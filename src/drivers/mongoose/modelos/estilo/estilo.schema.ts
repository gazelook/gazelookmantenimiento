import { Schema } from 'mongoose'

const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' }
}
export const EstiloModelo = new Schema({
    codigo: String,
    media: {
        ref: 'media',
        type: Schema.Types.ObjectId
    },
    color: {
        ref: 'catalogo_colores',
        type: Schema.Types.ObjectId
    },
    tipo: {
        ref: 'catalogo_estilos',
        type: Schema.Types.ObjectId
    },
    estado: {
        ref: 'catalogo_estado',
        type: Schema.Types.String
    },
}, schemaOptions);