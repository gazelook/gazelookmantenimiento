import { Schema } from 'mongoose'


const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' }
}

export const accionModelo = new Schema({
    codigo: String,//cod_catalogoAccion
    nombre: String,
    estado: {
        ref: 'catalogo_estados',
        type: Schema.Types.String
    }
}, schemaOptions);



//const CatalogoAccionModelo = mongoose.model<mongoose.Document>('catalogo_acciones', campos)

//export default CatalogoAccionModelo