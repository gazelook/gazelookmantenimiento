import {Schema} from 'mongoose'
import { codigosCatalogoHistorico } from 'src/shared/enum-sistema';
import * as mongoosePaginate from 'mongoose-paginate-v2';

const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion'}
}
 
export const HistoricoModelo = new Schema({
    datos: Object,//Objeto de datos
    usuario:{
        type:Schema.Types.String
    },
    accion:{
        ref:'catalogo_accion',
        type:Schema.Types.String
    },
    entidad: {
        ref:'catalogo_entidades',
        type:Schema.Types.String
    },
    tipo:{
        type:String,
        default:codigosCatalogoHistorico.default
    }
}, schemaOptions);
HistoricoModelo.plugin(mongoosePaginate);