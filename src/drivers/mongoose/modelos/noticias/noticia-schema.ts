import { Schema } from 'mongoose'
import * as mongoosePaginate from 'mongoose-paginate-v2';
 
const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' }
}

export const noticiaModelo = new Schema({
    estado: {
        ref: 'catalogo_estados',
        type: Schema.Types.String
    },
    localidad: {
        ref: 'catalogo_localidad',
        type: Schema.Types.String
    },
    autor: String,
    adjuntos:[{
        ref:'album',
        type:Schema.Types.ObjectId
    }],
    perfil: {
        ref: 'perfil',
        type: Schema.Types.ObjectId
    },
    votos: [{
        ref: 'voto_noticia',
        type: Schema.Types.ObjectId
    }],
    totalVotos: { type: Number, default: 0 },
    traducciones: [{
        ref: 'traduccion_noticia',
        type: Schema.Types.ObjectId
    }],
    medias:[{
        ref:'media',
        type:Schema.Types.ObjectId
    }],
}, schemaOptions);
noticiaModelo.plugin(mongoosePaginate);
