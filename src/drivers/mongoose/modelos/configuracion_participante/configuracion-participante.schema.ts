import {Schema} from 'mongoose'

const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' }
}
 
export const ConfiguracionParticipanteModelo = new Schema({
    propietario:{
        ref:'participante_asociacion',
        type:Schema.Types.ObjectId
    },
    asignado:{
        ref:'participante_asociacion',
        type:Schema.Types.ObjectId
    },
    configuracionEstilo:{
        ref:'configuracion_estilo',
        type:Schema.Types.ObjectId
    },
    estado:{
        ref:'catalogo_estado',
        type:Schema.Types.String
    }
}, schemaOptions);