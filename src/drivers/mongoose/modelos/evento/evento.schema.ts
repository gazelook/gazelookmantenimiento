import { Schema } from 'mongoose'


const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' }
}

export const EventoModelo = new Schema({
    fechaInicio: Date,
    fechaFin: Date,
    configuracionEvento:{
        ref: 'configuracion_evento',
        type: Schema.Types.String
    },
    proyectos: [{
        ref: 'proyecto',
        type: Schema.Types.ObjectId
    }],
    estado:{
        ref:'catalogo_estados',
        type: Schema.Types.String
    },
}, schemaOptions);