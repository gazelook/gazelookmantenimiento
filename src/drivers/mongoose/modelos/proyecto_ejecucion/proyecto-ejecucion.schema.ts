import {Schema} from 'mongoose'

const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' }
}
 
export const ProyectoEjecucionModelo = new Schema({
    proyecto:{
        ref: 'proyecto',
        type: Schema.Types.ObjectId
    },
    traducciones: [{
        ref: 'traduccion_proyecto_ejecucion',
        type: Schema.Types.ObjectId
    }],
    estado:{
        ref:'catalogo_estado',
        type:Schema.Types.String
    }
}, schemaOptions)