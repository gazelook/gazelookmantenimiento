
import {Schema} from 'mongoose';

const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' }
}

export const BalanceModelo = new Schema({
    estado:{
        ref:'catalogo_estado',
        type:Schema.Types.String
    },
    valorActual: Number,
    totalIngreso: Number,
    totalEgreso: Number,
    proviene: {
        ref: 'balance',
        type: Schema.Types.ObjectId
    },
    transacciones:[{
        ref: 'transacciones',
        type: Schema.Types.ObjectId
    }],
}, schemaOptions);



