import {Schema} from 'mongoose'
import * as mongoosePaginate from 'mongoose-paginate-v2';


const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' }
}

export const usuarioModelo = new Schema({
    email:String,
    contrasena:String,
    fechaNacimiento: Date,
    aceptoTerminosCondiciones:Boolean,
    perfilGrupo:Boolean,
    emailVerificado:Boolean,
    menorEdad:Boolean,
    emailResponsable: String,
    responsableVerificado: Boolean,
    nombreResponsable: String,
    idioma:{
        ref:'catalogo_idiomas',
        type:Schema.Types.String
    },
    estado:{
        ref:'catalogo_estados',
        type:Schema.Types.String
    },
    perfiles: [
        {
            ref:'perfil',
            type:Schema.Types.ObjectId
        }
    ],
    transacciones: [
        {
            ref:'transaccion',
            type:Schema.Types.ObjectId
        }
    ],
    suscripciones: [
        {
            ref:'suscripcion',
            type:Schema.Types.ObjectId
        }
    ],
    dispositivos: [
        {
            ref:'dispositivo',
            type:Schema.Types.ObjectId
        }
    ],
    rolSistema: [
        {
            ref:'rol_sistema',
            type:Schema.Types.ObjectId
        }
    ],
    direccion: {
        ref:'direccion',
        type:Schema.Types.ObjectId
    },
}, schemaOptions);
usuarioModelo.plugin(mongoosePaginate);
