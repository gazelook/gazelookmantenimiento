import { Schema } from 'mongoose';

const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' }
}

export const CatalogoRolModelo = new Schema({
    codigo: String,
    nombre: String,
    descripcion: String,
    tipo: {
        ref: 'catalogo_tipo_rol',
        type: Schema.Types.String
    },
    estado: {
        ref: 'catalogo_estados',
        type: Schema.Types.String
    },
}, schemaOptions);