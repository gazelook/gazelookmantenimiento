import {Schema} from 'mongoose'
import * as mongoosePaginate from 'mongoose-paginate-v2';

const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' }
}
 
export const PensamientoModelo = new Schema({
    perfil:{
        ref:'perfil',
        type:Schema.Types.ObjectId
    },
    traducciones:[{
            ref:'traduccion_pensamiento',
            type:Schema.Types.ObjectId
    }],
    publico: Boolean,
    estado:{
        ref:'catalogo_estado',
        type:Schema.Types.String
    },
}, schemaOptions);
PensamientoModelo.plugin(mongoosePaginate);