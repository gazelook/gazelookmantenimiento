
import {Schema} from 'mongoose';

const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' }
}

export const EstrategiaModelo = new Schema({
    estado:{
        ref:'catalogo_estado',
        type:Schema.Types.String
    },
    fechaCaducidad: Date,
    presupuesto: Number,
    justificacion: String,
    adjuntos: [{
        ref: 'media',
        type: Schema.Types.ObjectId
    }],
    moneda:{
        ref: 'catalogo_tipo_moneda',
        type: Schema.Types.ObjectId
    },
}, schemaOptions);



