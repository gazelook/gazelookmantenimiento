import { Schema } from 'mongoose'

const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' }
}

export const CatalogoColoresModelo = new Schema({
    codigo: String,
    tipo: {
        ref: 'catalogo_tipo_colores',
        type: Schema.Types.ObjectId
    },
    estado: {
        ref: 'catalogo_estado',
        type: Schema.Types.String
    },
}, schemaOptions);
