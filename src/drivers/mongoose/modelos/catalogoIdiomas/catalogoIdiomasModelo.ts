import {Schema} from 'mongoose'

const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' }
}
 
export const catalogoIdiomasModelo = new Schema({
    codigo:String,
    nombre:String,
    codNombre: String,
    idiomaSistema: Boolean,
    estado:{
        ref:'catalogo_estado',
        type:Schema.Types.String
    }
    
}, schemaOptions);
