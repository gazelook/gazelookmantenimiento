import { String } from 'aws-sdk/clients/acm';
import {Schema} from 'mongoose'

const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' }
}

export const tokenUsuarioModelo = new Schema({
    token:String,
    tokenRefresh: String,
    ultimoAcceso: {type: Date, default: Date.now},
    usuario:{
        ref:'usuario',
        type:Schema.Types.ObjectId
    },
    emailUsuario: String, 

}, schemaOptions);
