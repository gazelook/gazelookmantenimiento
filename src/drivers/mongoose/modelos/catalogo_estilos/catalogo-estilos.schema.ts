import { Schema } from 'mongoose'

const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' }
}

export const CatalogoEstilosModelo = new Schema({
    codigo: String,
    nombre: String,
    descripcion:String,
    estado: {
        ref: 'catalogo_estado',
        type: Schema.Types.String
    },
}, schemaOptions);
