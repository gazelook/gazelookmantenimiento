import {Schema} from 'mongoose'

const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' }
}

export const CatalogoMensajeModelo= new Schema({
    codigo:String,
    nombre:String,
    descripcion: String,
    estado:{
        ref:'catalogo_estados',
        type: Schema.Types.String
    }
}, schemaOptions);