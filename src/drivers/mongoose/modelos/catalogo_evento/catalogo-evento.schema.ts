import { Schema } from 'mongoose'
import { Timestamp } from 'rxjs/internal/operators/timestamp';

const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' }
}

export const CatalogoEventoModelo = new Schema({
    codigo:String,
    nombre: String,
    configuraciones: [{
        ref: 'configuracion_evento',
        type: Schema.Types.String
    }],
    formulas:[{
        ref:'formula_evento',
        type: Schema.Types.String
    }],
    estado:{
        ref:'catalogo_estados',
        type: Schema.Types.String
    },
}, schemaOptions);