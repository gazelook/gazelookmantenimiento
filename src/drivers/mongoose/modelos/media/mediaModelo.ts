import { Schema } from 'mongoose';

const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' }
}

export const MediaModelo = new Schema({
    /* archivos: [{
        tipo:{
            ref: 'catalogo_tipo_archivo',
            type: Schema.Types.String
        },
        idArchivo: {
            ref: 'archivo',
            type: Schema.Types.ObjectId
        }
    }], */
    principal: {
        ref: 'archivo',
        type: Schema.Types.ObjectId
    },
    miniatura: {
        ref: 'archivo',
        type: Schema.Types.ObjectId
    },
    catalogoMedia: {
        ref: 'catalogo_media',
        type: Schema.Types.String
    },
    estado: {
        ref: 'catalogo_estado',
        type: Schema.Types.String
    },
    traducciones: [{
        ref: 'traduccion_media',
        type: Schema.Types.ObjectId
    }]
}, schemaOptions);