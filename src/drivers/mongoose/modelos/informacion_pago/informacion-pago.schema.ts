import {Schema} from 'mongoose'

const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' }
}

export const informacionPago = new Schema({
    idPago: String,
    datos: Object
}, schemaOptions);
