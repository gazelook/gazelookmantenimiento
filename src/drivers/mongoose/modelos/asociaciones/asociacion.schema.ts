import { Schema } from 'mongoose'

const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' }
}

export const AsociacionModelo = new Schema({
    estado: {
        ref: 'catalogo_estados',
        type: Schema.Types.String
    },
    tipo: {
        ref: 'catalogo_tipo_asociacion',
        type: Schema.Types.String
    }, 
    nombre: String,
    //tipo:[CatalogoTipoAsociacionModelo],
    foto: {
        ref: 'media',
        type: Schema.Types.ObjectId
    },
    participantes: [{
        ref: 'participante_asociacion',
        type: Schema.Types.ObjectId
    }],
    conversacion:{
        ref: 'conversacion', 
        type: Schema.Types.ObjectId
    },
    privado: {type: Boolean, default: true},

}, schemaOptions);
