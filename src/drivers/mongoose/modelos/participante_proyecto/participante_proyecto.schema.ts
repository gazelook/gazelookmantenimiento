import { Schema } from 'mongoose'
const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' }
}
 
export const ParticipanteProyectoModelo = new Schema({
    coautor:{
        ref:'perfil',
        type:Schema.Types.ObjectId
    },
    proyecto:{
        ref:'proyecto',
        type:Schema.Types.ObjectId
    },
    roles:[{
        ref:'rol_entidad',
        type:Schema.Types.ObjectId
    }],
    comentarios:[{
        ref:'comentarios',
        type:Schema.Types.ObjectId
    }],
    configuraciones:[{
        ref:'configuraciones',
        type:Schema.Types.ObjectId
    }],
    estado:{
        ref:'catalogo_estado',
        type:Schema.Types.String
    },
    totalComentarios: Number
    
}, schemaOptions);
