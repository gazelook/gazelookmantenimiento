import { Schema } from 'mongoose';


const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' }
}

export const CatalogoTipoRolModelo = new Schema({
    codigo: Schema.Types.String,
    estado: {
        ref: 'catalogo_estados',
        type: Schema.Types.String
    },
    nombre: String,
}, schemaOptions);



//const CatalogoAccionModelo = mongoose.model<mongoose.Document>('catalogo_acciones', campos)

//export default CatalogoAccionModelo