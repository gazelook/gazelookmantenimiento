import { Schema } from 'mongoose';


const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' }
}

export const CatalogoTipoVotoModelo = new Schema({
    codigo: Schema.Types.String,
    nombre: String,
    votoAdmin:Boolean,
    estado: {
        ref: 'catalogo_estados',
        type: Schema.Types.String
    }
}, schemaOptions);