import { Schema } from 'mongoose'
import * as mongoosePaginate from 'mongoose-paginate-v2';

const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' }
}
 
export const ParticipanteAsociacionModelo = new Schema({
    estado:{ 
        ref:'catalogo_estados',
        type:Schema.Types.String
    },
    invitadoPor:{
        ref:'participante_asociacion',
        type:Schema.Types.ObjectId
    },
    perfil:{
        ref:'perfil',
        type:Schema.Types.ObjectId
    },
    asociacion:{
        ref:'asociacion',
        type:Schema.Types.ObjectId
    },
    sobrenombre:String,
    roles:[{
        ref:'rol',
        type:Schema.Types.ObjectId
    }],
    configuraciones:[{
        ref:'configuracion',
        type:Schema.Types.ObjectId
    }],
    contactoDe:{
        ref:'perfil',
        type:Schema.Types.ObjectId
    },

    

}, schemaOptions);
ParticipanteAsociacionModelo.plugin(mongoosePaginate);
