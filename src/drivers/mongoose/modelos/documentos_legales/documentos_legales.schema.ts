import {Schema} from 'mongoose';
import * as mongoosePaginate from 'mongoose-paginate-v2';

const schemaOptions = {
    timestamps: { createdAt: 'fechaCreacion', updatedAt: 'fechaActualizacion' }
}

export const DocumentosLegalesModelo = new Schema({
    estado:{
        ref:'catalogo_estados',
        type:Schema.Types.String
    },
    origen:{
        ref:'catalogo_origen_documento',
        type:Schema.Types.String
    },
    version:Number,
    usuario:{
        ref:'usuario',
        type:Schema.Types.ObjectId
    },
    adjuntos:[{
        ref:'media',
        type:Schema.Types.ObjectId
    }],
}, schemaOptions);

DocumentosLegalesModelo.plugin(mongoosePaginate);
