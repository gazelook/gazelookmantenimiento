import { Document } from 'mongoose';
import { Timestamp } from 'rxjs/internal/operators/timestamp';

export interface Historico extends Document{   

    readonly datos: object,
    readonly usuario:string,
    readonly accion:string,
    readonly entidad: string,
    //readonly registro: String
    readonly fechaCreacion: Date
    readonly tipo: string

}

