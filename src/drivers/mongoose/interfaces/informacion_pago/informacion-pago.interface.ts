import { informacionPago } from './../../modelos/informacion_pago/informacion-pago.schema';
import { Document } from 'mongoose';

export interface InformacionPago extends Document{
    idPago: string,
    datos: object

};