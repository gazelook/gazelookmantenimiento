import { Document } from 'mongoose';

export interface RolSistema extends Document {
    readonly estado: string,
    readonly rolesEspecificos: Array<string>,
    readonly rol: string
    readonly nombre: string
}

