import { Document } from 'mongoose';

export interface ParticipanteProyecto extends Document{
    readonly coautor:string,
    readonly proyecto:string,
    readonly roles:Array<string>,
    readonly estado:string,
    readonly totalComentarios:number,
    readonly configuraciones: Array<string>,
    readonly comentarios: Array<string>
}

