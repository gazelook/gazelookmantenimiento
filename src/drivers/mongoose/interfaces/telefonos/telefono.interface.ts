import { Document } from 'mongoose';

export interface Telefono extends Document {
    readonly pais: string,
    readonly numero: string,
    readonly estado: string
}

