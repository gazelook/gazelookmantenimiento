import { Document } from 'mongoose';

export interface TraducionTipoPerfil extends Document{

    readonly nombre: string,
    readonly descripcion: string,
    readonly idioma:string,
    readonly original: boolean,
    readonly codigoTipoPerfil:string

}

