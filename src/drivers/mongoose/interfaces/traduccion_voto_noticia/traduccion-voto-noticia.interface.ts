import { String } from 'aws-sdk/clients/acm';
import { Document } from 'mongoose';

export interface TraduccionesVotoNoticia extends Document{   
    readonly descripcion: string,
    readonly idioma: string,
    readonly estado: string,
    readonly original: boolean,
    readonly referencia: string
}

