import { Document } from 'mongoose';

export interface Suscripcion extends Document {
    estado: string;
    tipo: string;
    transaccion: string;
    fechaFinalizacion: Date;
    usuario: string;
    referencia: string;
};