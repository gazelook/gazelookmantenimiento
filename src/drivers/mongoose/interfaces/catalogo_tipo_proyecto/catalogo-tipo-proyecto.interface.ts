import { Document } from 'mongoose';

export interface CatalogoTipoProyecto extends Document{

    readonly codigo:string,
    readonly traducciones: Array<string>,
    readonly estado:string,

}