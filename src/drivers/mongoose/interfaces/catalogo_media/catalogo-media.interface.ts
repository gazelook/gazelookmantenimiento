import { Document } from 'mongoose';

export interface CatalogoMedia extends Document {

    readonly codigo: string,
    readonly nombre: string,
    readonly estado: string
}

