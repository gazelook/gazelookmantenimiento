import { Document } from 'mongoose';

export interface RolEntidad extends Document {
    readonly estado: string,
    readonly acciones: Array<string>,
    readonly entidad: string
    readonly rol: string
    readonly nombre: string
}

