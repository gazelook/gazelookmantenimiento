import { Document } from 'mongoose';

export interface TraduccionCatalogoTipoMoneda extends Document{
    readonly nombre: string;
    readonly idioma: string;
    readonly original: boolean;
    readonly estado: string;
    readonly catalogoTipoMoneda: string;
    readonly fechaCreacion: Date;
    readonly fechaActualizacion: Date;

}

