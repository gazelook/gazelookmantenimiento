import { Document } from 'mongoose';
import { String } from 'aws-sdk/clients/acm';

export interface CatalogoTipoColores extends Document {

    readonly codigo: string,
    readonly nombre: string,
    readonly formula: string,
    readonly estado: string

}