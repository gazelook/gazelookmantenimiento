import { Document } from 'mongoose';

export interface CatalogoOrigen extends Document {

    readonly codigo: string,
    readonly estado: string,
    readonly nombre: string,
    readonly descripcion: string,
    readonly ingreso: number,

}

