import { Document } from 'mongoose';

export interface TraduccionLocalidad extends Document {
    readonly nombre: string,
    readonly idioma: string,
    readonly original: boolean
}