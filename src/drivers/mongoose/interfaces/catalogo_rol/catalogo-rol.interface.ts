import { Document } from 'mongoose';

export interface CatalogoRol extends Document {

    readonly codigo: string;
    readonly nombre: string;
    readonly tipo: string;
    readonly estado: string;
    readonly descripcion: string,

}