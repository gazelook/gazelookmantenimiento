import { Document } from 'mongoose';

export interface CatalogoTipoBeneficiario extends Document{
    readonly codigo: string,
    readonly nombre: string,
    readonly estado: string
    readonly descripcion: string
}