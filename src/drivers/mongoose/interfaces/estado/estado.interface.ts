import { Document } from 'mongoose';
export interface CatalogoEstado extends Document{   

    readonly codigo:string,
    readonly nombre: string,
    readonly descripcion: string,
    readonly entidad: string,
    readonly accion: string
}

