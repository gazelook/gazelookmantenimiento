import { Document } from 'mongoose';
import { bool } from 'aws-sdk/clients/signer';

export interface ConfiguracionEstilo extends Document{
    readonly codigo: string,
    readonly estado: string,
    readonly entidad: string,
    readonly silenciada:boolean,
    readonly tonoNotificacion: string,
    readonly estilos: Array<string>,
    readonly tipo: string,
    readonly catalogoParticipante: string
}

