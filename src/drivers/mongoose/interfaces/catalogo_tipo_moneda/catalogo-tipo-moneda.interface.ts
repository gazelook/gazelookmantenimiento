import { Document } from 'mongoose';

export interface CatalogoTipoMoneda extends Document {

    readonly estado: string
    readonly codigo: string,
    readonly nombre: string,
    readonly predeterminado: boolean
}