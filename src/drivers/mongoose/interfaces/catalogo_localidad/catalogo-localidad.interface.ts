import { Document } from 'mongoose';
// import { String } from 'aws-sdk/clients/acm';

export interface CatalogoLocalidad extends Document {

    readonly catalogoPais: string,
    readonly nombre: string,
    readonly codigo: string,
    readonly codigoPostal: string,
    readonly estado: string,
    /* readonly traducciones: Array<String>, */
}