import { Document } from 'mongoose';

export interface TipoMedia extends Document{

    readonly codigo:string,
    readonly nombre:string,
    readonly estado:string

}