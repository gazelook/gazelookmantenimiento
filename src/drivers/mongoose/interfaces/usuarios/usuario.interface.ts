import { Document } from 'mongoose';

export interface Usuario extends Document {

    readonly email: string,
    readonly contrasena: string,
    readonly codigo: string,
    readonly fechaNacimiento: Date,
    readonly aceptoTerminosCondiciones: boolean,
    readonly perfilGrupo: boolean,
    readonly emailVerificado: boolean,
    readonly menorEdad: boolean,
    readonly emailResponsable: string,
    readonly responsableVerificado: boolean,
    readonly nombreResponsable: string,
    readonly idioma: string,
    readonly estado: string,
    readonly perfiles: Array<any>,
    readonly transacciones: Array<string>,
    readonly suscripciones: Array<string>
    readonly dispositivos: Array<string>
    readonly rolSistema: Array<string>,
    readonly fechaCreacion: Date,
    readonly fechaActualizacion: Date;
    readonly direccion: string
}

