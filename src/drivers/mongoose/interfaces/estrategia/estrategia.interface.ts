import { Document } from 'mongoose';

export interface Estrategia extends Document{
    readonly estado:string,
    readonly adjuntos: Array<string>,
    readonly presupuesto: number,
    readonly fechaCaducidad:Date,
    readonly justificacion: string,
    readonly moneda:string
}