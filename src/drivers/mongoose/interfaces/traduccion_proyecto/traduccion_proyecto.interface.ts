import { Document } from 'mongoose';

export interface TraduccionProyecto extends Document{   
    
    readonly titulo:  string,
    readonly tituloCorto: string,
    readonly descripcion: string,
    readonly tags:Array< string>,
    readonly idioma: string,
    readonly original: Boolean,
    readonly estado: string
    readonly proyecto:  string

}

