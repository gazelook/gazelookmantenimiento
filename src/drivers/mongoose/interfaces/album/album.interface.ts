import { Document } from 'mongoose';

export interface Album extends Document {

    readonly media: Array<string>,
    readonly tipo: string
    readonly portada: string,
    readonly traducciones: Array<string>,
    readonly estado: string,
    readonly predeterminado: boolean,
    r
}