import { Document } from 'mongoose';

export interface VotoProyecto extends Document {
    readonly estado: string,
    readonly perfil: string,
    readonly proyecto: string,
    readonly traducciones: Array<string>,
    readonly tipo: string,
    readonly numeroVoto:number,
    readonly configuracion:string
}
