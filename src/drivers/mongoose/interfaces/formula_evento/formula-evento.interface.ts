import { Document } from 'mongoose';
import { String } from 'aws-sdk/clients/acm';

export interface FormulaEvento extends Document {

    readonly codigo: string,
    readonly formula: string,
    readonly descripcion: string,
    readonly catalogoEvento: string,
    readonly prioridad: number,
    readonly estado: string

}