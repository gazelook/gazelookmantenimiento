import { Document } from 'mongoose';

export interface CatalogoPais extends Document {

    readonly codigo: string,
    readonly codigoNombre: string,
    readonly estado: string,
    readonly codigoTelefono: string,
    readonly traducciones: Array<string>

}