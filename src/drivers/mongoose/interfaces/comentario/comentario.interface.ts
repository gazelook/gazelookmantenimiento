import { Document } from 'mongoose';

export interface Comentario extends Document{
    readonly estado: string,
    readonly adjuntos: Array<string>,
    readonly coautor: string,
    readonly importante:string
    readonly traducciones: Array<string>,
    readonly tipo:string,
    readonly proyecto: string,
    readonly fechaCreacion: Date,
    readonly fechaActualizacion:Date
}

