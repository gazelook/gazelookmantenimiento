import { Document } from 'mongoose';

export interface TraduccionDireccion extends Document {
  readonly descripcion: string;
  readonly idioma: string;
  readonly original: boolean;
  readonly direccion: string;
  readonly estado: string;
}
