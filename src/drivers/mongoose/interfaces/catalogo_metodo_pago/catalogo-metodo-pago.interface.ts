import { Document } from 'mongoose';

export interface CatalogoMetodoPago extends Document {

    readonly codigo: string,
    readonly nombre: string,
    readonly descripcion: string,
    readonly estado: string
    readonly icono: string
}

