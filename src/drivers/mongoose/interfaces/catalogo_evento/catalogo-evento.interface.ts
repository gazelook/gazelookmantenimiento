import { Document } from 'mongoose';
import { TimeStamp } from 'aws-sdk/clients/discovery';

export interface CatalogoEvento extends Document {

    readonly codigo: string,
    readonly nombre: string,
    readonly configuraciones: Array<string>,
    readonly formulas: Array<string>,
    readonly estado: string


}