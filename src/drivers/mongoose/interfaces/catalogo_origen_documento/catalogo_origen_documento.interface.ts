import { Document } from 'mongoose';

export interface CatalogoOrigenDocumento extends Document{
    readonly codigo: string,
    readonly nombre: string,
    readonly estado: string
    readonly descripcion: string
}