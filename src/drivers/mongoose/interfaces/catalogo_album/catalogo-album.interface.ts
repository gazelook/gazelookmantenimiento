import { Document } from 'mongoose';

export interface CatalogoAlbum extends Document {

    readonly codigo: string,
    readonly nombre: string,
    readonly descripcion: string,
    readonly estado: string

}