import { Document } from 'mongoose';

export interface CatalogoPorcentajeProyecto extends Document{
    readonly codigo:string,
    readonly tipo: string,
    readonly porcentaje:number,
    readonly prioridad:number,
    readonly estado:string,
    readonly fechaCreacion:Date,
    readonly fechaActualizacion:Date
}