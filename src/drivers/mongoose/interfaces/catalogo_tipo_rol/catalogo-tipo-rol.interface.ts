import { Document } from 'mongoose';

export interface CatalogoTipoRol extends Document {

    readonly codigo: string;
    readonly estado: string;
    readonly nombre: string;

}