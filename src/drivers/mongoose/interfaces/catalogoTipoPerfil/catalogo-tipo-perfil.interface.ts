import { Document } from 'mongoose';

export interface TipoPerfil extends Document{

    readonly codigo:string,
    readonly traducciones:string
    readonly estado:string

}

