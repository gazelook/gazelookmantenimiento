
import { Document } from 'mongoose';



export interface Evento extends Document {

    readonly fechaInicio: Date,
    readonly fechaFin: Date,
    readonly configuracionEvento:string,
    readonly proyectos: Array<string>,
    readonly estado: string
    
}