import { Document } from 'mongoose';

export interface Proyecto extends Document{   

    readonly perfil:string,
    readonly tipo:string,
    readonly localidad:string,
    readonly participantes:Array<string>,//lista de participantes del proyecto
    readonly recomendadoAdmin: boolean,
    readonly valorEstimado: number,
    readonly adjuntos:Array<string>,
    readonly medias:Array<string>,
    readonly votos: Array<string>,
    readonly totalVotos:number,
    readonly traducciones:Array<string>,
    readonly estrategia: string,
    readonly comentarios: Array<string>,
    readonly moneda:string,
    readonly estado:string

}

