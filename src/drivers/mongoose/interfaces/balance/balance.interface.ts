import { Document } from 'mongoose';

export interface Balance extends Document{
    readonly estado:string,
    readonly valorActual:number,
    readonly totalIngreso:number,
    readonly totalEgreso:number,
    readonly proviene:number,
    readonly transaciones: Array<string>,



}

