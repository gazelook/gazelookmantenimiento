import { Document } from 'mongoose';

export interface ConfiguracionPersonalizada extends Document{

    readonly configuracion:string,
    readonly version: number

}

