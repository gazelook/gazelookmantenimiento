import { Document } from 'mongoose';

export interface CatalogoEntidad extends Document{   

    readonly codigo:string,
    readonly nombre: string,
    readonly estado: string
}

