import { Document } from 'mongoose';

export interface Asociacion extends Document {

    readonly participantes: Array<string>,
    readonly estado: string
    readonly tipo: string,
    readonly foto: string,
    readonly nombre: string,
    readonly conversacion: string,
    readonly privado: boolean,
}