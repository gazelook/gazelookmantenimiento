import { Document } from 'mongoose';
import { String } from 'aws-sdk/clients/acm';

export interface CatalogoTokenUsuarioLocalidad extends Document {

    readonly codigoTipo: string,
    readonly nombre: string,
    readonly estado: string,
    readonly descripcion: string
}