import { Document } from 'mongoose';
import { String } from 'aws-sdk/clients/acm';

export interface CatalogoConfiguracion extends Document {

    readonly codigo: string,
    readonly nombre: string,
    readonly estado: string

}