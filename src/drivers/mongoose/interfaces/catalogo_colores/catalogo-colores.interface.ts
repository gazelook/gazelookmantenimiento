import { Document } from 'mongoose';
import { String } from 'aws-sdk/clients/acm';

export interface CatalogoColores extends Document {

    readonly codigo: string,
    readonly tipo: string,
    readonly estado: string

}