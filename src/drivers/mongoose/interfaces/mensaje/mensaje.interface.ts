import { Document } from 'mongoose';

export interface Mensaje extends Document{   

    readonly estado: String,
    readonly tipo:String,
    readonly estatus:String,
    readonly traducciones:Array<String>,
    readonly importante: Boolean,
    readonly adjuntos:Array<String>,
    readonly conversacion:String,
    readonly propietario:String,
    readonly entregadoA:Array<String>,
    readonly leidoPor:Array<String>,
    readonly eliminadoPor:Array<String>,

  
}
  