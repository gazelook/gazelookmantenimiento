import { Document } from 'mongoose';

export interface TraduccionesNoticias extends Document{   
    readonly tituloCorto: string,
    readonly titulo: string,
    readonly descripcion: string,
    readonly tags: Array<string>,
    readonly idioma: string,
    readonly original: boolean,
    readonly estado: string,
    readonly referencia: string,
}

