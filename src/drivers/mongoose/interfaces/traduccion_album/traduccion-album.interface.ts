import { Document } from 'mongoose';

export interface TraduccionMedia extends Document {
    readonly nombre: string,
    readonly idioma: string,
    readonly original: boolean
}