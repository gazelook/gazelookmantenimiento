import { Document } from 'mongoose';
import { TraduccionDireccion } from '../traduccion_direccion/traduccion-direccion.interface';

export interface Direccion extends Document {
    readonly localidad: string;
    readonly pais: string;
    readonly latitud: number;
    readonly longitud: number;
    traducciones?: Array<TraduccionDireccion>;
    readonly estado: string;
}

