import { Document } from 'mongoose';

export interface Email extends Document {
    readonly codigo: string,
    readonly emailDestinatario: string,
    readonly usuario: string,
    readonly perfil: string,
    readonly estado: string,
    readonly fechaCreacion: Date,
    readonly fechaValidacion: Date,
    readonly tokenEmail: string,
    readonly enviado: boolean,
    readonly descripcionEnvio: string,
    readonly datosProceso: any
}
