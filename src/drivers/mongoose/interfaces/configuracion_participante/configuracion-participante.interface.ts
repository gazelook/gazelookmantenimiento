import { Document } from 'mongoose';

export interface ConfiguracionParticipante extends Document{

    readonly propietario:string,
    readonly asignado:string,
    readonly configuracionEstilo:string,
    readonly estado:string

}

