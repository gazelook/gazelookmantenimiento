import { Document } from 'mongoose';

export interface TraduccionCatalogoMetodoPago extends Document{
    readonly nombre: string;
    readonly idioma: string;
    readonly original: boolean;
    readonly codigo: string;
    readonly estado: string;
    readonly fechaCreacion: Date;
    readonly fechaActualizacion: Date;

}

