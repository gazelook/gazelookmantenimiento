export interface MailOptions {
    to: string;
    subject: string;
    html: any;
    attachments?: any;
}