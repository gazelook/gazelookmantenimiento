import { Injectable } from '@nestjs/common';
import { createTransport } from 'nodemailer';
import { ConfigService } from 'src/config/config.service';
import { MailOptions } from './models/mail-options';
import * as Mail from 'nodemailer/lib/mailer';
import { google } from 'googleapis';

@Injectable()
export class NodeMailerService {

    private nodemailerTransport: Mail;
    private readonly PASSWORD_EMAIL = this.config.get('PASSWORD_CORREO_GMAIL');
    private readonly EMAIL_MANTENIMIENTO = this.config.get<string>('EMAIL_MANTENIMIENTO');
    private OAuth2 = google.auth.OAuth2;

    private readonly GOOGLE_CLIENT_ID: string;
    private readonly GOOGLE_CLIENT_SECRET: string;
    private readonly OAUTH_PLAYGROUND: string;
    private readonly REFRESH_TOKEN: string;


    constructor(private config: ConfigService) {
        this.GOOGLE_CLIENT_ID = this.config.get('GOOGLE_CLIENT_ID');
        this.GOOGLE_CLIENT_SECRET = this.config.get('GOOGLE_CLIENT_SECRET');
        this.OAUTH_PLAYGROUND = this.config.get('OAUTH_PLAYGROUND');
        this.REFRESH_TOKEN = this.config.get('REFRESH_TOKEN');

    }

    async enviarEmail(mailOptions: MailOptions) {

        const dataEmailOptions = {
            ...mailOptions,
            from: `GAZELOOK <${this.EMAIL_MANTENIMIENTO}>`
        }

        try {

            const oauth2Client = await new google.auth.OAuth2(
                this.GOOGLE_CLIENT_ID,
                this.GOOGLE_CLIENT_SECRET,
                this.OAUTH_PLAYGROUND,
            );
            await oauth2Client.setCredentials({
                refresh_token: this.REFRESH_TOKEN,
            });
            // toca refrescar el token cada cierto tiempo
            // https://developers.google.com/oauthplayground
            const accessToken = await oauth2Client.getAccessToken();


            const transporter = createTransport({
                service: 'gmail',
                auth: {
                    type: 'OAuth2',
                    user: this.EMAIL_MANTENIMIENTO,
                    clientId: this.GOOGLE_CLIENT_ID,
                    clientSecret: this.GOOGLE_CLIENT_SECRET,
                    refreshToken: this.REFRESH_TOKEN,
                    accessToken: accessToken
                },
                /* authMethod: 'NTLM',
                secure: false,
                tls: {
                    rejectUnauthorized: false
                },
                debug: true */
            });
            const resultEmail = await transporter.sendMail(dataEmailOptions);
            return resultEmail;
        } catch (error) {
            throw error;
        }

    }

    async enviarEmailOld(mailOptions: MailOptions) {

        const dataEmailOptions = {
            ...mailOptions,
            from: `GAZELOOK <${this.EMAIL_MANTENIMIENTO}>`
        }

        this.nodemailerTransport = createTransport({
            name: "gazelook.com",
            service: 'gmail',
            // host: 'smtp.gmail.com',
            // port: 465,
            // secure: true,
            auth: {
                user: this.EMAIL_MANTENIMIENTO, // Cambialo por tu email
                pass: this.PASSWORD_EMAIL // Cambialo por tu password
            }
        });

        return new Promise((resolve, reject) => {

            try {
                this.nodemailerTransport.sendMail(dataEmailOptions, function (error, info) {
                    if (error) {
                        resolve(error); // or use reject(false) but then you will have to handle errors
                    }
                    else {
                        resolve(info);
                    }
                });
            } catch (error) {
                resolve(error)
            }

        })
    }

}