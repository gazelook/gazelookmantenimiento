import {
  HttpStatus, Injectable,
  NestMiddleware
} from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import * as passport from 'passport';
import { RespuestaInterface } from './../shared/respuesta-interface';

@Injectable()
export class AuthMiddleware implements NestMiddleware {

  constructor(
    private readonly i18n: I18nService
  ) {

  }

   use(req: any, res: any, next: () => void) {
    
    const respuesta = new RespuestaInterface;
    passport.authenticate(
      'headerapikey',
      { session: false, failureRedirect: '/api/unauthorized' },
      async value  => {
        if (value) {
          next();
        } else {
          let codIdioma= req.headers.idioma
          if(!codIdioma){
            codIdioma='en'
          }
          const NO_AUTORIZADO = await this.i18n.translate(codIdioma.concat('.NO_AUTORIZADO'), {
              lang: codIdioma
          });
          respuesta.codigoEstado = HttpStatus.UNAUTHORIZED;
          respuesta.respuesta = {
              mensaje: NO_AUTORIZADO //'UNAUTHORIZED'
          }
          return res.send(respuesta);
        }
      },
    )(req, res, next);
  }
}
