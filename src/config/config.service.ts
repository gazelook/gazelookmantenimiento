// src/config/config.service.ts
import { Injectable } from '@nestjs/common';
import { ConfigManager } from '@nestjsplus/config';
import * as Joi from '@hapi/joi';


@Injectable()
export class ConfigService extends ConfigManager {

    provideConfigSpec() {
        return {
            API_KEY: {
                validate: Joi.string(),
                required: true,
                default: process.env.API_KEY

            },
            PORT: {
                validate: Joi.number()
                    .min(5000)
                    .max(65535),
                required: true,
                default: process.env.PORT
            },
            HOST: {
                validate: Joi.string(),
                required: true,
                default: process.env.HOST
            },
            HOST_REMOTO: {
                validate: Joi.string(),
                required: true,
                default: process.env.HOST_REMOTO
            },
            EMAIL_MANTENIMIENTO: {
                validate: Joi.string(),
                required: true,
                default: process.env.EMAIL_MANTENIMIENTO
            },
            EMAIL_CONTACT: {
                validate: Joi.string(),
                required: true,
                default: process.env.EMAIL_CONTACT
            },
            HOST_LOCAL_PRINCIPAL: {
                validate: Joi.string(),
                required: true,
                default: process.env.HOST_LOCAL_PRINCIPAL
            },

            S3_ACCESS_KEY: {
                validate: Joi.string(),
                required: false,
                default: process.env.S3_ACCESS_KEY

            },
            S3_SECTRET_ACCESS_KEY: {
                validate: Joi.string(),
                required: false,
                default: process.env.S3_SECTRET_ACCESS_KEY
            },
            S3_BUCKET: {
                validate: Joi.string(),
                required: false,
                default: process.env.S3_BUCKET
            },
            S3_FOLDER: {
                validate: Joi.string(),
                required: false,
                default: process.env.S3_FOLDER
            },
            URL_CLOUDFRONT: {
                validate: Joi.string(),
                required: false,
                default: process.env.URL_CLOUDFRONT
            },
            S3_BUCKET_INFORMACION_CUENTA: {
                validate: Joi.string(),
                required: false,
                default: process.env.S3_BUCKET_INFORMACION_CUENTA
            },
            S3_FOLDER_DATOS_CUENTA: {
                validate: Joi.string(),
                required: false,
                default: process.env.S3_FOLDER_DATOS_CUENTA
            },
            URL_S3_INFORMACION_CUENTA: {
                validate: Joi.string(),
                required: false,
                default: process.env.URL_S3_INFORMACION_CUENTA
            },

            S3_BUCKET_DOCUMENTOS_USUARIO: {
                validate: Joi.string(),
                required: true,
                default: process.env.S3_BUCKET_DOCUMENTOS_USUARIO
            },
            S3_FOLDER_DOCUMENTOS: {
                validate: Joi.string(),
                required: true,
                default: process.env.S3_FOLDER_DOCUMENTOS
            },
            URL_S3_DOCUMENTOS_USUARIO: {
                validate: Joi.string(),
                required: true,
                default: process.env.URL_S3_DOCUMENTOS_USUARIO
            },

            MONGO_URI: {
                validate: Joi.string(),
                required: true,
            },
            JWT_SECRET: {
                validate: Joi.string(),
                required: true,
                default: process.env.JWT_SECRET
            },
            URL_SERVER_HISTORICO: {
                validate: Joi.string(),
                required: true,
                default: process.env.URL_SERVER_HISTORICO
            },
            URL_RECURSOS_SISTEMA: {
                validate: Joi.string(),
                required: true,
                default: process.env.URL_RECURSOS_SISTEMA
            },
            GOOGLE_CLIENT_ID: {
                validate: Joi.string(),
                required: true,
                default: process.env.GOOGLE_CLIENT_ID
            },
            GOOGLE_CLIENT_SECRET: {
                validate: Joi.string(),
                required: true,
                default: process.env.GOOGLE_CLIENT_SECRET
            },
            REFRESH_TOKEN: {
                validate: Joi.string(),
                required: true,
                default: process.env.REFRESH_TOKEN
            },
            ACCESS_TOKEN: {
                validate: Joi.string(),
                required: true,
                default: process.env.ACCESS_TOKEN
            },
            OAUTH_PLAYGROUND: {
                validate: Joi.string(),
                required: true,
                default: process.env.OAUTH_PLAYGROUND
            },
            COINPAYMENTEZ_SECRET: {
                validate: Joi.string(),
                required: true,
                default: process.env.COINPAYMENTEZ_SECRET,
            },
            COINPAYMENTEZ_KEY: {
                validate: Joi.string(),
                required: true,
                default: process.env.COINPAYMENTEZ_KEY,
            },
            COINMARKETCAP_KEY: {
                validate: Joi.string(),
                required: true,
                default: process.env.COINMARKETCAP_KEY,
            },
        };
    }
}