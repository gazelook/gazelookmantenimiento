import { Module, Global } from '@nestjs/common';
import { ConfigManagerModule } from '@nestjsplus/config';
import { ConfigService } from './config.service';

@Global()
@Module({
  imports: [
    ConfigManagerModule.register({
      envKey: 'NODE_ENV',
      useEnv: {
        folder: 'configsEnv',
      },
      allowExtras: true,
    
      
    }),
    
  ],
  providers: [ConfigService],
  exports: [ConfigService],
})
export class ConfigModule {}