import { NestFactory, HttpAdapterHost } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { Logger, BadRequestException, NotFoundException } from '@nestjs/common';
import { pathFileMain } from './shared/path';
import { appendFile } from 'fs'
import { NestExpressApplication } from '@nestjs/platform-express';
import { resolve } from 'path';
import { ValidationPipe422 } from './shared/myvalidator';
import * as basicAuth from 'express-basic-auth';
import { ValidationExceptionFilter } from './shared/filters/validator-exception.filter';

try {


  async function bootstrap() {
    const app = await NestFactory.create<NestExpressApplication>(
      AppModule,
      { cors: true }
    )


    app.useStaticAssets(resolve('./src/public'));
    app.setBaseViewsDir(resolve('./src/views'));
    app.setViewEngine('hbs');


    app.use((req, res, next) => {
      res.header('Access-Control-Allow-Origin', '*');
      res.header('Access-Control-Allow-Headers', 'apiKey, idioma, Authorization');
      res.header('Access-Control-Expose-Headers', 'totalDatos, totalPaginas, proximaPagina, anteriorPagina');
      res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
      res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
      next();
    })
    app.enableCors({
      // origin: true,
      // methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
      // credentials: true,
    });
    //app.useGlobalInterceptors(new LoggingInterceptor());

    const port = process.env.PORT;
    const date = new Date();

    // app.useGlobalPipes(new ValidationPipe());
    app.useGlobalPipes(new ValidationPipe422());
    app.useGlobalFilters(new ValidationExceptionFilter())

    const { httpAdapter } = app.get(HttpAdapterHost);
    appendFile(pathFileMain,
      `\n[${date}] Server running on http://localhost:${port}`,
      (error) => {
        if (error) {
          throw new BadRequestException(error);
        }
      });

    // Swagger
    // solo para pruebas en produccion no se va a ver la documentacion
    const SWAGGER_ENVS = ['qa', 'dev', 'staging'];
    if (SWAGGER_ENVS.includes(process.env.NODE_ENV)) {
      app.use(['/docs', '/docs-json'], basicAuth({
        challenge: true,
        users: {
          [process.env.SWAGGER_USER]: process.env.SWAGGER_PASSWORD,
        },
      }));

      const config = new DocumentBuilder()
        .setTitle('GAZELOOK API Docs')
        .setDescription('Main API Documentation')
        .setVersion('1.0')
        .addBearerAuth()
        .build();

      const document = SwaggerModule.createDocument(app, config);
      SwaggerModule.setup('docs', app, document);
    }

    await app.listen(port);
    Logger.log(`Server running on http://localhost:${port}`, 'Bootstrap');
  }
  bootstrap();


} catch (error) {
  throw new NotFoundException(error);
}
