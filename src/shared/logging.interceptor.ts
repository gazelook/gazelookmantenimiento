import { Injectable, NestInterceptor, ExecutionContext, Logger, CallHandler, createParamDecorator } from '@nestjs/common'
import { Observable } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { pathFile } from './path'
import { appendFile } from 'fs';
@Injectable()
export class LoggingInterceptor implements NestInterceptor {

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {

    
    // get the request and log it to the console
    const ctx = context.switchToHttp();
    const request = ctx.getRequest();
    const response = ctx.getResponse();

    // console.log('request----------------------->: ', request)
    // console.log('response: ', response.req.user.user)
    const statusCode = response.statusCode;
    const httpRequest = {
      headers: {
        'Content-Type': request.get('Content-Type'),
        Referer: request.get('referer'),
        'User-Agent': request.get('User-Agent')
      }
    }
    const url = request.originalUrl;
    const browser = httpRequest.headers['User-Agent'];
    const ip = request.ip;
    const referer = request.get('referer')
    const params = JSON.stringify(request.params);
    const query = JSON.stringify(request.query);
    const method = request.method;
    const now = Date.now();
    const date = new Date();
    return next
      .handle()
      .pipe(
        map((content) => {
          Logger.log(`Request recibida a ${url} de IP => ${ip}, Browser => ${browser}, Referer => ${referer}, Method => ${method}, Query => ${query}, Params => ${params} - StatusCode => ${statusCode} - Response => ${JSON.stringify(content)} - Tiempo petición => ${Date.now() - now}ms`)
          appendFile(pathFile,
            `\n[${date}] Request recibida a ${url} de IP => ${ip}, Browser => ${browser}, Referer => ${referer}, Method => ${method}, Query => ${query}, Params => ${params} - StatusCode => ${statusCode} - Response => ${JSON.stringify(content)} - Tiempo petición => ${Date.now() - now}ms`,
            (error) => {
              if (error) {
                throw (error)
              }
            });
          return content;
        }),
      );
  }
}
// @Injectable()
// export class LoggingInterceptor implements NestInterceptor{

//         intercept(context: ExecutionContext, next: CallHandler): Observable<any> {

//         const req = context.switchToHttp().getRequest();
//         const httpRequest = {
//             headers: {
//                 'Content-Type': req.get('Content-Type'),
//                 Referer: req.get('referer'),
//                 'User-Agent': req.get('User-Agent')
//               }
//         }

//         const method = req.method;
//         const body= JSON.stringify(req.body);
//         //const body= JSON.stringify(response);
//         const query= JSON.stringify(req.query);
//         const params= JSON.stringify(req.params);
//         //const status = response.statusCode;
//         const url = req.url;
//         const port = req.port;
//         //const ip = IpAddress;
//         const ip = req.ip;
//         const browser = httpRequest.headers['User-Agent'];
//         const referer = req.get('referer')
//         const now = Date.now();
//         const date = new Date();
//             appendFile(pathFile, 
//             `\n[${date}] Request recibida a ${url}, IP => ${ip}, Browser => ${browser}, Referer => ${referer}, Method => ${method}, Body => ${body}, Query => ${query}, Params => ${params} - ${Date.now() - now}ms`,
//             (error)=>{
//                 if(error){
//                     throw (error)
//                 }
//             });
//         return (next
//             .handle()
//             .pipe(
//                 tap((response)=> 
//                     Logger.log(`Request recibida a ${url} de IP => ${ip}, Browser => ${browser}, Referer => ${referer}, Method => ${method}, Body => ${response.body}, Query => ${query}, Params => ${params} - Tiempo petición => ${Date.now() - now}ms \n${response}`, 
//                     //context.getClass().name,
//                     ), 
//                 ),
//             )   
//         );

//     }

// }

