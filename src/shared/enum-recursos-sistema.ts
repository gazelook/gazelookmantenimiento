export enum recursosSistemaEN {
    HEADER_EMAIL_EN = '/recursos-sistema/mail/header-mail-ENG.png',
    FOOTER_EMAIL_EN = '/recursos-sistema/mail/footer-mail-ENG.png',
    HEADER_EMAIL_ES = '/recursos-sistema/mail/header-mail-ESP.png',
    FOOTER_EMAIL_ES = '/recursos-sistema/mail/footer-mail-ESP.png',
    HEADER_EMAIL_FR = '/recursos-sistema/mail/header-mail-FRA.png',
    FOOTER_EMAIL_FR = '/recursos-sistema/mail/footer-mail-FRA.png',
    HEADER_EMAIL_IT = '/recursos-sistema/mail/header-mail-ITA.png',
    FOOTER_EMAIL_IT = '/recursos-sistema/mail/footer-mail-ITA.png',
    HEADER_EMAIL_DE = '/recursos-sistema/mail/header-mail-GER.png',
    FOOTER_EMAIL_DE = '/recursos-sistema/mail/footer-mail-GER.png',
    HEADER_EMAIL_PT = '/recursos-sistema/mail/header-mail-POR.png',
    FOOTER_EMAIL_PT = '/recursos-sistema/mail/footer-mail-POR.png',
}

export enum contactEmail {
    contactEN ="contactEN@gazelook.com",
    contactES= "contactES@gazelook.com",
    contactFR= "contactFR@gazelook.com",
    contactDE= "contactDE@gazelook.com",
    contactIT = "contactIT@gazelook.com",
    contactPT= "contactPT@gazelook.com"
}

export enum politicasPrivacidad {
    pdfEN ="/recursos-sistema/mail/politica-privacidad.pdf",
    pdfES= "/recursos-sistema/mail/politica-privacidad.pdf",
    pdfFR= "/recursos-sistema/mail/politica-privacidad.pdf",
    pdfDE= "/recursos-sistema/mail/politica-privacidad.pdf",
    pdfIT = "/recursos-sistema/mail/politica-privacidad.pdf",
    pdfPT= "/recursos-sistema/mail/politica-privacidad.pdf"
}

export enum pdfBienvenida {
    pdfEN ="/recursos-sistema/mail/bienvenido-EN.pdf",
    pdfES= "/recursos-sistema/mail/bienvenido-ES.pdf",
    pdfFR= "/recursos-sistema/mail/bienvenido-FR.pdf",
    pdfDE= "/recursos-sistema/mail/bienvenido-DE.pdf",
    pdfIT = "/recursos-sistema/mail/bienvenido-IT.pdf",
    pdfPT= "/recursos-sistema/mail/bienvenido-PT.pdf"
}

export enum pdfBienvenidaNombre {
    pdfEN ="IMPORTANT INFORMATION.pdf",
    pdfES= "INFORMACIÓN IMPORTANTE.pdf",
    pdfFR= "INFORMATIONS IMPORTANTES.pdf",
    pdfDE= "WICHTIGE INFORMATIONEN.pdf",
    pdfIT = "IMPORTANTI INFORMAZIONI.pdf",
    pdfPT= "INFORMAÇÕES IMPORTANTES.pdf"
}

