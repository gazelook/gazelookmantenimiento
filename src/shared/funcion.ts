import { CatalogoEstadoService } from 'src/entidades/catalogos/casos_de_uso/catalogo-estado.service';
import { CatalogoEntidadService } from 'src/entidades/catalogos/casos_de_uso/catalogo-entidad.service';
import { estadoDeMensaje, estadosPartAso, idiomas, numeroDias } from "./enum-sistema";
import { RespuestaInterface } from "./respuesta-interface";
import { I18nService } from 'nestjs-i18n';

export class Funcion {

    constructor(
        private readonly i18n?: I18nService,
        private readonly catalogoEntidadService?: CatalogoEntidadService,
        private readonly catalogoEstadoService?: CatalogoEstadoService
    ) { }

    numeroDia = numeroDias;
    
    obtenerIdiomaDefecto(idioma) {
        if (idioma) {
            return idioma
        }
        return idiomas.ingles;
    }

    enviarRespuesta(codigoEstado, mensaje?, datos?) {
        const respuesta = new RespuestaInterface;

        respuesta.codigoEstado = codigoEstado;
        respuesta.respuesta = {
            mensaje: mensaje,
            datos: datos
        }

        return respuesta;
    }
    enviarRespuestaOptimizada(resp) {
        const respuesta = new RespuestaInterface;
        respuesta.codigoEstado = resp.codigoEstado;
        if (resp.mensaje && !resp.datos) {
            respuesta.respuesta = {
                mensaje: resp.mensaje,
            }

        }
        if (resp.datos && !resp.mensaje) {
            respuesta.respuesta = {
                datos: resp.datos,
            }
        }
        if (resp.datos && resp.mensaje) {
            respuesta.respuesta = {
                mensaje: resp.mensaje,
                datos: resp.datos,
            }
        }
        if (resp.mensaje && resp.token) {
            respuesta.respuesta = {
                mensaje: resp.mensaje,
                token: resp.token
            }
        }
        return respuesta;

    }

    seleccionarFiltroMensaje(filtro) {
        switch (filtro) {
            case estadoDeMensaje.enviado:
                return 'CTM_1';
            case estadoDeMensaje.entregado:
                return 'CTM_2';
            case estadoDeMensaje.leido:
                return 'CTM_3';
            case estadoDeMensaje.todos:
                return 1;
            default:
                return 1;
        }
    }

    verificarEstadoAsociacion(filtro) {
        switch (filtro) {
            case estadosPartAso.cancelada:
                return true;
            case estadosPartAso.enviada:
                return true;
            case estadosPartAso.contacto:
                return true;
            case estadosPartAso.rechazada:
                return true;
            case estadosPartAso.eliminado:
                return true;
            default:
                return false;
        }
    }

    async obtenerTraduccionEstatica(codigoIdioma, codigoMensaje) {

        return await this.i18n.translate(codigoIdioma.concat('.').concat(codigoMensaje), {
            lang: codigoIdioma
        });
    }

    async obtenerCatalogoEstadoEntidad(nombreEntidad, nombreEstado) {
        const entidad = await this.catalogoEntidadService.obtenerNombreEntidad(nombreEntidad);
        const estado = await this.catalogoEstadoService.obtenerNombreEstado(nombreEstado, entidad.codigo);

        return {
            catalogoEntidad: entidad,
            catalogoEstado: estado
        };
    }

    async codigoAutoincrementableCatalogoRol(getUltimoCatRol) {

        let codigoUlt;
        //Obtiene el codigo del ultimo anuncio
        let codigoAnuncio;
        if (getUltimoCatRol && getUltimoCatRol.codigo) {
            codigoUlt = getUltimoCatRol.codigo;
            //Obtencion de solo numeros en la cadena
            let regex = /(\d+)/g;
            let ultimoNumeroAnuncio = codigoUlt.match(regex);
            ultimoNumeroAnuncio = parseInt(ultimoNumeroAnuncio[0]);
            console.log('ultimoNumeroAnuncio: ', ultimoNumeroAnuncio)

            codigoAnuncio = 'CATROL_'.concat(ultimoNumeroAnuncio)

            return codigoAnuncio;
        }
    }

    round(num) {
        var m = Number((Math.abs(num) * 100).toPrecision(15));
        return Math.round(m) / 100 * Math.sign(num);
    }

    seleccionarDia(numeroDia) {
        switch (numeroDia) {
    
          case this.numeroDia.lunes:
            return 0;
          case this.numeroDia.martes:
            return 1;
          case this.numeroDia.miercoles:
            return 2;
          case this.numeroDia.jueves:
            return 3;
          case this.numeroDia.viernes:
            return 4;
          case this.numeroDia.sabado:
            return 5;
          case this.numeroDia.domingo:
            return 6;
          default:
            return 0;
        }
      }

}