export enum filtroBusqueda {
  NUEVOS_PROYECTO = 'nuevos_proyectos',
  MENOS_VOTADOS = 'menos_votados',
  MAS_VOTADOS = 'mas_votados',
  VOTO = 'voto',
  FECHA = 'fecha'
}


export enum idiomas {
  espanol = 'es',
  ingles = 'en',
  aleman = 'de',
  frances = 'fr',
  italiano = 'it',
  portugues = 'pt'
}

export enum codIdiomas {
  espanol = 'IDI_1',
  ingles = 'IDI_2',
  aleman = 'IDI_3',
  portugues = 'IDI_4',
  frances = 'IDI_5',
  italiano = 'IDI_6',
}

// leidos de mensajes
export enum estadoDeMensaje {
  enviado = 'enviado',
  entregado = 'entregado',
  leido = 'leido',
  todos = 'todos'
}
export enum formulasEvento {
  TOTAL_VOTOS = 0,
  PORCENTAJE_VOTO_GAZELOOK = 0.16
}


//Codigos de roles
export enum codigosRol {
  //Codigo de rol de propietario
  cod_rol_ent_propietario = "CATROL_4",
  //Codigo de rol de CoAutor
  cod_rol_ent_co_autor = "CATROL_2",
  //Codigo de rol de usuario del sistema
  cod_rol_usuario_sistema = "CATROL_5",
  //Codigo rol del coordinador gazelook
  cod_rol_coordinador_gazelook = "CATROL_3",
}


//codigo de los roles del sistema
export enum codigosRolSistema {
  cod_rol_sis_gazelook = 'CATROL_3'

}

//Codigos de las configuraciones de eventos
export enum codigosConfEvento {
  cod_configuracion_evento_proyecto = 'CONFEVT_1'

}

//Codigos de las catalogo tipo votos
export enum codigosCatTipoVoto {
  cod_cat_tipo_voto_normal = "CTVOTO_1",
  cod_cat_tipo_voto_admin = "CTVOTO_3"
}

//Codigos de TipoPerfil
export enum codigosTipoPerfil {
  //Codigo perfil grupo
  perfil_grupo = "TIPERFIL_4",
}

//Codigos de TipoPerfil
export enum estadosPerfil {
  //Codigo
  hibernado = "EST_19",
  activa = "EST_20",
  creado = "EST_88",
  eliminado = "EST_89"
}

export enum estadosUsuario {
  //Codigo
  activa = "EST_1",
  inactivaPago = "EST_2",
  bloqueado = "EST_3",
  bloqueoContrasena = "EST_4",
  activaNoVerificado = "EST_5",
  eliminado = "EST_6",
  bloqueadoSistema = "EST_7",
  rechazadoResponsable = "EST_196",
  bloqueadoRefund = "EST_282",
  noPermitirAcceso = "EST_283",
  cripto = 'EST_284',
  inactivaPagoPaymentez = 'EST_291'
}

export enum estadosTransaccion {
  //Codigo
  activa = "EST_8",
  eliminada = "EST_9",
  cancelada = "EST_10",
  rechazada = "EST_11",
  pendiente = "EST_80"
}

export enum codigosMetodosPago {
  //Codigo
  stripe = 'METPAG_1',
  payments1 = 'METPAG_3',
  payments2 = 'METPAG_6',
  panama = 'METPAG_7',
  cripto = 'METPAG_5',
}

export enum codigosArchivosSistema {
  //Codigo
  archivo_default_album_general = "CAT_ARC_DEFAULT_01",
  archivo_default_album_perfil = "CAT_ARC_DEFAULT_02",
  archivo_default_proyectos = "CAT_ARC_DEFAULT_03",
  archivo_default_noticias = "CAT_ARC_DEFAULT_04",
  archivo_default_contactos = "CAT_ARC_DEFAULT_05",
}

export enum codigoCatalogosTipoEmail {
  //Codigo
  validacion_correo_normal = "CTEM_1",
  validacion_correo_responsable = "CTEM_2",
  transferir_proyecto = "CTEM_3",
  recuperar_contrasenia = "CTEM_4",
  actualizacion_contrasenia = "CTEM_5",
  peticion_datos = "CTEM_6",
  eliminacion_datos = "CTEM_7",
  menor_edad_cambio_email = "CTEM_8",
  descargar_informacion_usuario = "CTEM_9",
  notificacionEstadoSuscripcion = "CTEM_11",
  confirmar_actualizacion_email = 'CTEM_12',
  enviar_recibo_pago_email = 'CTEM_13',
  contact_gazelook_email = 'CTEM_14'
}

export enum propiedadesPerfil {
  //Codigo
  pensamientos = "pensamientos",
  noticias = "noticias",
  proyectos = "proyectos",
  asociaciones = "asociaciones",
  telefonos = "telefonos",
  direcciones = "direcciones",
  album = "album"
}

export enum estadoActivoEntidades {
  //Codigo
  pensamientos = "EST_22",
  traduccionPensamientos = "EST_86",
  noticias = "EST_53",
  traduccionNoticias = "EST_59",
  proyectos = "EST_110",
  traduccionProyectos = "EST_76",
  asociaciones = "EST_35",
  telefonos = "EST_29",
  direcciones = "EST_32",
  album = "EST_47",
  perfiles = "EST_20",
  traduccionMedia = "EST_49",
  media = "EST_24",
  participante_proyecto = "EST_153",
  voto_proyecto = "EST_119",
  voto_noticia = "EST_68"
}

export enum estadosPartAso {
  enviada = 'EST_96',
  eliminado = 'EST_93',
  rechazada = 'EST_95',
  cancelada = 'EST_94',
  contacto = 'EST_159',
}

// codigos de catalogo historico
export enum codigosCatalogoHistorico {
  default = 'CATHIS_01',
  solicitudInformacion = 'CATHIS_02',
  solicitudRetiro = 'CATHIS_03',
  reparticionFondos = 'CATHIS_04',
}

export enum nombreEntidades {
  usuarios = 'usuarios',
  proyectos = 'proyectos',
  noticias = 'noticias',
  pensamientos = 'pensamientos',
  comunicacion = 'comunicacion',
  transaccion = 'transaccion',
  compras = 'compras',
  perfiles = 'perfiles',
  contactos = 'contactos',
  comentarios = 'comentarios',
  busquedas = 'busquedas',
  media = 'media',
  dispositivo = 'dispositivo',
  email = 'email',
  metodoPago = 'metodoPago',
  tipoMoneda = 'tipoMoneda',
  origen = 'origen',
  beneficiario = 'beneficiario',
  archivo = 'archivo',
  telefono = 'telefono',
  direccion = 'direccion',
  asociacion = 'asociacion',
  participanteAsociacion = 'participanteAsociacion',
  conversacion = 'conversacion',
  mensaje = 'mensaje',
  configuracion = 'configuracion',
  votoProyecto = 'votoProyecto',
  participanteProyecto = 'participanteProyecto',
  estrategia = 'estrategia',
  configuracionEvento = 'configuracionEvento',
  evento = 'evento',
  formulaEvento = 'formulaEvento',
  suscripcion = 'suscripcion',
  catalogoSuscripcion = 'catalogoSuscripcion',
  catalogoTipoMedia = 'catalogoTipoMedia',
  album = 'album',
  traduccionMedia = 'traduccionMedia',
  catalogoTipoEmail = 'catalogoTipoEmail',
  catalogoPais = 'catalogoPais',
  catalogoLocalidad = 'catalogoLocalidad',
  traduccionNoticia = 'traduccionNoticia',
  catalogoMedia = 'catalogoMedia',
  catalogoArchivoDefault = 'catalogoArchivoDefault',
  catalogoTipoPerfil = 'catalogoTipoPerfil',
  votoNoticia = 'votoNoticia',
  traduccionVotoNoticia = 'traduccionVotoNoticia',
  catalogoTipoProyecto = 'catalogoTipoProyecto',
  traduccionCatalogoTipoProyecto = 'traduccionCatalogoTipoProyecto',
  traduccionProyecto = 'traduccionProyecto',
  catalogoAlbum = 'catalogoAlbum',
  catalogoIdiomas = 'catalogoIdiomas',
  traduccionPensamientos = 'traduccionPensamientos',
  catalogoTipoAsociacion = 'catalogoTipoAsociacion',
  traduccionMensajes = 'traduccionMensajes',
  catalogoMensaje = 'catalogoMensaje',
  catalogoTipoComentario = 'catalogoTipoComentario',
  traduccionComentario = 'traduccionComentario',
  traduccionVotoProyecto = 'traduccionVotoProyecto',
  balance = 'balance',
  rolSistema = 'rolSistema',
  catalogoTipoRol = 'catalogoTipoRol',
  catalogoConfiguracion = 'catalogoConfiguracion',
  catalogoTipoVoto = 'catalogoTipoVoto',
  catalogoRol = 'catalogoRol',
  proyectoEjecucion = 'proyectoEjecucion',
  traduccionProyectoEjecución = 'traduccionProyectoEjecución',
  catalogoColores = 'catalogoColores',
  catalogoTipoColores = 'catalogoTipoColores',
  catalogoEvento = 'catalogoEvento',
  rolEntidad = 'rolEntidad',
  configuracionEstilo = 'configuracionEstilo',
  configuracionPersonalizada = 'configuracionPersonalizada',
  configuracionPredeterminada = 'configuracionPredeterminada',
  estilos = 'estilos',
  catalogoEstilos = 'catalogoEstilos',
  CatalogoPaticipanteConfiguracion = 'CatalogoPaticipanteConfiguracion',
  configuracionParticipante = 'configuracionParticipante',
  tokenUsuario = 'tokenUsuario',
  documentosLegales = 'documentosLegales',
  catalogoOrigenDocumento = 'catalogoOrigenDocumento',
  catalogoHistorico = 'catalogoHistorico',

}

//Nombre de acciones
export enum nombreAcciones {
  ver = 'ver',
  modificar = 'modificar',
  eliminar = 'eliminar',
  crear = 'crear'
}

//Nombre de catalogo Estados
export enum nombrecatalogoEstados {
  activa = 'activa',
  inactivaPago = 'inactivaPago',
  bloqueado = 'bloqueado',
  bloqueoContrasena = 'bloqueoContrasena',
  activaNoVerificado = 'activaNoVerificado',
  eliminado = 'eliminado',
  bloqueadoSistema = 'bloqueadoSistema',
  rechazadoResponsable = 'rechazadoResponsable',
  hibernado = 'hibernado',
  historico = 'historico',
  retiroUsuario = 'retiroUsuario',
  pendiente = 'pendiente',
  contacto = 'contacto',
  enviada = 'enviada',
  aceptada = 'aceptada',
  sinAsignar = 'sinAsignar',
  rechazoEmail = 'rechazoEmail',
  confirmadoEmail = 'confirmadoEmail'
}

// nombre catalogo tipo email 
export enum nombreCatalogoTipoEmail {
  validacion = 'validacion',
  validacionResponsable = 'validacionResponsable',
  trasferirProyecto = 'trasferirProyecto',
  recuperarContrasenia = 'recuperarContrasenia',
  actualizacionContrasenia = 'actualizacionContrasenia',
  peticionDatos = 'peticionDatos',
  eliminacionDatos = 'eliminacionDatos',
  notificacionUsuario = 'notificacionUsuario',
  confirmarActualizacionEmail = 'confirmarActualizacionEmail'
}

// catalogo dispositivo
export enum estadosDispositivo {
  activo = 'EST_45',
  eliminado = 'EST_46'
}

export enum codigosEstadosMedia {
  activa = 'EST_24',
  eliminado = 'EST_25',
  sinAsignar = 'EST_26'
}


export enum servidor {
  path_descargar_informacion = "/api/informacion-usuario",
  path_eliminar_datos_usuario = "/api/usuario/eliminar-datos-usuario-completo",
  path_eliminar_cuenta_usuario = "/api/usuario/eliminar-cuenta-usuario",
  path_transferir_proyecto = "/api/transferencia-proyecto-ok",
  path_actualizar_cambio_email = "/api/usuario/actualizar-cambio-email"
}

export enum codigoDispositivo {
  codigo = 'DIS'
}

export enum codigosEstadosCatalogoPorcentajeProyecto {
  activa = "EST_211",
  eliminado = "EST_212"
}

export enum codigosEstadosCatalogoPorcentajeFinanciacion {
  activa = "EST_213",
  eliminado = "EST_214"
}

export enum codigosEstadosBonificacion {
  activa = "EST_219",
  eliminado = "EST_220"
}

export enum codigosEstadosCatalogoPorcentajeEsperaFondos {
  activa = "EST_221",
  eliminado = "EST_222"
}

export enum codigosEstadosFondosTipoProyecto {
  activa = "EST_217",
  eliminado = "EST_218"
}

export enum codigosEstadosFondosFinanciacion {
  activa = "EST_215",
  eliminado = "EST_216"
}

export enum codigosEstadosBeneficiario {
  activa = "EST_16",
  eliminado = "EST_18"
}

export enum filtroBusquedaGastoOperacional {
  MUNDIAL = 'mundial',
  PAIS = 'pais',
  LOCALIDAD = 'localidad'
}

export enum codigosEstadosFinanciamientoEsperaFondos {
  activa = "EST_223",
  eliminado = "EST_224"
}

export enum codigosCatalogoRol {
  administrador = "CATROL_1",
  coautor = "CATROL_2",
  coordinadorGazelook = "CATROL_3",
  propietario = "CATROL_4",
  usuario = "CATROL_5",
  estrategas = "CATROL_6",
  lector = "CATROL_7",
  escritor = "CATROL_8",
  administradorAnuncios = "CATROL_9",
  administradorTraductorAnuncios = "CATROL_10",
  administradorGeneralAnuncios = "CATROL_12",
  subAdministradorGeneral = "CATROL_13",
  contabilidad = "CATROL_14",
  administradorFinca = "CATROL_15",
  administradorProgramadores = "CATROL_16",
  subAdministradorProgramadores = "CATROL_17",
  administradorVoluntariado = "CATROL_18",
  alojamientoPersonalOficina = "CATROL_19"
}

export enum nombreRolSistema {
  usuarioNormal = "Usuario normal",
  coordinadorGazelook = "Coordinador Gazelook",
  administradorGazelook = "Administrador Gazelook",
  administradorAnunciosGazelook = "Administrador Anuncios Gazelook",
  administradorTraductorAnunciosGazelook = "Administrador Traductor de Anuncios Gazelook"

}

export enum codigoEntidades {
  entidadUsuarios = 'ENT_1',
  entidadProyectos = 'ENT_2',
  entidadNoticias = 'ENT_3',
  entidadPensamientos = 'ENT_4',
  entidadTransaccion = 'ENT_6',
  entidadPerfiles = 'ENT_8',
  entidadcontactos = 'ENT_9',
  entidadComentarios = 'ENT_10',
  entidadMedia = 'ENT_12',
  entidadDispositivo = 'ENT_13',
  entidadBeneficiario = 'ENT_18',
  entidadTelefono = 'ENT_20',
  entidadDireccion = 'ENT_21',
  entidadAsociacion = 'ENT_22',
  entidadParticipanteAsociacion = 'ENT_23',
  entidadConversacion = 'ENT_24',
  entidadMensaje = 'ENT_25',
  entidadVotoProyecto = 'ENT_31',
  entidadParticipanteProyecto = 'ENT_32',
  entidadEstrategia = 'ENT_34',
  entidadSuscripcion = 'ENT_38',
  entidadAlbum = 'ENT_42',
  entidadCatalogoLocalidad = 'ENT_46',
  entidadTraduccionNoticia = 'ENT_47',
  entidadVotoNoticia = 'ENT_51',
  entidadTraduccionProyecto = 'ENT_55',
  fondosFinanciamiento = 'ENT_95',
  fondosTipoProyecto = 'ENT_96',
  entidadBonificacion = 'ENT_97',
  intercambio = 'ENT_100',
  traduccionIntercambio = 'ENT_101',
  catalogoTipoIntercambio = 'ENT_102',
  traduccionCatalogoTipoIntercambio = 'ENT_103',
  entidadNotificacionesFirefase = 'ENT_107',
  entidadCatalogoRol = 'ENT_70',
  entidadRolEntidad = 'ENT_76',
  entidadRolSistema = 'ENT_66',
}

export enum estadosProyecto {
  proyectoActivo = "EST_110",
  proyectoEliminado = "EST_111",
  proyectoPreEstrategia = "EST_112",
  proyectoEnEsperaFondos = "EST_113",
  proyectoEnEstrategia = "EST_114",
  proyectoEnRevision = "EST_115",
  proyectoEnEjecucion = "EST_116",
  proyectoEnEsperaDonacion = "EST_117",
  proyectoFinalizado = "EST_118",
  proyectoForo = "EST_195"
}

export enum codigosTipoProyectos {
  gazelook = "CAT_TIPO_PROY_03",
  local = "CAT_TIPO_PROY_02",
  pais = "CAT_TIPO_PROY_04",
  mundial = "CAT_TIPO_PROY_01"
}

export enum estadoNotificacionesFirebase {
  activa = 'EST_241',
  eliminado = 'EST_242'
}

export enum tipoNotificacionFirebase {
  usuario = 'TIPNOTFIR_1',
  sistema = 'TIPNOTFIR_2'
}

export enum accionNotificacionFirebase {
  ver = 'ACCNOTFIR_1',
  actualizarTokenDispositivos = 'ACCNOTFIR_2'
}

export enum expiracionToken {
  dias = '10d',
  segundos = '432000s'
}
export enum codigosEstadoCatalogoRol {
  activa = 'EST_137',
  eliminado = 'EST_138'
}

export enum codigosEstadoRolEntidad {
  activa = 'EST_157',
  eliminado = 'EST_158'
}

export enum codigosEstadoRolSistema {
  activa = 'EST_131',
  eliminado = 'EST_132'
}

export enum codigosEstadoAlbum {
  activa = 'EST_82',
  eliminado = 'EST_83'
}

export enum catalogoOrigen {
  suscripciones = 'CATORI_1',
  gastos_operativos = 'CATORI_2',
  donacion = 'CATORI_3',
  ganancia_proyecto = 'CATORI_4',
  asignacion_fondo = 'CATORI_5',
  valor_extra = 'CATORI_6',
  fondos_reservados = 'CATORI_7',
  monto_sobrante = 'CATORI_8',
  fondos_reservados_fiscalizacion_proyectos = 'CATORI_9',
  fondos_reservados_gazelook = 'CATORI_10',
}
export enum descripcionTransPagosGazelook {
  suscripcionDeAlta = 'Suscripción de alta',
  suscripcion = 'Suscripción',
  gastoOperacional = 'Gasto operativo',
  valorExtra = 'Valor extra',
  fondosReservados = 'Fondos reservados',
  montoSobrante = 'Monto sobrante de la última repartición de fondos',
  fondosReservadosFiscalizacionProyectos = 'Fondos reservados para la fizcalizacion de los proyectos'
}

// datos pago stripe
export enum pagoStripe {
  currency = 'USD',
  payment_method_types = 'card',
  apiVersion = '2020-03-02'
}

export enum nombreEstadosPagoStripe {
  succeeded = 'succeeded',
  COMPLETED = 'COMPLETED'
}

export enum contactEmails {
  email_es = 'contactes@gazelook.com',
  email_de = 'contactde@gazelook.com',
  email_fr = 'contactfr@gazelook.com',
  email_pt = 'contactpt@gazelook.com',
  email_en = 'contacten@gazelook.com',
  email_it = 'contactit@gazelook.com'
}

export enum estadoMetodosPago {
  activa = 'EST_14',
  desactivado = 'EST_63',
  eliminado = 'EST_15'
}

export enum estadosTraduccionMetodosPago {
  activa = 'EST_276',
  eliminado = 'EST_277'
}

export enum numeroDias {
  domingo = 0,
  lunes = 1,
  martes = 2,
  miercoles = 3,
  jueves = 4,
  viernes = 5,
  sabado = 6
}

//Codigo de estados comentarios
export enum codigoEstadosComentario {
  activa = 'EST_105',
  eliminado = 'EST_106',
  historico = 'EST_107'
}

export enum codigosEstadosPartAsociacion {
  enviada = 'EST_96',
  eliminado = 'EST_93',
  rechazada = 'EST_95',
  aceptada = 'EST_92',
  cancelada = 'EST_94',
  contacto = 'EST_159',
  hibernado = 'EST_193',
}

export enum filtroBusquedaAportaciones {
  suscripcion = 'suscripcion',
  valorExtra = 'valorExtra',
}

export enum filtroBusquedaAportacionesMetodoPago {
  stripe = 'stripe',
  paymentez = 'paymentez',
  cripto = 'cripto',
  panama = 'panama'
}

//tipo beneficiario
export enum beneficiario {
  usuario = 'BENEF_1',
  proyecto = 'BENEF_2',
}

export enum codigosCatalogoAcciones {
  modificar = 'ACC_1',
  crear = 'ACC_4',
  ver = 'ACC_1',
  eliminar = 'ACC_3',
}

export enum porcentajeComisionPaymentez {
  porcentaje = 0.045,
}

export enum porcentajeComisionCoinpaiments {
  porcentaje = 0.05,
}