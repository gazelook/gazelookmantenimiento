import {
    ArgumentsHost, Catch, HttpException,
    HttpStatus,
    Logger
} from '@nestjs/common';
import { BaseExceptionFilter } from '@nestjs/core';
import { appendFile } from 'fs';
import { I18nService } from 'nestjs-i18n';
import { pathFile } from '../path';
import { RespuestaInterface } from '../respuesta-interface';
@Catch()
export class HttpErrorFilter extends BaseExceptionFilter {

    constructor(private readonly i18n: I18nService) {
        super();

    }
    async catch(exception: HttpException, host: ArgumentsHost) {
        
        const respuesta = new RespuestaInterface;
        const ctx = host.switchToHttp();
        const request = ctx.getRequest();
        const response = ctx.getResponse();
        const status =
            exception instanceof HttpException
                ? exception.getStatus()
                : HttpStatus.INTERNAL_SERVER_ERROR;
        //const status = exception.getStatus();
        const date = new Date();
        let errorResponse = {
            status: status,
            timestamp: new Date().toISOString(),
            path: request.url,
            ip: request.ip,
            browser: request.get('User-Agent'),
            method: request.method,
            message: exception || null,
        }
        response.set(errorResponse)
        
        Logger.error(`${request.method} ${request.url}`,
            JSON.stringify(errorResponse),
            'ExceptionFilter',
        );
        appendFile(pathFile,
            `\n[${date}] [EXCEPTION] [${errorResponse.status}] [${request.method}] Request recibida a ${request.path}, IP => ${errorResponse.ip}, Browser => ${errorResponse.browser}, ${JSON.stringify(errorResponse)}`,
            (error) => {
                if (error) {
                    throw (error)
                }
            });

            // console.log('errorResponse----------------------->: ', errorResponse)

        if (errorResponse.status == 401) {
            let codIdioma = request.headers.idioma
            // console.log('codIdioma: ', codIdioma)
            if (!codIdioma) {
                codIdioma = 'en'
            }
            const NO_AUTORIZADO = await this.i18n.translate(codIdioma.concat('.NO_AUTORIZADO'), {
                lang: codIdioma
            });
            respuesta.codigoEstado = HttpStatus.UNAUTHORIZED;
            respuesta.respuesta = {
                mensaje: NO_AUTORIZADO //'UNAUTHORIZED'
            }
            return response.send(respuesta);
        } else {
            response.status(status).json(errorResponse);
        }


    }

}