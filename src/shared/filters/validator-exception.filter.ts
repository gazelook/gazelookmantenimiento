import { ArgumentsHost, BadRequestException, Catch, ExceptionFilter, HttpException } from "@nestjs/common";
import { Response } from 'express';
import { ResponseValidators } from "../models/response-validators";

// Captura las excepciones de class validator
@Catch(BadRequestException)
export class ValidationExceptionFilter implements ExceptionFilter<BadRequestException> {
  public catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp()
    const response = ctx.getResponse() as Response
    
    const exceptionResponse: any = exception.getResponse();
    const responseValidators: ResponseValidators = exceptionResponse;

    response
      .status(responseValidators.statusCode || exception.getStatus())
      .json({
        codigoEstado: responseValidators.statusCode || exception.getStatus(),
        respuesta: {
          mensaje: responseValidators.message,
        }

      });

  }
}