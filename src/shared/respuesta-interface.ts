import { HttpStatus, Headers } from '@nestjs/common';
export class RespuestaInterface {
    codigoEstado: HttpStatus;
    respuesta: Respuesta;
    headers?: Headers;
}

export class Respuesta {
    datos?: any;
    mensaje?: string;
    token?: string;
    tokenRefresh?: string;
}