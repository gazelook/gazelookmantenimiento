import { RespuestaInterface } from 'src/shared/respuesta-interface';
import { PipeTransform, Injectable, ArgumentMetadata, BadRequestException, ValidationPipe, UnprocessableEntityException, Catch, ExceptionFilter, ArgumentsHost, HttpStatus } from '@nestjs/common';
import { validate } from 'class-validator';
import { plainToClass } from 'class-transformer';
import { Response } from 'express';



@Injectable()
export class MyValidationPipe implements PipeTransform<any> {
  async transform(value: any, { metatype }: ArgumentMetadata) {
    if (!metatype || !this.toValidate(metatype)) {
      return value;
    }
    const object = plainToClass(metatype, value);
    const errors = await validate(object);
    if (errors.length > 0) {
      throw new BadRequestException('Validation failed....');
    }
    return value;
  }

  private toValidate(metatype: Function): boolean {
    const types: Function[] = [String, Boolean, Number, Array, Object];
    return !types.includes(metatype);
  }
}

export class ValidationPipe422 extends ValidationPipe {
    public async transform (value, metadata: ArgumentMetadata) {
      try {
        return await super.transform(value, metadata)
      } catch (e) {
        if (e instanceof BadRequestException) {
          throw new UnprocessableEntityException(e.message)
        }
      }
    }
  }
