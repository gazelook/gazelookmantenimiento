# BACKEND GAZELOOK
  Wep API-REST, for system administration and execution of secondary processes of the main API.

## TECHNOLOGIES
  The technologies applied to the project are:
  - Nesjts CLI: 7.4.1
  - Node: 13.8.0
  - Nestjs: 7.0.0
  - Typescript: 3.7.4
  - Mongo: 4.4.5
  - AWS S3


## INSTALACIÓN
To set up the project, it is necessary to download and install the following tools:

- NODE JS 13.X (Incluye NPM)

  Windows: https://nodejs.org/es/download/

  Linux:
  ```
  curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
  ```
  ```
  sudo apt-get install -y nodejs
  ```

- GIT

  Windows: https://git-scm.com/downloads

  Linux:
  ```
  apt-get install git
  ```

- CLI Nestjs

  windows && Linux:
  ```
  npm i -g @nestjs/cli
  ```

Download the project from the terminal.

  windows && Linux:
  ```
  git clone https://byjose007@bitbucket.org/gazelookspace/apigaze.git
  ```
  Una vez clonado el repositorio, nos creamos nuestra rama partiendo de master 

  Descargar las dependencias.
  ```
  npm install
  ```

Before executing the project

Create database `dbname` and import data from the folder gazelookQa

Execute the following script
```
mongorestore --uri dbhost -d dbname dbdirectory

Ejm:
mongorestore --uri mongodb+srv://gazelook:<PASSWORD>@cluster0.89jao.mongodb.net -d gazelookQa C:\repaldoBD\gazelookQa

```



## EXECUTION

From the project folder, execute the command according to your needs.

Before executing check the connection string in the configsEnv\qa.env path file.

Just change `MONGO_URI` to your connection string if necessary.

```bash

# watch mode
$ npm run start:Qa

```

  If we navigate to `http://localhost:5000/api/` we can see our server running.


## STRUCTURE

The project is structured by folders as follows:
      doc/
         Documents and prerequisites to execute the project
      src/
        |-- drivers/
                    Here should go main configurations and elements, which will be used in the whole application globally
            |-- [+] logging/
                            Config for managing system logs
            |-- [+] mongoose/
                        Mongo models and schemas
            |-- [+] interfaces/
            |-- [+] models/

         |-- [+] Entities/
            |-- [+] Entity (User)/
                    |-- [+] drivers/
                               Set of drivers that convert data from the most convenient format for use cases and entities to the most convenient format for some external elements such as the database or the web.
                    |-- [+] use_cases/
                                Business rules (services) that define how our system behaves, defining the necessary input data, and what will be its output.
                    |-- [+] entity/
                                Entities encapsulate the business rules of the entire enterprise. An entity can be an object with methods or it can be a set of data and functions.
                    |-- [+] drivers/
                                The outermost layer is generally composed of frameworks and tools such as the database, the web framework, etc.

## Git Best Practices Guide

### Introduction

The purpose of this section is to have a common wiki with the best practices that we have to follow to keep the project
we have to follow to keep the project tidy.


## Nomenclature

Directories : my_directory_name (use_cases)
files: filename.type.ts (my_user.service.ts)
variables: myVariable (userProfile) 
classes: MyClass (UserProfile)
functions: myFunction (getUsers)
constants: MY_CONSTANT (PROFILE_TYPE)
API URL: /get-profile-user/id

## Documentation endpoint

http verbs describe the actions by themselves 

GET /users- Returns a list of users
GET /users/12- Returns a specific user
POST /users- Creates a new user
PUT /users/12- Updates user #12
PATCH /users/12- Partially updates user #12
DELETE /users/12- Deletes user #12






## API dynamic translations

Install environment variable on server or local

Open command prompt:

Windows

1. `setx TRANSLATOR_TEXT_SUBSCRIPTION_KEY "4ef8d7e4672245e4bbf8a37d1fc909a8"`

2. `setx TRANSLATOR_TEXT_ENDPOINT https://api.cognitive.microsofttranslator.com`

Linux o Mac/os:

1. `export TRANSLATOR_TEXT_SUBSCRIPTION_KEY=4ef8d7e4672245e4bbf8a37d1fc909a8`

2. `export TRANSLATOR_TEXT_ENDPOINT=https://api.cognitive.microsofttranslator.com`

3. `source ~/.bashrc`

To use the translation api we have created a global method that will receive as parameters the language code sent for example: `es`, `en`, `fr` etc. and also the text to be translated.

So that it can be called every time a dynamic translation is required, it is located in /src/multiLanguage/translation-dynamic.

To import it :
`import { translateText } from 'src/multiLanguage/translation-dynamic'`.

To use it you can call it for example:
`const textTranslateText= await translateText(language, text);`

The preferred language will be sent through the header.

As a reference of the logic that must be applied each time it is required to be used, it can be referenced in the entity `thoughts` in the use case `get-thoughts.service`.

It should be noted that the translation api will only be called if the translation is not stored to avoid wasting unnecessary api resources.

Languages allowed by the API

    ┌─────────────────────────┬─────────────────┐
    │ Language                │ Language code   │
    │ Afrikaans               │ af              │
    │ Arabic                  │ ar              │
    │ Bangla                  │ bn              │
    │ Bosnian(Latin)          │ bs              │
    │ Bulgarian               │ bg              │
    │ Cantonese (Traditional) │ yue             │
    │ Catalan                 │ ca              │
    │ Chinese Simplified      │ zh-Hans         │
    │ Chinese Traditional     │ zh-Hant         │
    │ Croatian                │ hr              │
    │ Czech                   │ cs              │
    │ Danish                  │ da              │
    │ Dutch                   │ nl              │
    │ English                 │ en              │
    │ Estonian                │ et              │
    │ Fijian                  │ fj              │
    │ Filipino                │ fil             │
    │ Finnish                 │ fi              │
    │ French                  │ fr              │
    │ German                  │ de              │
    │ Greek                   │ el              │
    │ Gujarati                │ gu              │
    │ Haitian Creole          │ ht              │
    │ Hebrew                  │ he              │
    │ Hindi                   │ hi              │
    │ Hmong Daw               │ mww             │
    │ Hungarian               │ hu              │
    │ Icelandic               │ is              │  
    │ Indonesian              │ id              │
    │ Irish                   │ ga              │
    │ Italian                 │ it              │
    │ Japanese                │ ja              │
    │ Kannada                 │ kn              │
    │ Kazakh                  │ kk              │
    │ Kiswahili               │ sw              │
    │ Klingon                 │ tlh-Latn        │
    │ Klingon (plqaD)         │ tlh-Piqd        │
    │ Korean                  │ ko              │
    │ Latvian                 │ lv              │
    │ Lithuanian              │ lt              │
    │ Malagasy                │ mg              │
    │ Malay                   │ ms              │
    │ Malayalam               │ ml              │
    │ Maltese                 │ mt              │
    │ Maori                   │ mi              │
    │ Marathi                 │ mr              │
    │ Norwegian               │ nb              │
    │ Persian                 │ fa              │
    │ Polish                  │ pl              │
    │ Portuguese (Brazil)     │ pt-br           │
    │ Portuguese (Portugal)   │ pt-pt           │
    │ Punjabi                 │ pa              │
    │ Queretaro Otomi         │ otq             │      
    │ Romanian                │ ro              │
    │ Russian                 │ ru              │
    │ Samoan                  │ sm              │
    │ Serbian (Cyrillic)      │ sr-Cyrl         │
    │ Serbian (Latin)         │ sr-Latn         │
    │ Slovak                  │ sk              │
    │ Slovenian               │ sl              │
    │ Spanish                 │ es              │
    │ Swedish                 │ sv              │
    │ Tahitian                │ ty              │
    │ Tamil                   │ ta              │
    │ Telugu                  │ te              │
    │ Thai                    │ th              │
    │ Tongan                  │ to              │
    │ Turkish                 │ tr              │
    │ Ukrainian               │ uk              │
    │ Urdu                    │ ur              │
    │ Vietnamese              │ vi              │
    │ Welsh                   │ cy              │
    │ Yucatec Maya            │ yua             │
    └─────────────────────────┴─────────────────┘

## Static translations with i18n(internationalization)

For static translations you must import:

`import { StaticTranslationController } from 'src/multiLanguage/controllers/static-translation-controller'`.

Then call it in the constructor of the controller where you want the translation, Example:

  `constructor( private readonly translationStaticController: TranslationStaticController)`.

Then it is called in the following way, example:

`const FALLO_DEVOLUTION_THOUGHTS= await this.traduccionEstaticaController.traduccionEstatica(headers.idioma, 'FALLO_DEVOLUTION_THOUGHTS')`

Where `headers. language` will be the language sent by the header, if no language is sent, `en` will be set as the default language inside the `statictranslation` function and THOUGHTS_DEVOLUTION_FAULT will be the key of the value to be translated, these key/value are in the path `src/i18n/*`, there you will find the files of the languages of translations that we need, in this case if new languages are implemented it should be added there with the same format of the previous files. 

IMPORTANT: all the routes that are created must be prepared to receive the language by the header with the following syntax example: `key:language, value:en`.

Example of the en.json file:

    {
      "MANTENIMIENTO_PLATAFORMA": "Sorry, we are doing maintenance on the platform, we will be back in a few minutes",
      "LLAMANDA_NO_CONTESTADA": "User did not answer your call",
      "LLAMADA_TERMINADA": "Call finished",
      "ACTUALIZACION_CORRECTA": "Successfully updated",
      "CREACION_CORRECTA": "It has been successfully created",
      "ELIMINACION_CORRECTA": "It has been correctly removed",
      "PARAMETROS_NO_VALIDOS": "Invalid parameters",
      "ERROR_ELIMINAR": "Cannot be deleted",
      "ERROR_ACTUALIZAR": "Data could not be updated",
      "ERROR_CREACION": "Data could not be registered",
      "ERROR_OBTENER": "Data could not be obtained",
      "COLECCION_VACIA": "Does not contain data",
      "NO_EXISTE_DOCUMENTO": "Document not found",
      "ERROR_VOTO": "Error while voting",
      "GAZELOOK_FUERA_SERVICIO": "We are working to provide you a better service, we will be back soon!",
      "VOTO_CORRECTO": "Vote correctly registered",
      "NO_AUTORIZADO": "Not authorized",
      "EMAIL_ENVIADO": "E-mail successfully sent",
      "EMAIL_NO_PUEDE_CREARSE": "E-mail cannot be sent",
      "EMAIL_VERIFICADO": "E-mail verified",
      "LINK_NO_VALIDO": "Invalid link",
      "CONTRASENIA_ACTUALIZADO": "Password has been successfully updated",
      "ERROR_CONTRASENIAS": "Passwords do not match",
      "ERROR_SOLICITUD": "Sorry, an error occurred while processing your request. Please try again later",
      "EXPERIMENTANDO_INCONVENIENTES": "We are experiencing problems, please try again later",
      "NO_EXISTE_COINCIDENCIAS": "No matches found",
      "SOLICITUD_EN_PROCESO": "Your request is being processed, you will be notified by mail when it is completed",
      "DATOS_ELIMINADOS": "Your information has been removed from the application",
      "EMAIL_REGISTRADO": "E-mail registered",
      "CREDENCIALES_INCORRECTAS": "Incorrect access credentials",
      "USUARIO_NO_REGISTRADO": "Unregistered user",
      "ERROR_TRADUCIR_NOTICIA": "News cannot be translated",

      "NOMBRE_CONTACTO_DISPONIBLE": "Contact name available",
      "EMAIL_YA_REGISTRADO": "E-mail already registered",
      "EMAIL_DISPONIBLE": "E-mail available",
      "CUENTA_REGISTRADA_NO_PAGO": "Your account is already registered, a new payment has been generated",
      "ERROR_METODO_PAGO": "Invalid payment method",
      "ERROR_TERMINOS_CONDICIONES": "Sorry, you must accept our terms and conditions in order to complete the registration process",
      "USUARIO_YA_REGISTRADO": "User already registered",
      "TRANSACCION_NO_VALIDA": "Invalid transaction",
      "ERROR_REGISTRO_PAGO": "Error while registering payment",
      "LIMITE_ARCHIVO": "File exceeds the allowed size",
      "ERROR_PROCESO_ARCHIVO": "File processing error",
      "ARCHIVO_NO_VALIDO": "Invalid file",
      "ERROR_CONVERSION_MONEDA": "Conversion error",
      "TRANSFERIR_PROYECTO": "Project transferred correctly",
      "EMAIL_NO_VALIDO": "Invalid e-mail",
      "ERROR_TRANSFERIR_PROYECTO": "Project could not be transferred",
      "SOLICITUD_CANCELADA": "Request cancelled",
      "CUENTA_RECHAZADA_RESPONSABLE": "Account rejected by the person in charge",
      "VALIDAR_CUENTA": "To validate your account created, please check your e-mail",
      "INACTIVA_PAGO": "Pay your subscription fee to use our services",
      "NO_PERMISO_ACCION": "You do not have the permission to perform this action",
      "ERROR_SUSCRIPCION": "Error when renewing subscription",
      "SUSCRIPCION_RENOVADA": "Subscription has been renewed",
      "ERROR_SALIR": "Error when exiting the application",
      "SESION_CERRADA": "Closed session"
    }