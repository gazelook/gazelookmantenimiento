# BACKEND GAZELOOK
  Wep API-REST, Para la administración del sistema y ejecución de procesos secundarios del API Principal.

## TECNOLOGIAS
  Las tecnologías aplicadas al proyecto son:
  - Nesjts CLI: 7.4.1
  - Node: 13.8.0
  - Nestjs: 7.0.0
  - Typescript: 3.7.4
  - Mongo: 4.4.5
  - AWS S3

## INSTALACIÓN
Para levantar el proyecto, es necesario descargar e instalar las siguientes herramientas:

- NODE JS 13.X (Incluye NPM)

  Windows: https://nodejs.org/es/download/

  Linux:
  ```
  curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
  ```
  ```
  sudo apt-get install -y nodejs
  ```

- GIT

  Windows: https://git-scm.com/downloads

  Linux:
  ```
  apt-get install git
  ```

- CLI Nestjs

  windows && Linux:
  ```
  npm i -g @nestjs/cli
  ```

Para descargarnos el proyecto, desde la terminal

  windows && Linux:
  ```
  git clone https://byjose007@bitbucket.org/gazelookspace/apigaze.git
  ```
  Una vez clonado el repositorio, nos creamos nuestra rama partiendo de master 

  Descargar las dependencias.
  ```
  npm install
  ```

Antes de ejecutar el proyecto

Crear base de datos `dbname` e importar datos de la carpeta gazelookQa

Ejecutar el siguiente script
```
mongorestore --uri dbhost -d dbname dbdirectory

Ejm:
mongorestore --uri mongodb+srv://gazelook:<PASSWORD>@cluster0.89jao.mongodb.net -d gazelookQa C:\repaldoBD\gazelookQa

```



## EJECUCIÓN

Desde la carpeta del proyecto, ejecutamos el comando según nuestras necesidades

Antes de ejecutar revisar la cadena de conexion en el archivo de la ruta configsEnv\qa.env

Solo cambiar `MONGO_URI` por su cadena de conexión en caso de ser necesario

```bash

# watch mode
$ npm run start:Qa

```

  Si navegamos a `http://localhost:5000/api/` podremos ver nuestro servidor ejecutandose .


## ESTRUCTURA

El proyecto está estructurado por carpetas de la siguiente manera:
      doc/
         Documentos y requisitos previos a ejecutar el proyecto
      src/
        |-- drivers/
                    Aquí deben ir configuraciones y elementos  principales, que se utilizarán en toda la aplicación a nivel global
            |-- [+] logging/
                            Config para gestionar logs del sistema
            |-- [+] mongoose/
                        Modelos y esquemas de mongo
            |-- [+] interfaces/
            |-- [+] modelos/

         |-- Entidades/
            |-- [+] entidad (Usuario)/
                    |-- [+] controladores/
                               Conjunto de controladores que convierten datos desde el formato más conveniente para los casos de uso y las entidades, al formato más conveniente para algunos elementos externos como la base de datos o la web
                    |-- [+] caso_de_uso/
                                Reglas de negocio (servicios) que definen como se comporta nuestro sistema, definiendo los datos de entrada necesarios, y cual será su salida
                    |-- [+] entidad/
                                Las entidades encapsulan las reglas de negocio de toda la empresa. Una entidad puede ser un objeto con métodos o puede ser un conjunto de datos y funciones.
                    |-- [+] drivers/
                                El capa más externa es generalmente compuesta por frameworks y herramientas tales como la base de datos, el framework web, etc

## Guía de buenas prácticas de git

### Introducción

Este apartado tiene como fin, tener una wiki común con las buenas prácticas que
tenemos que seguir para conservar ordenado el proyecto.


## Nomenclatura

Directorios : nombre_de_mi_directorio (casos_de_uso)
ficheros: nombre-fichero.tipo.ts (mi-usuario.service.ts)
variables: miVariable (usuarioPerfil) 
clases: MiClase (UsuarioPerfil)
funciones: miFuncion (obtenerUsuarios)
constantes: MI_CONSTANTE (TIPO_DE_PERFIL)
URL API: /obtener-perfil-usuario/id

## Documentacion rutas Api

Los verbos http describen por si solos las aciones 

GET /usuarios- Devuelve una lista de usuarios
GET /usuarios/12- Devuelve un usuario específico
POST /usuarios- Crea un nuevo usuario
PUT /usuarios/12- Actualiza el usuario #12
PATCH /usuarios/12- Actualiza parcialmente el usuario #12
DELETE /usuarios/12- Elimina el usuario #12






## API traducciones dinámicas

Instalar variable de entorno en servidor o local

Abrir command prompt:

Windows

1. `setx TRANSLATOR_TEXT_SUBSCRIPTION_KEY "4ef8d7e4672245e4bbf8a37d1fc909a8"`

2. `setx TRANSLATOR_TEXT_ENDPOINT https://api.cognitive.microsofttranslator.com`

Linux o Mac/os:

1. `export TRANSLATOR_TEXT_SUBSCRIPTION_KEY=4ef8d7e4672245e4bbf8a37d1fc909a8`

2. `export TRANSLATOR_TEXT_ENDPOINT=https://api.cognitive.microsofttranslator.com`

3. `source ~/.bashrc`

Para utilizar el api de traducciones se ha creado un metodo global que recibira como parametros el codigo de idioma enviado por ejemplo: `es`, `en`, `fr` etc. y ademas el texto que se va a traducir. 

Para que se pueda llamar cada vez que se requiera una traduccion dinamica se encuentra en /src/multiIdioma/traduccion-dinamica.

Para importarlo : 
`import { traducirTexto } from 'src/multiIdioma/traduccion-dinamica'`

Para utilizarlo se lo puede llamar por ejemplo:
`const textoTraducido= await traducirTexto(idioma, texto);`

El idioma de preferencia se lo enviara por la cabecera (header).

Como referencia de la logica que se debe aplicar cada vez que se lo requiera utlizar se pueden referenciar en la entidad `pensamientos` en el caso de uso `obtener-pensamientos.service`

Cabe recalcar que  solo se llamara al api de traducciones solo si la traducción no esta almacenada para evitar gastar recursos del api innecesarios.

Lenguajes permitidos por el API 

    ┌─────────────────────────┬─────────────────┐
    │ Language                │ Language code   │
    │ Afrikaans               │ af              │
    │ Arabic                  │ ar              │
    │ Bangla                  │ bn              │
    │ Bosnian(Latin)          │ bs              │
    │ Bulgarian               │ bg              │
    │ Cantonese (Traditional) │ yue             │
    │ Catalan                 │ ca              │
    │ Chinese Simplified      │ zh-Hans         │
    │ Chinese Traditional     │ zh-Hant         │
    │ Croatian                │ hr              │
    │ Czech                   │ cs              │
    │ Danish                  │ da              │
    │ Dutch                   │ nl              │
    │ English                 │ en              │
    │ Estonian                │ et              │
    │ Fijian                  │ fj              │
    │ Filipino                │ fil             │
    │ Finnish                 │ fi              │
    │ French                  │ fr              │
    │ German                  │ de              │
    │ Greek                   │ el              │
    │ Gujarati                │ gu              │
    │ Haitian Creole          │ ht              │
    │ Hebrew                  │ he              │
    │ Hindi                   │ hi              │
    │ Hmong Daw               │ mww             │
    │ Hungarian               │ hu              │
    │ Icelandic               │ is              │  
    │ Indonesian              │ id              │
    │ Irish                   │ ga              │
    │ Italian                 │ it              │
    │ Japanese                │ ja              │
    │ Kannada                 │ kn              │
    │ Kazakh                  │ kk              │
    │ Kiswahili               │ sw              │
    │ Klingon                 │ tlh-Latn        │
    │ Klingon (plqaD)         │ tlh-Piqd        │
    │ Korean                  │ ko              │
    │ Latvian                 │ lv              │
    │ Lithuanian              │ lt              │
    │ Malagasy                │ mg              │
    │ Malay                   │ ms              │
    │ Malayalam               │ ml              │
    │ Maltese                 │ mt              │
    │ Maori                   │ mi              │
    │ Marathi                 │ mr              │
    │ Norwegian               │ nb              │
    │ Persian                 │ fa              │
    │ Polish                  │ pl              │
    │ Portuguese (Brazil)     │ pt-br           │
    │ Portuguese (Portugal)   │ pt-pt           │
    │ Punjabi                 │ pa              │
    │ Queretaro Otomi         │ otq             │      
    │ Romanian                │ ro              │
    │ Russian                 │ ru              │
    │ Samoan                  │ sm              │
    │ Serbian (Cyrillic)      │ sr-Cyrl         │
    │ Serbian (Latin)         │ sr-Latn         │
    │ Slovak                  │ sk              │
    │ Slovenian               │ sl              │
    │ Spanish                 │ es              │
    │ Swedish                 │ sv              │
    │ Tahitian                │ ty              │
    │ Tamil                   │ ta              │
    │ Telugu                  │ te              │
    │ Thai                    │ th              │
    │ Tongan                  │ to              │
    │ Turkish                 │ tr              │
    │ Ukrainian               │ uk              │
    │ Urdu                    │ ur              │
    │ Vietnamese              │ vi              │
    │ Welsh                   │ cy              │
    │ Yucatec Maya            │ yua             │
    └─────────────────────────┴─────────────────┘

## Traducciones estaticas con i18n(internacionalización)

Para las traducciones estaticas se debe importar:

`import { TraduccionEstaticaController } from 'src/multiIdioma/controladores/traduccion-estatica-controller'`

Luego llamarlo en el constructor del controlador donde se desea la traduccion, Ejemplo:

  `constructor( private readonly traduccionEstaticaController: TraduccionEstaticaController)`

Despues se llama de la siguiente manera, ejemplo:

`const FALLO_DEVOLUCION_PENSAMIENTOS= await this.traduccionEstaticaController.traduccionEstatica(headers.idioma, 'FALLO_DEVOLUCION_PENSAMIENTOS')`

Donde `headers.idioma` sera el idioma enviado por la cabecera (header), si no se envia idioma se pondra `en` como idioma predeterminado dentro de la funcion `traduccionEstatica` y FALLO_DEVOLUCION_PENSAMIENTOS  será la clave del valor que se desee traducir, estos clave/valor se encuentran en la ruta `src/i18n/*`, ahi se encontraran los archivos de los idiomas de traducciones que necesitemos, en este caso si se implementan nuevos idiomas se lo deberia agregar ahi con el mismo formato de los archivos anteriores. 

IMPORTANTE: todas las rutas que se creen deben estar preparadas para recibir el idioma por la cabecera(header) con la siguiente sintaxis ejemplo: `key:idioma, value:en`

Ejemplo del archivo es.json:

    {
      "MANTENIMIENTO_PLATAFORMA": "Lo sentimos estamos dando mantenimiento a la plataforma, volvemos en unos minutos",
      "LLAMANDA_NO_CONTESTADA": "El usuario no ha contestado su llamada",
      "LLAMADA_TERMINADA": "Llamada terminada",
      "ACTUALIZACION_CORRECTA": "Se ha actualizado con éxito",
      "CREACION_CORRECTA": "Se ha creado con éxito",
      "ELIMINACION_CORRECTA": "Se ha eliminado correctamente",
      "PARAMETROS_NO_VALIDOS": "Parámetros no válidos",
      "ERROR_ELIMINAR": "No se puede eliminar",
      "ERROR_ACTUALIZAR": "No se ha podido actualizar los datos",
      "ERROR_CREACION": "No se ha podido registrar los datos",
      "ERROR_OBTENER": "No se ha podido obtener los datos",
      "COLECCION_VACIA": "No contiene datos",
      "NO_EXISTE_DOCUMENTO": "No se ha encontrado el documento",
      "ERROR_VOTO": "Error al realizar su voto",
      "GAZELOOK_FUERA_SERVICIO": "Estamos trabajando para brindarle un mejor servicio, ¡volveremos pronto!",
      "VOTO_CORRECTO": "Voto registrado correctamente",
      "NO_AUTORIZADO": "No autorizado",
      "EMAIL_ENVIADO": "E-mail enviado exitosamente",
      "EMAIL_NO_PUEDE_CREARSE": "El e-mail no puede enviarse",
      "EMAIL_VERIFICADO": "E-mail verificado",
      "LINK_NO_VALIDO": "Enlace no válido",
      "CONTRASENIA_ACTUALIZADO": "Contraseña actualizada con éxito",
      "ERROR_CONTRASENIAS": "Contraseñas no idénticas",
      "ERROR_SOLICITUD": "Lo sentimos, ocurrió un error al procesar su solicitud. Intente más tarde",
      "EXPERIMENTANDO_INCONVENIENTES": "Estamos experimentando inconvenientes, intente más tarde",
      "NO_EXISTE_COINCIDENCIAS": "No se han encontrado coincidencias",
      "SOLICITUD_EN_PROCESO": "Su solicitud está siendo procesada, se le notificará al correo cuando finalice",
      "DATOS_ELIMINADOS": "Su información ha sido eliminada de la aplicación",
      "EMAIL_REGISTRADO": "E-mail registrado",
      "CREDENCIALES_INCORRECTAS": "Credenciales de acceso incorrectas",
      "USUARIO_NO_REGISTRADO": "Usuario no registrado",
      "ERROR_TRADUCIR_NOTICIA": "No se puede traducir la noticia",

      "NOMBRE_CONTACTO_DISPONIBLE": "Nombre de contacto disponible",
      "EMAIL_YA_REGISTRADO": "E-mail ya registrado",
      "EMAIL_DISPONIBLE": "E-mail disponible",
      "CUENTA_REGISTRADA_NO_PAGO": "Su cuenta ya está registrada, se ha generado un nuevo pago",
      "ERROR_METODO_PAGO": "Método de pago no válido",
      "ERROR_TERMINOS_CONDICIONES": "Lo sentimos, debe aceptar nuestros términos y condiciones para poder completar el proceso de registro",
      "USUARIO_YA_REGISTRADO": "Usuario ya registrado",
      "TRANSACCION_NO_VALIDA": "Transacción no válida",
      "ERROR_REGISTRO_PAGO": "Error al registrar el pago",
      "LIMITE_ARCHIVO": "El archivo supera el tamaño permitido",
      "ERROR_PROCESO_ARCHIVO": "Error al procesar el archivo",
      "ARCHIVO_NO_VALIDO": "Archivo no válido",
      "ERROR_CONVERSION_MONEDA": "Error de conversión",
      "TRANSFERIR_PROYECTO": "Proyecto transferido correctamente",
      "EMAIL_NO_VALIDO": "E-mail no válido",
      "ERROR_TRANSFERIR_PROYECTO": "No se pudo transferir el proyecto",
      "SOLICITUD_CANCELADA": "Solicitud cancelada",
      "CUENTA_RECHAZADA_RESPONSABLE": "Cuenta rechazada por el responsable",
      "VALIDAR_CUENTA": "Para validar la creación de su cuenta, por favor revise su e-mail",
      "INACTIVA_PAGO": "Realice el pago de su suscripción para hacer uso de nuestros servicios",
      "NO_PERMISO_ACCION": "No tiene el permiso para realizar esta acción",
      "ERROR_SUSCRIPCION": "Error al renovar la suscripción",
      "SUSCRIPCION_RENOVADA": "La suscripción ha sido renovada",
      "ERROR_SALIR": "Error al salir de la aplicación",
      "SESION_CERRADA": "Sesión cerrada"
    }