#!/bin/bash
REMOTE_IMAGE_TAG="${GAZELOOK_DOCKER_REPOSITORY}/${GAZELOOK_API_SUPPORT_NAME}:${GAZELOOK_API_SUPPORT_IMAGE_VERSION}"

echo "==> Stopping microservice ${GAZELOOK_API_SUPPORT_NAME}... "
docker stop "${GAZELOOK_API_SUPPORT_NAME}"
echo "==> Deleting microservice ${GAZELOOK_API_SUPPORT_NAME}... "
docker rm "${GAZELOOK_API_SUPPORT_NAME}"
echo "==> Deleting image tag ${REMOTE_IMAGE_TAG}... "
docker rmi "${REMOTE_IMAGE_TAG}"
echo "==> Building docker image ${REMOTE_IMAGE_TAG}... "
docker build "${API_SUPPORT_PATH_PROJECT}" -f Dockerfile -t "${REMOTE_IMAGE_TAG}"