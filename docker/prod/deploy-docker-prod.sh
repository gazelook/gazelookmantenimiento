#!/bin/bash
REMOTE_IMAGE_TAG="${GAZELOOK_DOCKER_REPOSITORY}/${GAZELOOK_API_SUPPORT_NAME}:${GAZELOOK_API_SUPPORT_IMAGE_VERSION}"
sh clean-docker.sh
echo "==> Pushing image ${REMOTE_IMAGE_TAG}... "
docker push "${REMOTE_IMAGE_TAG}"